#include "StdAfx.h"
#ifdef WIN32
#define HACKY_CRASH_FIXES 1		// SEH stuff
#endif

bool isHostile(Object* objA, Object* objB)// B is hostile for A?
{
	if(!objA || !objB)
		return false;
	bool hostile = false;

	if(objA == objB)
		return false;   // can't attack self.. this causes problems with buffs if we dont have it :p

	if(objA->GetTypeId() == TYPEID_CORPSE)
		return false;

	if(objB->GetTypeId() == TYPEID_CORPSE)
		return false;
	if (objA->IsUnit()&&objB->IsUnit()&&!objA->IsPet() && !objB->IsPet())
	{
		uint32 ffaA = ((Unit*)objA)->GetFFA();
		uint32 ffaB = ((Unit*)objB)->GetFFA();
		if( ffaA > 0 && ffaB > 0 )
		{
			if( ffaA >= 11 && ffaA <= 13 && ffaB >= 11 && ffaB <= 13 )
				return ffaA != ffaB;

			if( ffaA >= 21 && ffaA <= 22 && ffaB >= 21 && ffaB <= 22 )
				return ffaA != ffaB;

			if( ffaA == 2 || ffaB == 2 )
				return false;

			if (objA->IsPlayer()&&objB->IsPlayer())
			{
				Player* pa = (Player*)objA;
				Player* pb = (Player*)objB;
				if( ffaA == 1 && ffaB == 1 )
				{
					uint32 groupid_a = pa->GetUInt32Value( PLAYER_FIELD_GROUP_ID );
					uint32 groupid_b = pb->GetUInt32Value( PLAYER_FIELD_GROUP_ID );
					if( groupid_a > 0 )
						return groupid_a != groupid_b;
					else
						return true;
					if( pa->GetHostileGuild() && pa->GetHostileGuild() == pb->GetGuildId() )
						return true;

					if( pb->GetHostileGuild() && pb->GetHostileGuild() == pa->GetGuildId() )
						return true;
				}
			}

			if( ffaA != ffaB )
				return true;
		}
	}

	if(objB->m_faction == NULL || objA->m_faction == NULL)
		return true;

	// added by gui for escort npc
	Creature* ca = NULL;
	Creature* cb = NULL;
	if( objA->IsCreature() )
	{
		ca = static_cast<Creature*>( objA );
	}
	if( objB->IsCreature() )
	{
		cb = static_cast<Creature*>( objB );
	}

	if( ca && cb )
	{
		if( ca->m_escort_qst && cb->m_faction->ID != ca->m_faction->ID && cb->m_faction->ID != 10000 )
			return true;
		if( cb->m_escort_qst && ca->m_faction->ID != cb->m_faction->ID && ca->m_faction->ID != 10000 )
			return true;
	}

	if(objA->m_faction->ID == 10000 || objB->m_faction->ID == 10000)
		return false;

	if( objA->m_faction->ID == 20000 )
	{
		if( objB->IsPlayer() || (!objB->IsPet() && objB->IsCreature()) )
		{
			uint32 ffa = static_cast<Unit*>( objB )->GetFFA();
			if( ffa == 11 )
				return false;
			else
				return true;
		}
	}
	if( objB->m_faction->ID == 20000 )
	{
		if( objA->IsPlayer() || (!objA->IsPet() && objA->IsCreature()))
		{
			uint32 ffa = static_cast<Unit*>( objA )->GetFFA();
			if( ffa == 11 )
				return false;
			else
				return true;
		}
	}

	uint32 faction = objB->m_faction->Mask;
	uint32 host = objA->m_faction->HostileMask;

	/*if( faction != 0 )
	{*/
		if(faction & host)
		{
			hostile = true;
		}
	/*}
	else
	{
		// default to true
		hostile = true;
	}*/

	// check friend/enemy list
	for(uint32 i = 0; i < 4; i++)
	{
		if(objA->m_faction->EnemyFactions[i] == objB->m_faction->Faction)
		{
			hostile = true;
			return true;
			break;
		}
		if(objA->m_faction->FriendlyFactions[i] == objB->m_faction->Faction)
		{
			hostile = false;
			return false;
			break;
		}
	}

	if(objA->IsPlayer())
		return true;
	return false;
	// PvP Flag System Checks
	// We check this after the normal isHostile test, that way if we're
	// on the opposite team we'll already know :p

	if( hostile && ( objA->IsPlayer() || objA->IsPet() || ( objA->IsUnit() && !objA->IsPlayer() && static_cast< Creature* >( objA )->IsTotem() && static_cast< Creature* >( objA )->GetTotemOwner()->IsPvPFlagged() ) ) )
	{
		if( objB->IsPlayer() )
		{
			// Check PvP Flags.
			if( static_cast< Player* >( objB )->IsPvPFlagged() )
				return true;
			else
				return false;
		}
		if( objB->IsPet() )
		{
#if defined(WIN32) && defined(HACKY_CRASH_FIXES)
			__try {
				// Check PvP Flags.
				if( static_cast< Pet* >( objB )->GetPetOwner() != NULL && static_cast< Pet* >( objB )->GetPetOwner()->GetMapMgr() == objB->GetMapMgr() && static_cast< Pet* >( objB )->GetPetOwner()->IsPvPFlagged() )
					return true;
				else
					return false;
			} __except(EXCEPTION_EXECUTE_HANDLER)
			{
				static_cast<Pet*>(objB)->ClearPetOwner();
				static_cast<Pet*>(objB)->SafeDelete();
			}
#else
			// Check PvP Flags.
			if( static_cast< Pet* >( objB )->GetPetOwner() != NULL && static_cast< Pet* >( objB )->GetPetOwner()->GetMapMgr() == objB->GetMapMgr() && static_cast< Pet* >( objB )->GetPetOwner()->IsPvPFlagged() )
				return true;
			else
				return false;
#endif
		}
	}

	return hostile;
}

/// Where we check if we object A can attack object B. This is used in many feature's
/// Including the spell class and the player class.
bool isAttackable(Object* objA, Object* objB, bool CheckStealth)// A can attack B?
{
	if(!objA || !objB || objB->m_factionDBC == NULL || objA->m_factionDBC == NULL)
		return false;

	if ((objA->IsUnit() && !static_cast<Unit *>(objA)->isAlive()) ||
		(objB->IsUnit() && !static_cast<Unit *>(objB)->isAlive()))
		return false;

	if(objB->m_faction == NULL || objA->m_faction == NULL )
		return true;

	if(objA == objB)
		return false;   // can't attack self.. this causes problems with buffs if we don't have it :p

	// ------------------------------------------------------------
	// added by gui for escort npc
	Creature* ca = NULL;
	Creature* cb = NULL;
	if( objA->IsCreature() )
	{
		ca = static_cast<Creature*>( objA );
	}
	if( objB->IsCreature() )
	{
		cb = static_cast<Creature*>( objB );
	}

	if (objA->IsUnit()&&objB->IsUnit()&&!objA->IsPet() && !objB->IsPet())
	{
		uint32 ffaA = ((Unit*)objA)->GetFFA();
		uint32 ffaB = ((Unit*)objB)->GetFFA();

		if( ffaA > 0 && ffaB > 0 )
		{
			if( ffaA >= 11 && ffaA <= 13 && ffaB >= 11 && ffaB <= 13 )
				return ffaA != ffaB;

			if( ffaA >= 21 && ffaA <= 22 && ffaB >= 21 && ffaB <= 22 )
				return ffaA != ffaB;

			if( ffaA == 2 || ffaB == 2 )
				return false;

			if (objA->IsPlayer()&&objB->IsPlayer())
			{
				Player* pa = (Player*)objA;
				Player* pb = (Player*)objB;
				if( ffaA == 1 && ffaB == 1 )
				{
					uint32 groupid_a = pa->GetUInt32Value( PLAYER_FIELD_GROUP_ID );
					uint32 groupid_b = pb->GetUInt32Value( PLAYER_FIELD_GROUP_ID );
					if( groupid_a > 0 )
						return groupid_a != groupid_b;
					else
						return true;
					if( pa->GetHostileGuild() && pa->GetHostileGuild() == pb->GetGuildId() )
						return true;

					if( pb->GetHostileGuild() && pb->GetHostileGuild() == pa->GetGuildId() )
						return true;
				}
			}

			if( ffaA != ffaB )
				return true;
		}
	}

	if( ca && cb )
	{
		if( ca->m_escort_qst && cb->m_faction->ID != ca->m_faction->ID && cb->m_faction->ID != 10000 )
			return true;
		if( cb->m_escort_qst && ca->m_faction->ID != cb->m_faction->ID && ca->m_faction->ID != 10000 )
			return true;
	}

	if(objA->m_faction->ID == 10000 || objB->m_faction->ID == 10000)
		return false;

	if(objA->GetTypeId() == TYPEID_CORPSE)
		return false;

	if(objB->GetTypeId() == TYPEID_CORPSE)
		return false;
	
	if( objA->m_faction->ID == 20000 )
	{
		if( objB->IsPlayer() || (!objB->IsPet() && objB->IsCreature()) )
		{
			uint32 ffa = ((Unit*)objB)->GetFFA();
			if( ffa == 11 )
				return false;
			else
				return true;
		}
	}
	if( objB->m_faction->ID == 20000 )
	{
		if( objA->IsPlayer() ||(!objA->IsPet() && objA->IsCreature()) )
		{
			uint32 ffa = static_cast<Unit*>( objA )->GetFFA();
			if( ffa == 11 )
				return false;
			else
				return true;
		}
	}
	// Players in feign death flags can't be attacked (where did you get this information from?)
	// Changed by Supa: Creatures cannot attack players with feign death flags.
	/*if(!objA->IsPlayer())
		if(objA->HasFlag(UNIT_FIELD_FLAGS_2, 0x00000001))
			return false;
	if(!objB->IsPlayer())
		if(objB->HasFlag(UNIT_FIELD_FLAGS_2, 0x00000001))
			return false;*/

	// Checks for untouchable, unattackable
	if(objA->IsUnit() && objA->HasFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NOT_ATTACKABLE_9 | UNIT_FLAG_MOUNTED_TAXI | UNIT_FLAG_NOT_SELECTABLE))
		return false;
	if(objB->IsUnit())
	{
		if(objB->HasFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NOT_ATTACKABLE_9 | UNIT_FLAG_MOUNTED_TAXI | UNIT_FLAG_NOT_SELECTABLE))
			return false;

		/// added by Zack : 
        /// we cannot attack sheathed units. Maybe checked in other places too ?
		/// !! warning, this presumes that objA is attacking ObjB
        /// Capt: Added the possibility to disregard this (regarding the spell class)
		if(static_cast<Unit *>(objB)->IsStealth() && CheckStealth)
			return false;
	}


	/*
	// handle for pets in duel
	if(objA->IsPet())
	{
		if(objB->IsPlayer())
			if(
				static_cast<Pet *>(objA)->GetPetOwner() &&
				static_cast<Pet *>(objA)->GetPetOwner()->DuelingWith == static_cast< Player* >(objB) && 
				static_cast<Pet *>(objA)->GetPetOwner()->GetDuelState() == DUEL_STATE_STARTED
				)
				return true;
		if(objB->IsPet())
			if(static_cast<Pet *>(objA)->GetPetOwner() &&
				static_cast<Pet *>(objB)->GetPetOwner() &&
				static_cast<Pet *>(objA)->GetPetOwner()->DuelingWith == static_cast<Pet *>(objB)->GetPetOwner() && 
				static_cast<Pet *>(objA)->GetPetOwner()->GetDuelState() == DUEL_STATE_STARTED
				)
				return true;
	}
	if(objB->IsPet())
	{
		if(objA->IsPlayer())
			if(
				static_cast<Pet*>(objB)->GetPetOwner() && static_cast<Pet *>(objB)->GetPetOwner() &&
				static_cast<Pet *>(objB)->GetPetOwner()->DuelingWith == static_cast< Player* >(objA) && 
				static_cast<Pet *>(objB)->GetPetOwner()->GetDuelState() == DUEL_STATE_STARTED
				)
				return true;
		else if(objA->IsPet())
			if(static_cast<Pet*>(objA)->GetPetOwner() && static_cast<Pet *>(objB)->GetPetOwner() &&
				static_cast<Pet*>(objB)->GetPetOwner() &&
				static_cast<Pet *>(objB)->GetPetOwner()->DuelingWith == static_cast<Pet *>(objA)->GetPetOwner() && 
				static_cast<Pet *>(objB)->GetPetOwner()->GetDuelState() == DUEL_STATE_STARTED
				)
				return true;
	}
	*/

	// handle for totems
	if(objA->IsUnit() && !objA->IsPlayer()) // must be creature
	{
		if(static_cast<Creature *>(objA)->IsTotem())
		{
			if(objB->IsPlayer())
				if( static_cast<Creature *>(objA)->GetTotemOwner() &&
					static_cast<Creature *>(objA)->GetTotemOwner()->DuelingWith == static_cast< Player* >(objB) && 
					static_cast<Creature *>(objA)->GetTotemOwner()->GetDuelState() == DUEL_STATE_STARTED
					)
					return true;
			if(objB->IsPet())
				if( static_cast<Creature *>(objA)->GetTotemOwner() &&
					static_cast<Creature *>(objA)->GetTotemOwner()->DuelingWith == static_cast<Pet *>(objB)->GetPetOwner() && 
					static_cast<Creature *>(objA)->GetTotemOwner()->GetDuelState() == DUEL_STATE_STARTED
					)
					return true;
		}
	}
	if(objB->IsUnit() && !objB->IsPlayer()) // must be creature
	{
		if(static_cast<Creature *>(objB)->IsTotem())
		{
			if(objA->IsPlayer())
				if( static_cast<Creature *>(objB)->GetTotemOwner() &&
					static_cast<Creature *>(objB)->GetTotemOwner()->DuelingWith == static_cast< Player* >(objA) && 
					static_cast<Creature *>(objB)->GetTotemOwner()->GetDuelState() == DUEL_STATE_STARTED
					)
					return true;
			if(objA->IsPet())
				if( static_cast<Creature *>(objB)->GetTotemOwner() &&
					static_cast<Creature *>(objB)->GetTotemOwner()->DuelingWith == static_cast<Pet *>(objA)->GetPetOwner() && 
					static_cast<Creature *>(objB)->GetTotemOwner()->GetDuelState() == DUEL_STATE_STARTED
					)
					return true;
		}
	}

	// do not let people attack each other in sanctuary
	// Dueling is already catered for
	AreaTable *atA;
	AreaTable *atB;
	if( objA->IsPet() && static_cast< Pet* >( objA )->GetPetOwner() )
		atA = AreaStorage.LookupEntry( static_cast< Pet* >( objA )->GetPetOwner()->GetAreaID() );
	else if( objA->IsPlayer() )
		atA = AreaStorage.LookupEntry( static_cast< Player* >( objA )->GetAreaID() );
	else
		atA = NULL;

	if( objB->IsPet() && static_cast< Pet* >( objB )->GetPetOwner() )
		atB = AreaStorage.LookupEntry( static_cast< Pet* >( objB )->GetPetOwner()->GetAreaID() );
	else if( objB->IsPlayer() )
		atB = AreaStorage.LookupEntry( static_cast< Player* >( objB )->GetAreaID() );
	else
		atB = NULL;

	// We have the area codes
	// We know they aren't dueling
	if (atA && atB)
	{
		if(atA->AreaFlags & 0x800 || atB->AreaFlags & 0x800)
			return false;
	}

	if(objA->m_faction == objB->m_faction)  // same faction can't kill each other unless in ffa pvp/duel
		return false;

	bool attackable = isHostile(objA, objB); // B is attackable if its hostile for A
	/*if((objA->m_faction->HostileMask & 8) && (objB->m_factionDBC->RepListId != 0) && 
		(objB->GetTypeId() != TYPEID_PLAYER) && objB->m_faction->Faction != 31) // B is attackable if its a neutral Creature*/

	// Neutral Creature Check
	if(objA->IsPlayer() || objA->IsPet())
	{
		if(objB->m_factionDBC->RepListId == -1 && objB->m_faction->HostileMask == 0 && objB->m_faction->FriendlyMask == 0)
		{
			attackable = true;
		}
	}
	else if(objB->IsPlayer() || objB->IsPet())
	{
		if(objA->m_factionDBC->RepListId == -1 && objA->m_faction->HostileMask == 0 && objA->m_faction->FriendlyMask == 0)
		{
			attackable = true;
		}
	}
	else
	if (objA->IsCreature() && static_cast<Creature *>(objA)->IsTotem())
	{
		if(objB->m_factionDBC->RepListId == -1 && objB->m_faction->HostileMask == 0 && objB->m_faction->FriendlyMask == 0)
		{
			attackable = true;
		}
	}
	else 
	if (objB->IsCreature() && static_cast<Creature *>(objB)->IsTotem())
	{
		if(objA->m_factionDBC->RepListId == -1 && objA->m_faction->HostileMask == 0 && objA->m_faction->FriendlyMask == 0)
		{
			attackable = true;
		}
	}

	return attackable;
}

bool isCombatSupport(Object* objA, Object* objB)// B combat supports A?
{
	if(!objA || !objB)
		return false;

	if(objA->GetTypeId() == TYPEID_CORPSE )
		return false;

	if(objB->GetTypeId() == TYPEID_CORPSE)
		return false;

	if(objB->m_faction == 0 || objA->m_faction == 0)
		return false;

	if (isHostile(objA, objB))
	{
		return false;
	}
	if (objA->IsUnit() && objB->IsUnit())
	{
		Unit* pA = (Unit*) objA;
		Unit* pB = (Unit*) objB;
		uint32 entry = pB->GetEntry();

		if( pB->GetAIInterface()->IsAIAlliance( entry ) )
			return true;
	}

	if( objA->IsPet() || objB->IsPet() ) // fixes an issue where horde pets would chain aggro horde guards and vice versa for alliance.
		return false;

	bool combatSupport = false;


	uint32 fSupport = objB->m_faction->FriendlyMask;
	uint32 myFaction = objA->m_faction->Mask;

	if(myFaction & fSupport)
	{
		combatSupport = true;
	}
	// check friend/enemy list
	for(uint32 i = 0; i < 4; i++)
	{
		if(objB->m_faction->EnemyFactions[i] == objA->m_faction->Faction)
		{
			combatSupport = false;
			break;
		}
		if(objB->m_faction->FriendlyFactions[i] == objA->m_faction->Faction)
		{
			combatSupport = true;
			break;
		}
	}
	return combatSupport;
}


bool isAlliance(Object* objA)// A is alliance?
{
	FactionTemplateDBC * m_sw_faction = dbcFactionTemplate.LookupEntry(11);
	FactionDBC * m_sw_factionDBC = dbcFaction.LookupEntry(72);
	if(!objA || objA->m_factionDBC == NULL || objA->m_faction == NULL)
		return true;

	if(m_sw_faction == objA->m_faction || m_sw_factionDBC == objA->m_factionDBC)
		return true;

	//bool hostile = false;
	uint32 faction = m_sw_faction->Faction;
	uint32 host = objA->m_faction->HostileMask;

	if(faction & host)
		return false;

	// check friend/enemy list
	for(uint32 i = 0; i < 4; i++)
	{
		if(objA->m_faction->EnemyFactions[i] == faction)
			return false;
	}

	faction = objA->m_faction->Faction;
	host = m_sw_faction->HostileMask;

	if(faction & host)
		return false;

	// check friend/enemy list
	for(uint32 i = 0; i < 4; i++)
	{
		if(objA->m_faction->EnemyFactions[i] == faction)
			return false;
	}

	return true;
}

