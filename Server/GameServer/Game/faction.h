#ifndef __FACTION_H
#define __FACTION_H

SERVER_DECL bool isHostile(Object* objA, Object* objB); // B is hostile for A?
SERVER_DECL bool isAttackable(Object* objA, Object* objB, bool CheckStealth = true); // A can attack B?
SERVER_DECL bool isCombatSupport(Object* objA, Object* objB); // B combat supports A?;
SERVER_DECL bool isAlliance(Object* objA); // A is alliance?

SUNYOU_INLINE bool isFriendly(Object* objA, Object* objB)// B is friendly to A if its not hostile
{
	return !isHostile(objA, objB);
}

SUNYOU_INLINE bool isSameFaction(Object* objA, Object* objB)
{
	if( !objA->IsValid() || !objB->IsValid() ) return false;
// 	try
// 	{
		if(!objA->m_faction || !objB->m_faction)
			return true;

		if (!objA->IsPet() && !objB->IsPet())
		{
			uint32 ffaA = ((Unit*)objA)->GetFFA();
			uint32 ffaB = ((Unit*)objB)->GetFFA();
			if( ffaA > 0 && ffaB > 0 )
			{
				if( ffaA >= 11 && ffaA <= 13 && ffaB >= 11 && ffaB <= 13 )
					return ffaA == ffaB;

				if( ffaA >= 21 && ffaA <= 22 && ffaB >= 21 && ffaB <= 22 )
					return ffaA == ffaB;

				if( ffaA == 2 || ffaB == 2 )
					return true;

				if (objA->IsPlayer()&&objB->IsPlayer())
				{
					Player* pa = (Player*)objA;
					Player* pb = (Player*)objB;
					if( ffaA == 1 && ffaB == 1 )
					{
						uint32 groupid_a = pa->GetUInt32Value( PLAYER_FIELD_GROUP_ID );
						uint32 groupid_b = pb->GetUInt32Value( PLAYER_FIELD_GROUP_ID );
						if( groupid_a > 0 )
							return groupid_a == groupid_b;
						else
							return false;
						if( pa->GetHostileGuild() && pa->GetHostileGuild() == pb->GetGuildId() )
							return false;

						if( pb->GetHostileGuild() && pb->GetHostileGuild() == pa->GetGuildId() )
							return false;
					}
				}

				if( ffaA != ffaB )
					return false;
			}
		}


		// added by gui for escort npc
		Creature* ca = NULL;
		Creature* cb = NULL;
		if( objA->IsCreature() )
		{
			ca = static_cast<Creature*>( objA );
		}
		if( objB->IsCreature() )
		{
			cb = static_cast<Creature*>( objB );
		}
		if( ca && cb )
		{
			if( ca->m_escort_qst && cb->m_faction->ID != ca->m_faction->ID && cb->m_faction->ID != 10000 )
				return false;
			if( cb->m_escort_qst && ca->m_faction->ID != cb->m_faction->ID && ca->m_faction->ID != 10000 )
				return false;
		}

		if( objA->m_faction->ID >= 1 && objA->m_faction->ID <= 3 && objB->m_faction->ID >= 1 && objB->m_faction->ID <= 3 )
			return true;

		if( objB->m_faction->ID == 10000 || objA->m_faction->ID == 10000 )
			return true;

		return (objB->m_faction->ID == objA->m_faction->ID);
// 	}
// 	catch (...)
// 	{
// 		return false;
// 	}
}

#endif
