#include "stdafx.h"
#include "Master.h"
#include "../../SDBase/Protocol/S2C.h"
#include "../Common/Protocol/GS2MS.h"

#include "Game/DatabaseCleaner.h"
#include "Game/CConsole.h"
#include "Game/ShopMgr.h"

#include "sunyou_getopt.h"
#define BANNER "SUNYOU %s r%u/%s-%s-%s :: Game Server"
//#include "../../Common/Net/PakPool.h"

#include "Net/GTParser.h"
#include "Net/GTListenSocket.h"
#include "Net/DSSocket.h"
#include "Net/MSSocket.h"
#include "Net/GS_SP_ListenServer.h"
#include "Net/GS_SP_Client.h"

#include "Game/activity_srv.h"

#ifndef WIN32
#include <sched.h>
#endif

#include "svn_revision.h"

#include <signal.h>
#include <mysql/mysql.h>

#include "../../new_common/Source/new_common.h"
#include "Game/InstanceQueue.h"

#include "Game/WebInterface.h"
#include "Game/DonationSystem.h"
#include "Game/CreditMgr.h"
#include "Game/LuaInterface.h"

#include "Game/CardSerialSystem.h"
#include "Game/TreasureMgr.h"
#include "Game/ShopActivity.h"
#include "Game/BroadTextMgr.h"
#include "Game/MultiLanguageStringMgr.h"


//zip_compress_strategy  cs;
using namespace LuaPlus;

LuaState* g_luastate = NULL;

zip_compress_strategy cs;

createFileSingleton( Master );
std::string LogFileName;
bool bLogChat;
bool crashed = false;

volatile bool Master::m_stopEvent = false;

// Database defines.
SERVER_DECL Database* Database_Character;
SERVER_DECL Database* Database_Realm;
SERVER_DECL Database* Database_World;

uint32 TIME_PILAO_GAME = 3*60*60;
uint32 TIME_BUJIANGKANG_GAME = 5*60*60;

// extern DayWatcherThread * dw;

gs_sp_listen_server* g_gs_sp_listen_server = NULL;
gs_sp_client* g_gs_sp_client = NULL;

void Master::_OnSignal(int s)
{
	switch (s)
	{
#ifndef WIN32
	case SIGHUP:
		sWorld.Rehash(true);
		break;
#endif
	case SIGINT:
	case SIGTERM:
	case SIGABRT:
#ifdef _WIN32
	case SIGBREAK:
#endif
		Master::m_stopEvent = true;
		break;
	}

	signal(s, _OnSignal);
}

Master::Master()
{
	m_ShutdownTimer = 0;
	m_ShutdownEvent = false;
	m_restartEvent = false;
}

Master::~Master()
{
}

struct Addr
{
	unsigned short sa_family;
	/* sa_data */
	unsigned short Port;
	unsigned long IP; // inet_addr
	unsigned long unusedA;
	unsigned long unusedB;
};

#define DEF_VALUE_NOT_SET 0xDEADBEEF

#ifdef WIN32
        static const char* default_config_file = "GS.conf";
#else
        static const char* default_config_file = "GS.conf";
#endif

bool bServerShutdown = false;

bool g_save = true;

char* g_mapLoadBuff = NULL;
CrossSrvMgr* g_crosssrvmgr = NULL;

bool Master::Init(int argc, char ** argv)
{
	/*
	InitRandomNumberGenerators();
	int n[7] = { 0 };
	for( int i = 0; i < 10000; ++i )
	{
		float chances[7];
		chances[0]=std::max(0.0f,100.0f-100.f);
		chances[1]=chances[0]+5.f;
		chances[2]=chances[1]+0.f;
		chances[3]=chances[2]+0.f;
		chances[4]=chances[3]+0.f;
		chances[5]=chances[4]+5.f;
		chances[6]=chances[5]+0.f;
		//--------------------------------roll------------------------------------------------------
		float Roll = RandomFloat(100.0f);
		uint32 r = 0;
		while (r<7&&Roll>chances[r])
		{
			r++;
		}
		if( r < 7 )
			++n[r];
	}
	*/
	m_group_id = 0;
	/*
	FILE* fpChenmi = fopen( "chenmi.conf", "r" );
	if( fpChenmi )
	{
		fscanf( fpChenmi, "%u, %u", &TIME_PILAO_GAME, &TIME_BUJIANGKANG_GAME );
		fclose( fpChenmi );
	}

	FILE* fpTestBonus = fopen( "bonus.record", "r" );
	if( fpTestBonus )
	{
		while( !feof(fpTestBonus ) )
		{
			int acct = 0;
			fscanf( fpTestBonus, "%u\n", &acct );
			Player::s_bonus_acct[acct] = 1;
		}
		fclose( fpTestBonus );
	}
	*/
	MyLog::Init();
	g_mapLoadBuff = (char*)malloc( 1024 * 1024 * 128 );

	char * config_file = (char*)default_config_file;

	int file_log_level = DEF_VALUE_NOT_SET;
	int screen_log_level = DEF_VALUE_NOT_SET;
	int do_check_conf = 1;
	int do_version = 0;
	int do_cheater_check = 0;
	int do_database_clean = 0;

	struct sunyou_option longopts[] =
	{
		{ "checkconf",			sunyou_no_argument,				&do_check_conf,			1		},
		{ "screenloglevel",		sunyou_required_argument,		&screen_log_level,		1		},
		{ "fileloglevel",		sunyou_required_argument,		&file_log_level,		1		},
		{ "version",			sunyou_no_argument,				&do_version,			1		},
		{ "conf",				sunyou_required_argument,		NULL,					'c'		},
		{ "realmconf",			sunyou_required_argument,		NULL,					'r'		},
		{ "databasecleanup",	sunyou_no_argument,				&do_database_clean,		1		},
		{ "cheatercheck",		sunyou_no_argument,				&do_cheater_check,		1		},
		{ 0, 0, 0, 0 }
	};

	char c;
	while ((c = sunyou_getopt_long_only(argc, argv, ":f:", longopts, NULL)) != -1)
	{
		switch (c)
		{
		case 'c':
			config_file = new char[strlen(sunyou_optarg)];
			strcpy(config_file, sunyou_optarg);
			break;

		case 0:
			break;
		default:
			printf("Usage: %s [--checkconf] [--screenloglevel <level>] [--fileloglevel <level>] [--conf <filename>] [--realmconf <filename>] [--version] [--databasecleanup] [--cheatercheck]\n", argv[0]);
			return true;
		}
	}

	// Startup banner
	UNIXTIME = time(NULL);
	g_localTime = *localtime(&UNIXTIME);

	CollideInterface.Init();
	g_luastate = LuaState::Create();

	g_multi_langage_string_mgr = new MultiLanguageStringMgr;
	g_multi_langage_string_mgr->Init();
	if( !g_multi_langage_string_mgr->SwitchLanguage( LANGUAGE_CHINESE_SIMPLIFIED ) )
	{
		printf( "chinese_simplified.lua cannot found or syntax error!" );
		return false;
	}

	do_check_conf = false;do_version = false;

	printf(BANNER, BUILD_TAG, BUILD_REVISION, CONFIG, PLATFORM_TEXT, ARCH);

	if( do_check_conf )
	{
		MyLog::log->notice( "Checking config file: %s", config_file );
		if( Config.MainConfig.SetSource(config_file, true ) )
			MyLog::log->notice( "Passed without errors." );
		else
			MyLog::log->error( "Encountered one or more errors." );

		/* test for die variables */
		string die;
		if( Config.MainConfig.GetString( "die", "msg", &die) || Config.MainConfig.GetString("die2", "msg", &die ) )
			MyLog::log->warn( "Die directive received: %s", die.c_str() );
	}

	printf( "The key combination <Ctrl-C> will safely shut down the server at any time.\n" );

#ifndef WIN32
	if(geteuid() == 0 || getegid() == 0)
		MyLog::log->warn( "You are running SunyouWorld as root.\nThis is not needed, and may be a possible security risk.\nIt is advised to hit CTRL+C now and\nstart as a non-privileged user.");
#endif

	InitRandomNumberGenerators();
	MyLog::log->notice( "Initialized Random Number Generators." );
	gWebInterface.Init();
	
	//ThreadPool.Startup();
	uint32 LoadingTime = getMSTime();

	MyLog::log->notice( "Loading Config Files...\n" );
	if( Config.MainConfig.SetSource( config_file ) )
		MyLog::log->notice( ">> GS.conf" );
	else
	{
		MyLog::log->error( ">> GS.conf" );
		return false;
	}

	m_group_id = Config.MainConfig.GetIntDefault( "GroupInfo", "ID", 1 );
	m_user_limit = Config.MainConfig.GetIntDefault( "GroupInfo", "UserLimit", 1 );

	if( !_StartDB() )
	{
		return false;
	}
	g_crosssrvmgr = new CrossSrvMgr;
	g_crosssrvmgr->Init();

	if(do_database_clean)
	{
		MyLog::log->notice("Entering database maintenance mode.");
		new DatabaseCleaner;
		DatabaseCleaner::getSingleton().Run();
		delete DatabaseCleaner::getSingletonPtr();
		MyLog::log->notice("Maintenence finished. Take a moment to review the output, and hit space to continue startup.");
		fflush(stdout);
	}

#ifdef GM_SCRIPT
	ScriptSystem = new ScriptEngine;
	ScriptSystem->Reload();
#endif

	new EventMgr;
	new World;

	/* load the config file */
	sWorld.Rehash(false);

	// Initialize Opcode Table
	WorldSession::InitPacketHandlerTable();

	string host = Config.MainConfig.GetStringDefault( "Listen", "Host", DEFAULT_HOST );
	int wsport = Config.MainConfig.GetIntDefault( "Listen", "WorldServerPort", DEFAULT_WORLDSERVER_PORT );

	if( !sWorld.SetInitialWorldSettings() )
	{
		MyLog::log->error( "SetInitialWorldSettings() failed. Something went wrong? Exiting." );
		return false;
	}

	if( do_cheater_check )
		sWorld.CleanupCheaters();

	sWorld.SetStartTime((uint32)UNIXTIME);

	_HookSignals();

	uint32 realCurrTime, realPrevTime;
	realCurrTime = realPrevTime = getMSTime();


	// Start Network Subsystem
	MyLog::log->notice( "Starting Network subsystem..." );

#ifdef _WIN32
	net_global::init_net_service( 1, 10, &cs, true, 100 );
#else
	net_global::init_net_service( 4, 10, &cs, true, 2048 );
#endif

	sScriptMgr.LoadScripts();
	new AIInterfaceManager();


	LoadingTime = getMSTime() - LoadingTime;
	MyLog::log->notice( "Ready for connections. Startup time: %ums\n", LoadingTime );

	/* write pid file */
	FILE * fPid = fopen( "sunyou.pid", "w" );
	if( fPid )
	{
		uint32 pid;
#ifdef WIN32
		pid = GetCurrentProcessId();
#else
		pid = getpid();
#endif
		fprintf( fPid, "%u", (unsigned int)pid );
		fclose( fPid );
	}

	Player::InitVisibleUpdateBits();
	//程序版本号获取并显示
// 	CModuleVersion ver;

#ifdef _WIN32
	if(!sGTListenSocket.create(Config.MainConfig.GetIntDefault("GateServer", "ListenPort", 0), 64, 2) )
#else
	if(!sGTListenSocket.create(Config.MainConfig.GetIntDefault("GateServer", "ListenPort", 0), 64, 8) )
#endif
	{
		MyLog::log->error("Game Server start faild! --- sGTListenSocket.Start");
		return false;
	}
	sGTListenSocket.set_thread_task_transfer_id_mode();
	MyLog::log->notice("Listen : Begin Listen GT on port[%u].", Config.MainConfig.GetIntDefault("GateServer", "ListenPort", 0));

	if( !g_crosssrvmgr->m_isInstanceSrv )
	{
		sDSSocket.connect(Config.MainConfig.GetStringDefault("DBServer", "IP", "127.0.0.1").c_str(), Config.MainConfig.GetIntDefault("DBServer", "Port", 0));
		MyLog::log->info( "connecting DBServer..." );
		sDSSocket.set_reconnect( true );
	}

	// surpass server . 
	if (m_group_id == 15)
	{
		// this server is the surpass server
		g_gs_sp_listen_server = new gs_sp_listen_server;
		if (!g_gs_sp_listen_server->createfromconf())
		{
			delete g_gs_sp_listen_server;
			g_gs_sp_listen_server = NULL ;
			return false ;
		}
		m_surpassserver = true ;
	}else
	{
		// connect to the surpass server
		g_gs_sp_client = new gs_sp_client;
		if (!g_gs_sp_client->createfromconf())
		{
			delete g_gs_sp_client;
			g_gs_sp_client = NULL;
		}
		m_surpassserver = false ;
	}

	g_instancequeuemgr.Init();

	unsigned int mysqlthreadsafe = mysql_thread_safe();
	if( !mysqlthreadsafe )
		MyLog::log->error( "mysql doesn't compiled on thread safe mode\n" );
	Database::StartThread();
	MyLog::log->notice( "database thread has been started...\n" );

	g_donation_system = new DonationSystem;
	g_treasure_mgr = new TreasureMgr;
	g_broadTextMgr_system = new BroadTextMgr;
	g_broadTextMgr_system->LoadBroadFromDB();

	g_shopactivity_system = new ShopActivity ;
	g_treasure_mgr->LoadTreasurePos(); // 先创建TreasureMgr 在创建activity_srv

	g_activity_srv = new activity_srv;
	g_activity_mgr->load();
	g_donation_system->Load();

	g_donation_system->CalcLadder();

	free( g_mapLoadBuff );
	g_mapLoadBuff = NULL;
	
	g_Serial_System = new CardSerialSystem ;
	g_Serial_System->Load();

	
	return true;
}

#define WORLD_UPDATE_DELAY 100

void Master::SetShutdownTimer( uint32 seconds )
{
	sWorld.SendWorldText( build_language_string( BuildString_WorldServerShutdownInSeconds, seconds ) );
	MyLog::gmlog->notice( build_language_string( BuildString_WorldServerShutdownInSeconds, seconds ) );

	m_ShutdownTimer = (uint32)UNIXTIME;
	sEventMgr.AddEvent( &sWorld, &World::ServerShutdownCountDown, (uint32)UNIXTIME + seconds, EVENT_SERVER_SHUTDOWN_COUNT_DOWN, 1000, seconds + 1, 0 );
}

bool Master::Run()
{
	// Socket loop!
	uint32 start;
	uint32 diff;
	uint32 last_time = getMSTime();
	uint32 etime;
	uint32 next_printout = getMSTime(), next_send = getMSTime();
	uint32 loopcounter = 0;
	time_t curTime;
#ifdef WIN32
	HANDLE hThread = GetCurrentThread();
#endif

	//m_timers[UPDATE_DISCONN].SetInterval(10000);
	m_timers[UPDATE_WORLD].SetInterval(WORLD_UPDATE_DELAY);
	m_timers[UPDATE_SESSION].SetInterval(10);
	m_timers[UPDATE_ONLINE_NUMBER].SetInterval( 1000 * 60 * 3 );
	m_timers[UPDATE_CONTRIBUTION_BILLBOARD].SetInterval( 1000 * 60 * 5 );
	m_timers[UPDATE_SERVER_STAT_LOG].SetInterval( 1000 * 60 * 5 );
	m_timers[UPDATE_LADDER_INFO].SetInterval( 1000 * 38 * 1 );
	m_timers[UPDATE_CLEAN_DB].SetInterval( 1000 * 60 * 60 * 1 );
	uint32 LastWorldUpdate=getMSTime();
	uint32 LastSessionsUpdate=getMSTime();

	/* voicechat */

	while( !m_stopEvent )
	{
		start = getMSTime();
		diff = start - last_time;
		last_time = start;

		/* since time() is an expensive system call, we only update it once per server loop */
		curTime = time(NULL);
		if( UNIXTIME != curTime )
		{
			UNIXTIME = curTime;
			g_localTime = *localtime(&UNIXTIME);
		}

		//calce time passed
		for(int i = 0; i < UPDATE_COUNT; i++)
			if(m_timers[i].GetCurrent()>=0)
				m_timers[i].Update( diff );
			else m_timers[i].SetCurrent(0);
		if( m_timers[UPDATE_WORLD].Passed() )
		{
			sAIInterfaceManager.Update();

			sDSSocket.run_no_wait();
			sGTListenSocket.run_no_wait();

			WorldDatabase.QueryTaskRun();
			CharacterDatabase.QueryTaskRun();
			RealmDatabase.QueryTaskRun();
			g_crosssrvmgr->Run();

			sWorld.Update( start - LastWorldUpdate );
			LastWorldUpdate = start;

			m_timers[UPDATE_WORLD].Reset();
			gWebInterface.Update();

			g_treasure_mgr->Update();
			g_broadTextMgr_system->Update();
			g_activity_mgr->update();
			g_donation_system->Update();
			g_shopactivity_system->Update();

			sWorld.UpdateSessions( 0 );
		}

		if( m_timers[UPDATE_ONLINE_NUMBER].Passed() )
		{
			if( g_localTime.tm_hour == 3 && g_localTime.tm_min >= 0 && g_localTime.tm_min <= 3 )
			{
				MyLog::log->info("clear all daily quest log");
				objmgr.ClearFinishedDailyQuests();
			}
			
			uint32 player_count = sWorld.m_sessions.size();
			uint32 iv = 20 + player_count / 10;
			if( iv > 200 )
				iv = 200;

			m_timers[UPDATE_WORLD].SetInterval( iv );

			m_timers[UPDATE_ONLINE_NUMBER].Reset();
		}

		if( !g_crosssrvmgr->m_isInstanceSrv )
		{
			if( m_timers[UPDATE_LADDER_INFO].Passed() )
			{
				tm* ptm = localtime( &UNIXTIME );
				if(ptm->tm_hour == 3 && ptm->tm_min == 0 && ptm->tm_sec >= 0 && ptm->tm_sec <= 40)
				{
					MyLog::log->notice("calculating bill board...");
					int starttime = get_tick_count();
					//for( int i = 0; i < LADDER_MAX; ++i )
					objmgr.LadderProc();
					MyLog::log->notice("calculate bill board finished. cost time:[%d]ms", get_tick_count() - starttime );
				}
				m_timers[UPDATE_LADDER_INFO].Reset();
			}
			if(m_timers[UPDATE_CLEAN_DB].Passed())
			{
				m_timers[UPDATE_CLEAN_DB].Reset();
				sWorld.ExecuteSqlToDBServer("delete from playertitles where time_left < %u and time_left > 0", (uint32)UNIXTIME);
			}
		}

		//g_creditmgr.Process();

		/* UPDATE */
		int interval = getMSTime() - start;
		if( interval < 10 )
		{
#ifdef WIN32
				WaitForSingleObject( hThread, 1 );
#else
				Sleep( 10 - interval );
#endif
		}
	}

	Release();
	return true;
}

void Master::Release()
{
	_UnhookSignals();

	sWorld.SaveAllPlayers();
	Sleep( 5000 );

	if (m_surpassserver && g_gs_sp_listen_server)
	{
		g_gs_sp_listen_server->stop();
	}
	sGTListenSocket.stop();
	sDSSocket.close();

	MyLog::log->info( "SaveAllPlayers done!" );

	exit( 0 );
	return;

// 	console->terminate();
// 	delete console;

	MyLog::log->notice( " Web Server shutdown ...." );
	gWebInterface.Release();

	// begin server shutdown
	MyLog::log->notice("Shutdown : Initiated at %s", ConvertTimeStampToDataTime( (uint32)UNIXTIME).c_str());

// 	if( lootmgr.is_loading )
// 	{
// 		MyLog::log->notice("Shutdown : Waiting for loot to finish loading..." );
// 		while( lootmgr.is_loading )
// 			Sleep( 100 );
// 	}

	// send a query to wake it up if its inactive
	MyLog::log->notice("Database : Clearing all pending queries..." );

	// kill the database thread first so we don't lose any queries/data
	CharacterDatabase.EndThreads();
	WorldDatabase.EndThreads();

// 	Log.Notice( "DayWatcherThread", "Exiting..." );
// 	dw->terminate();
// 	dw = NULL;

	MyLog::log->notice("Network : Shutting down network subsystem." );

	bServerShutdown = true;
	//ThreadPool.Shutdown();

	sWorld.ShutdownClasses();
	MyLog::log->notice("World : ~World()" );
	delete World::getSingletonPtr();

	sScriptMgr.UnloadScripts();
	delete ScriptMgr::getSingletonPtr();

	MyLog::log->notice("EventMgr : ~EventMgr()" );
	delete EventMgr::getSingletonPtr();

	MyLog::log->notice("ShopMgr : ~ShopMgr()" );
	delete ShopMgr::getSingletonPtr();

	MyLog::log->notice("Database : Closing Connections..." );
	_StopDB();

	MyLog::log->notice("Database : Deleting Network Subsystem..." );

	// remove pid
	remove( "sunyou.pid" );
	delete &g_instancequeuemgr;
	net_global::free_net_service();

	LuaState::Destroy( g_luastate );
	MyLog::log->notice("Shutdown : Shutdown complete.");
}

bool Master::_StartDB()
{
	string hostname, username, password, database;
	int port = 0;
	int type = 1;
	//string lhostname, lusername, lpassword, ldatabase;
	//int lport = 0;
	//int ltype = 1;
	// Configure Main Database

	bool result = Config.MainConfig.GetString( "WorldDatabase", "Username", &username );
	Config.MainConfig.GetString( "WorldDatabase", "Password", &password );
	result = !result ? result : Config.MainConfig.GetString( "WorldDatabase", "Hostname", &hostname );
	result = !result ? result : Config.MainConfig.GetString( "WorldDatabase", "Name", &database );
	result = !result ? result : Config.MainConfig.GetInt( "WorldDatabase", "Port", &port );
	result = !result ? result : Config.MainConfig.GetInt( "WorldDatabase", "Type", &type );
	Database_World = Database::CreateDatabaseInterface(type);

	if(result == false)
	{
		MyLog::log->error("sql : One or more parameters were missing from WorldDatabase directive.");
		return false;
	}

	// Initialize it
	if( !WorldDatabase.Initialize(hostname.c_str(), (unsigned int)port, username.c_str(),
		password.c_str(), database.c_str(), Config.MainConfig.GetIntDefault( "WorldDatabase", "ConnectionCount", 3 ), 16384 ) )
	{
		MyLog::log->error("sql : Main database initialization failed. Exiting." );
		return false;
	}

	result = Config.MainConfig.GetString( "CharacterDatabase", "Username", &username );
	Config.MainConfig.GetString( "CharacterDatabase", "Password", &password );
	result = !result ? result : Config.MainConfig.GetString( "CharacterDatabase", "Hostname", &hostname );
	result = !result ? result : Config.MainConfig.GetString( "CharacterDatabase", "Name", &database );
	result = !result ? result : Config.MainConfig.GetInt( "CharacterDatabase", "Port", &port );
	result = !result ? result : Config.MainConfig.GetInt( "CharacterDatabase", "Type", &type );
	Database_Character = Database::CreateDatabaseInterface(type);

	if(result == false)
	{
		MyLog::log->error("sql : One or more parameters were missing from Database directive." );
		return false;
	}

	// Initialize it
	if( !CharacterDatabase.Initialize( hostname.c_str(), (unsigned int)port, username.c_str(),
		password.c_str(), database.c_str(), Config.MainConfig.GetIntDefault( "CharacterDatabase", "ConnectionCount", 5 ), 16384 ) )
	{
		MyLog::log->error("sql : Main database initialization failed. Exiting." );
		return false;
	}

	result = Config.MainConfig.GetString( "RealmDatabase", "Username", &username );
	Config.MainConfig.GetString( "RealmDatabase", "Password", &password );
	result = !result ? result : Config.MainConfig.GetString( "RealmDatabase", "Hostname", &hostname );
	result = !result ? result : Config.MainConfig.GetString( "RealmDatabase", "Name", &database );
	result = !result ? result : Config.MainConfig.GetInt( "RealmDatabase", "Port", &port );
	result = !result ? result : Config.MainConfig.GetInt( "RealmDatabase", "Type", &type );
	Database_Realm = Database::CreateDatabaseInterface(type);

	if(result == false)
	{
		MyLog::log->error("sql : One or more parameters were missing from Database directive." );
		return false;
	}

	// Initialize it
	if( !RealmDatabase.Initialize( hostname.c_str(), (unsigned int)port, username.c_str(),
		password.c_str(), database.c_str(), Config.MainConfig.GetIntDefault( "LogonDatabase", "ConnectionCount", 5 ), 16384 ) )
	{
		MyLog::log->error("sql : Main database initialization failed. Exiting." );
		return false;
	}
	return true;
}

void Master::_StopDB()
{
	delete Database_World;
	delete Database_Character;
	delete Database_Realm;
}

void Master::_HookSignals()
{
	signal( SIGINT, _OnSignal );
	signal( SIGTERM, _OnSignal );
	signal( SIGABRT, _OnSignal );
#ifdef _WIN32
	signal( SIGBREAK, _OnSignal );
#else
	signal( SIGHUP, _OnSignal );
	signal(SIGUSR1, _OnSignal);
#endif
}

void Master::_UnhookSignals()
{
	signal( SIGINT, 0 );
	signal( SIGTERM, 0 );
	signal( SIGABRT, 0 );
#ifdef _WIN32
	signal( SIGBREAK, 0 );
#else
	signal( SIGHUP, 0 );
#endif

}

#ifdef WIN32


// Crash Handler
void OnCrash( bool Terminate )
{
	MyLog::log->error("Crash Handler : Advanced crash handler initialized.");
	try
	{
		if( World::getSingletonPtr() != 0 )
		{
			MyLog::log->notice("sql : Waiting for all database queries to finish..." );
			WorldDatabase.EndThreads();
			CharacterDatabase.EndThreads();
			MyLog::log->notice("sql : All pending database operations cleared." );
			sWorld.SaveAllPlayers();
			MyLog::log->notice("sql : Data saved." );
		}
	}
	catch(...)
	{
		MyLog::log->error("sql : Threw an exception while attempting to save all data." );
	}

	MyLog::log->error("Server : Closing." );

	// Terminate Entire Application
	if( Terminate )
	{
		HANDLE pH = OpenProcess( PROCESS_TERMINATE, TRUE, GetCurrentProcessId() );
		TerminateProcess( pH, 1 );
		CloseHandle( pH );
	}
}

#endif
