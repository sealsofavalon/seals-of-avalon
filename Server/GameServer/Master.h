#ifndef _MASTER_H
#define _MASTER_H

#include "Common.h"
//#include "Singleton.h"
#include "Config/ConfigEnv.h"
#include "Game/MainServerDefines.h"
#include "../LuaPlusForLinux/LuaPlus.h"


#ifndef _VERSION
# define _VERSION "2.0.x"
#endif

#if PLATFORM == PLATFORM_WIN32
# define _FULLVERSION _VERSION "-SVN (Win32)"
#else
# define _FULLVERSION _VERSION "-SVN (Unix)"
#endif

#ifdef _DEBUG
#define BUILDTYPE "Debug"
#else
#define BUILDTYPE "Release"
#endif

enum
{
//	UPDATE_DISCONN = 0,
	UPDATE_WORLD,
	UPDATE_SESSION,
	UPDATE_ONLINE_NUMBER,
	UPDATE_CONTRIBUTION_BILLBOARD,
	UPDATE_SERVER_STAT_LOG,
	UPDATE_LADDER_INFO,
	UPDATE_CLEAN_DB,
	UPDATE_COUNT,
};

#define DEFAULT_LOOP_TIME 0 /* 0 millisecs - instant */
#define DEFAULT_LOG_LEVEL 0
#define DEFAULT_PLAYER_LIMIT 100
#define DEFAULT_WORLDSERVER_PORT 8129
#define DEFAULT_REALMSERVER_PORT 3724
#define DEFAULT_HOST "0.0.0.0"
#define DEFAULT_REGEN_RATE 0.15
#define DEFAULT_XP_RATE 1
#define DEFAULT_DROP_RATE 1
#define DEFAULT_REST_XP_RATE 1
#define DEFAULT_QUEST_XP_RATE 1
#define DEFAULT_SAVE_RATE 300000	// 5mins

class ConsoleThread;
class WorldRunnable;
extern bool g_save;

class Master : public Singleton<Master>
{
public:
	Master();
	~Master();
	bool Init(int argc, char ** argv);
	void Release();
	void Shundown();
	void SetShutdownTimer( uint32 seconds );
	bool Run();
	bool m_ShutdownEvent;
	uint32 m_ShutdownTimer;

	static volatile bool m_stopEvent;
	int m_group_id;
	bool m_surpassserver;
	int m_user_limit;

	bool m_restartEvent;

private:
	bool _StartDB();
	void _StopDB();

	void _HookSignals();
	void _UnhookSignals();

	static void _OnSignal(int s);

	ConsoleThread * console;
//	WorldRunnable * wr;

	IntervalTimer m_timers[UPDATE_COUNT];
};

#define sMaster Master::getSingleton()

#endif
