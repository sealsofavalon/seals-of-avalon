#include "StdAfx.h"
#include "DSBuilder.h"
initialiseSingleton( CDSBuilder );
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//GS版本协议（包含GS编号）
/*
协议头：CltSockID ＋0x0801 ＋ GS版本号（4字节）+GS编号（1字节） ＋ 0x00＋
*/
bool CDSBuilder::BuildVersionPkg(MSG_GS2DB::stVersionPkg& MsgOut)
{
	//MsgOut.nGSInx	= sConfig.m_byGSIdx;
	return true;
}

//小退请求
/*
协议头：CltSockID＋0x0807 ＋0x00000000＋0x0000（2字节） 
协议体：len+Account+len +角色名+len+角色数据
*/
bool CDSBuilder::BuildLogoutReqPkg(MSG_GS2DB::stLogoutReq& MsgOut, Player* pPlayer)
{
	//MsgOut.nTransID		= pPlayer->GetMapObjID();
// 	MsgOut.strUserID	= pPlayer->m_strUserID;
// 	MsgOut.strName		= pPlayer->m_name;
// 	
// 	//角色数据
// 	if(!MsgOut.dataRole.FromPlayer(pPlayer))
// 	{
// 		MyLog::log->error("CDBServerPacketBuilder::BuildLogoutReqPkg, FromPlayer return false");
// 		return false;
// 	}
	return true;
}

//大退请求
/*
协议头：CltSockID＋0x0809  ＋0x00000000＋ 0x0000（2字节）
协议体：len+Account+len +角色名+len+角色数据
*/
bool CDSBuilder::BuildExitReqPkg(MSG_GS2DB::stExitReq& MsgOut, Player* pPlayer)
{
	//MsgOut.nTransID		= pPlayer->GetMapObjID();
// 	MsgOut.strUserID	= pPlayer->m_strUserID;
// 	MsgOut.strName		= pPlayer->m_name;
// 
// 	//角色数据
// 	if(!MsgOut.dataRole.FromPlayer(pPlayer))
// 	{
// 		MyLog::log->error("CDBServerPacketBuilder::BuildExitReqPkg, FromPlayer return false");
// 		return false;
// 	}
	return true;
}