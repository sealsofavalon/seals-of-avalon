#pragma once
#include "Singleton.h"
#include "../../Common/Protocol/GS2DB.h"

class CPlayer;

class CDSBuilder : public Singleton< CDSBuilder >
{
public:
	//GS版本协议（包含GS编号）
	bool BuildVersionPkg(MSG_GS2DB::stVersionPkg& MsgOut);
	//小退请求
	bool BuildLogoutReqPkg(MSG_GS2DB::stLogoutReq& MsgOut, Player* pPlayer);
	//大退请求
	bool BuildExitReqPkg(MSG_GS2DB::stExitReq& MsgOut, Player* pPlayer);
};
#define sDSBuild CDSBuilder::getSingleton()