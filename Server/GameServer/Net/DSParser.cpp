#include "StdAfx.h"
#include "DSParser.h"
#include "DSSocket.h"
#include "DSBuilder.h"
#include "GTBuilder.h"
#include "GTSocket.h"
// #include "../Game/MapInfo.h"
initialiseSingleton( CDSParser );
bool CDSParser::ParsePacket(CDSSocket* pDBServer, char* pData, ui32	nSize)
{
	if(!pDBServer)
		return false;

	DWORD dwStartTick = getMSTime();

	CPacketUsn pakTool(pData, nSize);

	switch(pakTool.GetProNo())
	{
	case MSG_DB2GS::ENTERGAME_ACK:
		{
			MSG_DB2GS::stEnterGameAck Msg;
			pakTool >> Msg;
			ProcessEnterGameAckPkg(pDBServer, Msg);
		}
		break;
	case MSG_DB2GS::LOGOUT_ACK:
		{
			MSG_DB2GS::stLogoutAck	Msg;
			pakTool >> Msg;
			ProcessLogoutAckPkg(pDBServer, Msg);
		}
		break;
	case MSG_DB2GS::EXIT_ACK:
		{
			MSG_DB2GS::stExitAck	Msg;
			pakTool >> Msg;
			ProcessExitAckPkg(pDBServer, Msg);
		}
		break;
	default :
		{
			MyLog::log->error("CDBServerPacketParser::ParsePacket Error porotcol:%u", pakTool.GetProNo());
		}
		break;
	}

	DWORD dwIntervalTick = getMSTime() - dwStartTick;
	if(dwIntervalTick > 100)
	{
		MyLog::log->error("CDBServerPacketParser::ParsePacket pro[%u] Last (%u)ms", pakTool.GetProNo(), dwIntervalTick);
	}

	return true;
	return false;
}

//角色上线回应
/*
协议头：CltSockID＋0x0802 ＋ 0x00000000（4字节）  ＋ ErrCode（1字节） + 0x00
协议体：[len＋角色数据]
ErrCode:0=成功(此时才有协议体),1=数据库异常,11=协议包错误，12＝找不到账号数据，13＝账号状态不对，14＝其它错误
*/
bool CDSParser::ProcessEnterGameAckPkg(CDSSocket* pDBServer, MSG_DB2GS::stEnterGameAck& Msg)
{
// 	Player* pPlayer = g_PlayerMng::instance()->FindPlayerByRoleName(Msg.roleData.m_strName.c_str());//g_PlayerMng::instance()->FindPlayerByRoleName(Msg.roleData.m_strName);
// 	if(!pPlayer)
// 	{
// 		return false;
// 	}
//
//
// 	MSG_S2C::stEnterGameAck MsgSend;
//
// 	int nExitInError = 0;
// 	if(Msg.nError)
// 	{
// 		//向gateserver发送错误信息
// 		//...
// 		nExitInError = Msg.nError;
// 		sGTBuilder.BuildEnterGameAckPkg(MsgSend, pPlayer, nExitInError);
// 		pPlayer->SendData(MsgSend);
// 		return false;
// 	}
//
// 	if(!pPlayer->Create( Msg.roleData) )
// 	{
// 		//向gateserver发送错误信息
// 		//...
// 		nExitInError = 14;
// 	}
// 	//g_PlayerMng::instance()->AddPlayerGuid(pPlayer);
// 	ObjectAccessor::Instance().AddObject(pPlayer);
// 	g_PlayerMng::instance()->RemovePlayer(pPlayer);
// 	if(nExitInError)
// 	{
// 		sGTBuilder.BuildEnterGameAckPkg(MsgSend, pPlayer, nExitInError);
// 		pPlayer->SendData(MsgSend);
//
// 		//string strPacket = g_DSBuilder::instance()->BuildExitReqPkg(pPlayer);
// 		//pDBServer->PostSend(strPacket.c_str(), strPacket.size());
// 		g_PlayerMng::instance()->RemovePlayer(pPlayer);
//
// 		return false;
// 	}
// 	//数据接收赋值完毕，向gateserver发送回应
// 	//...
// 	sGTBuilder.BuildEnterGameAckPkg(MsgSend, pPlayer, 0);
// 	pPlayer->SendData(MsgSend);
//
// 	pPlayer->m_ItemContainer.SendItemInfo();
// 	pPlayer->m_ItemContainer.SendEquipInfo();
// 	//角色上线协议发送
// 	//..
//
// 	//test
// 	pPlayer->SendSysMessage("欢迎进入尚世纪!!!");

	return true;
}

//角色小退回应
/*
协议头：CltSockID＋0x0808 ＋ 0x00000000  ＋ ErrCode（1字节） + 0x00
ErrCode:0=成功,1=协议错误，2=找不到账号数据，3＝账号状态不对，4＝其它错误
*/
bool CDSParser::ProcessLogoutAckPkg(CDSSocket* pDBServer, const MSG_DB2GS::stLogoutAck& Msg)
{
// 	Player* pPlayer = g_PlayerMng::instance()->FindPlayerByID(Msg.nTransID);
// 	if(!pPlayer)
// 	{
// 		return false;
// 	}

	return true;
}

//角色大退回应
/*
协议头：CltSockID＋0x080A ＋ 0x00000000  ＋ ErrCode（1字节） + 0x00
ErrCode:0=成功,1=协议错误，2=找不到账号数据，3＝账号状态不对，4＝其它错误
*/
bool CDSParser::ProcessExitAckPkg(CDSSocket* pDBServer, const MSG_DB2GS::stExitAck& Msg)
{
// 	Player* pPlayer = g_PlayerMng::instance()->FindPlayerByID(Msg.nTransID);
// 	if(!pPlayer)
// 	{
// 		return false;
// 	}

	return true;
}
