#pragma once
#include "Singleton.h"
#include "../../Common/Protocol/DB2GS.h"

class CDSSocket;
class CDSParser : public Singleton< CDSParser >
{
public:
	bool ParsePacket(CDSSocket* pDBServer, char* pData, ui32	nSize);

	//角色上线回应
	bool ProcessEnterGameAckPkg(CDSSocket* pDBServer, MSG_DB2GS::stEnterGameAck& Msg);
	//角色小退回应
	bool ProcessLogoutAckPkg(CDSSocket* pDBServer, const MSG_DB2GS::stLogoutAck& Msg);
	//角色大退回应
	bool ProcessExitAckPkg(CDSSocket* pDBServer, const MSG_DB2GS::stExitAck& Msg);
};
#define sDSParser CDSParser::getSingleton()
