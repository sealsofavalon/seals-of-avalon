#include "StdAfx.h"
#include "DSSocket.h"
#include "DSParser.h"

initialiseSingleton( CDSSocket );
CDSSocket::CDSSocket() : tcp_client( *net_global::get_io_service() )
{
	m_isreconnect = true;
}



void CDSSocket::on_connect()
{
	MyLog::log->info( "connect db server success" );
	tcp_client::on_connect();
}

void CDSSocket::on_connect_failed( boost::system::error_code error )
{
	MyLog::log->info( "connect db server failed, error message:%s", error.message().c_str() );
}

void CDSSocket::on_close( const boost::system::error_code& error )
{
	MyLog::log->error( "disconnected from db server, error message:%s", error.message().c_str() );
	MyLog::log->error( "please check the configuration of db server" );
	tcp_client::on_close( error );
}

void CDSSocket::proc_message( const message_t& msg )
{
	sDSParser.ParsePacket( this, (char*)msg.data, msg.len );
}

void CDSSocket::PostSend( const void* data, uint16 len )
{
	send_message( data, len );
}

void CDSSocket::PostSend( const PakHead& msg )
{
	CPacketSn cps;
	cps << msg;
	cps.SetProLen();
	send_message( cps.GetBuf(), cps.GetSize() );
}
