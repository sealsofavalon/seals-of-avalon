#pragma once

#include "Singleton.h"

#include "../../new_common/Source/net/tcpclient.h"

class CDSSocket :
	public tcp_client, public Singleton< CDSSocket >
{
public:
	CDSSocket();

	virtual void on_connect();
	virtual void on_connect_failed( boost::system::error_code error );
	virtual void on_close( const boost::system::error_code& error );
	virtual void proc_message( const message_t& msg );

	void PostSend( const PakHead& Msg );
	void PostSend( const void* data, uint16 len );
};
#define sDSSocket CDSSocket::getSingleton()
