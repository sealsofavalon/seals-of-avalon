#include "StdAfx.h"
#include "GS_SP_Client.h"
#include "GTListenSocket.h"
#include "../../Common/Protocol/GS2GT.h"

gs_sp_client::gs_sp_client() : tcp_client( *net_global::get_io_service() )
{
	set_reconnect( true );	
}

gs_sp_client::~gs_sp_client()
{

}

bool gs_sp_client::createfromconf()
{
	QueryResult* qr = RealmDatabase.Query("select * from group_conf where groupid = 15");
	if (qr)
	{
		Field* f = qr->Fetch();
		m_sp_gs_ip = f[1].GetString();
		m_sp_client_port = f[2].GetUInt32();
		m_sp_gs_port = f[4].GetUInt32();
		connect(m_sp_gs_ip.c_str(), m_sp_gs_port );

		delete qr;
		return true;
	}
	return false ;
}

void gs_sp_client::on_close( const boost::system::error_code& error )
{
	tcp_client::on_close(error);
}

void gs_sp_client::on_connect()
{
	tcp_client::on_connect();

	MyLog::log->info("connect gsp [ip:%s] [port:%d] success!", m_sp_gs_ip.c_str(), m_sp_gs_port);
	MSG_GS2GSP::stGSInit msg;
	msg.groupid = sMaster.m_group_id;
	PostSend(msg);
}

void gs_sp_client::on_connect_failed( boost::system::error_code error )
{
	
}

void gs_sp_client::proc_message(const message_t &msg)
{
	CPacketUsn pakTool(msg.data, msg.len);
	switch( pakTool.GetProNo() )
	{
	case MSG_GSP2GS::MSG_RETURN_2_MOTHER:
		{
			MSG_GSP2GS::stReturn2Mother MsgRecv;
			pakTool >> MsgRecv;

			WorldSession* s = sWorld.FindSessionByTransID( MsgRecv.transid );
			if( s )
				s->RealEnterGame();
		}
		break;
	case MSG_GSP2GS::MSG_JUMP_REQ:
		{
			MSG_GSP2GS::stJumpReq MsgRecv;
			pakTool >> MsgRecv;
			WorldSession* pSession = sWorld.FindSession( MsgRecv.acct );
			if( pSession && pSession->_player )
			{
				MSG_GS2GT::stJumpGS jumpmsg;
				jumpmsg.transid = pSession->_SessionID;
				jumpmsg.groupid = 15;
				pSession->SendPacket( jumpmsg );

				pSession->_player->BeforeJump2InstanceSrv();
				sWorld.wait_save_sql = true;
				pSession->m_disableMessage = true;
				pSession->LogoutPlayer( true, false );
				pSession->m_disableMessage = false;
				sWorld.wait_save_sql = false;

				MSG_GS2GSP::stJumpAck ack;
				ack.acct = pSession->GetAccountID();
				ack.transid = pSession->_SessionID;
				ack.gmflags = pSession->gmflags;
				ack.gateid = pSession->_GTSocket->m_nGateID;
				PostSend( ack );
			}
		}
		break;
	default:
		assert( 0 );
		break;
	}
}

void gs_sp_client::PostSend( PakHead &msg )
{
	CPacketSn cps;
	cps << msg;
	cps.SetProLen();
	send_message( cps.GetBuf(), cps.GetSize() );
}
