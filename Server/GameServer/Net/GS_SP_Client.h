#ifndef __GS_SP_CLIENT_H__
#define __GS_SP_CLIENT_H__

#include "../../new_common/Source/net/tcpclient.h"


class gs_sp_client : public tcp_client
{
public:
	gs_sp_client();
	~gs_sp_client(void);
	
	bool createfromconf();
	virtual void on_close( const boost::system::error_code& error );
	virtual void on_connect();
	virtual void on_connect_failed( boost::system::error_code error );
	virtual void proc_message( const message_t& msg );

	void PostSend( PakHead& msg );
private:
	std::string m_sp_gs_ip;		// surpass server ip
	int m_sp_gs_port;			// surpass server listen gs port 
	int m_sp_client_port;		// surpass server listen the client port
};

extern gs_sp_client* g_gs_sp_client;

#endif
