#include "StdAfx.h"
#include "GS_SP_ListenServer.h"
#include "GS_SP_Socket.h"

gs_sp_listen_server::gs_sp_listen_server() : tcp_server( 2 )
{
	for (int i = 0; i < 16; i++ )
	{
		m_SPGSSocket[i] = NULL ;
	}

	m_listenPort = 0;
}

bool gs_sp_listen_server::createfromconf()
{
	QueryResult* qr = RealmDatabase.Query("select * from group_conf where groupid = 15");
	if (qr)
	{
		Field* f = qr->Fetch();
		m_listenPort = f[4].GetUInt32();
		delete qr;

		if( !create( m_listenPort, 64, 1 ) )
		{
			MyLog::log->error("init surpass_listen_server listen port:%d failed", m_listenPort);
			return false;
		}

		return true;
	}
	return false ;
}

tcp_session* gs_sp_listen_server::create_session()
{
	return  new gs_sp_scocket( *net_global::get_io_service() );
}

void gs_sp_listen_server::stop()
{
	tcp_server::stop();
}

void gs_sp_listen_server::add_gs_sp_socket(gs_sp_scocket* pSocket)
{
	if (pSocket->m_gs_groupid > 0 && pSocket->m_gs_groupid < 15)
	{
		m_SPGSSocket[pSocket->m_gs_groupid] = pSocket;
	}
}

void gs_sp_listen_server::remove_gs_sp_socket(gs_sp_scocket* pSocket)
{
	if (pSocket->m_gs_groupid > 0 && pSocket->m_gs_groupid < 15)
	{
		m_SPGSSocket[pSocket->m_gs_groupid] = NULL;
	}
}

gs_sp_scocket* gs_sp_listen_server::get_gs( uint8 groupid )
{
	return m_SPGSSocket[groupid];
}

