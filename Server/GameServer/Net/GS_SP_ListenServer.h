#ifndef __GS_SP_LISTENSERVER_H__
#define __GS_SP_LISTENSERVER_H__
// 跨服服务器侦听服务器
#include "../../new_common/Source/net/tcpserver.h"
class gs_sp_scocket;

class gs_sp_listen_server : public tcp_server
{
public:
	gs_sp_listen_server();
	~gs_sp_listen_server(){;}
	
	bool createfromconf();
	virtual void stop();
	virtual tcp_session* create_session();

	void add_gs_sp_socket(gs_sp_scocket* pSocket);
	void remove_gs_sp_socket(gs_sp_scocket* pSocket);
	gs_sp_scocket* get_gs( uint8 groupid );

private:
	gs_sp_scocket* m_SPGSSocket[16];
	ui32 m_listenPort ;
};

extern gs_sp_listen_server* g_gs_sp_listen_server;

#endif
