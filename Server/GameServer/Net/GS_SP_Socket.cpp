#include "StdAfx.h"
#include "GS_SP_Socket.h"
#include "GS_SP_ListenServer.h"
#include "../Game/InstanceQueue.h"

gs_sp_scocket::gs_sp_scocket(boost::asio::io_service &is) : tcp_session( is )
{
	m_gs_groupid = 0 ;
}
gs_sp_scocket::~gs_sp_scocket()
{

}
void gs_sp_scocket::on_accept( tcp_server* p )
{
	m_strIP = this->get_remote_address_string();
	tcp_session::on_accept( p );
}

void gs_sp_scocket::on_close(const boost::system::error_code &error)
{
	g_gs_sp_listen_server->remove_gs_sp_socket(this);
	MyLog::log->notice( "gs_scoket disconnected! ip:%s id:%u error_no:%d error_message:%s", m_strIP.c_str(), m_gs_groupid, error.value(), error.message().c_str() );
	tcp_session::on_close( error );
}

void gs_sp_scocket::proc_message(const message_t &msg)
{
	CPacketUsn pakTool(msg.data, msg.len);
	switch(pakTool.GetProNo())
	{
	case MSG_GS2GSP::MSG_GS_INIT:
		{
			if (g_gs_sp_listen_server)
			{
				MSG_GS2GSP::stGSInit MsgRecv;
				pakTool>>MsgRecv;
				m_gs_groupid =  MsgRecv.groupid;

				g_gs_sp_listen_server->add_gs_sp_socket(this);
				MyLog::log->info("add game server groupid:%d ip:%s", m_gs_groupid, m_strIP.c_str());
			}
		}
		break;
	case MSG_GS2GSP::MSG_JUMP_ACK:
		{
			MSG_GS2GSP::stJumpAck ack;
			pakTool >> ack;
			g_crosssrvmgr->ApplyJump( ack.acct, ack.transid, ack.gateid, ack.gmflags );
		}
		break;
	case MSG_GS2GSP::MSG_QUEUE_INSTANCE_REQ:
		{
			MSG_GS2GSP::stQueueInstanceReq  MsgRecv;
			pakTool>> MsgRecv;
			g_instancequeuemgr.Add_GSCMember(m_gs_groupid,MsgRecv.mapid,  MsgRecv.v);
		}
		break;
	case MSG_GS2GSP::MSG_LEAVE_QUEUE_INSTANCE_REQ:
		{
			MSG_GS2GSP::stLeaveQueueInstanceReq MsgRecv;
			pakTool>> MsgRecv;
			g_instancequeuemgr.Leave_GSCMember(m_gs_groupid,MsgRecv.guid);
		}
		break;
	}
}

void gs_sp_scocket::PostSend( PakHead &msg )
{
	CPacketSn cps;
	cps << msg;
	cps.SetProLen();
	send_message( cps.GetBuf(), cps.GetSize() );
}
