#ifndef __GS_SP_SOCKET_H__
#define __GS_SP_SOCKET_H__
//连接跨服服务器
#include "../../new_common/Source/net/tcpsession.h"

class gs_sp_scocket : public tcp_session
{
public:
	gs_sp_scocket( boost::asio::io_service& is );
	~gs_sp_scocket(void);

	virtual void on_accept( tcp_server* p );
	virtual void on_close( const boost::system::error_code& error );
	virtual void proc_message( const message_t& msg );

	void PostSend( PakHead& msg );
public:
	string m_strIP;
	ui32 m_gs_groupid;
};

#endif
