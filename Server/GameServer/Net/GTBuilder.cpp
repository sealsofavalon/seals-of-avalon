#include "StdAfx.h"
#include "GTBuilder.h"
initialiseSingleton( CGTBuilder );
//回应角色进入游戏
/*
协议头：0x1002 ＋PlayIndex(4字节) + ErrCode（1字节） + 0x00 
协议体：len＋角色名＋性别（4位）＋职业（4位）＋等级（1字节）+经验（4字节）+本级最大经验值(4字节)＋金币数（4字节）＋HP（2字节）＋MaxHP（2字节）＋MP（2字节）＋MaxMP（2字节）
＋PK值（2字节）＋声望（2字节）＋len＋官职＋力量（2字节）＋体质（2字节）＋智力（2字节）＋法力（2字节）＋强壮（2字节）
＋物理攻击下限（2字节）＋物理攻击上限（2字节）＋物理防御下限（2字节）＋物理防御上限（2字节）＋法术攻击下限（2字节）
+法术攻击上限（2字节）＋法术防御下限（2字节）＋法术防御上限（2字节）＋幸运（1字节）＋固定外观（8字节）＋状态外观（4字节）
+len+MapCode+PosX（4字节float）＋PosY（4字节float）＋PosZ（4字节float）+角色方向(4字节float)

固定外观：发型（4位）＋头发颜色（4位）＋头盔ID（2字节）＋衣服ID（2字节）＋武器ID（2字节）＋0x00
状态外观：包括中毒和各技能状态等等（4字节，以位表示各状态）
注意：ErrCode =0表示成功登陆（此时才有协议体），,1=数据库异常,11=协议包错误，12＝找不到账号数据，13＝账号状态不对，14＝其它错误
*/
bool CGTBuilder::BuildEnterGameAckPkg(MSG_S2C::stEnterGameAck& MsgOut, Player* pPlayer, BYTE byErrorCode)
{
	MsgOut.guid				= pPlayer->GetGUID();
	MsgOut.nTransID			= pPlayer->GetSession()->_SessionID;
	MsgOut.nError			= byErrorCode;
	
	if(!byErrorCode)
	{
		//MsgOut.strName		= pPlayer->m_name;
		//MsgOut.nSex			= pPlayer->m_bySex;
		//MsgOut.nJob			= pPlayer->m_byJob;

// 		MsgOut.nLevel		= pPlayer->m_byLevel;
// 		MsgOut.nCurExp		= pPlayer->m_dwExp;
// 		MsgOut.nMaxExp		= pPlayer->m_pPlayerProp->dwExp;
// 		MsgOut.nMoney		= pPlayer->m_dwMoney;
// 		MsgOut.nCurHP		= pPlayer->m_wCurHP;
// 		MsgOut.nMaxHP		= pPlayer->m_wMaxHP;
// 		MsgOut.nCurMP		= pPlayer->m_wCurMP;
// 		MsgOut.nMaxMP		= pPlayer->m_wMaxMP;

		//MsgOut.strMap		= pPlayer->m_pMapInfo->m_strMapCode;

		//MsgOut.x			= pPlayer->m_flmpCurPos.x;
		//MsgOut.y			= pPlayer->m_flmpCurPos.y;
		//MsgOut.z			= pPlayer->m_flmpCurPos.z;
		//MsgOut.dir			= pPlayer->m_flDirection;
	}
	return true;
}

//角色进入地图(发给自己)
/*
协议头：0x1012＋PlayIndex(4字节) ＋0x0000 
协议体：len + MapCode + PosX（4字节float）＋PosY（4字节float）＋PosZ（4字节float）+角色方向(4字节float)
注意:如果是gm指令传送,就算还是在本地图,也会使用本协议.
*/
bool CGTBuilder::BuildEnterMapPkg(MSG_S2C::stEnterMapPkg& MsgOut, Player* pPlayer)
{
// 	MsgOut.nTransID		= pPlayer->GetSession()->m_SessionID;
// 	MsgOut.guid			= pPlayer->GetGUID();
// 
// 	MsgOut.strMap		= pPlayer->m_pMapInfo->m_strMapCode;
// 	MsgOut.x			= pPlayer->m_flmpCurPos.x;
// 	MsgOut.y			= pPlayer->m_flmpCurPos.y;
// 	MsgOut.z			= pPlayer->m_flmpCurPos.z;
// 	MsgOut.dir			= pPlayer->m_flDirection;
	return true;
}


//小退回应包
/*
协议头：CltSockID+0x1006 ＋PlayerIdx  ＋ ErrCode（1字节） + 0x00
ErrCode:0=成功,1=协议错误，2=找不到账号数据，3＝账号状态不对，4＝其它错误
*/
bool CGTBuilder::BuildLogoutAckPkg(MSG_GS2GT::stLogoutAck& MsgOut, Player* pPlayer, BYTE byErrCode)
{
	MsgOut.guid				= pPlayer->GetGUID();
	MsgOut.nError		= byErrCode;
	return true;
}

//大退回应包
/*
协议头：CltSockID +0x1008 ＋PlayerIdx  ＋ ErrCode（1字节） + 0x00
ErrCode:0=成功,1=协议错误，2=找不到账号数据，3＝账号状态不对，4＝其它错误
*/
bool CGTBuilder::BuildExitAckPkg(MSG_GS2GT::stExitAck& MsgOut, Player* pPlayer, BYTE byErrCode)
{
	MsgOut.guid				= pPlayer->GetGUID();
	MsgOut.nError			= byErrCode;
	return true;
}

//角色行走回应
/*
协议头：0x1024 ＋PlayerIdx(4字节)+ErrorCode(1字节)+0x00（1字节）
协议体：结果PosX（4字节float）＋结果PosY（4字节float）＋结果PosZ（4字节float）+结果角色方向(4字节float)
ErrorCode：0表示成功,其它表示失败
*/
// bool CGTBuilder::BuildPlayerMoveAckPkg(MSG_S2C::stPlayerMoveAck& MsgOut, Player* pPlayer, BYTE byErrCode /* = 0 */)
// {
// 	MsgOut.nError		= byErrCode;
// 	MsgOut.guid				= pPlayer->GetGUID();
// 
// // 	MsgOut.x			= pPlayer->m_flmpCurPos.x;
// // 	MsgOut.y			= pPlayer->m_flmpCurPos.y;
// // 	MsgOut.z			= pPlayer->m_flmpCurPos.z;
// // 	MsgOut.dir			= pPlayer->m_flDirection;
// 
// 	return true;
// }

//其它角色行走通知
/*
协议头：0x1025 ＋PlayerIdx(4字节)+ 0x0000（2字节）
协议体：新PosX（4字节float）＋新PosY（4字节float）＋新PosZ（4字节float）+新角色方向(4字节float)
*/
// bool CGTBuilder::BuildCreatureMovePkg(MSG_S2C::stCreatureMovePkg& MsgOut,Unit* pCreature, const FLMAPPOINT& flmpTarget, float flDirection)
// {
// 	MsgOut.guid				= pCreature->GetGUID();
// 	
// 	//协议体
// 	MsgOut.x			= flmpTarget.x;
// 	MsgOut.y			= flmpTarget.y;
// 	MsgOut.z			= flmpTarget.z;
// 	MsgOut.dir			= flDirection;
// 
// 	return true;
// }

//角色跑动回应
/*
协议头：0x1028 ＋PlayerIdx(4字节)+ErrorCode(1字节)+0x00（1字节）
协议体：结果PosX（4字节float）＋结果PosY（4字节float）＋结果PosZ（4字节float）+结果角色方向(4字节float)
ErrorCode：0表示成功,其它表示失败.
*/
// bool CGTBuilder::BuildPlayerRunAckPkg(MSG_S2C::stPlayerRunAck& MsgOut, Player* pPlayer, BYTE byErrCode /* = 0 */)
// {
// 	MsgOut.nError		= byErrCode;
// 	MsgOut.guid				= pPlayer->GetGUID();
// 
// // 	MsgOut.x			= pPlayer->m_flmpCurPos.x;
// // 	MsgOut.y			= pPlayer->m_flmpCurPos.y;
// // 	MsgOut.z			= pPlayer->m_flmpCurPos.z;
// // 	MsgOut.dir			= pPlayer->m_flDirection;
// 	return true;
// }

//其它角色跑动通知
/*
协议头：0x1029 ＋PlayerIdx(4字节)+ 0x0000（2字节）
协议体：新PosX（4字节float）＋新PosY（4字节float）＋新PosZ（4字节float）+新角色方向(4字节float)
*/
// bool CGTBuilder::BuildCreatureRunPkg(MSG_S2C::stCreatureRunPkg& MsgOut, Unit* pCreature, const FLMAPPOINT& flmpTarget, float flDirection)
// {
// 	MsgOut.guid			= pCreature->GetGUID();
// 	MsgOut.x			= flmpTarget.x;
// 	MsgOut.y			= flmpTarget.y;
// 	MsgOut.z			= flmpTarget.z;
// 	MsgOut.dir			= flDirection;
// 	return true;
// }

//死亡包
/*
协议头：0x1013 ＋CreatureID(4字节)+ 0x0000（2字节）
// */
// bool CGTBuilder::BuildCreatureDiePkg(MSG_S2C::stCreatureDiePkg& MsgOut, Unit* pCreature)
// {
// 	MsgOut.guid				= pCreature->GetGUID();
// 	return true;
// }

//复活回应包
/*
协议头：0x1015 ＋CreatureID(4字节)+ 复活类型（1字节）+Errcode（1字节）
协议体：等级（1字节）+经验（4字节）+本级最大经验值(4字节)＋HP（2字节）＋MaxHP（2字节）＋MP（2字节）＋MaxMP（2字节）＋
力量（2字节）＋体质（2字节）＋智力（2字节）＋法力（2字节）＋强壮（2字节）＋物理攻击下限（2字节）＋物理攻击上限（2字节）＋
物理防御下限（2字节）＋物理防御上限（2字节）＋法术攻击下限（2字节）+法术攻击上限（2字节）＋法术防御下限（2字节）＋
法术防御上限（2字节）＋固定外观（8字节）＋状态外观（4字节）+len+MapCode+PosX（4字节float）＋PosY（4字节float）＋
PosZ（4字节float）+角色方向(4字节float)

固定外观：发型（4位）＋头发颜色（4位）＋头盔ID（2字节）＋衣服ID（2字节）＋武器ID（2字节）＋0x00
状态外观：包括中毒和各技能状态等等（4字节，以位表示各状态）
Errcode: 0=成功(此时才有协议体), 其他失败
*/
// bool CGTBuilder::BuildReliveAckPkg(MSG_S2C::stReliveAck& MsgOut, Player* pPlayer, BYTE byReliveType, BYTE byErrCode/* =0 */)
// {
// 	MsgOut.guid				= pPlayer->GetGUID();
// 	MsgOut.nReliveType	= byReliveType;
// 	MsgOut.nError		= byErrCode;
// 
// 	if(byErrCode == 0)
// 	{
// 		MsgOut.nLevel		= pPlayer->GetLevel();
// 		MsgOut.nExp			= pPlayer->GetExp();
// 		MsgOut.nMaxExp		= pPlayer->m_pPlayerProp->dwExp;
// 		MsgOut.nCurHP		= pPlayer->GetCurHP();
// 		MsgOut.nMaxHP		= pPlayer->m_wMaxHP;
// 		MsgOut.nCurMP		= pPlayer->GetCurMP();
// 		MsgOut.nMaxMP		= pPlayer->m_wMaxMP;

// 		if(pPlayer->m_pMapInfo)
// 		{
// 			MsgOut.strMap	= pPlayer->m_pMapInfo->m_strMapCode;
// 		}
// 		MsgOut.x			= pPlayer->m_flmpCurPos.x;
// 		MsgOut.y			= pPlayer->m_flmpCurPos.y;
// 		MsgOut.z			= pPlayer->m_flmpCurPos.z;
// 		MsgOut.dir			= pPlayer->m_flDirection;
// 	}
// 	return true;
// }

//复活消息通知包
/*
协议头：0x1016 ＋CreatureID(4字节)+ 0x0000（2字节）
协议体：PosX（4字节float）＋PosY（4字节float）＋PosZ（4字节float）+角色方向(4字节float)＋
		HP百分比(1字节)+MP百分比(1字节)+固定外观（8字节）＋状态外观（4字节）。
固定外观：发型（4位）＋头发颜色（4位）＋头盔ID（2字节）＋衣服ID（2字节）＋武器ID（2字节）＋0x00
状态外观：包括中毒和各技能状态等等（4字节，以位表示各状态）
*/
// bool CGTBuilder::BuildRelivePkg(MSG_S2C::stRelivePkg& MsgOut, Unit* pCreature)
// {
// 	MsgOut.guid			= pCreature->GetGUID();
// // 	MsgOut.x			= pCreature->m_flmpCurPos.x;
// // 	MsgOut.y			= pCreature->m_flmpCurPos.y;
// // 	MsgOut.z			= pCreature->m_flmpCurPos.z;
// // 	MsgOut.dir			= pCreature->m_flDirection;
// 
// // 	MsgOut.nHPPercent	= pCreature->GetCurHP()*100/pCreature->m_wMaxHP;
// // 	MsgOut.nMPPercent	= pCreature->GetCurMP()*100/pCreature->m_wMaxMP;
// 	return true;
// }

//攻击回应
/*
协议头：0x1033 ＋PlayerIdx(4字节)+ ErrorCode（1字节） + 0x00（1字节）
协议体：wMagicID(2字节) + wAnimationID(2字节) 
*/
// bool CGTBuilder::BuildPlayerAttackAckPkg(MSG_S2C::stPlayerAttackAck& MsgOut, Player* pPlayer, BYTE byError, ui16 wMagicID, ui16 wAnimationID)
// {
// 	MsgOut.guid				= pPlayer->GetGUID();
// 	MsgOut.nError		= byError;
// 
// 	MsgOut.nMagicID		= wMagicID;
// 	MsgOut.nAnimationID	= wAnimationID;
// 
// 	return true;
// }

//攻击效果广播
/*
协议头：0x1034 ＋PlayerIdx(4字节)+ ErrorCode（1字节） + 0x00（1字节）
协议体：wMagicID（魔法ID 2字节） + wAnimationID(2字节) + byTargetsNum(1字节) + CreatureID1(4字节) + CreatureID2(4字节) + ...
*/
// bool CGTBuilder::BuildPlayerAttackPkg(MSG_S2C::stPlayerAttackPkg& MsgOut, Player* pPlayer, BYTE byError, ui16 wMagicID, ui16 wAnimationID, Unit* pTargets, BYTE byTargetsNum)
// {
// 	MsgOut.guid				= pPlayer->GetGUID();
// 	MsgOut.nError		= byError;
// 
// 	MsgOut.nMagicID		= wMagicID;
// 	MsgOut.nAnimationID	= wAnimationID;
// // 	for (int i = 0; i < byTargetsNum; i++)
// // 	{
// // 		Unit* pCreature = pTargets+i;
// // 		MsgOut.vTarget.push_back(pCreature->GetMapObjID());
// // 	}
// 	return true;
// }

//升级广播
/*
协议头：0x103B ＋PlayerIdx(4字节)+ 0x00（1字节） + 0x00（1字节）
协议体：wNewLevel（新等级 2字节）
*/
// bool CGTBuilder::BuildPlayerLevelPkg(MSG_S2C::stPlayerLevelUpPkg& MsgOut, Player* pPlayer)
// {
// 	MsgOut.guid				= pPlayer->GetGUID();
// 
// 	//MsgOut.nLevel		= pPlayer->GetLevel();
// 	return true;
// }

//
//string CGTPacketBuilder::BuildMapItemPickUpBst(Player* pPlayer, CMapItem* pMapItem)
//{
//	string strPackage = "";
//	TRY_BEGIN
//		//协议头
//		STNETMSGHEADER msgHeader;
//	memset(&msgHeader, 0, sizeof(STNETMSGHEADER));
//	msgHeader.wProNO = GS_GT_PLAYER_LEVEL_UP;
//	msgHeader.dwCreatureID = pPlayer->GetMapObjID();
//
//	//协议体
//	CFieldCombiner fieldBody;
//	fieldBody.AddWord(pPlayer->GetLevel());
//
//	strPackage.append((char*)&msgHeader, sizeof(STNETMSGHEADER));
//	strPackage.append(fieldBody.GetBuffer(), fieldBody.GetSize());
//	TRY_END
//		return strPackage;
//}


bool CGTBuilder::BuildPartyMemberStatsChangedPacket(MSG_S2C::stGroupMemberStat& MsgOut, Player *player)
{
// 	ui32 mask;// = player->GetGroupUpdateFlag();
// 
// 	if (mask == GROUP_UPDATE_FLAG_NONE)
// 		return false;
// 
// 	if (mask & GROUP_UPDATE_FLAG_POWER_TYPE)                // if update power type, update current/max power also
// 		mask |= (GROUP_UPDATE_FLAG_CUR_POWER | GROUP_UPDATE_FLAG_MAX_POWER);
// 
// 	if (mask & GROUP_UPDATE_FLAG_PET_POWER_TYPE)            // same for pets
// 		mask |= (GROUP_UPDATE_FLAG_PET_CUR_POWER | GROUP_UPDATE_FLAG_PET_MAX_POWER);
// 
// 	ui32 byteCount = 0;
// 	for (int i = 1; i < GROUP_UPDATE_FLAGS_COUNT; ++i)
// 		if (mask & (1 << i))
// 			byteCount += GroupUpdateLength[i];
// 
// 	MsgOut.guid			= player->GetGUID();
// 	MsgOut.mask			= mask;
// 
// 	ByteBuffer data;
// 
// 	if (mask & GROUP_UPDATE_FLAG_STATUS)
// 	{
// 		if (player)
// 		{
// 			if (player->IsPvP())
// 				data << (ui16) (MEMBER_STATUS_ONLINE | MEMBER_STATUS_PVP);
// 			else
// 				data << (ui16) MEMBER_STATUS_ONLINE;
// 		}
// 		else
// 			data << (ui16) MEMBER_STATUS_OFFLINE;
// 	}
// 
// 	if (mask & GROUP_UPDATE_FLAG_CUR_HP)
// 		data << (ui16) player->GetHealth();
// 
// 	if (mask & GROUP_UPDATE_FLAG_MAX_HP)
// 		data << (ui16) player->GetMaxHealth();
// 
// 	Powers powerType = player->getPowerType();
// 	if (mask & GROUP_UPDATE_FLAG_POWER_TYPE)
// 		data << (uint8) powerType;
// 
// 	if (mask & GROUP_UPDATE_FLAG_CUR_POWER)
// 		data << (ui16) player->GetPower(powerType);
// 
// 	if (mask & GROUP_UPDATE_FLAG_MAX_POWER)
// 		data << (ui16) player->GetMaxPower(powerType);
// 
// 	if (mask & GROUP_UPDATE_FLAG_LEVEL)
// 		data << (ui16) player->getLevel();
// 
// 	if (mask & GROUP_UPDATE_FLAG_ZONE)
// 		data << (ui16) player->GetZoneId();
// 
// 	if (mask & GROUP_UPDATE_FLAG_POSITION)
// 		data << (ui16) player->GetPositionX() << (ui16) player->GetPositionY();
// 
// 	if (mask & GROUP_UPDATE_FLAG_AURAS)
// 	{
// 		uint64 auramask;// = player->GetAuraUpdateMask();
// 		data << uint64(auramask);
// 		for(uint32 i = 0; i < MAX_AURAS; ++i)
// 		{
// 			if(auramask & (uint64(1) << i))
// 			{
// 				data << ui16(player->GetUInt32Value(UNIT_FIELD_AURA + i));
// 				data << uint8(1);
// 			}
// 		}
// 	}
// 
// 	Pet *pet = player->GetPet();
// 	if (mask & GROUP_UPDATE_FLAG_PET_GUID)
// 	{
// 		if(pet)
// 			*data << (uint64) pet->GetGUID();
// 		else
// 			*data << (uint64) 0;
// 	}
// 
// 	if (mask & GROUP_UPDATE_FLAG_PET_NAME)
// 	{
// 		if(pet)
// 			*data << pet->GetName();
// 		else
// 			*data << (uint8)  0;
// 	}
// 
// 	if (mask & GROUP_UPDATE_FLAG_PET_MODEL_ID)
// 	{
// 		if(pet)
// 			*data << (ui16) pet->GetDisplayId();
// 		else
// 			*data << (ui16) 0;
// 	}
// 
// 	if (mask & GROUP_UPDATE_FLAG_PET_CUR_HP)
// 	{
// 		if(pet)
// 			*data << (ui16) pet->GetHealth();
// 		else
// 			*data << (ui16) 0;
// 	}
// 
// 	if (mask & GROUP_UPDATE_FLAG_PET_MAX_HP)
// 	{
// 		if(pet)
// 			*data << (ui16) pet->GetMaxHealth();
// 		else
// 			*data << (ui16) 0;
// 	}
// 
// 	if (mask & GROUP_UPDATE_FLAG_PET_POWER_TYPE)
// 	{
// 		if(pet)
// 			*data << (uint8)  pet->getPowerType();
// 		else
// 			*data << (uint8)  0;
// 	}
// 
// 	if (mask & GROUP_UPDATE_FLAG_PET_CUR_POWER)
// 	{
// 		if(pet)
// 			*data << (ui16) pet->GetPower(pet->getPowerType());
// 		else
// 			*data << (ui16) 0;
// 	}
// 
// 	if (mask & GROUP_UPDATE_FLAG_PET_MAX_POWER)
// 	{
// 		if(pet)
// 			*data << (ui16) pet->GetMaxPower(pet->getPowerType());
// 		else
// 			*data << (ui16) 0;
// 	}
// 
// 	if (mask & GROUP_UPDATE_FLAG_PET_AURAS)
// 	{
// 		if(pet)
// 		{
// 			uint64 auramask = pet->GetAuraUpdateMask();
// 			*data << uint64(auramask);
// 			for(uint32 i = 0; i < MAX_AURAS; ++i)
// 			{
// 				if(auramask & (uint64(1) << i))
// 				{
// 					*data << ui16(pet->GetUInt32Value(UNIT_FIELD_AURA + i));
// 					*data << uint8(1);
// 				}
// 			}
// 		}
// 		else
// 			*data << (uint64) 0;
// 	}

	//MsgOut.data.assign((const char*)data.contents(), data.size());
	return true;
}
