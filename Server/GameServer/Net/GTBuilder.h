#pragma once
#include "Singleton.h"
#include "../../../SDBase/Public/PlayerDef.h"
#include "../../../SDBase/Public/ItemDef.h"
#include "../../Common/Protocol/GS2GT.h"
#include "../../../SDBase/Protocol/S2C.h"
#include "../../../SDBase/Protocol/S2C_Group.h"
#include "../Utility/GlobeFunc.h"

class Item;
class Player;
class Unit;
class CGTBuilder : public Singleton< CGTBuilder >
{
public:
	//...
	bool BuildEnterGameAckPkg(MSG_S2C::stEnterGameAck& MsgOut, Player* pPlayer, BYTE byErrorCode);//回应角色进入游戏

// 	bool BuildMapItemPkg(MSG_S2C::stMapItemAppearPkg& MsgOut, CMapItem* pItem);
// 	bool BuildMapItemDisPkg(MSG_S2C::stMapItemDisAppearPkg& MsgOut, CMapItem* pItem);
	
	bool BuildEnterMapPkg(MSG_S2C::stEnterMapPkg& MsgOut, Player* pPlayer);//角色进入地图(发给自己)
	bool BuildLogoutAckPkg(MSG_GS2GT::stLogoutAck& MsgOut, Player* pPlayer, BYTE byErrCode);//小退回应包
	bool BuildExitAckPkg(MSG_GS2GT::stExitAck& MsgOut, Player* pPlayer, BYTE byErrCode);//大退回应包

//// 	bool BuildCreatureHpMpPkg(MSG_S2C::stHPMPPkg& MsgOut, Unit* pCreature, ui16 wMagicID = 0, BYTE byIsSelf = 0);//HPMP修改通知
//// 	bool BuildPlayerExpPkg(MSG_S2C::stExpPkg& MsgOut, Player pPlayer);//等级经验通知
//// 	bool BuildPlayerPropPkg(MSG_S2C::stPlayerPropPkg& MsgOut, Player pPlayer);//角色属性通知
//	bool BuildCreatureDiePkg(MSG_S2C::stCreatureDiePkg& MsgOut, Unit* pCreature);//死亡包
//	bool BuildReliveAckPkg(MSG_S2C::stReliveAck& MsgOut, Player* pPlayer, BYTE byReliveType, BYTE byErrCode=0);//复活回应包
//	bool BuildRelivePkg(MSG_S2C::stRelivePkg& MsgOut, Unit* pCreature);//复活消息通知包
//// 	bool BuildPlayerLevelPkg(MSG_S2C::stPlayerLevelUpPkg& MsgOut, Player* pPlayer);//升级消息通知包
//
//	bool BuildPlayerAttackAckPkg(MSG_S2C::stPlayerAttackAck& MsgOut, Player* pPlayer, BYTE byError, ui16 wMagicID, ui16 wAnimationID);//攻击回应
//	bool BuildPlayerAttackPkg(MSG_S2C::stPlayerAttackPkg& MsgOut, Player* pPlayer, BYTE byError, ui16 wMagicID, ui16 wAnimationID, Unit* pTargets, BYTE byTargetsNum);//攻击效果广播
//

	bool BuildPartyMemberStatsChangedPacket(MSG_S2C::stGroupMemberStat& MsgOut, Player* player);
};
#define sGTBuilder CGTBuilder::getSingleton()