#include "StdAfx.h"
#include "GTListenSocket.h"
#include "GTSocket.h"
initialiseSingleton( CGTListenSocket );

CGTListenSocket::CGTListenSocket() : tcp_server( 1 )
{
	set_security( false );
	memset( m_mapAccpetedSockets, 0, sizeof( m_mapAccpetedSockets ) );
}

CGTListenSocket::~CGTListenSocket(void)
{
}

tcp_session* CGTListenSocket::create_session()
{
	return new CGTSocket( *net_global::get_io_service() );
}

void CGTListenSocket::stop()
{
	memset( m_mapAccpetedSockets, 0, sizeof( m_mapAccpetedSockets ) );
	tcp_server::stop();
}

void CGTListenSocket::AddAcceptedSocket(CGTSocket* pSocket)
{
	m_mapAccpetedSockets[pSocket->m_nGateID] = pSocket;
}

void CGTListenSocket::RemoveAcceptedSocket(CGTSocket* pSocket)
{
	m_mapAccpetedSockets[pSocket->m_nGateID] = NULL;
}

CGTSocket* CGTListenSocket::GetGate( uint8 gateid )
{
	return m_mapAccpetedSockets[gateid];
}

void CGTListenSocket::SendToGateSevers(const char* szBuf, int iLen)
{
}

void CGTListenSocket::UpdateGateServerStates()
{

}
