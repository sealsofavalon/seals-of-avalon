#pragma once

#include "../../new_common/Source/net/tcpserver.h"
#include "Singleton.h"

class CGTSocket;

class CGTListenSocket :
	public tcp_server, public Singleton< CGTListenSocket >
{
public:
	CGTListenSocket();
	virtual ~CGTListenSocket(void);

	virtual void stop();
	virtual tcp_session* create_session();

	void AddAcceptedSocket(CGTSocket* pSocket);
	void RemoveAcceptedSocket(CGTSocket* pSocket);
	void SendToGateSevers(const char* szBuf, int iLen);
	void UpdateGateServerStates();
	CGTSocket* GetGate( uint8 gateid );
private:
	CGTSocket* m_mapAccpetedSockets[16];
};
#define sGTListenSocket CGTListenSocket::getSingleton()
