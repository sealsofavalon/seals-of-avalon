#include "StdAfx.h"
#include "GTParser.h"
#include "GTBuilder.h"
#include "DSBuilder.h"
#include "GTSocket.h"
#include "DSSocket.h"
#include "GTListenSocket.h"
#include "../Master.h"

initialiseSingleton( CGTParser );
bool CGTParser::Init()
{
	return false;
}

bool CGTParser::ParsePacket(CGTSocket* pGT, char* pData, ui32 nLen)
{
	if(!pGT)
		return false;

	DWORD dwStartTick = getMSTime();

	CPacketUsn pakTool(pData, nLen);
	uint16 msg_type = pakTool.GetProNo();

	if(msg_type >= MSG_C2S::NUM_MSG_TYPES)
		MyLog::log->error("[Session] Received out of range packet with opcode 0x%.4X", pakTool.GetProNo());

	if( msg_type == MSG_C2S::GROUP_ID_REQ )
	{
		MSG_C2S::stGroupIDReq MsgRecv;
		pakTool >> MsgRecv;

		pGT->m_nGateID = MsgRecv.groupid;
		sGTListenSocket.AddAcceptedSocket( pGT );
		MyLog::log->notice( "gate:%u connected...", MsgRecv.groupid );
	}
	else if(msg_type == MSG_C2S::ENTERGAME_REQ)
	{
		MSG_C2S::stEnterGameReq MsgRecv;
		pakTool >> MsgRecv;

		WorldSession * pSession = new WorldSession(MsgRecv.AccountID, MsgRecv.nTransID, MsgRecv.guid, MsgRecv.strRole, MsgRecv.AccountName, pGT, MsgRecv.strIP);
		pSession->gmflags = MsgRecv.GMFlags;
		pSession->LoadSecurity(MsgRecv.GMFlags);
		pSession->m_muted = MsgRecv.Muted;
		pSession->m_baned = MsgRecv.Baned;
		pSession->m_bFangchenmiAccount = MsgRecv.bFangchenmiAccount;
		pSession->m_LastLogout = MsgRecv.LastLogout;
		pSession->m_ChenmiTime = MsgRecv.ChenmiTime;

		bool result = sWorld.AddSession(pSession);
		if( !result )
		{
			delete pSession;
			return false;
		}
		sWorld.AddGlobalSession(pSession);
		if(pSession->HasGMPermissions() && pSession)
			sWorld.gmList.insert(pSession);

		if( !sWorld.EnterGameReq( pSession ) )
			sWorld.DeleteSession( pSession );
		//暂时GS自行读取数据库,没有经过DB中转
	}
	else if( msg_type == MSG_C2S::MSG_CANEL_ENTER_GAME_LINE )
	{
		WorldSession* pSession = sWorld.FindSessionByTransID( pakTool.GetTransID() );
		if( pSession )
			sWorld.DeleteSession( pSession );
		return true;
	}
	else if( msg_type == MSG_C2S::MSG_ACT_TOOL_ACCESS_REQ )
	{
		MSG_C2S::stActToolAccessReq req;
		pakTool >> req;
		WorldSession * pSession = new WorldSession(0xFFFFFFFF, req.nTransID, 0xFFFFFFFF, "activity tool", "activity tool", pGT, "127.0.0.1");
		pSession->_GTSocket = pGT;
		pSession->LoadSecurity( "a" );
		pSession->m_muted = 0;
		pSession->m_baned = 0;
		pSession->m_bFangchenmiAccount = 0;
		pSession->m_LastLogout = (uint32)UNIXTIME;
		pSession->m_ChenmiTime = (uint32)UNIXTIME;

		WorldSession *session = sWorld.FindSession(pSession->GetAccountID());
		if( session )
		{
			pSession->Disconnect();
			MyLog::log->info("WorldSession : activity tool is already online!");
			return false;
		}

		sWorld.m_sessions[pSession->GetAccountID()] = pSession;
		sWorld.m_transSessions[pSession->_SessionID] = pSession;

		g_activity_mgr->tool_session = pSession;
		MSG_S2C::stActToolAccessAck ack;
		ack.pass = 1;
		ack.servertime = UNIXTIME;
		pSession->SendPacket( ack );
		//sWorld.AddGlobalSession(pSession);
	}
	else
	{
		WorldSession* pSession = sWorld.FindSessionByTransID( pakTool.GetTransID() );
		//if( !pSession )
		 //检验客户端有效性
		if( g_activity_mgr->tool_session && pSession == g_activity_mgr->tool_session )
		{
			pSession->HandleMsg(pakTool);
		}
		else
		{
			if( !pSession )
			{
				//MyLog::log->error("CGTParser::ParsePacket, connot find session by TransID！");
				return false;
			}
			if( !pSession->_player && pakTool.GetProNo() == MSG_C2S::CMSG_PLAYER_LOGOUT )
			{
				sWorld.DeleteSession( pSession );
				return false;
			}

			if(pSession->bDeleted || pSession->IsLoggingOut())
			{
				MyLog::log->error("session cann't process msg while logging out or deleting");
				return false;
			}
			pSession->HandleMsg(pakTool);
		}
		// if(/*pPlayer->GetCreatureState() == eCreatureStateDie && */pakTool.GetProNo() != MSG_C2S::CHAT_REQ &&
		//	 pakTool.GetProNo() != MSG_C2S::LOGOUT_REQ && pakTool.GetProNo() != MSG_C2S::EXIT_REQ && pakTool.GetProNo() != MSG_C2S::RELIVE_REQ)
		//{
		//	MyLog::log->error("CGTParser::ParsePacket, 死亡时发送不可处理协议[%d]！", pakTool.GetProNo());
		//	return false;
		//}
	}

	/*map<ui16, OpcodeHandler>::const_iterator iter = m_opcodeTable.find(pakTool.GetProNo());
	if( iter != m_opcodeTable.end() )
	{
		(this->*iter->second.handler)(pPlayer, pakTool);
	}
	else
		MyLog::log->error("CGTParser::ParsePacket, unresolved protocol[%u]", pakTool.GetProNo());*/

	DWORD dwIntervalTick = getMSTime() - dwStartTick;
	if(dwIntervalTick > 100)
	{
		MyLog::log->error("CGTParser::ParsePacket pro[%u] Last (%u)ms", pakTool.GetProNo(), dwIntervalTick);
	}

	return true;
}

//小退
//bool CGTParser::ProcessLogoutReqPkg(Player* pPlayer, CPacketUsn& pakTool)
//{
//	MSG_C2S::stLogoutReq MsgRecv;
//	pakTool >> MsgRecv;
//
//	//将回应结果发送给gateserver
//	//...
//// 	MSG_GS2GT::stLogoutAck MsgSend;
//// 	sGTBuilder.BuildLogoutAckPkg(MsgSend, pPlayer, 0);
//// 	pPlayer->SendData(MsgSend);
////
//// 	pPlayer->Logout(true);
//
//	return true;
//}
//
