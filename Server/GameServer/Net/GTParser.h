#pragma once
#include "Singleton.h"
#include "../../../SDBase/Protocol/C2S.h"
#include "../../Common/Protocol/GT2GS.h"
#include "Common.h"
class Player;
class CGTSocket;

class CGTParser : public Singleton< CGTParser >
{
public:
	bool Init();

public:
	bool ParsePacket(CGTSocket* pGateServer, char* pData, ui32 nLen);

	/*
	//创建角色请求
	static bool ProcessCreateRoleReqPkg(class CGTSocket* pGateServer, const string& strPacket);

	//账号角色列表请求
	static bool ProcessRoleListReqPkg(class CGTSocket* pGateServer, const string& strPacket);

	//删除角色请求
	static bool ProcessDeleteRoleReqPkg(class CGTSocket* pGateServer, const string& strPacket);

	//恢复角色请求
	static bool ProcessResumeRoleReqPkg(class CGTSocket* pGateServer, const string& strPacket);

	//账号下线通知
	static bool ProcessExitPkg(class CGTSocket* pGateServer, const string& strPacket);
	*/
};
#define sGTParser CGTParser::getSingleton()
