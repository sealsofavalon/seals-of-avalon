#include "StdAfx.h"
#include "GTSocket.h"
#include "GTListenSocket.h"
#include "GTParser.h"
#include "../Game/ObjectMgr.h"
CGTSocket::CGTSocket( boost::asio::io_service& is ) : tcp_session( is )
{
	m_nGateID = 0;
}

CGTSocket::~CGTSocket(void)
{
}

void CGTSocket::on_close( const boost::system::error_code& error )
{
	sGTListenSocket.RemoveAcceptedSocket(this);
	MyLog::log->error( "GT connection closed IP:%s", m_strIP.c_str() );
	tcp_session::on_close( error );
	objmgr.KickPlayersByGate( this );
}

void CGTSocket::on_accept( tcp_server* p )
{
	m_strIP = this->get_remote_address_string();
	m_dwIP = this->get_remote_address_ui();
	m_iPort = this->get_remote_port();
	MyLog::log->info( "A GT Connection[%s] Accepted", m_strIP.c_str() );

	tcp_session::on_accept( p );
}

void CGTSocket::proc_message( const message_t& msg )
{
	sGTParser.ParsePacket( this, (char*)msg.data, msg.len );
}

void CGTSocket::run()
{
	tcp_session::run();
	for( std::map<CPacketSn*, uint32>::iterator it = m_sendqueue.begin(); it != m_sendqueue.end(); ++it )
	{
		if( (uint32)UNIXTIME >= it->second )
		{
			CPacketSn* cps = it->first;
			send_message( cps->GetBuf(), cps->GetSize() );
			delete cps;
			m_sendqueue.erase( it );
			break;
		}
	}
}

void CGTSocket::PostSend( const void* data, uint16 len )
{
	send_message( data, len );
}

void CGTSocket::PostSend( const PakHead& msg, uint32 delay )
{
	if( delay == 0 )
	{
		CPacketSn cps;
		cps << msg;
		cps.SetProLen();
		send_message( cps.GetBuf(), cps.GetSize() );
	}
	else
	{
		CPacketSn* cps = new CPacketSn;
		*cps << msg;
		cps->SetProLen();
		m_sendqueue[cps] = (uint32)UNIXTIME + delay;
	}
}
