#pragma once


#include "../../new_common/Source/net/tcpsession.h"

class CGTSocket :
	public tcp_session
{
public:
	CGTSocket( boost::asio::io_service& is );
	virtual ~CGTSocket(void);
	//重载,添加加密功能

	virtual void on_close( const boost::system::error_code& error );
	virtual void on_accept( tcp_server* p );
	virtual void proc_message( const message_t& msg );
	virtual void run();

	void PostSend( const PakHead& Msg, uint32 delay = 0 );
	void PostSend( const void* data, uint16 len );

public:
	string m_strIP;
	DWORD  m_dwIP;
	int m_iPort;
	time_t m_tLastMsgTime;
	uint32 m_nGateID;
	std::map<CPacketSn*, uint32> m_sendqueue;
};
