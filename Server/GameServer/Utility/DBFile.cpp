//#include "StdAfx.h"
#include "DBFile.h"

#pragma warning(disable : 4996)



void TableHeader::GetData(char * Buff)
{
	m_sLength = *(unsigned short *)(Buff);
	m_sHeaderLength = *(unsigned short *)(Buff+2);
	m_cDBFileType = *(Buff+4);
	m_cBlockSize = *(Buff+5);
	m_lRecordNum = *(unsigned long *)(Buff+6);
	m_sBlockNum = *(unsigned short *)(Buff+10);
	m_sTotalBlockNum = *(unsigned short *)(Buff+12);
	m_sFirstBlock = *(unsigned short *)(Buff+14);
	m_sLastBlock = *(unsigned short *)(Buff+16);
	m_cFieldNum = *(Buff+33);
	m_cKeyFieldNum = *(Buff+35);
	m_sFreeBlock = *(unsigned short *)(Buff+77);
}
