#pragma once

#include <string>
#include <cstdio>

using namespace std;
class FieldDescription
{
public:
	FieldDescription(): m_cFieldType(0), m_cFieldLength(0), m_strFieldName(""){}
	~FieldDescription(){}
	char	m_cFieldType;				// 01  A	02  D		03  S		05  $		06  N		0C  M		0D  B
	char	m_cFieldLength;				// The length for $ and N is always 8.
										// The length for D is always 4.
										// The length for S is always 2.
										// The length for A ranges from 1 to 255 (01h to FFh).
										// The length for M ranges from 11 to 250 (0Bh to FAh).
										// The length for B ranges from 10 to 250 (0Ah to FAh).
	string	m_strFieldName;

};

class TableHeader
{
public:
	void GetData(char * Buff);
	unsigned short	m_sLength;				// Record length
	unsigned short	m_sHeaderLength;		// Length of the header block.
	unsigned char	m_cDBFileType;			// File type
											// 00 - DB file for an keyed table
											// 02 - DB file for an unkeyed table
	unsigned char	m_cBlockSize;			// Data block size code	01 - Block size is 1k		02 - Block size is 2k		03 - Block size is 3k (not used in 4.5)		04 - Block size is 4k
	unsigned long	m_lRecordNum;			// Number of records in DB
	unsigned short	m_sBlockNum;			// Number of blocks in use
	unsigned short	m_sTotalBlockNum;		// Total blocks in file
	unsigned short	m_sFirstBlock;			// First data block (always 1)
	unsigned short	m_sLastBlock;			// Last block in use
	//char			m_cSpace1[15];			//
	unsigned char	m_cFieldNum;			// Number of fields
	//char			m_cSpace2;
	unsigned char	m_cKeyFieldNum;			// Number of key fields
	//char			m_cSpace3[41];
	unsigned short	m_sFreeBlock;			// Block number of first free block
};

class DBBlock
{
public:
	unsigned short	m_sNextBlock;			// Next block number (Zero if last block)
	unsigned short	m_sPrevBlock;			// Previous block number (Zero if first block)
	unsigned short	m_sOffsetLastBlock;		// Offset of last record in block
};

template <class T>
class CDBFile
{
public:
	CDBFile(void);
public:
	virtual ~CDBFile(void);

	bool	Load(string strFile);
	bool	ReadBlock(int nBuffLength);

	string				m_strFileName;
	FILE*				m_hDBFile;

	TableHeader			m_DBHeader;
	char				m_cBlockBuff[4*1024];
};



const int SizeTbl[5] = {0, 1*1024, 2*1024, 3*1024, 4*1024};

template <class T>
CDBFile<T>::CDBFile(void):
m_hDBFile(NULL),
m_strFileName("")
{

}

template <class T>
CDBFile<T>::~CDBFile(void)
{
}


template <class T>
bool CDBFile<T>::Load(string strFile)
{
	bool bRet = true;
	m_hDBFile = fopen(strFile.c_str(), "rb");
	if (!m_hDBFile)
	{
		// open failed
		return false;
	}

	// read table header
	// can't read directly, cause memory align
	// fread(&m_DBHeader, sizeof(char), sizeof(TableHeader), m_hDBFile);
	char HeaderBuff[79];
	fread(&HeaderBuff, sizeof(char), 79, m_hDBFile);
	m_DBHeader.GetData(HeaderBuff);

	// read field info
	fseek(m_hDBFile, 0x78, SEEK_SET);
	FieldDescription fieldDes;
	for (int i = 0; i < m_DBHeader.m_cFieldNum; i++)
	{
		fread(&fieldDes, sizeof(char), 2, m_hDBFile);
		//m_vecFieldDes[i] = fieldDes;
	}

	//If the block is empty, the offset is set to 0 minus record length.
	unsigned short usEmptyBlockOffset = unsigned short(0 - m_DBHeader.m_sLength);

	// read block info and records
	DBBlock dbBlock;
	int nStartPos = m_DBHeader.m_sHeaderLength + (m_DBHeader.m_sFirstBlock - 1) * SizeTbl[m_DBHeader.m_cBlockSize];
	fseek(m_hDBFile, nStartPos, SEEK_SET);
	for (int i = 0; i < m_DBHeader.m_sBlockNum; i++)
	{
		fread(&dbBlock, sizeof(char), sizeof(DBBlock), m_hDBFile);
		if (dbBlock.m_sOffsetLastBlock != usEmptyBlockOffset)
		{
			dbBlock.m_sOffsetLastBlock += m_DBHeader.m_sLength;
			/*int nReadNum = dbBlock.m_sOffsetLastBlock/1024;
			char * pBuff = m_cBlockBuff;*/
			fread(m_cBlockBuff, sizeof(char), dbBlock.m_sOffsetLastBlock/*+m_DBHeader.m_sLength*/, m_hDBFile);
			if(!ReadBlock(dbBlock.m_sOffsetLastBlock))
			{
				bRet = false;
				break;
			}
		}

		//m_vecBlockInfo[i] = dbBlock;
		if (dbBlock.m_sNextBlock == 0)
		{
			break;
		}
		nStartPos = m_DBHeader.m_sHeaderLength + (dbBlock.m_sNextBlock - 1) * SizeTbl[m_DBHeader.m_cBlockSize];
		fseek(m_hDBFile, nStartPos, SEEK_SET);
	}
	fclose(m_hDBFile);
	return bRet;
}

template <class T>
bool CDBFile<T>::ReadBlock(int nBuffLength)
{
	bool bRet = true;
	int nRecordNum = nBuffLength/m_DBHeader.m_sLength;
	char * pBuff = m_cBlockBuff;
	for (int i = 0; i < nRecordNum; i++)
	{
		T* pRecord = new T;
		if(!pRecord->ReadRecord(m_cBlockBuff+i*m_DBHeader.m_sLength))
		{
			bRet = false;
			break;
		}
	}
	return bRet;
	return false;
};

