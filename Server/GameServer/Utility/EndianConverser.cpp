#include "StdAfx.h"
#include "EndianConverser.h"

CEndianConverser::CEndianConverser(void)
{
}

CEndianConverser::~CEndianConverser(void)
{
}

char CEndianConverser::ReadChar(const char * Buff)
{
	return Buff[0];
}

BYTE CEndianConverser::ReadBYTE(const char * Buff)
{
	return (BYTE)(Buff[0]);
}

short CEndianConverser::ReadShort(const char *Buff)
{
	short siVal;
	Converse(Buff, sizeof(short), (char*)&siVal);
	return siVal;
}

ui16 CEndianConverser::ReadWORD(const char *Buff)
{
	ui16 wVal;
	Converse(Buff, sizeof(ui16), (char*)&wVal);
	return wVal;
}

long CEndianConverser::ReadLong(const char *Buff)
{
	long lVal;
	Converse(Buff, sizeof(long), (char*)&lVal);
	return lVal;
}

DWORD CEndianConverser::ReadDWORD(const char *Buff)
{
	DWORD dwVal;
	Converse(Buff, sizeof(DWORD), (char*)&dwVal);
	return dwVal;
}

string CEndianConverser::ReadString(const char *Buff, int length)
{
	string strVal = "";
	strVal.append(Buff, strlen(Buff));
	return strVal;
}

void CEndianConverser::ReadString(const char * Buff, int length, char* szStrOut)
{
	memcpy(szStrOut, Buff, length);
}

/*
string CEndianConverser::ReadBlob(const char *Buff, int length)
{
	return "";
}
*/

void CEndianConverser::Converse(const char * bufIn, int length, char* bufOut)
{
	for (int i = 0; i < length; ++i)
	{
		bufOut[i] = bufIn[length-i-1];
	}
	bufOut[length-1] ^= 0x80;
}