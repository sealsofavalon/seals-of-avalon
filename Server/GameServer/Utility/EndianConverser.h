#pragma once
#include "Common.h"
#include <string>

using namespace std;

class CEndianConverser
{
private:
	CEndianConverser(void);
	~CEndianConverser(void);

public:
	static char ReadChar(const char * Buff);
	static BYTE ReadBYTE(const char * Buff);
	static short ReadShort(const char * Buff);
	static ui16 ReadWORD(const char * Buff);
	static long ReadLong(const char * Buff);
	static DWORD ReadDWORD(const char * Buff);
	static string ReadString(const char * Buff, int length);
	static void ReadString(const char * Buff, int length, char* szStrOut);
	//string ReadBlob(const char * Buff, int length);

protected:

	static void Converse(const char * bufIn, int length, char* bufOut);
};
