#pragma once

//=================================================================
//文件头：GlobeDefine.h
//说明：包含了一些程序中会用到的定义
//作者：lbr
//创建日期：2007-11-13
//=================================================================

#define SUB(a,b) ((a)=((a>b) ? (a-b) : 0))
#define DIFVAl(a,b) (((a)>(b)) ? ((a)-(b)) : 0)

#define IS_NULL_POINTER(a) (NULL == (a))
#define OFFSET(z,w) (int)(&(((z*)0)->w))
//
//#define PACKET_START_SYMBOL '#'
//#define PACKET_END_SYMBOL '!'

#define PACKET_HEADER_LEN 12

#define MAX_PACKAGE_LEN	1024*16
#define INIT_FREE_PKG_NUM 1024

#define PLAYER_LEVEL_MAX 50

#define INIT_USER_POOL_SIZE 1024*4
#define INIT_PLAYER_POOL_SIZE 1024*4
#define INIT_MONSTER_POOL_SIZE 1024*4
#define INIT_MAPITEM_POOL_SIZE 1024*4

#define INVAlID_MAPOBJ_IDX 0xFFFFFFFF
//#define PLAYER_ARRAY_LEN 0X00010000
//#define MIN_PLAYER_IDX	0
//#define MAX_PLAYER_IDX	0X0000FFFF
//#define MONSTER_ARRAY_LEN	0X000F0000
//#define MIN_MONSTER_IDX	0X00010000
//#define MAX_MONSTER_IDX	0X000FFFFF
//#define MAPITEM_ARRAY_LEN 0X00100001
//#define MIN_MAPITEM_IDX 0X00100000
//#define MAX_MAPITEM_IDX 0X00200000
#define PLAYER_ARRAY_LEN 10000
#define MIN_PLAYER_IDX	0
#define MAX_PLAYER_IDX	9999
#define MONSTER_ARRAY_LEN	10000
#define MIN_MONSTER_IDX	10000
#define MAX_MONSTER_IDX	19999
#define MAPITEM_ARRAY_LEN 50000
#define MIN_MAPITEM_IDX 20000
#define MAX_MAPITEM_IDX 69999
#define NPC_ARRAY_LEN 1000
#define MIN_NPC_IDX 70000
#define MAX_NPC_IDX 71000
//#define MAPITEM_ARRAY_LEN 0X00F00000
//#define MIN_MAPITEM_IDX 0X00100000
//#define MAX_MAPITEM_IDX 0X00FFFFFF

#define MAX_PLAYER_NUM_ONE_SERVER PLAYER_ARRAY_LEN

#define FLOATDATA_MIN_DIF	0.0001
#define CHAT_MSG_BROADCAST_RANGE  100

#define PLAYER_WALK_MAX_STEP 10	//行走的最长步长
#define PLAYER_RUN_MAX_STEP 15	//跑动的最长步长

#define PLAYER_WALK_INTERVAL 100 //角色行走最小时间间隔

#define PLAYER_RESUMEHPMP_INTERVAL 6000 //HPMP恢复时间间隔6秒

#define MAX_MONSTER_DIRECTION 7

#define INVALID_MONSTER_MAGIC_ID 0x0 //怪物非法技能ID
#define MIN_ATTACK_DISTANCE 1

#define MONSTER_CHANGE_TARGET_INTERVAL 2000 //2秒内是未受到到目标玩家的攻击,此时另一玩家来攻击,则锁定最近攻击的对象
#define MONSTER_IDLE_MOVE_INTERVAL 20000
#define MONSTER_BONE_APPEAR_INTERVAL 60000 //怪物尸骨停留时间


#define ITEM_APPEAR_INTEAVAL 60000 //道具在地图上停留时间
#define ITEM_DROP_LOCK 30000 //道具出现后拾取锁定时间