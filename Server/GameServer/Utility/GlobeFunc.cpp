#include "stdafx.h"
#include "GlobeFunc.h"
#include "GlobeDefine.h"
#include <math.h>

/*八方向图示		 4
		  3			 /\ Y        5
	  		\		 |         /
			  \		 |       /
				\	 |     /
				  \	 |   /
					\| /			  X  \
 2---------------------------------------/  6
					/| \
				  /	 |   \
				/	 |     \
			  /		 |       \
			/		 |         \
		  /			 |           \
		1			 |             7
					 0
*/
const static int cs_iDirToXY[8][2] = {{0,-1},{-1,-1},{-1,0},{-1,1},{0,1},{1,1},{1,0},{1,-1}};
const float SY_PI = 3.1415926535897932f;

/////////////////////////////////////////////////////////////////////////
tagFLMapPoint::tagFLMapPoint(const tagMapPoint& mpSrc)
{
	x = SERVERXYZ_2_CLIENTXYZ(mpSrc.x);
	y = SERVERXYZ_2_CLIENTXYZ(mpSrc.y);
	z = SERVERXYZ_2_CLIENTXYZ(mpSrc.z);
};

//////////////////////////////////////////////////////////////////////////

bool CGameRandom::Init()
{
	srand( (unsigned)time( NULL ) );
	return true;
}

float CGameRandom::GetRandom()
{
	return ((float)rand())/RAND_MAX;
}
int CGameRandom::GetRandom(int nMin, int nMax)
{
	return nMin + rand()%(nMax - nMin + 1);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
CMapRegion::CMapRegion(DWORD dwMinX, DWORD dwMaxX, DWORD dwMinY, DWORD dwMaxY)
:m_dwMinX(dwMinX),m_dwMaxX(dwMaxX),m_dwMinY(dwMinY),m_dwMaxY(dwMaxY)
{

}

CMapRegion::CMapRegion(const MAPPOINT& mpCenter, DWORD dwRange)
{
	m_dwMinX = DIFVAl(mpCenter.x, dwRange);
	m_dwMaxX = mpCenter.x + dwRange;
	m_dwMinY = DIFVAl(mpCenter.y, dwRange);
	m_dwMaxY = mpCenter.y + dwRange;
}

bool CMapRegion::IsEmpty()
{
	if(m_dwMinX == m_dwMaxX && m_dwMinY == m_dwMaxY)
	{//只有一个点
		return true;
	}
	return false;
}


bool CMapRegion::IsCrossWithRegion(const MAPPOINT& mpCenter, DWORD dwRange)
{
	DWORD dwMinX = DIFVAl(mpCenter.x, dwRange);
	DWORD dwMaxX = mpCenter.x + dwRange;
	DWORD dwMinY = DIFVAl(mpCenter.y, dwRange);
	DWORD dwMaxY = mpCenter.y + dwRange;

	return IsCrossWithRegion(dwMinX, dwMaxX, dwMinY, dwMaxY);
}

bool CMapRegion::IsCrossWithRegion(DWORD dwMinX, DWORD dwMaxX, DWORD dwMinY,  DWORD dwMaxY)
{
	if(dwMinX > m_dwMaxX || dwMaxX < m_dwMinX ||
		dwMinY > m_dwMaxY || dwMaxY < m_dwMinY)
	{
		return false;
	}

	return true;
}

bool CMapRegion::IsCrossWithRegion(const CMapRegion& mrRegion)
{
	return IsCrossWithRegion(mrRegion.m_dwMinX, mrRegion.m_dwMaxX, mrRegion.m_dwMinY, mrRegion.m_dwMaxY);
}

bool CMapRegion::IsInRegion(const MAPPOINT& mpPoint)
{
	if(mpPoint.x < m_dwMinX || mpPoint.x > m_dwMaxX ||
		mpPoint.y < m_dwMinY || mpPoint.y > m_dwMaxY)
		return  false;

	return true;
}

CMapRegion CMapRegion::GetCrossRegion(const CMapRegion& mrRegion)
{
	if(!IsCrossWithRegion(mrRegion.m_dwMinX, mrRegion.m_dwMaxX, mrRegion.m_dwMinY, mrRegion.m_dwMaxY))
	{
		return CMapRegion();
	}

	CMapRegion mrReturn;

	mrReturn.m_dwMinX = max(m_dwMinX, mrRegion.m_dwMinX);
	mrReturn.m_dwMaxX = min(m_dwMaxX, mrRegion.m_dwMaxX);
	mrReturn.m_dwMinY = max(m_dwMinY, mrRegion.m_dwMinY);
	mrReturn.m_dwMaxY = min(m_dwMaxY, mrRegion.m_dwMaxY);

	return mrReturn;
}

CMapRegion CMapRegion::GetExpandRegion(const CMapRegion& mrRegion)
{
	CMapRegion mrReturn;

	mrReturn.m_dwMinX = min(m_dwMinX, mrRegion.m_dwMinX);
	mrReturn.m_dwMaxX = max(m_dwMaxX, mrRegion.m_dwMaxX);
	mrReturn.m_dwMinY = min(m_dwMinY, mrRegion.m_dwMinY);
	mrReturn.m_dwMaxY = max(m_dwMaxY, mrRegion.m_dwMaxY);

	return mrReturn;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////

ui16 GetDistance(const MAPPOINT& mpStart, const MAPPOINT& mpEnd)
{
	ui16 wDistance = 0XFFFF;
	ui16 wDistanceX = 0;
	if(mpStart.x >= mpEnd.x)
	{
		wDistanceX = mpStart.x - mpEnd.x;
	}
	else
	{
		wDistanceX = mpEnd.x - mpStart.x;
	}

	ui16 wDistanceY = 0;
	if(mpStart.y >= mpEnd.y)
	{
		wDistanceY = mpStart.y - mpEnd.y;
	}
	else
	{
		wDistanceY = mpEnd.y - mpStart.y;
	}

	wDistance = max(wDistanceX, wDistanceY);
	return wDistance;
}
float GetAngle(const MAPPOINT& mpStart, const MAPPOINT& mpEnd)
{
	float fAngle = -M_PI;
	int wDistanceX = mpEnd.x - mpStart.x;
	int wDistanceY = mpEnd.y - mpStart.y;
	if (wDistanceY == 0)
	{
		if (wDistanceX >= 0)
		{
			fAngle = M_PI_2 * 3;
		}
		else
		{
			fAngle = M_PI_2;
		}
	}
	else
	{
		float dTan = ((float)wDistanceX)/wDistanceY;
		fAngle = atan(dTan);
		if (wDistanceY > 0)			//X轴上方
		{
			fAngle = M_PI + fAngle;
		}
		else
		{
			if (wDistanceX > 0)		//Y轴右方
			{
				fAngle = M_PI * 2 + fAngle;
			}
		}
	}
	return fAngle;
}

MAPPOINT  GetPoint(const MAPPOINT& mpStart, BYTE byDirection, ui16 wDistance)
{
	MAPPOINT mpTarget;
	int iX = ((int)mpStart.x) + cs_iDirToXY[byDirection][0]*wDistance;
	int iY = ((int)mpStart.y) + cs_iDirToXY[byDirection][1]*wDistance;
	mpTarget.x = max(0, iX);
	mpTarget.y = max(0, iY);
	return mpTarget;
}

/*八方向图示		 4
		  3			 /\ Y        5
	  		\		 |         /
			  \		 |       /
				\	 |     /
				  \	 |   /
					\| /			  X  \
 2---------------------------------------/  6
					/| \
				  /	 |   \
				/	 |     \
			  /		 |       \
			/		 |         \
		  /			 |           \
		1			 |             7
					 0
*/
float ServerDir2ClientDir(BYTE byDirection)
{
	float flDirection = byDirection*SY_PI/4;
	return flDirection;
}

//返回八方向方向
BYTE GetDirection(const MAPPOINT& mpStart, const MAPPOINT& mpEnd)
{
	BYTE byRet = 0;
	int iXD = ((int)(mpEnd.x)) - mpStart.x;
	int iYD = ((int)(mpEnd.y)) - mpStart.y;
	
	int iXLen = iXD*iXD;
	int iYLen = iYD*iYD;
	int iSideLen = iXLen + iYLen;	

	if(iXD == 0)
	{
		if(iYD > 0)
		{
			byRet = 4;
		}
		else if(iYD < 0)
		{
			byRet = 0;
		}
	}
	else if(iYD == 0)
	{
		if(iXD > 0)
		{
			byRet = 6;
		}
		else if(iXD < 0)
		{
			byRet = 2;
		}
	}
	else if(iXD > 0)
	{
		if(iYD > 0)
		{
			if(iXLen*4 < iSideLen)
			{
				byRet = 4;
			}
			else if(iYLen*4 < iSideLen)
			{
				byRet = 6;
			}
			else
			{
				byRet = 5;
			}
		}
		else
		{
			if(iXLen*4 < iSideLen)
			{
				byRet = 0;
			}
			else if(iYLen*4 < iSideLen)
			{
				byRet = 6;
			}
			else
			{
				byRet = 7;
			}

		}
	}
	else
	{
		if(iYD > 0)
		{
			if(iXLen*4 < iSideLen)
			{
				byRet = 4;
			}
			else if(iYLen*4 < iSideLen)
			{
				byRet = 2;
			}
			else
			{
				byRet = 3;
			}
		}
		else
		{
			if(iXLen*4 < iSideLen)
			{
				byRet = 0;
			}
			else if(iYLen*4 < iSideLen)
			{
				byRet = 2;
			}
			else
			{
				byRet = 1;
			}

		}

	}
	return byRet;
}