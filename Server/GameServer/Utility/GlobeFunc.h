#pragma once
#include "Common.h"
#define CLIENTXYZ_2_SERVERXYZ(a) (((ui16)(a))<<2)
#define SERVERXYZ_2_CLIENTXYZ(a) (((float)(a))/4)

class CGameRandom
{
public:
	CGameRandom(){}
	~CGameRandom(){}
	static bool Init();
	static float GetRandom();							//返回0~1的浮点数
	static int GetRandom(int nMin, int nMax);			//返回[nMin,nMax]间的整数
protected:
private:
};

struct tagMapPoint;

typedef struct tagFLMapPoint
{
	float x;
	float y;
	float z;
	tagFLMapPoint(float flX = 0, float flY = 0, float flZ = 0):x(flX),y(flY),z(flZ){};
	tagFLMapPoint(const tagMapPoint& mpSrc);
} FLMAPPOINT, * PFLMAPPOINT;

typedef struct tagMapPoint
{
	ui16 x;
	ui16 y;
	ui16 z;
	tagMapPoint(ui16 wX = 0, ui16 wY = 0, ui16 wZ = 0):x(wX),y(wY),z(wZ){};
	tagMapPoint(const FLMAPPOINT& flmpSrc)
	{
		x = CLIENTXYZ_2_SERVERXYZ(flmpSrc.x);
		y = CLIENTXYZ_2_SERVERXYZ(flmpSrc.y);
		z = CLIENTXYZ_2_SERVERXYZ(flmpSrc.z);
	};
	bool IsValid()
	{
		if(x == 0 && y == 0 && z == 0)
		{
			return false;
		}
		return true;
	}

	bool operator == (const tagMapPoint& mpPoint)const 
	{
		return (x == mpPoint.x && y == mpPoint.y && z == mpPoint.z);
	}
} MAPPOINT, * PMAPPOINT;


typedef class CMapRegion
{
public:
	DWORD m_dwMinX;
	DWORD m_dwMaxX;
	DWORD m_dwMinY;
	DWORD m_dwMaxY;
	CMapRegion(DWORD dwMinX = 0, DWORD dwMaxX = 0, DWORD dwMinY = 0,  DWORD dwMaxY = 0);
	CMapRegion(const MAPPOINT& mpCenter, DWORD dwRange);
	
	bool IsEmpty();	
	bool IsCrossWithRegion(const MAPPOINT& mpCenter, DWORD dwRange);
	bool IsCrossWithRegion(DWORD dwMinX, DWORD dwMaxX, DWORD dwMinY,  DWORD dwMaxY);
	bool IsCrossWithRegion(const CMapRegion& mrRegion);
	bool IsInRegion(const MAPPOINT& mpPoint);

	CMapRegion GetCrossRegion(const CMapRegion& mrRegion);
	CMapRegion GetExpandRegion(const CMapRegion& mrRegion);
} MAPREGION, * PMAPREGION;

ui16 GetDistance(const MAPPOINT& mpStart, const MAPPOINT& mpEnd);
float GetAngle(const MAPPOINT& mpStart, const MAPPOINT& mpEnd);
MAPPOINT  GetPoint(const MAPPOINT& mpStart, BYTE byDirection, ui16 wDistance);
float	  ServerDir2ClientDir(BYTE byDirection);
BYTE	  GetDirection(const MAPPOINT& mpStart, const MAPPOINT& mpEnd);//返回八方向方向

MAPREGION GetMapRegion();
