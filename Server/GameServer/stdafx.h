#ifndef GAME_SERVER_STDAFX_HEAD
#define GAME_SERVER_STDAFX_HEAD

#define _ACTIVITY_PROTOCOL

#include "Utility/GlobeDefine.h"
#include "../../../SDBase/Public/TypeDef.h"
#include "../../../SDBase/Public/DBCStores.h"
#include "../../../SDBase/Public/SkillDef.h"
#include "../../../SDBase/Public/GossipDef.h"
#include "../../../SDBase/Public/QuestDef.h"
#include "../../../SDBase/Public/CreatureDef.h"
#include "../../../SDBase/Public/UnitDef.h"
#include "../../../SDBase/Public/PlayerDef.h"
#include "../../../SDBase/Public/MailDef.h"
#include "../../../SDBase/Public/SpellDef.h"
#include "../../../SDBase/Public/MapDef.h"
#include "../../../SDBase/Public/itemprototype.h"
#include "../../../SDBase/Public/Timer.h"
#include "../../../SDBase/Public/UpdateMask.h"
#include "../../../SDBase/Public/UpdateFields.h"
#include "../../../SDBase/Public/ByteBuffer.h"
//#include "../../../SDBase/Protocol/Opcodes.h"
#include "../../../SDBase/Protocol/GS2GS.h"

#include "../../Common/share/Config/Config.h"
//#include "../../Common/share/AuthCodes.h"
#include "../../Common/share/svn_revision.h"
#include "../../Common/share/Auth/MD5.h"
#include "../../Common/share/zlib/zlib.h"
#include "../../Common/share/crc32.h"
#include "../../Common/share/Storage.h"
#include "../../Common/share/LocationVector.h"
#include "../../Common/share/CallBack.h"

#include <stdio.h>
#include <stdlib.h>
#include "../../Common/Com/Utility.h"

#include "../../Common/share/Common.h"
#include "../../Common/share/Threading/Mutex.h"
#include "../../Common/share/MersenneTwister.h"

#include "Database/DatabaseEnv.h"
extern time_t UNIXTIME;		/* update this every loop to avoid the time() syscall! */
extern tm g_localTime;
#define COLLISION
#include "Game/CollideInterface.h"
//#include "WoWGuid.h"
#include "Game/AreaTrigger.h"
#include "Game/ServerDef.h"
#include "Game/ObjectStorage.h"

#include "Game/MapDef.h"
#include "Game/ServerDef.h"

#include "Game/EventMgr.h"
#include "Game/EventableObject.h"
#include "Game/Object.h"
#include "Game/Item.h"
#include "Game/Container.h"
#include "Game/ItemInterface.h"
#include "Game/Unit.h"

//#include "Game/battlegroundmgr.h"
#include "Game/MailSystem.h"
#include "Game/Group.h"
#include "Game/CellHandler.h"
#include "Game/WorldSession.h"
#include "Game/Map.h"

#include "Game/MapMgr.h"

#include "Game/Quest.h"
#include "Game/Player.h"
#include "Game/Creature.h"
#include "Game/Pet.h"
#include "Game/GameObject.h"
//#include "Game/DynamicObject.h"
#include "Game/AuctionHouse.h"
#include "Game/Corpse.h"
#include "Game/World.h"
#include "Game/MapScriptInterface.h"
#include "Game/Stats.h"

#include "Game/ObjectMgr.h"
//#include "Game/TransporterHandler.h"
//#include "Game/LfgMgr.h"
#include "Game/LootMgr.h"
#include "Game/ScriptMgr.h"
//#include "Game/TaxiMgr.h"
//#include "Game/QuestMgr.h"
//#include "Game/WeatherMgr.h"
#include "Game/ChannelMgr.h"
#include "Game/AuctionMgr.h"
#include "Game/AnimalMgr.h"

#include "Game/ObjectStorage.h"
#include "Game/WorldCreator.h"
#include "Game/SpellNameHashes.h"
#include "Game/SpellDef.h"
#include "Game/Spell.h"
#include "Game/SpellAuras.h"
#include "Game/faction.h"
#include "Game/HonorHandler.h"
#include "Game/NPCHandler.h"
#include "Game/SkillNameMgr.h"

#include "Game/WordFilter.h"
#include "Game/NameTables.h"
#include "Game/mapcell.h"
#include "Game/TransporterHandler.h"
//#include "Game/Arenas.h"
//#include "Game/ArenaTeam.h"

//#include "Game/BaseConsole.h"
//#include "Game/ConsoleCommands.h"
#include "Game/Chat.h"
#include "Game/Channel.h"
#include "Game/Guild.h"

#include "Game/AIInterface.h"
#include "Game/AIInterfaceManager.h"

#include "Game/activity.h"
#include "Master.h"
#include "Game/CrossSrvMgr.h"
#include "Game/DynamicObject.h"

#endif // GAME_SERVER_STDAFX_HEAD
