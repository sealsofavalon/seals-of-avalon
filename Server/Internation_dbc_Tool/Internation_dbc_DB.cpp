#include "StdAfx.h"
#include "Utils/AreaDB.h"
#include "Utils/AreaTriggerDB.h"
#include "Utils/CastleNpcListDB.h"
#include "Utils/CPlayerTitleDB.h"
#include "Utils/CreatureName.h"
#include "Utils/GameObjectDB.h"
#include "Utils/InstanceMsg.h"
#include "Utils/ItemDB.h"
#include "Utils/LevelTipsDB.h"
#include "Utils/MapInfoDB.h"
#include "Utils/NewBirdDB.h"
#include "Utils/NewBirdEventDB.h"
#include "Utils/NpcTextDB.h"
#include "Utils/QuestDB.h"
#include "Utils/SpellDB.h"
#include "Utils/ClientNoticeText.h"
#include "Utils/ClientFlagNotice.h"
#include "Utils/ItemEnchant.h"
#include "Singleton.h"
#include "../../../new_common/Source/new_common.h"
#include "../../..//new_common/Source/ssl/crc32.h"
#include "../Common/share/Database/Database.h"
#include "../Common/share/Database/MySQLDatabase.h"
#include "../Common/share/mysql/mysql.h"
#include "../Common/share/Common.h"


Database* g_World_db = NULL;
International_DBC* g_pkInternational_dbc = NULL;

std::string DBAnisToUTF8(const std::string& str)
{
	char acUnicode[4096];
	char acUTF8[4096];

	//Ansi 转换为 Unicode
	ui16 usLen = (unsigned short)MultiByteToWideChar(936, 0, str.c_str(), str.size(), (LPWSTR)acUnicode, sizeof(acUnicode)/2);

	//Unicode 转换为 Utf8
	usLen = (unsigned short)WideCharToMultiByte(CP_UTF8, 0, (LPCWSTR)acUnicode, usLen, acUTF8, sizeof(acUTF8), NULL, false);

	//return std::string(acUTF8, usLen);
	////字符串中间可能存在'\0';
	//kString.resize(usLen);
	//kString.replace(0, usLen, acUTF8, usLen);
	return std::string(acUTF8, usLen);
}

std::string DBUTF8ToAnis(const std::string& str)
{
	char acUnicode[4096];
	char acAnsi[2048];

	unsigned short usLen;

	//UTF8 转换为 Unicode
	usLen = (unsigned short)MultiByteToWideChar(CP_UTF8, 0, str.c_str(), str.size(), (LPWSTR)acUnicode, sizeof(acUnicode)/2);

	//Unicode 转换为 Ansi
	usLen = (unsigned short)WideCharToMultiByte(936, 0, (LPCWSTR)acUnicode, usLen, acAnsi, sizeof(acAnsi), NULL, false);

	return std::string(acAnsi, usLen);
}

International_DBC::International_DBC()
{

}
void International_DBC::ShutDownSystem()
{
	
	if (g_pkClientFlagNotice)
	{
		g_pkClientFlagNotice->Unload();
		delete g_pkClientFlagNotice;
	}
	if (g_pkClientNoticetext)
	{
		g_pkClientNoticetext->UnLoadClientText();
		delete g_pkClientNoticetext;
	}
	
	if (g_pkAreaDB)
	{
		g_pkAreaDB->Unload();
		delete g_pkAreaDB;
	}
	if (g_pkAreaTriggerDB)
	{
		g_pkAreaTriggerDB->Unload();
		delete g_pkAreaTriggerDB;
	}
	if (g_pkCastleNpcListDB)
	{
		g_pkCastleNpcListDB->Unload();
		delete g_pkCastleNpcListDB;
	}
	if (g_pkPlayerTitle)
	{
		g_pkPlayerTitle->Unload();
		delete g_pkPlayerTitle ;
	}
	if (g_pkPlaeryTitleReward)
	{
		g_pkPlaeryTitleReward->Unload();
		delete g_pkPlaeryTitleReward; 
	}
	 
	if (g_pkCreatureName)
	{
		g_pkCreatureName->Unload();
		delete g_pkCreatureName; 
	}
	if (g_pkGameObjectDB)
	{
		g_pkGameObjectDB->Unload();
		delete g_pkGameObjectDB;
	}

	if (g_pkInstanceMsgDB)
	{
		g_pkInstanceMsgDB->Unload();
		delete g_pkInstanceMsgDB;
	}
	if (g_pkItemDB)
	{
		g_pkItemDB->Unload();
		delete g_pkItemDB;
	}

	if (g_pkLevelTipsDB)
	{
		g_pkLevelTipsDB->Unload();
		delete g_pkLevelTipsDB;
	}

	if (g_pkMapInfoDB)
	{
		g_pkMapInfoDB->Unload();
		delete g_pkMapInfoDB;
	}

	if (g_pkNewBirdCategroyDB)
	{
		g_pkNewBirdCategroyDB->Unload();
		delete g_pkNewBirdCategroyDB;
	}

	
	if (g_pkNewBirdEventDB)
	{
		g_pkNewBirdEventDB->Unload();
		delete g_pkNewBirdEventDB;
	}

	if (g_pkNpcTex)
	{
		g_pkNpcTex->Unload();
		delete g_pkNpcTex;
	}

	if (g_pkQusetInfo)
	{
		g_pkQusetInfo->Unload();
		delete g_pkQusetInfo;
	}

	if (g_pkSpellInfo)
	{
		g_pkSpellInfo->Unload();
		delete g_pkSpellInfo;
	}

	g_World_db->Shutdown();
	delete g_World_db;
	MyLog::Release();
}
BOOL International_DBC::InitSystem()
{
	FILE*fp = fopen("International_dbc.conf", "r");
	if (!fp)
	{
		return FALSE ;
	}
	char key[32];
	char value[32];
	std::string  ip, username, password, dataname;
	unsigned int port ;

	fscanf( fp, "%s %s\n", key, value );
	ip = value;
	fscanf( fp, "%s %s\n", key, value );
	dataname = value;
	fscanf( fp, "%s %s\n", key, value );
	username = value;
	fscanf( fp, "%s %s\n", key, value );
	password = value;
	fscanf( fp, "%s %u\n", key, &port );
	fclose( fp );

	MyLog::Init();
	unsigned int mysqlthreadsafe = mysql_thread_safe();
	if( !mysqlthreadsafe )
	{
		MyLog::log->error( "mysql lib is not a thread safe mode!!!!!!!!!" );
		return FALSE;
	}
	Database::StartThread();
	g_World_db = new MySQLDatabase();

	//连接数据库 
	printf("connect database ip:%s\n", ip.c_str());
	if (!g_World_db->Initialize(ip.c_str(), port, username.c_str(), password.c_str(), dataname.c_str(), 5, 16384))
	{
		printf("connect failed\n");
		return FALSE ;
	}
	printf("connect success\n");

	printf("begin loading db \n");
	if (!LoadDbFile())
	{
		printf("load failed\n");
		return FALSE ;
	}

	printf("begin read data \n");
	if (!LoadInternationalData())
	{
		return FALSE ;
	}

	return TRUE ;
}


BOOL International_DBC::LoadDbFile()
{
	g_pkClientFlagNotice = new ClientFlagNotice;
	if (!g_pkClientFlagNotice->Load("DBFiles\\ClientFlagNotice.txt"))
	{
		return FALSE ;
	}
	g_pkClientNoticetext = new ClientNotice;
	if (!g_pkClientNoticetext->LoadClientText("DBFiles\\ClientNotice.txt"))
	{
		return FALSE ;
	}
	g_pkAreaDB = new CAreaDB ;
	if (!g_pkAreaDB->Load("DBFiles\\area.db"))
	{
		return FALSE ;
	}
	g_pkAreaTriggerDB = new CAreaTriggerDB;
	if (!g_pkAreaTriggerDB->Load("DBFiles\\areatriggers.db"))
	{
		return FALSE ;
	}

	g_pkCastleNpcListDB = new  CCastleNpcListDB;
	if (!g_pkCastleNpcListDB->Load("DBFiles\\castle_npc_list.db"))
	{
		return FALSE ;
	}
	g_pkPlayerTitle = new TitleDB;
	if (!g_pkPlayerTitle->Load("DBFiles\\title.db"))
	{
		return FALSE ;
	}

	g_pkPlaeryTitleReward = new TiTleRewardDB;
	if (!g_pkPlaeryTitleReward->Load("DBFiles\\title_reward.DB"))
	{
		return FALSE ;
	}

	g_pkCreatureName = new CCreatureNameDB;
	if (!g_pkCreatureName->Load("DBFiles\\creature_names.db"))
	{
		return FALSE ;
	}

	g_pkGameObjectDB = new CGameObjectDB;
	if (!g_pkGameObjectDB->Load("DBFiles\\gameobject_names.DB"))
	{
		return FALSE ;
	}
	

	g_pkInstanceMsgDB = new CInstanceMsg;
	if (!g_pkInstanceMsgDB->Load("DBFiles\\instancemsg.db"))
	{
		return FALSE ;
	}

	g_pkItemEnchantDB = new CItemEnchantDB;
	if (!g_pkItemEnchantDB->Load("DBFiles\\enchantentry_dbc.DB"))
	{
		return FALSE ;
	}

	g_pkItemDB = new CItemDB();
	if (!g_pkItemDB->Load("DBFiles\\Item_template.DB"))
	{
		return FALSE ;
	}

	g_pkLevelTipsDB = new CLevelTipsDB;
	if (!g_pkLevelTipsDB->Load("DBFiles\\questtips.DB"))
	{
		return FALSE ;
	}

	g_pkMapInfoDB = new CMapInfoDB;
	if (!g_pkMapInfoDB->Load("DBFiles\\worldmap_info.db"))
	{
		return FALSE ;
	}

	g_pkNewBirdCategroyDB = new CNewBirdCategroyDB;
	if (!g_pkNewBirdCategroyDB->Load("DBFiles\\newbirdcategory.db"))
	{
		return FALSE ;
	}

	g_pkNewBirdEventDB = new CNewBirdEventDB;
	if (!g_pkNewBirdEventDB->Load("DBFiles\\newbirdevent.db"))
	{
		return FALSE ;
	}

	g_pkNpcTex = new CNpcTextDB;
	if (!g_pkNpcTex->Load("DBFiles\\NpcText.db"))
	{
		return FALSE ;
	}

	g_pkQusetInfo = new CQuestDB;
	if (!g_pkQusetInfo->Load("DBFiles\\quests.db"))
	{
		return FALSE ;
	}

	g_pkSpellInfo = new CSpellDB;
	if (!g_pkSpellInfo->Load("DBFiles\\spell.db"))
	{
		return FALSE ;
	}

	return TRUE ;
}
BOOL International_DBC::LoadInternationalData()
{
	QueryResult* qr = g_World_db->Query( "select * from international_db_text" );
	if (qr)
	{
		do 
		{
			Field* f = qr->Fetch();
			CRCData* pkcrcData = new CRCData;
			pkcrcData->crc = f[0].GetUInt32();
			pkcrcData->ch_str = (f[1].GetString());
			for (int i = L_START ; i < L_MAX; i++)
			{
				pkcrcData->en_str[i] = (f[2+i].GetString());
			}

			if (pkcrcData->ch_str.find("<font:黑体") != string::npos)
			{
				MyLog::log->error("find font tag crc:%d", pkcrcData->crc);
			}
			
			pkcrcData->bUse = false ;
			m_CrcDataMap.insert(std::map<ui32 , CRCData*>::value_type(pkcrcData->crc, pkcrcData));

		} while (qr->NextRow());

		printf("load crc table success....\n");
		delete qr;
		//return TRUE;
	}
	
	QueryResult* qra = g_World_db->Query("select * from international_db_flag_text" );
	if (qra)
	{
		do 
		{
			Field* f = qra->Fetch();
			CRCData* pkcrcData = new CRCData;
			pkcrcData->crc = f[0].GetUInt32();
			
			pkcrcData->ch_str = (f[1].GetString());
			for (int i = L_START ; i < L_MAX; i++)
			{
				pkcrcData->en_str[i] = (f[2+i].GetString());
			}
			
			pkcrcData->bUse = false ;
			m_FlagDataMap.insert(std::map<ui32 , CRCData*>::value_type(pkcrcData->crc, pkcrcData));
		} while (qra->NextRow());

		delete qra ;
	}
	return TRUE ;

}
BOOL International_DBC::DoInternational()
{
	g_pkClientNoticetext->DoInternational();
	g_pkAreaDB->DoInternational();
	g_pkAreaTriggerDB->DoInternational();
	g_pkCastleNpcListDB->DoInternational();
	g_pkPlayerTitle->DoInternational();
	g_pkPlaeryTitleReward->DoInternational();
	g_pkCreatureName->DoInternational();
	g_pkGameObjectDB->DoInternational();
	g_pkInstanceMsgDB->DoInternational();
	g_pkItemDB->DoInternational();
	g_pkLevelTipsDB->DoInternational();
	g_pkMapInfoDB->DoInternational();
	g_pkNewBirdCategroyDB->DoInternational();
	g_pkNewBirdEventDB->DoInternational();
	g_pkNpcTex->DoInternational();
	g_pkQusetInfo->DoInternational();
	g_pkSpellInfo->DoInternational();
	g_pkItemEnchantDB->DoInternational();
	g_pkClientFlagNotice->DoInternationalFlagdb();

	return TRUE ;
}
CRCData* International_DBC::FindCRCData(ui32 crc)
{
	std::map<ui32, CRCData*>::iterator it = m_CrcDataMap.find(crc);
	if (it != m_CrcDataMap.end())
	{
		it->second->bUse = true;
		return  it->second;
	}
	std::map<ui32, CRCData*>::iterator itSave = m_NeedSaveDataMap.find(crc);
	if (itSave != m_NeedSaveDataMap.end())
	{
		return itSave->second ;
	}
	return NULL ;
}
BOOL International_DBC::NeedSave()
{
	return (m_NeedSaveDataMap.size() > 0 ||  m_NeedSaveFlagDataMap.size() > 0);
}
CRCData* International_DBC::FindFlagData(ui32 index)
{
	std::map<ui32, CRCData*>::iterator it  = m_FlagDataMap.find(index);
	if (it != m_FlagDataMap.end())
	{
		it->second->bUse = true ;
		return it->second;
	}
	std::map<ui32, CRCData*>::iterator itSave = m_NeedSaveFlagDataMap.find(index);
	if (itSave != m_NeedSaveFlagDataMap.end())
	{
		return itSave->second ;
	}
	
	return NULL ;
}
void International_DBC::AddFlagData(ui32 index, std::string str)
{
	if (str.length() == 0)
	{
		return ;
	}

	if (!FindFlagData(index))
	{
		CRCData* pkData = new CRCData;
		pkData->crc = index;
		pkData->ch_str = str;
		pkData->en_str[L_EN] = "";
		pkData->bUse = true;

		m_NeedSaveFlagDataMap.insert(std::map<ui32, CRCData*>::value_type(index, pkData));
	}
}
void International_DBC::AddCRCData(const char* str)
{
	ui32 len = strlen(str);
	if (len== 0)
	{
		return ;
	}
	

	ui32 crc = ssl::__crc32( (const unsigned char*)str,len);
	if (_stricmp("落虹平原", str) == 0)
	{
		int i = 0;
	}
	if (!FindCRCData(crc))
	{
		CRCData* pkData = new CRCData;
		pkData->crc = crc;
		pkData->ch_str = str;
		for (int i = L_START; i < L_MAX; i++)
		{
			pkData->en_str[i] = "";
		}
		pkData->bUse = true;
		m_NeedSaveDataMap.insert(std::map<ui32, CRCData*>::value_type(crc, pkData));
	}
}
BOOL International_DBC::SaveInternationalDate() //
{
	FILE* fp = fopen("log.txt", "w+");
	
	std::string strCLO ;
	std::string strData;
	for (int i = L_START; i < L_MAX; i++)
	{
		strCLO += DataName[i];
		if ( i != L_MAX - 1)
		{
			strCLO += ",";
		}
	}


	std::map<ui32, CRCData*>::iterator it = m_NeedSaveDataMap.begin();
	{
		scoped_sql_transaction_proc sstp( g_World_db );
		for (it; it != m_NeedSaveDataMap.end(); ++it)
		{
			if (it->second)
			{
				strData.clear();
				for (int i = L_START; i < L_MAX; i++)
				{
					strData += "'" + it->second->en_str[i]+ "' ";
					if (i != L_MAX - 1)
					{
						strData += ",";
					}
				}
					
				g_World_db->TransactionExecute( "replace into international_db_text (ch_crc, ch_text, %s) values(%u, '%s', %s)",strCLO.c_str() ,it->second->crc, (it->second->ch_str).c_str(), strData.c_str() );		
				m_CrcDataMap.insert(std::map<ui32, CRCData*>::value_type(it->second->crc, it->second));
			}
		}	
		
		std::map<ui32, CRCData*>::iterator it1 = m_NeedSaveFlagDataMap.begin() ;
		for (it1; it1 != m_NeedSaveFlagDataMap.end(); ++it1)
		{
			if (it1->second)
			{
				strData.clear();
				for (int i = L_START; i < L_MAX; i++)
				{
					strData += "'" + it1->second->en_str[i]+ "'";
					if (i != L_MAX - 1)
					{
						strData += ",";
					}
				}

				g_World_db->TransactionExecute( "replace into international_db_flag_text (Entry, ch_text, %s) values(%u, '%s', %s)",strCLO.c_str() ,it1->second->crc, (it1->second->ch_str).c_str(), strData.c_str() );		
			
				m_FlagDataMap.insert(std::map<ui32, CRCData*>::value_type(it1->second->crc, it1->second));
			}
		}	

		sstp.success();
		fclose(fp);
	}

	
	printf("save success\n");
	return TRUE;
}

void International_DBC::ShowLanguage()
{
	for ( int i = L_START; i < L_MAX; i++)
	{
		printf("language flag  char[%d]: %s \n", i, language[i]);
	}
}

#include <iostream>
#include <fstream>
BOOL International_DBC::FindLange(const char* languagestr, int& lg)
{
	int i = L_START ;
	for ( i; i < L_MAX; i++)
	{
		if (_stricmp(languagestr, language[i]) == 0)
		{
			lg = i ;
			return TRUE ;
		}
	}

	return FALSE ;
}
BOOL International_DBC::LoadLanguageFile(const char* pszName, int lg, int stype)
{
	if (stype != 0 && stype != 1)
	{
		return FALSE ;
	}
	// loading
	ifstream file ;
	file.open(pszName);
	string line ;
	string txt ;
	ui32 index ;
	printf("Load  file ...\n");
	ui32 count = 0 ;
	while(getline(file, line))
	{
		if (line.length() > 0)
		{
			size_t startpos = line.find_first_of(" ");
			if (startpos != string::npos)
			{
				index = strtoul(line.substr(0, startpos).c_str(), NULL, 10);
				txt = line.substr(startpos + 1, line.length() - 1);
				if (stype == 0)
				{
					m_TranslateMap[lg].insert(make_pair(index, txt));
				}else
				{
					m_TranslateFlagMap[lg].insert(make_pair(index, txt));
				}

				count++;
			}	
		}
	}
	file.close();
	printf("Add.Language data %u\n", count);

	return TRUE ;
}

BOOL International_DBC::SaveTranslateData(int lg, int itype)
{
	
	if (lg < L_MAX && lg >= L_START)
	{
		std::map<ui32, std::string> savemap  ;

		if (itype == 0)
		{
			savemap = m_TranslateMap[lg];
		}else if (itype == 1)
		{
			savemap = m_TranslateFlagMap[lg];
		}

		std::map<ui32, std::string>::iterator it = savemap.begin() ;
		scoped_sql_transaction_proc sstp( g_World_db );
		for (it; it != savemap.end(); ++it)
		{
			if (it->second.length())
			{
				if (itype == 0)
				{
					g_World_db->TransactionExecute( "Update  international_db_text SET %s = '%s' WHERE ch_crc = %u", DataName[lg], it->second.c_str(),it->first);	
				}else
				{
					g_World_db->TransactionExecute( "Update  international_db_flag_text SET %s = '%s' WHERE Entry = %u", DataName[lg], it->second.c_str(),it->first);	
				}	
			}
		}	
		sstp.success();
		printf("save success\n");
	}
	
	return TRUE ;
}
BOOL International_DBC::ClearTranslateData(int lg, int itype)
{
	if (lg < L_MAX &&  lg >= L_START)
	{
		if (itype == 0)
		{
			m_TranslateMap[lg].clear();
		}else if (itype == 1)
		{
			m_TranslateFlagMap[lg].clear();
		}
	}


	return TRUE ;
}