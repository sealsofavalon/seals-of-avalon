#ifndef __INTERNATION_DBC_H__
#define __INTERNATION_DBC_H__

enum
{
	L_START = 0 ,
	L_EN = L_START,

	L_MAX,
};
static const char* language[] = {"en",};
static const char* DataName[] = {"en_text",};

struct CRCData  
{
	ui32 crc ;
	std::string ch_str;
	std::string en_str[L_MAX];

	bool bUse  ;  // is used 
};

class International_DBC
{
public:
	International_DBC();
	//Init
	BOOL InitSystem();

	//shut
	void ShutDownSystem();
	//load data
	BOOL LoadDbFile();
	BOOL LoadInternationalData();
	
	

	void ShowLanguage();
	BOOL FindLange(const char* languagestr, int& lg);
	BOOL LoadLanguageFile(const char* pszName, int lg ,int stype);
	// do
	BOOL DoInternational();
	//find data
	void AddCRCData(const char* str); 
	void AddFlagData(ui32 index, std::string str);
	BOOL NeedSave();
	CRCData* FindCRCData(ui32 crc);
	CRCData* FindFlagData(ui32 index);
	
	//save
	BOOL SaveInternationalDate(); //
	BOOL SaveTranslateData(int lg, int itype);
	BOOL ClearTranslateData(int lg, int itype);
protected:
	std::map<ui32, CRCData*> m_CrcDataMap;
	std::map<ui32, CRCData*> m_NeedSaveDataMap;
	std::map<ui32, CRCData*> m_FlagDataMap;
	std::map<ui32, CRCData*> m_NeedSaveFlagDataMap;

	std::map<ui32, std::string> m_TranslateMap[L_MAX] ;
	std::map<ui32, std::string> m_TranslateFlagMap[L_MAX] ;
private:
};

extern International_DBC* g_pkInternational_dbc;
#endif