#include "stdafx.h"


static BOOL begin = TRUE;

int doAsk()
{
	HANDLE stdOut = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(stdOut, FOREGROUND_RED | FOREGROUND_INTENSITY  );

	printf("\n\n\nCommand:Input 0 load Translation file\nCommand:Input 1 DoInternational\n");
	std::string input;
	std::cin>> input ;

	if (_stricmp(input.c_str(), "1") == 0)
	{
		return 1;
	} 
	if(_stricmp(input.c_str(), "0") == 0)
	{
		return 0;
	}

	return 2 ;
}
BOOL doAskTranslate()
{
	HANDLE stdOut = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(stdOut, FOREGROUND_GREEN | FOREGROUND_INTENSITY  );
	
	printf("Input create command:file language type \n\nExample:notice.txt zh 1 \n\n");
	g_pkInternational_dbc->ShowLanguage();
	printf("\nType value: 0 - notice text, 1 - flag notice text \n");
	std::string file, Language, type; 
	std::cin>> file >> Language >> type;
	
	int iType = atoi(type.c_str()) ;
	if (iType != 0 && iType != 1)
	{
		SetConsoleTextAttribute(stdOut, FOREGROUND_RED | FOREGROUND_INTENSITY  | COMMON_LVB_UNDERSCORE);
		printf("\nerror type ,type value: 0 - notice text, 1 - flag notice text \n");
		doAskTranslate() ;
	}

	int lg ;
	if (!g_pkInternational_dbc->FindLange(Language.c_str(), lg))
	{
		printf("\nerror language");
		doAskTranslate() ;
		return FALSE ;
		
	}
	if (!g_pkInternational_dbc->LoadLanguageFile(file.c_str(), lg, iType))
	{
		doAskTranslate() ;
	}
	
	printf("\ndo you save the translate data . language : %s  Y/N \n", Language.c_str());
	std::string input ;
	std::cin >> input;

	if (_stricmp(input.c_str(), "Y") == 0)
	{
		g_pkInternational_dbc->SaveTranslateData(lg, iType);
	}else
	{
		g_pkInternational_dbc->ClearTranslateData(lg, iType);
	}

	return true ;
}
BOOL doInternational()
{
	HANDLE stdOut = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(stdOut, FOREGROUND_GREEN | FOREGROUND_INTENSITY  );

	if(begin)
	{
		printf("Begin insert new date \n");
		if (g_pkInternational_dbc->DoInternational())
		{
			if (g_pkInternational_dbc->NeedSave())
			{
				printf("save .....\n");
				g_pkInternational_dbc->SaveInternationalDate();
				
			}else
			{
				g_pkInternational_dbc->ShutDownSystem();
				printf("DB has no new data\n");
			}

		}else
		{	
			g_pkInternational_dbc->ShutDownSystem();
			printf("failed");
		}

		begin = FALSE;
	}else
	{
		printf("DB has no new data\n");
	}

	return TRUE;
}
int main(int argc, char* argv[])
{
	if (g_pkInternational_dbc == NULL)
	{
		g_pkInternational_dbc = new International_DBC;
		if (!g_pkInternational_dbc->InitSystem())
		{
			printf("Init failed\n");
			//g_pkInternational_dbc->ShutDownSystem();
			return 1;
		}

		printf("init success\n");
	}
	
	while(1)
	{
		int  Ask = doAsk();
		if (Ask == 0)
		{
			doAskTranslate();

		}else if (Ask == 1)
		{
			doInternational();
		}
	}
	return 1;
}