#include "StdAfx.h"
#include "AreaDB.h"

CAreaDB* g_pkAreaDB = NULL;
bool CAreaDB::Load(const char* pcName)
{
	if( !CDBFile::Load(pcName) )
		return false;

	unsigned int uiAreaId;
	AreaInfo* pkAreaInfo;
	std::string str;
    char* pDesc;
	for( unsigned int ui = 0; ui < GetRecordCount(); ++ui )
	{
        uiAreaId = GetUInt(ui, CAreaDB::EntryID);
        GetString(ui, CAreaDB::Name, str);


        pkAreaInfo = new AreaInfo;

        //�ֽ����ֺ�����
        pDesc = (char*)strchr(str.c_str(), '(');
        if(pDesc)
        {
            pkAreaInfo->kDesc = pDesc;
            *pDesc = '\0';
            pkAreaInfo->kName = str;
        }
        else
        {
            pkAreaInfo->kName = str;
        }

		pkAreaInfo->uiFlags = GetUInt(ui, CAreaDB::Flags);

		pkAreaInfo->mapId = GetUInt(ui, CAreaDB::MapID);
		pkAreaInfo->minLevel = GetUInt(ui, CAreaDB::Min_Level);
		pkAreaInfo->maxLevel = GetUInt(ui, CAreaDB::Max_Level);
		pkAreaInfo->AllowableRace = GetUInt(ui, CAreaDB::Race);
		pkAreaInfo->entry = uiAreaId;

		m_kAreaMap.insert(std::map<unsigned int, AreaInfo*>::value_type(uiAreaId, pkAreaInfo));
	}

	return true;
}
void CAreaDB::DoInternational()
{
	std::map<unsigned int, AreaInfo*>::iterator it = m_kAreaMap.begin();
	for (it; it != m_kAreaMap.end(); ++it)
	{
		if (it->second)
		{
			g_pkInternational_dbc->AddCRCData(it->second->kName.c_str());
			g_pkInternational_dbc->AddCRCData(it->second->kDesc.c_str());
		}
	}

}
void CAreaDB::Unload()
{
	std::map<unsigned int, AreaInfo*>::iterator it = m_kAreaMap.begin();
	for (it; it != m_kAreaMap.end();)
	{
		delete it->second;
		it->second = NULL ;
		++it;
	}

	m_kAreaMap.clear(); 
	CDBFile::Unload();
}


