#pragma once

#include "DBFile.h"

struct AreaInfo 
{
	enum
	{
		PVP = 0x01,
		SAFEZONE = 0x02,
	};

	unsigned int entry;
	string kName;
	string kDesc;
	unsigned int uiFlags;
	unsigned int minLevel;
	unsigned int maxLevel;
	unsigned int AllowableRace;
	uint32 mapId;
;
};

class CAreaDB : public CDBFile
{
public:
    enum
	{
		EntryID = 0, //unsigned int
		MapID = 1,
		Flags = 4, //unsigned int
        Name = 7, //string
		Min_Level = 10,//unsigned int
		Max_Level = 11,//unsigned int
		Race = 12,//unsigned int
	};

	virtual bool Load(const char* pcName);
	virtual void Unload();
	
	virtual void DoInternational();
	std::map<unsigned int, AreaInfo*> m_kAreaMap;
  

private:
};

extern CAreaDB* g_pkAreaDB;