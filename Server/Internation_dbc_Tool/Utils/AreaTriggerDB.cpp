#include "StdAfx.h"
#include "AreaTriggerDB.h"

CAreaTriggerDB* g_pkAreaTriggerDB = NULL;

bool CAreaTriggerDB::Load(const char* pcName)
{
	if(!CDBFile::Load(pcName))
		return false;

	for(unsigned int ui = 0; ui < GetRecordCount(); ui++)
	{
		stAreaTrigger sTemp;
		std::string str;
		sTemp.id = GetUInt(ui, 0);
		sTemp.type = GetUInt(ui, 1);
		sTemp.map = GetUInt(ui, 2);
		sTemp.screen = GetUInt(ui, 3);
		GetString(ui, 4, str);
		sTemp.name = (char*)str.c_str();
		m_AreaTriggers[sTemp.id] = sTemp;
	}

	return true;
}

void CAreaTriggerDB::DoInternational()
{
	stdext::hash_map<ui32, stAreaTrigger>::iterator it = m_AreaTriggers.begin();
	for (it; it!= m_AreaTriggers.end(); ++it)
	{
		g_pkInternational_dbc->AddCRCData(it->second.name.c_str());	
	}
}