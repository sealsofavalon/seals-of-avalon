#include "stdafx.h"
#include "CPlayerTitleDB.h"


TitleDB* g_pkPlayerTitle = NULL;
TiTleRewardDB* g_pkPlaeryTitleReward = NULL;

bool TitleDB::Load(const char* pcName)
{
	if (!CDBFile::Load(pcName))
	{
		return false;
	}

	for(unsigned int ui = 0; ui < GetRecordCount(); ui++)
	{
		TitleInfo sTemp;

		std::string str;
		sTemp.id = GetUInt(ui, 0);

		GetString(ui, 1, str);
		sTemp.stTitle = str ;

		sTemp.stProperty = GetUInt(ui, 2);
		sTemp.Class = GetUInt(ui, 3);

		GetString(ui, 4, str);
		sTemp.stDesc = str ;

		sTemp.durTimeId = GetUInt(ui, 5);

		m_TitleMap.insert(std::make_pair(sTemp.id, sTemp));
	}

	return true;
}

TitleInfo* TitleDB::GetTitleInfo(ui32 TitleID)
{
	std::map<ui32 , TitleInfo>::iterator it = m_TitleMap.find(TitleID);
	if(it != m_TitleMap.end())
		return &it->second;

	return NULL;
}
void TitleDB::DoInternational()
{
	std::map<ui32 , TitleInfo>::iterator it = m_TitleMap.begin();
	for (it ; it != m_TitleMap.end(); ++it)
	{
		g_pkInternational_dbc->AddCRCData(it->second.stTitle.c_str());
		g_pkInternational_dbc->AddCRCData(it->second.stDesc.c_str());
	}
}
bool TitleDB::GetTitleInfoList(std::vector<TitleInfo>& vInfo)
{
	std::map<ui32 , TitleInfo>::iterator it = m_TitleMap.begin();
	while(it != m_TitleMap.end())
	{
		vInfo.push_back(it->second);
		++it;
	}
	return vInfo.size()?true:false;
}
//////////////////////////////////////////////////////////////////////////////
bool TiTleRewardDB::Load(const char* pcName)
{
	if (!CDBFile::Load(pcName))
	{
		return false;
	}
	TitleRewardInfo sTemp;
	for(unsigned int ui = 0; ui < GetRecordCount(); ui++)
	{
		sTemp.TitleId = GetUInt(ui, 0);
		sTemp.RewardItemID = GetUInt(ui, 1);
		sTemp.RewardItemCount = GetUInt(ui, 2);
		m_vTitleReward.push_back(sTemp);
	}
	return true;
}
void TiTleRewardDB::DoInternational()
{
	
}
bool TiTleRewardDB::GetTitleReward(ui32 titleId, ui32& RewardItemid, ui32& RewardItemCount)
{
	for(size_t ui = 0 ; ui < m_vTitleReward.size() ; ui++)
	{
		if(m_vTitleReward[ui].TitleId == titleId)
		{
			RewardItemid = m_vTitleReward[ui].RewardItemID;
			RewardItemCount = m_vTitleReward[ui].RewardItemCount;
			return true;
		}
	}
	return false;
}