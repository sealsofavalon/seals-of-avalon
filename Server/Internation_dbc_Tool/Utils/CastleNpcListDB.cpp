#include "StdAfx.h"
#include "CastleNpcListDB.h"

CCastleNpcListDB* g_pkCastleNpcListDB = NULL;
bool CCastleNpcListDB::Load(const char* pcName)
{
	if( !CDBFile::Load(pcName) )
		return false;

	std::string str;
	for( unsigned int ui = 0; ui < GetRecordCount(); ++ui )
	{
		CastleNpcList castleNpcList;
		castleNpcList.uNpcEntry = GetUInt(ui, CCastleNpcListDB::EntryID);
		castleNpcList.GoldCost = GetUInt(ui, CCastleNpcListDB::GoldCost);
		castleNpcList.GuildPointCost = GetUInt(ui, CCastleNpcListDB::Points);
		GetString(ui, CCastleNpcListDB::Comments, str);
		castleNpcList.Comments = str;

		m_vNpcMap.insert(make_pair(castleNpcList.uNpcEntry, castleNpcList));
	}

	return true;
}
void CCastleNpcListDB::DoInternational()
{
	std::map<uint32, CastleNpcList>::iterator it=  m_vNpcMap.begin();
	for(it; it!= m_vNpcMap.end(); ++it)
	{
		g_pkInternational_dbc->AddCRCData(it->second.Comments.c_str());
	}
}
bool CCastleNpcListDB::GetNpcvMap(std::vector<CastleNpcList> &vNpclist)
{
	if (m_vNpcMap.size())
	{
		std::map<uint32, CastleNpcList>::iterator it = m_vNpcMap.begin();
		while(it != m_vNpcMap.end())
		{
			vNpclist.push_back(it->second);
			++it;
		}
		return true;
	}
	return false;
}