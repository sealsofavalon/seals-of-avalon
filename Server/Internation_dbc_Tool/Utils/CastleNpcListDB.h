#pragma once

#include "DBFile.h"
struct CastleNpcList
{
	uint32 uNpcEntry;
	uint32 GoldCost;
	uint32 GuildPointCost;
	std::string Comments;
};
class CCastleNpcListDB : public CDBFile
{
public:
    enum
	{
		EntryID = 0, //unsigned int
		GoldCost = 1, //unsigned int
        Points = 2, //unsigned int
		Comments = 3,//string

	};

    virtual bool Load(const char* pcName);

	bool GetNpcvMap(std::vector<CastleNpcList> &vNpclist);
	virtual void DoInternational();


	std::map<uint32, CastleNpcList> m_vNpcMap;

};

extern CCastleNpcListDB* g_pkCastleNpcListDB;