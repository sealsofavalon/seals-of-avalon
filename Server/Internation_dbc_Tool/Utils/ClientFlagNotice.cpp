#include "StdAfx.h"
#include "ClientFlagNotice.h"
#include <iostream>
#include <fstream>

ClientFlagNotice* g_pkClientFlagNotice = NULL ;

ClientFlagNotice::ClientFlagNotice()
{

}
BOOL ClientFlagNotice::Load(const char* pcname)
{
	ifstream file ;
	file.open(pcname);
	string line ;
	string txt ;
	ui32 index ;
	while(getline(file, line))
	{
		if (line.length() > 0)
		{
			size_t startpos = line.find_first_of(" ");
			if (startpos != string::npos)
			{
				index = strtoul(line.substr(0, startpos).c_str(), NULL, 10);
				txt = line.substr(startpos + 1, line.length() - 1);
				m_FlagNoticeMap.insert(std::make_pair(index, txt));
			}	
		}
	}
	file.close();
	return TRUE ;
}
void ClientFlagNotice::Unload()
{

}
void ClientFlagNotice::DoInternationalFlagdb()
{
	std::map<ui32, std::string>::iterator it = m_FlagNoticeMap.begin();
	for (it; it != m_FlagNoticeMap.end(); ++it)
	{
		g_pkInternational_dbc->AddFlagData(it->first, it->second);
	}

}