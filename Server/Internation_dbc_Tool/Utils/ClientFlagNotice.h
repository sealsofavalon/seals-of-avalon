#ifndef __CLIENTFLAGNOTICE_H__
#define __CLIENTFLAGNOTICE_H__

class ClientFlagNotice
{
public:
	ClientFlagNotice();
	BOOL Load(const char* pcname);
	void Unload();
	void DoInternationalFlagdb();
protected:
private:
	std::map<ui32, std::string> m_FlagNoticeMap;
};
extern ClientFlagNotice* g_pkClientFlagNotice ;
#endif