#ifndef __CLIENTNOTICE_H__
#define __CLIENTNOTICE_H__

class ClientNotice
{
public:
	ClientNotice();
	BOOL LoadClientText(const char* filename);
	void DoInternational();
	void UnLoadClientText();
protected:
	std::vector<std::string> m_ClientText ;
private:
};

extern ClientNotice* g_pkClientNoticetext;
#endif