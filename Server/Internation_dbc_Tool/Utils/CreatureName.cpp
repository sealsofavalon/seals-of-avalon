
#include "StdAfx.h"
#include "CreatureName.h"

CCreatureNameDB* g_pkCreatureName = NULL ;
bool CCreatureNameDB::Load(const char* pcName)
{
	if( !CDBFile::Load(pcName) )
		return false;

	unsigned int usValue;
	for( unsigned int ui = 0; ui < GetRecordCount(); ui++ )
	{
		stCNDB sTemp;
		usValue = GetUInt(ui, 0);
		GetString(ui, 1, sTemp.name);
		GetString(ui, 2, sTemp.titlename);
		sTemp.Type = GetUInt(ui, 3);
		sTemp.modelid = GetUInt(ui, 7);
		sTemp.anispeedWalk = GetFloat(ui, 10);
		sTemp.anispeedRun = GetFloat(ui, 11);
		sTemp.sound_death = GetUInt(ui, 12);
		sTemp.sound_attack1 = GetUInt(ui, 13);
		sTemp.sound_attack2 = GetUInt(ui, 14);
		sTemp.sound_stand = GetUInt(ui, 15);
		sTemp.sound_run = GetUInt(ui, 16);
		m_mCreatureName.insert(make_pair(usValue, sTemp));
	}

	return true;
}

bool CCreatureNameDB::FindName(ui32 entry, string& name)
{
	stdext::hash_map<ui32, stCNDB>::iterator itr = m_mCreatureName.find(entry);
	if( itr != m_mCreatureName.end() )
	{
		name = itr->second.name ;
		return true;
	}
	return false;
}
bool CCreatureNameDB::FindAllName(ui32 entry, string& name)
{
	stdext::hash_map<ui32, stCNDB>::iterator itr = m_mCreatureName.find(entry);
	if( itr != m_mCreatureName.end() )
	{
		name = itr->second.name ;
		if (itr->second.titlename.length())
		{
			name += " ("+ itr->second.titlename+ ")";
		}

		return true;
	}
	return false;
}
bool CCreatureNameDB::IsNPC(ui32 entry)
{
	stdext::hash_map<ui32, stCNDB>::iterator itr = m_mCreatureName.find(entry);
	if( itr != m_mCreatureName.end() )
	{
		return itr->second.Type?true:false;
	}
	return false;
}

bool CCreatureNameDB::FindTitleName(ui32 entry, std::string& titleName)
{
	stdext::hash_map<ui32, stCNDB>::iterator itr = m_mCreatureName.find(entry);
	if( itr != m_mCreatureName.end() )
	{
		titleName = itr->second.titlename;
		if (titleName.length() > 0)
		{
			return true;
		}else
		{
			return false ;
		}
	}
	return false;
}

bool CCreatureNameDB::FindModelId(ui32 entry, ui32& modelid)
{
	stdext::hash_map<ui32, stCNDB>::iterator itr = m_mCreatureName.find(entry);
	if( itr != m_mCreatureName.end() )
	{
		modelid = itr->second.modelid;
		return true;
	}
	return false;
}

bool CCreatureNameDB::FindWalkSpeed(ui32 entry, float& value)
{
	stdext::hash_map<ui32, stCNDB>::iterator itr = m_mCreatureName.find(entry);
	if( itr != m_mCreatureName.end() )
	{
		value = itr->second.anispeedWalk;
		return true;
	}
	return false;
}

bool CCreatureNameDB::FindRunSpeed(ui32 entry, float& value)
{
	stdext::hash_map<ui32, stCNDB>::iterator itr = m_mCreatureName.find(entry);
	if( itr != m_mCreatureName.end() )
	{
		value = itr->second.anispeedRun;
		return true;
	}
	return false;
}

void CCreatureNameDB::DoInternational()
{
	stdext::hash_map<ui32, stCNDB>::iterator it = m_mCreatureName.begin();
	for (it; it!=m_mCreatureName.end(); ++it)
	{
		g_pkInternational_dbc->AddCRCData(it->second.name.c_str());
		g_pkInternational_dbc->AddCRCData(it->second.titlename.c_str());
	}
}
bool CCreatureNameDB::FindCNDB(ui32 entry, stCNDB& cndb)
{
	stdext::hash_map<ui32, stCNDB>::iterator itr = m_mCreatureName.find(entry);
	if( itr != m_mCreatureName.end() )
	{
		cndb = itr->second;
		return true;
	}

	return false;
}