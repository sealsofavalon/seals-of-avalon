#ifndef GAMEOBECTDB_H
#define GAMEOBECTDB_H

#include "DBFile.h"

class CGameObjectDB : public CDBFile
{
public:
	struct stGameObject
	{
		uint32 ID;
		uint32 Type;
		uint32 DisplayID;
		std::string Name;
		uint32 SpellFocus;
		uint32 CastSpell;
		uint32 ReqSpell;
		uint32 QuestID;
		uint32 ReqItem;
		uint32 SpawnTime;
		uint32 NewObj;
		uint32 MineMin;
		uint32 MineMax;

		uint32 QuestItemID;
		//uint32 sound9;
	};
	virtual void DoInternational();
	virtual bool Load(const char* pcName);

	stGameObject* GetGameObject(ui32 uiGameObectId);


private:
	stdext::hash_map<ui32, stGameObject> m_GameObjects;
};

extern CGameObjectDB* g_pkGameObjectDB;

#endif