#include "StdAfx.h"
#include "InstanceMsg.h"

CInstanceMsg* g_pkInstanceMsgDB = NULL;

bool CInstanceMsg::Load(const char* pcName)
{
	if( !CDBFile::Load(pcName) )
		return false;

	std::string str;
	for( unsigned int ui = 0; ui < GetRecordCount(); ++ui )
	{
		uint16 mapid, time;
		mapid = GetUInt(ui, CInstanceMsg::MapId);
		time  = GetUInt(ui, CInstanceMsg::Time);
        GetString(ui, CInstanceMsg::Desc, str);

		InstaceMsgstruct temp;
		temp.descmsg = str;
		temp.time = float(time);
		InstanceMsgMap::iterator it = m_InstanceMsgMap.find(mapid);
		if (it != m_InstanceMsgMap.end())
		{
			it->second.push_back(temp);
		}
		else
		{
			std::vector<InstaceMsgstruct> vtemp;
			vtemp.push_back(temp);
			m_InstanceMsgMap.insert(make_pair(mapid, vtemp));
		}
	}
	return true;
}
void ExchangeSort(std::vector<InstaceMsgstruct> &vec)
{
	if ( !vec.size() )
	{
		return;
	}
	float temp;
	std::string tempstr;
	for(UINT i = 0 ; i < vec.size() - 1; i ++)
		for(UINT j = i + 1 ; j < vec.size() ; j++)
		{
			if(vec[i].time < vec[j].time)
			{
				temp = vec[i].time;
				vec[i].time = vec[j].time;
				vec[i].time = temp;

				tempstr = vec[i].descmsg;
				vec[i].descmsg = vec[j].descmsg;
				vec[j].descmsg = tempstr;
			}

		}
}
void CInstanceMsg::DoInternational()
{
	std::map<uint16, std::vector<InstaceMsgstruct>>::iterator it = m_InstanceMsgMap.begin();
	for (it; it != m_InstanceMsgMap.end(); ++it)
	{
		for (int i = 0 ; i < it->second.size(); i++)
		{
			g_pkInternational_dbc->AddCRCData(it->second[i].descmsg.c_str());
		}
	}
}
bool CInstanceMsg::GetInstanceVMsg(ui32 mapid, float timefileter, std::vector<InstaceMsgstruct> &vec)
{
	InstanceMsgMap::iterator it = m_InstanceMsgMap.find(mapid);
	if (it != m_InstanceMsgMap.end())
	{
		vec = it->second;
		std::vector<InstaceMsgstruct>::iterator itvec = vec.begin(); 
		while(itvec != vec.end())
		{
			if (itvec->time > timefileter)
			{
				itvec = vec.erase(itvec);
				continue;
			}
			itvec++;
		}
		ExchangeSort(vec);
		return true;
	}
	return false;
}