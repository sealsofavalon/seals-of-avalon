#pragma once

#include "DBFile.h"
struct InstaceMsgstruct
{
	float time;
	std::string descmsg;
};
class CInstanceMsg : public CDBFile
{
public:
    enum
	{
		entry = 0,
        MapId = 1, //uint16
		Time = 2, //uint16
		Desc = 3,//string
	};

	virtual bool Load(const char* pcName);

	bool GetInstanceVMsg(ui32 mapid, float timefileter, std::vector<InstaceMsgstruct> &vec);
	virtual void DoInternational();
	typedef std::map<uint16, std::vector<InstaceMsgstruct>> InstanceMsgMap;
	InstanceMsgMap m_InstanceMsgMap;
};

extern CInstanceMsg* g_pkInstanceMsgDB;