#include "StdAfx.h"
#include "ItemDB.h"
#include "SpellDB.h"

CItemDB* g_pkItemDB = NULL ;
CItemDB::CItemDB()
{
	//Load(pcName);
}

CItemDB::~CItemDB()
{
	m_ItemProperty.clear();
}

bool CItemDB::IsMountItem(ui32 id)
{
	ItemPrototype_Client* Info = GetInfo(id);
	if(Info)
	{
		for (ui32 i = 0; i < 5; i++)
		{
			ui32 spellid = Info->Spells[i].Id;
			bool ret = g_pkSpellInfo->IsMountSpell(spellid);
			if(ret)
				return true;
		}
	}
	return false;
}

void CItemDB::DoInternational()
{
	std::map<ui32, ItemPrototype_Client>::iterator it= m_ItemProperty.begin();
	for (it; it!=m_ItemProperty.end(); ++it)
	{
		g_pkInternational_dbc->AddCRCData(it->second.C_name.c_str());
		g_pkInternational_dbc->AddCRCData(it->second.C_desc.c_str());
		g_pkInternational_dbc->AddCRCData(it->second.lowercase_name.c_str());
	}
}
bool CItemDB::IsShapeItem(ui32 id)
{
	ItemPrototype_Client* Info = GetInfo(id);
	if(Info)
	{
		for (ui32 i = 0; i < 5; i++)
		{
			ui32 spellid = Info->Spells[i].Id;
			bool ret = g_pkSpellInfo->IsShapeSpell(spellid);
			if(ret)
				return true;
		}
	}
	return false;
}

bool CItemDB::IsSkillBook(ui32 id)
{
	ItemPrototype_Client* Info = GetInfo(id);
	if(Info)
	{
		for (ui32 i = 0; i < 5; i++)
		{
			ui32 spellid = Info->Spells[i].Id;
			bool ret = g_pkSpellInfo->IsSkillBook(spellid);
			if(ret)
				return true;
		}
	}
	return false;
}

bool CItemDB::Load(const char* pcName)
{
	if(!CDBFile::Load(pcName))
		return false;

	std::string sName;
	ItemPrototype_Client sBaseInfo;
	for(unsigned int ui = 0; ui < GetRecordCount(); ui++)
	{
		sBaseInfo.ItemId		= GetUInt(ui, 0);
		sBaseInfo.Class			= GetUInt(ui, 1);
		sBaseInfo.SubClass		= GetUInt(ui, 2);
		//sBaseInfo.unknown_bc	= GetUInt(ui, 3);
		GetString(ui, 3, sBaseInfo.C_name);
		sBaseInfo.DisplayInfoID[0][0]	= GetUInt(ui, 4);
		sBaseInfo.DisplayInfoID[0][1]	= GetUInt(ui, 5);
		sBaseInfo.DisplayInfoID[1][0]	= GetUInt(ui, 6);
		sBaseInfo.DisplayInfoID[1][1]	= GetUInt(ui, 7);
		sBaseInfo.DisplayInfoID[2][0]	= GetUInt(ui, 8);
		sBaseInfo.DisplayInfoID[2][1]	= GetUInt(ui, 9);
		sBaseInfo.Quality		= GetUInt(ui, 10);
		sBaseInfo.BuyPrice		= GetUInt(ui, 12);
		sBaseInfo.SellPrice		= GetUInt(ui, 13);
		sBaseInfo.InventoryType = GetUInt(ui ,14);
		sBaseInfo.AllowableClass = GetUInt(ui, 15);
		sBaseInfo.AllowableRace	= GetUInt(ui, 16);
		sBaseInfo.ItemLevel		= GetUInt(ui, 17);
		sBaseInfo.RequiredLevel = GetUInt(ui ,18);
		sBaseInfo.RequiredPlayerRank1 = GetUInt(ui, 22);
		sBaseInfo.RequiredPlayerRank2 = GetUInt(ui, 23);
		sBaseInfo.MaxCount		= GetUInt(ui, 27);
		sBaseInfo.ContainerSlots = GetUInt(ui, 28);
		for (ui32 i = 0; i < 10; i++)
		{
			sBaseInfo.Stats[i].Type = GetUInt(ui, 29+i*2);
			sBaseInfo.Stats[i].Value = GetUInt(ui, 29+i*2+1);
		}
		for (ui32 i = 0; i < 5; i++)
		{
			sBaseInfo.Damage[i].Min = (float)GetUInt(ui, 49+i*3);
			sBaseInfo.Damage[i].Max = (float)GetUInt(ui, 49+i*3+1);
			sBaseInfo.Damage[i].Type = GetUInt(ui, 49+i*3+2);
		}

		//sBaseInfo.Delay			= GetUInt(ui, 70 - 1);
		sBaseInfo.Range			= (float)GetUInt(ui, 65);
		sBaseInfo.Armor			= GetUInt(ui, 64);
		for (ui32 i = 0; i < 5; i++)
		{
			sBaseInfo.Spells[i].Id	= GetUInt(ui, 66+i*6);
			sBaseInfo.Spells[i].Trigger = GetUInt(ui, 66+i*6+1);
			sBaseInfo.Spells[i].Charges = GetUInt(ui, 66+i*6+2);
			sBaseInfo.Spells[i].Cooldown = GetUInt(ui, 66+i*6+3);
			sBaseInfo.Spells[i].Category = GetUInt(ui, 66+i*6+4);
			sBaseInfo.Spells[i].CategoryCooldown = GetUInt(ui, 66+i*6+5);
		}
		sBaseInfo.Bonding		= GetUInt(ui, 96);
		GetString(ui, 97, sBaseInfo.C_desc);
		sBaseInfo.QuestId		= GetUInt(ui, 99);
		sBaseInfo.ItemSet		= GetUInt(ui, 106);
		sBaseInfo.MaxDurability	= GetUInt(ui, 107);
		//sBaseInfo.BagFamily		= GetUInt(ui, 124);
		GetString(ui, 121, sBaseInfo.C_icon);
		GetString(ui, 122, sBaseInfo.C_mapmodel);
		GetString(ui, 123, sBaseInfo.C_model);
		sBaseInfo.LongOrShortMesh = GetUInt(ui, 122);
		m_ItemProperty.insert(std::make_pair(sBaseInfo.ItemId, sBaseInfo));
	}

	return true;
}

ItemPrototype_Client* CItemDB::GetInfo(ui32 dwId)
{
	ItemPrototype_Client* Info = NULL;

	std::map<ui32, ItemPrototype_Client>::iterator it = m_ItemProperty.find(dwId);
	if(it != m_ItemProperty.end())
	{
		Info = &it->second;
	}

	return Info;
}
ItemPrototype_Client* CItemDB::GetInfo(const char* name)
{
	ItemPrototype_Client* Info = NULL;
	std::map<ui32, ItemPrototype_Client>::iterator it = m_ItemProperty.begin();
	while(it != m_ItemProperty.end())
	{
		if (_stricmp(it->second.C_name.c_str(), name) == 0)
		{
			Info = &it->second;
			break;
		}
	}
	return Info;
}

bool CItemDB::GetInfo(ui32 Class, std::vector<ItemPrototype_Client>& vInfo)
{
	std::map<ui32, ItemPrototype_Client>::iterator it = m_ItemProperty.begin();
	while(it != m_ItemProperty.end())
	{
		if (it->second.Class == Class)
		{
			vInfo.push_back(it->second);
		}
		++it;
	}
	return vInfo.size()?true:false;
}