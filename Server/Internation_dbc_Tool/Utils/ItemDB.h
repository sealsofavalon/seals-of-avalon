#ifndef ITEMDB_H
#define ITEMDB_H

#include "DBFile.h"
class CItemManager;

typedef enum
{
	ICT_INVLALID = 0,
	ICT_BAG = 1,
	ICT_EQUIP = 2,
	ICT_PLAYERTRADE = 7,
	ICT_BANK = 12,
	ICT_DEPOT,
	ICT_INVALID_POS = 0xFFFE,
	//ICT_MAP
}ITEM_CONTAINER_TYPE;

struct ItemPrototype_Client : public ItemPrototype
{
	string C_name;
	string C_desc;
	string C_model;//暂时存储在这里,以后需要从displayinfo=>modelname
	string C_mapmodel;
	string C_icon;
	int LongOrShortMesh;
};
class CItemDB : public CDBFile
{
friend class CItemManager;

public:
	~CItemDB();
	CItemDB();
	//CItemDB(const char* pcName);
	virtual bool Load(const char* pcName);


	ItemPrototype_Client* GetInfo(ui32 dwId);
	ItemPrototype_Client* GetInfo(const char* name);

	virtual void DoInternational();
	bool GetInfo(ui32 Class, std::vector<ItemPrototype_Client>& vInfo);

	bool IsMountItem(ui32 id);
	bool IsShapeItem(ui32 id);
	bool IsSkillBook(ui32 id);
protected:
	
	

	std::map<ui32, ItemPrototype_Client> m_ItemProperty;
};

extern CItemDB* g_pkItemDB;
#endif
