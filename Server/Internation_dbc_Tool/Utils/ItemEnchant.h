#ifndef ITEMENCHANT_H
#define ITEMENCHANT_H

#include "DBFile.h"

struct ItemEnchantProto
{
	ui32 uiEntryId;
	ui32 uiType[3];
	ui32 uiMin[3];
	ui32 uiMax[3];
	ui32 uiSpell[3];
	std::string sName;
	ui32 uiVisual;
	ui32 uiEnchantGroups;
	ui32 uiGemEntry;
	ui32 uiProcChance;
};
class CItemEnchantDB : public CDBFile
{
public:
	enum
	{
		EntryID = 0, //unsigned int
		TYPE1 = 1, //unsigned int
		TYPE2 = 2, //unsigned int
		TYPE3 = 3,//unsigned int
		MIN1 = 4,//unsigned int
		MIN2 = 5,//unsigned int
		MIN3 = 6,//unsigned int
		MAX1 = 7,//unsigned int
		MAX2 = 8,//unsigned int
		MAX3 = 9,//unsigned int
		SPELL1 = 10,//unsigned int
		SPELL2 = 11,//unsigned int
		SPELL3 = 12,//unsigned int
		NAME = 13,//String
		VISUAL = 14,//unsigned int
		ENCHANTGROUPS = 15,//unsigned int
		GEMENTRY = 16,//unsigned int
		PROCCHANCE = 17,//unsigned int
	};

	CItemEnchantDB();
	~CItemEnchantDB();

	virtual bool Load(const char* pcName);
	virtual void DoInternational();

	bool GetEnchantProperty(ui32 EntryId , ItemEnchantProto& itemEnchantProto);
protected:


	std::map<ui32, ItemEnchantProto> m_ItemEnchant;
};
struct refineItemEffect
{
	ui32 entry;
	UINT rarity;
	ui32 min_refineLevel;
	ui32 max_refineLevel;
	ui32 InventoryType;
	ui64 displayId;
};
class CRefineitemeffectDB : public CDBFile
{
public:
	CRefineitemeffectDB();
	~CRefineitemeffectDB();

	virtual bool Load(const char* pcName);

	bool GetRefineItemEffect(struct ItemPrototype_Client* pItem, UINT jinglianLevel, ui32& effect_display_ID);
protected:
	std::vector<refineItemEffect> m_ItemEnchant;
};

extern CRefineitemeffectDB* g_pkRefineItemEffectDB;
extern CItemEnchantDB* g_pkItemEnchantDB;
#endif
