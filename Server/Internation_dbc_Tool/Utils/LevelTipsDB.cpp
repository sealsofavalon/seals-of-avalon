#include "StdAfx.h"
#include "LevelTipsDB.h"

CLevelTipsDB* g_pkLevelTipsDB = NULL;
bool CLevelTipsDB::Load(const char* pcName)
{
	if( !CDBFile::Load(pcName) )
		return false;

	stLevelTips tip;
	
	for( unsigned int ui = 0; ui < GetRecordCount(); ++ui )
	{
        tip.ntype  = GetUInt(ui, 1);
		tip.race   = GetUInt(ui, 2);
		tip.nclass  = GetUInt(ui, 3);
		tip.level  = GetUInt(ui, 4);
        GetString(ui, 5, tip.tip);

		vLvlTips.push_back(tip);
	}

	return true;
}

void CLevelTipsDB::DoInternational()
{
	for(int i = 0; i < vLvlTips.size(); i++)
	{
		g_pkInternational_dbc->AddCRCData(vLvlTips[i].tip.c_str());
	}
}
bool CLevelTipsDB::GetTips(uint32 level, uint32 race, uint32 nclass, std::vector<stLevelTips>& vTips) const
{
	bool bFound = false;
	for(int i = 0; i < (int)vLvlTips.size(); i++)
	{
		if(vLvlTips[i].level == level && vLvlTips[i].race & 1<<(race-1) && vLvlTips[i].nclass & 1<<(nclass-1))
		{
			vTips.push_back(vLvlTips[i]);
			bFound = true;
		}
	}
	return bFound;
}