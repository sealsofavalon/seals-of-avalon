#pragma once

#include "DBFile.h"

class CLevelTipsDB : public CDBFile
{
public:
	struct stLevelTips
	{
		uint32 id;
		uint32 ntype;
		uint32 race;
		uint32 nclass;
		uint32 level;
		std::string tip;
	};
    virtual bool Load(const char* pcName);
	virtual void DoInternational();
    bool GetTips(uint32 level, uint32 race, uint32 nclass, std::vector<stLevelTips>& vTips) const;

private:
	std::vector<stLevelTips> vLvlTips;
};

extern CLevelTipsDB* g_pkLevelTipsDB;