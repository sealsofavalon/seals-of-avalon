#include "StdAfx.h"
#include "NewBirdDB.h"

CNewBirdCategroyDB* g_pkNewBirdCategroyDB = NULL;


bool CNewBirdCategroyDB::Load(const char* pcName)
{
	if( !CDBFile::Load(pcName) )
		return false;

	std::string str;
	uint16 Id;
	uint16 SubId;
	for( unsigned int ui = 0; ui < GetRecordCount(); ++ui )
	{
		GetString(ui, CNewBirdCategroyDB::Title, str);
		Id = GetUInt(ui, CNewBirdCategroyDB::Categroy);
		SubId = GetUInt(ui, CNewBirdCategroyDB::SubCategroy);
		AppNewItem(Id, SubId, str.c_str());
	}

	return true;
}
void CNewBirdCategroyDB::DoInternational()
{
	for (int i = 0 ; i < m_VCategroy.size(); i++)
	{
		for(int j = 0 ; j < m_VCategroy[i].SubCatgory.size(); j++)
		{

			g_pkInternational_dbc->AddCRCData(m_VCategroy[i].SubCatgory[j].title.c_str());
		}
	}
}
void CNewBirdCategroyDB::AppNewItem(uint16 Id, uint16 SubId, const char* title)
{
	std::vector<NewBirdCategroyStruct>::iterator it = m_VCategroy.begin();
	while(it != m_VCategroy.end())
	{
		if (it->CategoryID == Id)
		{
			SubCategroyStruct Temp;
			Temp.Id = SubId;
			Temp.title = title;
			it->SubCatgory.push_back(Temp);
			return;
		}
		++it;
	}
	NewBirdCategroyStruct NewTemp;
	NewTemp.CategoryID = Id;
	SubCategroyStruct Temp;
	Temp.Id = SubId;
	Temp.title = title;
	NewTemp.SubCatgory.push_back(Temp);
	m_VCategroy.push_back(NewTemp);
}
bool CNewBirdCategroyDB::GetCategroyContent(uint16 CategoryID, std::vector<SubCategroyStruct> &vec)
{
	std::vector<NewBirdCategroyStruct>::iterator it = m_VCategroy.begin();
	while(it != m_VCategroy.end())
	{
		if (it->CategoryID == CategoryID)
		{
			vec = it->SubCatgory;
			return true;
		}
		++it;
	}
	return false;
}