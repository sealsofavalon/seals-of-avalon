#pragma once

#include "DBFile.h"
struct NewBirdEventStruct
{
	unsigned int eventId;
	std::string title;
	std::vector<std::string> info;
};
class CNewBirdEventDB : public CDBFile
{
public:
    enum
	{
        entry = 0,
		eventId = 1,
		title = 2,
		info = 3,
	};
    virtual bool Load(const char* pcName);

	bool GetNewBirdEventStr(unsigned int eventid, std::vector<std::string>& info);
	bool GetNewBirdEventTitle(unsigned int eventid, std::string& title);
	virtual void DoInternational();
private:
	void AddNewEntry(unsigned int eventId, std::string& title, std::string& info);
	std::vector<NewBirdEventStruct> m_NewBirdEventStr;

};

extern CNewBirdEventDB* g_pkNewBirdEventDB;