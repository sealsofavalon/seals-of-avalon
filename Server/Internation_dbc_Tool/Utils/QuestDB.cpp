#include "StdAfx.h"
#include "QuestDB.h"
#include "GameObjectDB.h"
#include "CreatureName.h"

CQuestDB* g_pkQusetInfo = NULL;

bool CQuestDB::Load(const char* pcName)
{
	if(!CDBFile::Load(pcName))
		return false;

	for(unsigned int ui = 0; ui < GetRecordCount(); ui++)
	{
		stQuest sTemp;
		std::string str;
		sTemp.id = GetUInt(ui, 0);
		sTemp.uiMinLevel = GetUInt(ui, 4);
		sTemp.uiQuestLevel = GetUInt(ui, 5);
		sTemp.uiReqRaces = GetUInt(ui, 7);
		sTemp.uiReqClass = GetUInt(ui, 8);
		GetString(ui, 19, str);
		sTemp.strTitle = (char*)str.c_str();
		GetString(ui, 20, str);
		sTemp.strDetails = (char*)str.c_str();
		GetString(ui, 21, str);
		sTemp.strObjective = (char*)str.c_str();
		GetString(ui, 22, str);
		sTemp.strCompletionText = (char*)str.c_str();
		GetString(ui, 23, str);
		sTemp.strInCompleteText = (char*)str.c_str();
		sTemp.uiReqItemId[0] = GetUInt(ui, 29);
		sTemp.uiReqItemId[1] = GetUInt(ui, 30);
		sTemp.uiReqItemId[2] = GetUInt(ui, 31);
		sTemp.uiReqItemId[3] = GetUInt(ui, 32);
		sTemp.uiReqItemCount[0] = GetUInt(ui, 33);
		sTemp.uiReqItemCount[1] = GetUInt(ui, 34);
		sTemp.uiReqItemCount[2] = GetUInt(ui, 35);
		sTemp.uiReqItemCount[3] = GetUInt(ui, 36);
		sTemp.uiReqKillMobOrGOId[0] = GetUInt(ui, 37);
		sTemp.uiReqKillMobOrGOId[1] = GetUInt(ui, 38);
		sTemp.uiReqKillMobOrGOId[2] = GetUInt(ui, 39);
		sTemp.uiReqKillMobOrGOId[3] = GetUInt(ui, 40);
		sTemp.uiReqKillMobOrGOCount[0] = GetUInt(ui, 41);
		sTemp.uiReqKillMobOrGOCount[1] = GetUInt(ui, 42);
		sTemp.uiReqKillMobOrGOCount[2] = GetUInt(ui, 43);
		sTemp.uiReqKillMobOrGOCount[3] = GetUInt(ui, 44);
		sTemp.uiReqHonor = GetUInt(ui, 49);

		for(unsigned int uiIndex = 0; uiIndex < 6; uiIndex++)
		{
			sTemp.uiRewChoiceItem[uiIndex] = GetUInt(ui, 50 + uiIndex);
		}
		for(unsigned int uiIndex = 0; uiIndex < 6; uiIndex++)
		{
			sTemp.uiRewChoiceItemCount[uiIndex] = GetUInt(ui, 56 + uiIndex);
		}
		for(unsigned int uiIndex = 0; uiIndex < 4; uiIndex++)
		{
			sTemp.uiRewItem[uiIndex] = GetUInt(ui, 62 + uiIndex);
		}
		for(unsigned int uiIndex = 0; uiIndex < 4; uiIndex++)
		{
			sTemp.uiRewItemCount[uiIndex] = GetUInt(ui, 66 + uiIndex);
		}
		sTemp.bRepeatable = GetUInt(ui, 101) != 0;
		sTemp.uiRewMoney = GetUInt(ui, 75);

		sTemp.uiRewXP = GetUInt(ui, 76);
		sTemp.uiRewHonor = GetUInt(ui, 78);
		sTemp.uiIsMainQuest = GetUInt(ui, 106);
		sTemp.uiToMainQuest = GetUInt(ui, 107);

		m_QuestInfo[sTemp.id] = sTemp;
	}
	return true;
}

void CQuestDB::DoInternational()
{
	stdext::hash_map<ui32, stQuest>::iterator it = m_QuestInfo.begin();
	for (it; it != m_QuestInfo.end(); ++it)
	{
		g_pkInternational_dbc->AddCRCData(it->second.strTitle.c_str());
		g_pkInternational_dbc->AddCRCData(it->second.strDetails.c_str());
		g_pkInternational_dbc->AddCRCData(it->second.strObjective.c_str());
		g_pkInternational_dbc->AddCRCData(it->second.strCompletionText.c_str());
		g_pkInternational_dbc->AddCRCData(it->second.strInCompleteText.c_str());

	}
}
//////////////////////////////////////////////////////////////////////////
CQuest_StarterDB* g_pkQuestStarterInfo = NULL;
bool CQuest_StarterDB::Load(const char* pcName)
{
	if(!CDBFile::Load(pcName))
		return false;

	uint32 QuestId = 0;
	uint32 NpcId = 0;
	for(unsigned int ui = 0; ui < GetRecordCount(); ui++)
	{
		QuestId = GetUInt(ui, 0);
		NpcId = GetUInt(ui, 1);
		m_vQuest_StarterInfo.insert(std::map<ui32, ui32>::value_type(QuestId, NpcId));
	}
	return true;
}

bool CQuest_StarterDB::GetStarterFormQuest(ui32 QuestId, ui32& NPCId)
{
	std::map<ui32, ui32>::iterator it = m_vQuest_StarterInfo.find(QuestId);
	if (it != m_vQuest_StarterInfo.end())
	{
		NPCId = it->second;
		return true;
	}
	return false;
}