#ifndef QUESTDB_H
#define QUESTDB_H

#include "DBFile.h"
class CQuest_StarterDB : public CDBFile
{
public:
	virtual bool Load(const char* pcName);
	
	bool GetStarterFormQuest(ui32 QuestId, ui32& NPCId);
protected:
	std::map<ui32, ui32> m_vQuest_StarterInfo;
};
extern CQuest_StarterDB* g_pkQuestStarterInfo;

class CQuestDB : public CDBFile
{
public:
	struct stQuest
	{
		ui32 id;
		std::string strTitle;
		std::string strDetails;
		std::string strObjective;
		std::string strCompletionText;
		std::string strInCompleteText;
		unsigned int uiReqItemId[4];
		unsigned int uiReqItemCount[4];
		unsigned int uiReqKillMobOrGOId[4];
		unsigned int uiReqKillMobOrGOCount[4];
		unsigned int uiMinLevel;
		unsigned int uiQuestLevel;
		unsigned int uiReqRaces;
		unsigned int uiReqClass;
		unsigned int uiReqHonor;
		unsigned int uiRewChoiceItem[6];
		unsigned int uiRewChoiceItemCount[6];
		unsigned int uiRewItem[4];
		unsigned int uiRewItemCount[4];
		unsigned int uiRewMoney;
		unsigned int uiRewXP;
		unsigned int uiRewHonor;
		bool bRepeatable;
		unsigned int uiIsMainQuest;
		unsigned int uiToMainQuest;
	};

	virtual bool Load(const char* pcName);
	virtual void DoInternational();

private:
	stdext::hash_map<ui32, stQuest> m_QuestInfo;
};

extern CQuestDB* g_pkQusetInfo;

#endif