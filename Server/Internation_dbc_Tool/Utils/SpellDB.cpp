#include "StdAfx.h"
#include "SpellDB.h"
CSpellDB* g_pkSpellInfo = NULL;
CSpellRangeDB* g_pkSpellRangeInfo = NULL;
CSpellDurationDB* g_pkSpellDurationInfo = NULL;

bool CSpellDB::GetSpellCDTIncrease(uint32 uiSpellId, uint32& cdtspellid, uint32& cdtincrease)
{	
	stSpellInfo* info = GetSpellInfo( uiSpellId );
	if( info )
	{
		for( int i = 0; i < 3; i++ )
		{
			if( info->uiEffectApplyAuraName[i] == 250 )
			{
				cdtspellid = info->uiEffectBasePoint[i];
				cdtincrease = info->uiEffectMiscValue[i];
				return true;
			}

		}
	}
	return false;
}

void CSpellDB::DoInternational()
{
	std::map<unsigned int, stSpellInfo>::iterator it = m_SpellInfo.begin();
	for (it; it != m_SpellInfo.end(); ++it)
	{
		g_pkInternational_dbc->AddCRCData(it->second.name.c_str());
		g_pkInternational_dbc->AddCRCData(it->second.description.c_str());
	}
}
bool CSpellDB::IsMountSpell(uint32 uiSpellId)
{
	stSpellInfo* info = GetSpellInfo( uiSpellId );
	if( info )
	{
		for( int i = 0; i < 3; i++ )
		{
			if( info->uiEffectApplyAuraName[i] >= 248 && info->uiEffectApplyAuraName[i] <= 249 )
			{
				return true;
			}

		}
	}
	return false;
}

bool CSpellDB::IsShapeSpell(uint32 uiSpellId)
{
	stSpellInfo* info = GetSpellInfo( uiSpellId );
	if( info )
	{
		for( int i = 0; i < 3; i++ )
		{
			if( info->uiEffectApplyAuraName[i] >= 244 && info->uiEffectApplyAuraName[i] <= 247 )
			{
				return true;
			}

		}
	}
	return false;
}

bool CSpellDB::IsSkillBook(uint32 uiSpellId)
{
	stSpellInfo* info = GetSpellInfo( uiSpellId );
	if( info )
	{
		for( int i = 0; i < 3; i++ )
		{
			if( info->uiEffectApplyAuraName[i] == 483 )
			{
				return true;
			}
		}
	}
	return false;
}

bool CSpellDB::Load(const char* pcName)
{
	if(!CDBFile::Load(pcName))
		return false;

	for(unsigned int ui = 0; ui < GetRecordCount(); ui++)
	{
		stSpellInfo stTemp;

		stTemp.uiId = GetUInt(ui, 0);						// 技能id
		stTemp.uiCategory = GetUInt(ui, 1);					// spell category
		stTemp.uiAttribute = GetUInt(ui, 4);				// spell attribute
		stTemp.uiCastIndex = GetUInt(ui, 15);				// cast index
		stTemp.uiTargets = GetUInt(ui, 10);					// 目标类型
		stTemp.uiTargetCreatureType = GetUInt(ui, 11);		// 目标对象类型
		stTemp.uiRecoverTime = GetUInt(ui, 16);				// 
		stTemp.uiCategoryRecoveryTime = GetUInt(ui, 17);	// 冷却时间
		stTemp.uiMaxLevel = GetUInt(ui, 24);				// 最大等级
		stTemp.uiBaseLevel = GetUInt(ui, 25);				// 基础等级
		stTemp.uiSpellLevel = GetUInt(ui, 26);				// 魔法等级
		stTemp.uiDurationIndex = GetUInt(ui, 27);			// duration index
		stTemp.uiManaCost = GetUInt(ui, 29);				// 魔法消耗
		stTemp.uiRangeIndex = GetUInt(ui, 33);				// range idx [ranget table]
		stTemp.uiVisualKit = GetUInt(ui, 112);				// 对应的效果包
		stTemp.flyspeed = GetFloat(ui, 34);					// 飞行速度

		stTemp.uiEffect[0] = GetUInt(ui, 58);
		stTemp.uiEffect[1] = GetUInt(ui, 59);
		stTemp.uiEffect[2] = GetUInt(ui, 60);

		stTemp.uiEffectBasePoint[0] = GetUInt(ui, 73);
		stTemp.uiEffectBasePoint[1] = GetUInt(ui, 74);
		stTemp.uiEffectBasePoint[2] = GetUInt(ui, 75);
		stTemp.uiEffectMiscValue[0] = GetUInt(ui, 103);
		stTemp.uiEffectMiscValue[1] = GetUInt(ui, 104);
		stTemp.uiEffectMiscValue[2] = GetUInt(ui, 105);

		stTemp.uiImplicitTarget[0] = GetUInt(ui, 79);
		stTemp.uiImplicitTarget[1] = GetUInt(ui, 80);
		stTemp.uiImplicitTarget[2] = GetUInt(ui, 81);

		stTemp.uiEffectRadius[0] = GetUInt(ui, 85);
		stTemp.uiEffectRadius[1] = GetUInt(ui, 86);
		stTemp.uiEffectRadius[2] = GetUInt(ui, 87);

		stTemp.uiEffectApplyAuraName[0] = GetUInt(ui, 88);
		stTemp.uiEffectApplyAuraName[1] = GetUInt(ui, 89);
		stTemp.uiEffectApplyAuraName[2] = GetUInt(ui, 90);
		GetString(ui, 117, stTemp.name);					// spell name

		stTemp.uiStartRecoveryTime = GetUInt(ui, 122);
		stTemp.uiMaxTargets = GetUInt(ui, 126);				// max target nb
		GetString(ui, 119, stTemp.description);				// spell intro
		GetString(ui, 135, stTemp.strIcon);					// icon 
		stTemp.uiSpellGroupType = GetUInt(ui, 125);					// SpellGroupType 

		m_SpellInfo.insert(std::make_pair(stTemp.uiId, stTemp));
	}

	return true;
}

stSpellInfo* CSpellDB::GetSpellInfo(unsigned int uiSpellId)
{
	std::map<unsigned int, stSpellInfo>::iterator it = m_SpellInfo.find(uiSpellId);

	if(it != m_SpellInfo.end())
		return &it->second;

	return NULL;
}

std::map<unsigned int, stSpellInfo>& CSpellDB::GetSpellInfos()
{
	return m_SpellInfo;
}

bool CSpellRangeDB::Load(const char* pcName)
{
	if (!CDBFile::Load(pcName))
		return false;

	for (unsigned ui = 0; ui < GetRecordCount(); ++ui)
	{
		stSpellRangeInfo record;
		record.entry = GetUInt(ui, 0);
		record.minrange = GetFloat(ui, 1);
		record.maxrange = GetFloat(ui, 2);

		m_SpellRangeInfo.insert(std::make_pair(record.entry, record));
	}

	return true;
}


stSpellRangeInfo* CSpellRangeDB::GetSpellRangeInfo(unsigned int uiEntryIdx)
{
	std::map<uint32, stSpellRangeInfo>::iterator it = m_SpellRangeInfo.find(uiEntryIdx);

	if (it != m_SpellRangeInfo.end())
		return &it->second;

	return NULL;
}


bool CSpellDurationDB::Load(const char* pcName)
{
	if (!CDBFile::Load(pcName))
		return false;

	for (unsigned ui = 0; ui < GetRecordCount(); ++ui)
	{
		stSpellDurationInfo record;
		record.entry = GetUInt(ui, 0);
		record.duration1 = GetUInt(ui, 1);
		record.duration2 = GetUInt(ui, 2);
		record.duration3 = GetUInt(ui, 3);

		m_SpellDurationInfo.insert(std::make_pair(record.entry, record));
	}

	return true;
}

stSpellDurationInfo* CSpellDurationDB::GetSpellDurationInfo(unsigned int uiEntryIdx)
{
	std::map<uint32, stSpellDurationInfo>::iterator it = m_SpellDurationInfo.find(uiEntryIdx);
	if (it != m_SpellDurationInfo.end())
		return &it->second;

	return NULL;
}

CQuerySpellDB* g_pkQuerySpellInfo = NULL;
bool CQuerySpellDB::Load(const char* pcName)
{
	if (!CDBFile::Load(pcName))
		return false;

	for (unsigned ui = 0; ui < GetRecordCount(); ++ui)
	{
		stQuerySpellInfo record;
		record.SpellId = GetUInt(ui, 0);
		record.AllowableClass = GetUInt(ui, 1);
		record.AllowableLevel = GetUInt(ui, 2);

		m_QuerySpellInfo.push_back(record);
	}
	return true; 
}

bool CQuerySpellDB::GetQuerySpellList(std::vector<stQuerySpellInfo>& vList)
{
	if (m_QuerySpellInfo.size())
	{
		vList = m_QuerySpellInfo;
		return true;
	}
	return false;
}
CSpellRadiusDB* g_pkSpellRadiusInfo = NULL ;
bool CSpellRadiusDB::Load(const char* pcName)
{
	if (!CDBFile::Load(pcName))
		return false;


	for (unsigned ui = 0; ui < GetRecordCount(); ++ui)
	{
		stSpellRadius record;
		record.index = GetUInt(ui, 0);
		record.radius = GetFloat(ui, 1);

		m_SpellEffectRadius.push_back(record);
	}

	return true;
}

bool CSpellRadiusDB::GetRadius(ui32 index, float& fRadius)
{
	for ( int i =0; i < m_SpellEffectRadius.size(); i++)
	{
		if (m_SpellEffectRadius[i].index == index)
		{
			fRadius = m_SpellEffectRadius[i].radius;
			return true;
		}
	}
	return false ;
}