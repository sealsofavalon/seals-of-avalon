#ifndef SPELLDB_H
#define SPELLDB_H

#include "DBFile.h"


struct stSpellInfo
{
	unsigned int uiId;
	unsigned int uiAttribute;
	unsigned int uiTargets;
	unsigned int uiTargetCreatureType;
	unsigned int uiRecoverTime;
	unsigned int uiCastIndex;
	unsigned int uiCategory;
	unsigned int uiCategoryRecoveryTime;
	unsigned int uiMaxLevel;
	unsigned int uiBaseLevel;
	unsigned int uiSpellLevel;
	unsigned int uiDurationIndex;
	unsigned int uiRangeIndex;
	unsigned int uiVisualKit;
	unsigned int uiMaxTargets;
	unsigned int uiManaCost;
	unsigned int uiStartRecoveryTime;

	float	flyspeed;

	unsigned int uiEffect[3];
	unsigned int uiEffectApplyAuraName[3];
	unsigned int uiEffectBasePoint[3];
	unsigned int uiEffectMiscValue[3];
	unsigned int uiImplicitTarget[3];

	unsigned int uiEffectRadius[3];

	std::string name;
	std::string strIcon;
	std::string description;

	unsigned int uiSpellGroupType;
};
struct stSpellRadius
{
	uint32 index ;
	float radius;
};
struct stSpellRangeInfo
{
	uint32	entry;
	double	minrange;
	double	maxrange;
};

struct stSpellDurationInfo
{
	uint32 entry;
	uint32 duration1;
	uint32 duration2;
	uint32 duration3;
};

struct stQuerySpellInfo
{
	uint32 SpellId;
	uint32 AllowableClass;
	uint32 AllowableLevel;
};


class CSpellDB : public CDBFile
{
public:
	virtual bool Load(const char* pcName);
	stSpellInfo* GetSpellInfo(unsigned int uiSpellId);
	std::map<unsigned int, stSpellInfo>& GetSpellInfos();
	bool IsMountSpell(uint32 uiSpellId);
	bool IsShapeSpell(uint32 uiSpellId);
	bool IsSkillBook(uint32 uiSpellId);
	virtual void DoInternational();
	bool GetSpellCDTIncrease(uint32 uiSpellId, uint32& cdtspellid, uint32& cdtincrease);
private:
	std::map<unsigned int, stSpellInfo> m_SpellInfo;
};

class CSpellRangeDB : public CDBFile
{
public:
	virtual bool Load(const char* pcName);
	stSpellRangeInfo* GetSpellRangeInfo(unsigned int uiEntryIdx);

private:
	std::map<uint32, stSpellRangeInfo> m_SpellRangeInfo;
};

class CSpellDurationDB : public CDBFile
{
public:
	virtual bool Load(const char* pcName);
	stSpellDurationInfo* GetSpellDurationInfo(unsigned int uiEntryIdx);

private:
	std::map<uint32, stSpellDurationInfo> m_SpellDurationInfo;
};
class CQuerySpellDB : public CDBFile
{
public:
	virtual bool Load(const char* pcName);
	bool GetQuerySpellList(std::vector<stQuerySpellInfo>& vList);

	std::vector<stQuerySpellInfo> m_QuerySpellInfo;
};
class CSpellRadiusDB : public CDBFile
{
public:
	virtual bool Load(const char* pcName);
	bool GetRadius(ui32 index, float& fRadius);

	std::vector<stSpellRadius> m_SpellEffectRadius;
};

extern CSpellDB* g_pkSpellInfo;
extern CSpellRangeDB* g_pkSpellRangeInfo;
extern CSpellDurationDB* g_pkSpellDurationInfo;
extern CSpellRadiusDB* g_pkSpellRadiusInfo;
extern CQuerySpellDB* g_pkQuerySpellInfo;

#endif

