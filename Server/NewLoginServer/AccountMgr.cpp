#include "stdafx.h"

CAccountManager lAccountMgr;
void CAccountManager::Destroy()
{
	boost::mutex::scoped_lock lock( m_LockUser );
	for(Account_MAP::iterator iter = m_auccountmap.begin(); iter != m_auccountmap.end(); ++iter)
	{
		account* pUser = iter->second;
		delete pUser;
	}
	m_auccountmap.clear();

	for(BanedOrMuted_MAP::iterator it = m_ban_mute_map.begin(); it != m_ban_mute_map.end(); ++it)
	{
		BanedOrMuted* data = it->second;
		delete data;
	}
	m_ban_mute_map.clear();
}
bool CAccountManager::Init()
{
	return LoadBanMute();
}

bool CAccountManager::LoadBanMute()
{
	MyLog::log->info("loading account save data...");
	QueryResult* result = RealmDB.Query("select * from account_save");
	if(result)
	{
		do
		{
			Field* f = result->Fetch();
			if( f )
			{
				uint32 acct = f[0].GetUInt32();
				BanedOrMuted* data = new BanedOrMuted;
				data->m_Baned = f[1].GetUInt32();
				data->m_Muted = f[2].GetUInt32();
				m_ban_mute_map[acct] = data;
			}
		}
		while( result->NextRow() );
		MyLog::log->info("loading account save data count[%d]...ok", result->GetRowCount());

		delete result;
	}
	return true;
}
BanedOrMuted* CAccountManager::Findbanmute(ui32 accid)
{
	BanedOrMuted_MAP::iterator it = m_ban_mute_map.find(accid);
	if (it != m_ban_mute_map.end())
	{
		return it->second;
	}
	return NULL ;
}
void CAccountManager::AddBan(ui32 accid, ui32 ban)
{
	account* paccount = FindAccount(accid);
	if (paccount)
	{
		paccount->m_Baned = ban;
	}

	BanedOrMuted* pData =  Findbanmute( accid);
	if (pData)
	{
		pData->m_Baned = ban;
	}else
	{
		BanedOrMuted* newData = new BanedOrMuted;
		newData->m_Baned = ban;
		newData->m_Muted = 0;
		m_ban_mute_map.insert(BanedOrMuted_MAP::value_type(accid, newData));
	}


}
void CAccountManager::AddMute(ui32 accid, ui32 mute)
{
	account* paccount = FindAccount(accid);
	if (paccount)
	{
		paccount->m_Muted = mute;
	}

	BanedOrMuted* pData =  Findbanmute( accid);
	if (pData)
	{
		pData->m_Muted = mute;
	}else
	{
		BanedOrMuted* newData = new BanedOrMuted;
		newData->m_Baned = 0;
		newData->m_Muted = mute;
		m_ban_mute_map.insert(BanedOrMuted_MAP::value_type(accid, newData));
	}
}
account* CAccountManager::FindAccount(ui32 nAccountID)
{
	Account_MAP::iterator it = m_auccountmap.find(nAccountID);
	if (it != m_auccountmap.end())
		return it->second;
	return NULL;
}
void CAccountManager::AddAccount(GateSocket* gt,ui32 acc, uint8 ischild, std::string& gmflag)
{
	account* p = FindAccount(acc);
	if (!p)
	{
		p = new account;
		p->m_nAccountID = acc;
		p->m_GMFlags = gmflag;
		p->m_bFangchenmiAccount = ischild;
		p->m_OnlineTime = (ui32)UNIXTIME ;
		AddAccount(p);
	}else
	{
		p->m_OnlineTime = (ui32)UNIXTIME;
	}
	p->m_pGTSocket = gt;
}
void CAccountManager::AddAccount(account* pUser)
{
	m_auccountmap[pUser->m_nAccountID] = pUser;
	BanedOrMuted* userbandata = lAccountMgr.Findbanmute(pUser->m_nAccountID);
	if (userbandata)
	{
		pUser->m_Baned = userbandata->m_Baned;
		pUser->m_Muted = userbandata->m_Muted;
	}else
	{
		pUser->m_Baned = 0;
		pUser->m_Muted = 0;
	}
	LoadChenmiTime(pUser, pUser->m_bFangchenmiAccount);
}
void CAccountManager::RemoveAccount(ui32 nAccountID)
{
	Account_MAP::iterator it = m_auccountmap.find(nAccountID);
	if (it != m_auccountmap.end())
	{	
		RemoveAccount(it->second);
	}
	
}
void CAccountManager::RemoveAccountByGateSocket(GateSocket* p)
{
	std::vector<account*> vp ;
	Account_MAP::iterator iter = m_auccountmap.begin();
	for(; iter != m_auccountmap.end(); ++iter)
	{
		account* pAccount = iter->second;
		if (pAccount->m_pGTSocket == p)
		{
			vp.push_back(pAccount);
		}
	}	
	for (int i = 0; i < vp.size(); i++)
	{
		RemoveAccount(vp[i]);
	}
}
void CAccountManager::RemoveAccount(account* pUser)
{
	SaveChenmiTime(pUser->m_nAccountID, (ui32)UNIXTIME - pUser->m_OnlineTime);

	Account_MAP::iterator it = m_auccountmap.find(pUser->m_nAccountID);
	if (it != m_auccountmap.end())
		m_auccountmap.erase(it);
	pUser->Reset();
	delete pUser;
	
}

void CAccountManager::LoadChenmiTime( account* pUser, bool isChild )
{
	ui32 acct = pUser->m_nAccountID;

	QueryResult* qr = RealmDB.Query( "select * from fangchenmi where acct = %u", acct );
	if( qr )
	{
		Field* f = qr->Fetch();
		pUser->m_LastLogout = f[1].GetUInt32();
		if( (uint32)UNIXTIME - pUser->m_LastLogout > 5*60*60 )
		{
			RealmDB.Execute( "update fangchenmi set chenmicount = 0 where acct = %u", acct );
			pUser->m_ChenmiTime = 0;
		}
		else
			pUser->m_ChenmiTime = f[3].GetUInt32();

		if( f[2].GetUInt32() != (isChild ? 1 : 0) )
		{
			if( isChild )
				RealmDB.Execute( "update fangchenmi set isfangchenmi = 1 where acct = %u", acct );
			else
				RealmDB.Execute( "update fangchenmi set isfangchenmi = 0, chenmicount = 0 where acct = %u", acct );
		}
		delete qr;
	}
	else
	{
		RealmDB.Execute( "replace into fangchenmi values(%u,%u,%d,0,0,0)", acct, (uint32)UNIXTIME, (isChild ? 1 : 0) );
	}

}
void CAccountManager::SaveChenmiTime(ui32 acct, uint32 seconds )
{
	RealmDB.Execute( "update fangchenmi set chenmicount = chenmicount + %u, lastlogin = %u where acct = %u", seconds, (uint32)UNIXTIME, acct );
}
