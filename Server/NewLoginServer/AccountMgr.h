#ifndef __ACCOUNT_MGR_H__
#define __ACCOUNT_MGR_H__

class GateSocket;

struct account
{
	account()
	{
		Reset();
	};
	void Reset()
	{
		m_nAccountID = 0;
		m_tLastMsgTime = 0;
		m_pGTSocket = NULL;
		m_bFangchenmiAccount = false;
		m_LastLogout = 0;
		m_OnlineTime = 0;
	}

	int    m_nAccountID;
	time_t m_tLastMsgTime;//上次消息的处理时间
	bool m_bFangchenmiAccount;//是否是未成年人账号
	uint32 m_LastLogout;
	uint32	m_ChenmiTime;//沉迷的游戏时间累积
	uint32  m_OnlineTime;//此次上线时间
	string m_GMFlags;
	ui32	m_Muted;
	ui32	m_Baned;
	GateSocket* m_pGTSocket;
};
struct BanedOrMuted
{
	ui32	m_Muted;
	ui32	m_Baned;
};
typedef hash_map<ui32, account*> Account_MAP;
typedef map<ui32, BanedOrMuted*> BanedOrMuted_MAP;
class CAccountManager
{
public:
	bool Init();
	account* FindAccount(ui32 nAccountID);
	void AddAccount(account* pUser);
	void AddAccount(GateSocket* gt, ui32 acc, uint8 ischild, std::string& gmflag);
	void RemoveAccount(ui32 nAccountID);
	void RemoveAccount(account* pUser);
	void RemoveAccountByGateSocket(GateSocket* p);
	void Destroy();

	// 禁止账号和禁言账号
	bool LoadBanMute();
	BanedOrMuted* Findbanmute(ui32 accid);
	void AddBan(ui32 accid, ui32 ban);
	void AddMute(ui32 accid, ui32 mute);

	//防沉迷
	void LoadChenmiTime( account* pUser, bool isChild );
	void SaveChenmiTime( ui32 acct, uint32 seconds );

protected:
private:
	boost::mutex m_LockUser;
	Account_MAP m_auccountmap;
	BanedOrMuted_MAP m_ban_mute_map;
};

extern CAccountManager lAccountMgr;
#endif