// NewLoginServer.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include "user_session.h"
#include "ls_server.h"
#include "gate/GateListenServer.h"

#include <iostream>
#include <fstream>

ls_server* g_user_server = NULL;
// cs_client* g_cs_client = NULL;
uint32 g_UNIXTIME = 0;
uint32 g_Ver = 0;
Database* Database_Realm = NULL;

zip_compress_strategy cs;

bool init()
{
	if( !MyLog::Init() )
	{
		printf( "cannot open file log.conf\n" );
		return false;
	}
	FILE* fp = fopen( "ls.conf", "r" );
	if( !fp )
	{
		MyLog::log->error( "cannot open file ls.conf" );
		return false;
	}
	
	


#ifdef _WIN32
	net_global::init_net_service( 8, 10, &cs, true, 100 );
#else
	net_global::init_net_service( 8, 10, &cs, true, 1024 );
#endif

	int port = 0, gsport = 0, acport = 0;
	
	int dbport = 0, dbType = 0;
	char dbip[32] = { 0 };
	char user[32] = { 0 };
	char pwd[32] = { 0 };
	char dbname[32] = { 0 };


	char acip[32] = { 0 };
	char temp[32] = { 0 };
	fscanf( fp, "%s %d\n", temp, &port );
	fscanf( fp, "%s %d\n", temp, &gsport);
	fscanf( fp, "%s %s %s %s %s %d %d\n", temp, dbip, user, pwd ,dbname, &dbport, &dbType);
	fscanf( fp, "%s %s %d\n", temp, acip, &acport );
	fscanf( fp, "%s %d\n", temp, &g_Ver);
	fclose( fp );

	/*
	if( ac_client_init( acip, acport ) )
		MyLog::log->error( "connect account center failed! please wait for reconnect!" );
	else
		MyLog::log->info( "connect account center succeed!" );
	*/

	Database_Realm = Database::CreateDatabaseInterface(1);
	if( !RealmDB.Initialize( dbip, dbport, user, pwd, dbname, 5 , 16384 ) )
	{
		MyLog::log->error("sql : Main database initialization failed. Exiting." );
		return false;
	}

	g_user_server = new ls_server;
	g_gate_listen_server = new GateListenServer;
#ifdef _WIN32
	if( !g_user_server->create( port, 50, 4 ) )
#else
	if( !g_user_server->create( port, 500, 10 ) )
#endif
	{
		MyLog::log->error( "listen port:%d failed!", port );
		return false;
	}
	MyLog::log->info( "listen port:%d succeed!", port );
	//g_cs_client = new cs_client;
	//g_cs_client->connect( csip, csport );
	//g_cs_client->set_reconnect( true );

	if (!lAccountMgr.Init())
	{
		MyLog::log->error("init account mgr failed");
		return false ;
	}
	
	if( !g_gate_listen_server->create( gsport, 30, 4 ) )
	{
		MyLog::log->error("init gate_listen_server listen port:%d failed");
		return false;
	}
	MyLog::log->info( "gate_listen_server listen port:%d", gsport );
	MyLog::log->info( "login server has started successfully" );
	return true;
}

void release()
{
	lAccountMgr.Destroy();
	//ac_client_release();
	net_global::free_net_service();
	safe_delete( g_user_server );
	safe_delete( g_gate_listen_server);
	RealmDB.Shutdown();
}

static void main_loop()
{
	while( true )
	{
		g_UNIXTIME = (uint32)time( NULL );
		g_localTime = *localtime(&UNIXTIME);
		RealmDB.QueryTaskRun();
		g_user_server->run();
		g_gate_listen_server->run();
	}
}

int main()
{
	if( init() )
		main_loop();
	release();
	return 0;
}

