#include "../stdafx.h"
#include "GateListenServer.h"
#include "GateSocket.h"

GateListenServer* g_gate_listen_server;
GateListenServer::GateListenServer() : tcp_server( 1 )
{
	for (int i = 0; i < 64; i++)
	{
		m_GateSocket[i] = NULL;
	}

}

void GateListenServer::stop()
{
	tcp_server::stop();
}

tcp_session* GateListenServer::create_session()
{
	return new GateSocket( *net_global::get_io_service() );
}
void GateListenServer::AddGateSocket(GateSocket* pSocket)
{
	if( pSocket->m_nGateID > 0 && pSocket->m_nGateID < 64)
		m_GateSocket[pSocket->m_nGateID] = pSocket;
	else
		assert(0);
}
GateSocket* GateListenServer::FindFreeGateSocket()
{
	GateSocket* p = NULL;
	for (int i = 0; i < 64; i++)
	{
		if (m_GateSocket[i] )
		{
			if (!p)
			{
				p = m_GateSocket[i];
			}else
			{
				if (p->m_onlines > m_GateSocket[i]->m_onlines)
				{
					p = m_GateSocket[i];
				}
			}
		}
	}

	return p ;
}
void GateListenServer::RemoveGateSocket(GateSocket* pSocket)
{
	m_GateSocket[pSocket->m_nGateID] = NULL;
}