#ifndef __GTLISTENSCOCKET_H__
#define __GTLISTENSCOCKET_H__

class GateSocket;

class GateListenServer : public tcp_server
{
public:
	GateListenServer();
	~GateListenServer(){;}

	virtual void stop();
	virtual tcp_session* create_session();

	void AddGateSocket(GateSocket* pSocket);
	void RemoveGateSocket(GateSocket* pSocket);
	GateSocket* FindFreeGateSocket();
private:
	GateSocket* m_GateSocket[64];
};

extern GateListenServer* g_gate_listen_server;

#endif
