#include "../stdafx.h"
#include "GateListenServer.h"
#include "GateSocket.h"

GateSocket::GateSocket( boost::asio::io_service& is ) : tcp_session( is )
{
	m_GateServerIP = 0;
	m_GateServerPort = 0;
	m_GateListenClientPort = 0;
	m_onlines = 0;
	m_nGateID = 0;
}

GateSocket::~GateSocket(void)
{
}

void GateSocket::on_accept( tcp_server* p )
{
	m_strIP = this->get_remote_address_string();
	m_GateServerIP = this->get_remote_address_ui();
	m_GateServerPort = this->get_remote_port();
	tcp_session::on_accept( p );
	MyLog::log->info( "add gate ip:%s", m_strIP.c_str());
}

void GateSocket::on_close( const boost::system::error_code& error )
{
	lAccountMgr.RemoveAccountByGateSocket(this);
	MyLog::log->notice( "gate disconnected! error_no:%d error_message:%s", error.value(), error.message().c_str() );
	g_gate_listen_server->RemoveGateSocket(this);
	tcp_session::on_close( error );
}

void GateSocket::proc_message( const message_t& msg )
{
	CPacketUsn pakTool(msg.data, msg.len);

	switch(pakTool.GetProNo())
	{
	case MSG_GT2LS::MSG_USER_ONLINE:
		{
			MSG_GT2LS::stUserOnline MsgRecv;
			pakTool>>MsgRecv;
			m_onlines++;
			account* puser= lAccountMgr.FindAccount(MsgRecv.acct);
			if (!puser)
			{
				puser = new account;
				puser->m_nAccountID = MsgRecv.acct;
				puser->m_OnlineTime = (ui32)UNIXTIME;
				puser->m_bFangchenmiAccount = false;
				lAccountMgr.AddAccount(puser);
			}
			puser->m_OnlineTime = (ui32)UNIXTIME;
			puser->m_pGTSocket = this;

		}break;
	case MSG_GT2LS::MSG_USER_OFFLINE:
		{
			MSG_GT2LS::stUserOffline MsgRecv;
			pakTool>>MsgRecv;
			m_onlines--;
			lAccountMgr.RemoveAccount(MsgRecv.acct);

		}break;
	case MSG_GT2LS::MSG_UPDATE_USER_INFO:
		{
			MSG_GT2LS::stUpdateUserInfo MsgRecv;
			pakTool>>MsgRecv;

			for (int i = 0; i < MsgRecv.users.size(); i++)
			{
				lAccountMgr.AddAccount(this, MsgRecv.users[i].acct, MsgRecv.users[i].ischild, MsgRecv.users[i].gmflags);
			}

			m_onlines += MsgRecv.users.size();
		}break;
	case MSG_GT2LS::MSG_MUTE_REQ:
		{
			MSG_GT2LS::stMuteReq MsgRecv;
			pakTool >> MsgRecv;
			lAccountMgr.AddMute(MsgRecv.acct, MsgRecv.muted);		
		}
		break;
	case MSG_GT2LS::MSG_BAN_REQ:
		{
			MSG_GT2LS::stBanReq MsgRecv;
			pakTool >> MsgRecv;
			lAccountMgr.AddBan(MsgRecv.acct, MsgRecv.banned);	
		}break;
	case MSG_GT2LS::MSG_INIT_REQ:
		{
			MSG_GT2LS::stInitReq MsgRecv;
			pakTool >> MsgRecv;
			m_GateListenClientPort = MsgRecv.port;
			m_GateListenClientIP = MsgRecv.ip;
			m_nGateID = MsgRecv.gateid;
			g_gate_listen_server->AddGateSocket(this);
		}break;
	}
}
void GateSocket::PostSend( PakHead& msg )
{
	CPacketSn cps;
	cps << msg;
	cps.SetProLen();
	send_message( cps.GetBuf(), cps.GetSize() );
}