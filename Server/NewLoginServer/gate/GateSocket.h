#ifndef __GTSCOCKET_H__
#define __GTSCOCKET_H__

class GateSocket : public tcp_session
{
public:
	GateSocket( boost::asio::io_service& is );
	~GateSocket(void);

	virtual void on_accept( tcp_server* p );
	virtual void on_close( const boost::system::error_code& error );
	virtual void proc_message( const message_t& msg );
	virtual void PostSend( PakHead& msg );
protected:
public:
	string m_strIP;
	ui32 m_GateServerIP;
	ui16 m_GateServerPort;

	int m_nGateID;
	ui32 m_onlines;

	ui32 m_state;

// Gate server listen client ip and port 
	ui16 m_GateListenClientPort;
	std::string m_GateListenClientIP;

	std::set<ui32> m_GateAccount;

};

#endif
