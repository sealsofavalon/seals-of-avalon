#include "stdafx.h"
#include "ls_server.h"
#include "user_session.h"

ls_server::ls_server(void) : tcp_server( 0 ), m_last_check_time( 0 )
{

}

ls_server::~ls_server(void)
{
}

tcp_session* ls_server::create_session()
{
	return new user_session;
}

void ls_server::run()
{
	tcp_server::run();
	if( m_unix_time - m_last_check_time > 60 * 5 )
	{
		m_ip_authfail.clear();
		m_last_check_time = m_unix_time;
	}
}

void ls_server::auth_fail( uint32 ip, std::string sip )
{
	std::map<uint32, uint32>::iterator it = m_ip_authfail.find( ip );
	if( it != m_ip_authfail.end() )
	{
		if( ++it->second > 20 )
		{
			add_ban_ip( ip, 120, net_global::BAN_REASON_TOO_MANY_AUTH_FAIL );
			MyLog::log->warn( "IP:[%s] auth failed over 3 times in 5 minutes. Ban it for 120 seconds.", sip.c_str() );
			m_ip_authfail.erase( it );
		}
	}
	else
		m_ip_authfail.insert( std::map<uint32, uint32>::value_type( ip, 1 ) );
}
