#ifndef _LS_SERVER_HEAD
#define _LS_SERVER_HEAD

class ls_server : public tcp_server
{
public:
	ls_server(void);
	~ls_server(void);

public:
	virtual tcp_session* create_session();

	virtual void run();
	void auth_fail( uint32 ip, std::string sip );

private:
	std::map<uint32, uint32> m_ip_authfail;
	uint32 m_last_check_time;
};

extern ls_server* g_user_server;

#endif