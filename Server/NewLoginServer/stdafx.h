// stdafx.h : 标准系统包含文件的包含文件，
// 或是经常使用但不常更改的
// 特定于项目的包含文件
//
#ifdef _WIN32
#pragma once
#include "targetver.h"
#endif

#include "Common.h"
#include "../../SDBase/Public/TypeDef.h"
#include "../Common/share/Database/Database.h"
#include "../Common/share/Database/MySQLDatabase.h"
#include "../Common/Com/Utility.h"
#include "../../new_common/Source/net/tcpserver.h"
#include "../../new_common/Source/net/tcpclient.h"
#include "../../new_common/Source/compression/zlib/ZlibCompressionStrategy.h"
#include "../../new_common/Source/log4cpp-1.0/MyLog.h"
#include "../ac_client/Source/ac_client_interface.h"
#include "../Common/Protocol/GT2LS.h"
#include "../Common/Protocol/LS2GT.h"
#include "../../SDBase/Protocol/C2LS.h"
#include "../../SDBase/Protocol/LS2C.h"

// TODO: 在此处引用程序需要的其他头文件

SERVER_DECL extern Database* Database_Realm;
#define RealmDB (*Database_Realm)

#include "AccountMgr.h"

extern time_t UNIXTIME;		/* update this every loop to avoid the time() syscall! */
extern tm g_localTime;
