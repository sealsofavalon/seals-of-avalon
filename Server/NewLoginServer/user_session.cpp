#include "stdafx.h"
#include "user_session.h"
#include "ls_server.h"
#include "gate/GateSocket.h"
#include "gate/GateListenServer.h"
#include "../../SDBase/Protocol/C2S.h"
#include "../../new_common/Source/utilities/task_thread_pool.h"

/*
class auth_task : public task
{
public:
	virtual void execute()
	{
		if( s->is_valid() && s->is_connected() )
			s->auth_req( acct, pwd );
	}
	virtual void end()
	{

	}
	virtual int get_thread_index()
	{
		return s->get_thread_index();
	}
	user_session* s;
	std::string acct;
	std::string pwd;
	uint32 idx;
};
*/

extern uint32 g_UNIXTIME;

user_session::user_session(void) : tcp_session( *net_global::get_io_service() )
{
	m_sessionid = 0;
	m_accid = 0;
	m_tablekey = 0;
	m_connecttime = 0;
	m_is_valid_connection = false;
	set_need_check_action_time( true );
}

user_session::~user_session(void)
{
}

void user_session::on_accept( tcp_server* p )
{
	m_state = JUST_CONNECTED;
	//delay_close_if_no_data( 8 );
	tcp_session::on_accept( p );
	//m_connecttime = g_UNIXTIME;
}

void user_session::on_close( const boost::system::error_code& error )
{
	tcp_session::on_close( error );
	//MSG_LS2CS::stReturn2Login Msg;
}

void user_session::proc_message( const message_t& msg )
{
try{

	CPacketUsn pakTool( (char*)msg.data, msg.len );

	switch(pakTool.GetProNo())
	{
	case MSG_C2S::CMSG_PING:
		{
		}
		break;
	case MSG_C2LS::VERSION_REQ:
		{
			if( m_state != JUST_CONNECTED )
			{
				MyLog::log->error( "recv MSG_C2LS::VERSION_REQ twice!" );
				return;
			}
			m_is_valid_connection = true;

			extern uint32 g_Ver;
			MSG_C2LS::stVersionReq MsgRecv;pakTool >> MsgRecv;
			if(MsgRecv.nVersion != g_Ver)
			{
				MSG_LS2C::stLoginAck ack;
				ack.nStat = MSG_LS2C::stLoginAck::LOGIN_VERSION_NOT_MATCH;
				send_packet(ack);
				MyLog::log->debug( "act[%u] ver[%d] not match[%d]!", m_accid, MsgRecv.nVersion, g_Ver );
				return;
			}

			m_tablekey = generate_table_key();

			MSG_LS2C::stVersionAck ack;
			ack.encrypt_table = m_tablekey;
			send_packet( ack );

			m_state = SENT_TABLE_KEY;

			MyLog::log->notice( "recv MSG_C2LS::VERSION_REQ" );
		}
		break;
	case MSG_C2LS::RETURN_LOGIN:
		{ 
			lAccountMgr.RemoveAccount(m_accid);
		}
		break;
	case MSG_C2LS::GROUP_SEL_REQ:
		{
		}
		break;
	case MSG_C2LS::LOGIN_REQ:
		{
			MSG_C2LS::stLoginReq req;
			pakTool >> req;
			/*
			auth_task* t = new auth_task;
			t->s = this;
			t->acct = req.account;
			t->pwd = req.password;
			m_father->push_task( t );
			*/
			this->auth_req( req.account, req.password );
			MyLog::log->notice( "IP:[%s] Mac:[%s] Account:[%s] try to logon", this->get_remote_address_string().c_str(), req.mac_address.c_str(), req.account.c_str() );
		}
		break;
	default :
		{
			MyLog::log->error( "recv unknown message:%u, close socket!!! IP:[%s]", pakTool.GetProNo(), get_remote_address_string().c_str() );
			this->close();
			net_global::write_close_log( "IP:[%s] recv unknown message:%u", this->get_remote_address_string().c_str(), pakTool.GetProNo() );
			return;
		}
		break;
	}
}
catch( const packet_exception& e )
{
	MyLog::log->error( "recv hack packet, exception:%s, close socket!!! IP:[%s]", e.what(), get_remote_address_string().c_str() );
	this->close();
	net_global::write_close_log( "IP:[%s] recv hack packet, exception:%s, close it", this->get_remote_address_string().c_str(), e.what() );
}

}

void user_session::auth_req( const std::string& acct, const std::string& pwd )
{
	if( m_state != SENT_TABLE_KEY )
	{
		MyLog::log->error( "recv MSG_C2LS::LOGIN_REQ have not sent table key before auth request!!!" );
		this->close();
		net_global::write_close_log( "IP:[%s] recv MSG_C2LS::LOGIN_REQ have not sent table key before auth request!", this->get_remote_address_string().c_str() );
		return;
	}
	m_state = AUTH_FAILED;

	MSG_LS2C::stLoginAck ack;

	ac_account_t acc;
	memset( &acc, 0, sizeof( acc ) );
	std::string md5pwd;
	decrypt_string( pwd, md5pwd, m_tablekey );
	int n = get_tick_count();
	//ac_result_t ret = ac_client_md5auth( acct.c_str(), md5pwd.c_str(), &acc );
	ac_result_t ret = AC_CLIENT_OK;
	QueryResult* qr = RealmDB.Query( "select id, password, gm_flag, id_card, safe_lock_flag from account where name = '%s'", acct.c_str() );
	if( qr )
	{
		Field* f = qr->Fetch();
		if( 0 != stricmp( f[1].GetString(), md5pwd.c_str() ) )
			ret = AC_CLIENT_ERROR_WRONG_PWD;
		else
		{
			acc.id = f[0].GetUInt32();
			strcpy( acc.gm_flag, f[2].GetString() );
			strcpy( acc.id_card, f[3].GetString() );
			acc.safe_lock_flag = f[4].GetUInt32();
		}
		delete qr;
	}
	else
		ret = AC_CLIENT_ERROR_WRONG_ACC;

	if( ret != AC_CLIENT_OK )
		g_user_server->auth_fail( get_remote_address_ui(), get_remote_address_string() );

	MyLog::log->notice( "ac_client_md5auth spend time:%d", get_tick_count() - n );
	if( ret == AC_CLIENT_OK )
	{
		if( !acc.safe_lock_flag )
		{
			MyLog::log->notice( "recv MSG_C2LS::LOGIN_REQ auth ok account = %s!", acct.c_str() );
			m_accid = acc.id;
			MyLog::log->info( "ls-login-account[%s] ok", acct.c_str() );
		}
		else
		{
			ack.nStat = MSG_LS2C::stLoginAck::LOGIN_ACCOUNT_LOCKED;
			send_packet( ack );
			MyLog::log->info("ls-login-account[%s] locked", acct.c_str());
			return;
		}
	}
	else if( ret == AC_CLIENT_ERROR_WRONG_PWD )
	{
		ack.nStat = MSG_LS2C::stLoginAck::LOGIN_PASSWORD_ERROR;
		send_packet( ack );
		MyLog::log->info("ls-login-account[%s] wrong password", acct.c_str());
		return;
	}
	/*
	else if( ret == AC_CLIENT_ERROR_DB )
	{
		ack.nStat = MSG_LS2C::stLoginAck::LOGIN_DATABASE_EXCEPTION;
		send_packet( ack );
		MyLog::log->notice( "recv MSG_C2LS::LOGIN_REQ database exception failed account id = %s!", acct.c_str() );
		MyLog::log->info("ls-login-account[%s] db error", acct.c_str());
		return;
	}
	else if( ret == AC_CLIENT_ERROR_DISCONNECTED )
	{
		ack.nStat = MSG_LS2C::stLoginAck::LOGIN_SERVER_CANNT_CONNECT;
		send_packet(ack);
		MyLog::log->notice( "recv MSG_C2LS::LOGIN_REQ server cann't connect id = %s!", acct.c_str() );
		return;
	}
	*/
	else
	{
		ack.nStat = MSG_LS2C::stLoginAck::LOGIN_USER_NOT_FOUND;
		send_packet( ack );
		MyLog::log->notice( "recv MSG_C2LS::LOGIN_REQ user not found account id = %s!", acct.c_str() );
		MyLog::log->info("ls-login-account[%s] not found", acct.c_str());
		return;
	}

	m_state = AUTH_PASS;
	ack.acct = m_accid;
	
	account* pUser = lAccountMgr.FindAccount(m_accid);
	if (pUser)
	{
		// online  kick
		if (pUser->m_pGTSocket)
		{
			MSG_LS2GT::stKickoutUser MsgSend;
			MsgSend.acct = pUser->m_nAccountID;
			pUser->m_pGTSocket->PostSend(MsgSend);
			lAccountMgr.RemoveAccount(pUser);
			net_global::write_close_log( "Recv LoginServer::stLoginReq but account:[%d(%s)] already online, kick it", pUser->m_nAccountID, acc.name);
			
		}else
		{
			ack.expire = pUser->m_Baned;
			ack.nStat = MSG_LS2C::stLoginAck::LOGIN_USER_ONLINE;
			lAccountMgr.RemoveAccount(pUser);
			send_packet(ack);
			return;
		}
	}
	
	// add new user.
	account* newuser = new account;
	newuser->m_nAccountID = m_accid;
	newuser->m_tLastMsgTime = time(NULL);
	newuser->m_GMFlags = acc.gm_flag;
	//newuser->m_AccountName = acc.name;
	
	//计算防沉迷
	acc.id_card[19] = '0';
	std::string stridcard = acc.id_card;
	int nIDLength = strlen(acc.id_card);
	int nYear,nMon,nDay;
	bool bFangchenmi = false;
	if (nIDLength == 15)
	{
		nYear	= atol(stridcard.substr(6, 2).c_str()) + 1900;
		nMon	= atol(stridcard.substr(8, 2).c_str());
		nDay	= atol(stridcard.substr(10,2).c_str());
	}
	else if(nIDLength == 18)
	{
		nYear	= atol(stridcard.substr(6, 4).c_str());
		nMon	= atol(stridcard.substr(10, 2).c_str());
		nDay	= atol(stridcard.substr(12,2).c_str());
	}
	else
		bFangchenmi = true;

	//根据身份证计算出生年月
	tm t = g_localTime;
	if(!bFangchenmi)
	{
		t.tm_year = nYear-1900;
		t.tm_mon = nMon;
		t.tm_mday = nDay;
		time_t birth = mktime(&t);
		bFangchenmi = g_UNIXTIME - birth < 18*365*24*60*60;
	}

	newuser->m_bFangchenmiAccount = bFangchenmi;
	lAccountMgr.AddAccount(newuser);
	ack.expire = newuser->m_Baned;
	if (ack.expire > (uint32)UNIXTIME )
	{
		ack.nStat = MSG_LS2C::stLoginAck::LOGIN_ACCOUNT_LOCKED;
	}else
	{
		ack.nStat = MSG_LS2C::stLoginAck::LOGIN_OK;
	}
	
	
	if(ack.nStat == MSG_LS2C::stLoginAck::LOGIN_ACCOUNT_LOCKED)
	{
		MyLog::log->notice("account:[%d] locked\n expire:[%s]",m_accid, ConvertTimeStampToDataTime(ack.expire).c_str());
	}

	// msg to gate
	GateSocket* pkGate = g_gate_listen_server->FindFreeGateSocket();
	if (pkGate)
	{
		ack.tmp_password =rand();
		MSG_LS2GT::stLoginReq ack2gate;
		ack2gate.acct = m_accid;
		ack2gate.tmp_password = ack.tmp_password;
		ack2gate.ischild = newuser->m_bFangchenmiAccount;
		ack2gate.lastlogout = newuser->m_LastLogout;
		ack2gate.chenmitime = newuser->m_ChenmiTime;
		ack2gate.gmflags = newuser->m_GMFlags;
		pkGate->PostSend(ack2gate);
		
		ack.ip = pkGate->m_GateListenClientIP;
		ack.port = pkGate->m_GateListenClientPort;

	}else
	{
		ack.nStat = MSG_LS2C::stLoginAck::LOGIN_SERVER_CANNT_CONNECT;
	}
	
	// to client
	send_packet(ack);
	MyLog::log->debug("acct[%d][%s] is %s child", newuser->m_nAccountID, acc.name, bFangchenmi?"":"not a");
}

void user_session::reset()
{
	m_state = JUST_CONNECTED;
	m_sessionid = 0;
	m_accid = 0;
	m_tablekey = 0;
	m_connecttime = 0;
	m_is_valid_connection = false;
	tcp_session::reset();
}

void user_session::run()
{
	tcp_session::run();
	/*
	if( m_connecttime && is_connected() && g_UNIXTIME - m_connecttime >= 30 )
	{
		if( m_state == JUST_CONNECTED ||
			m_state == SENT_TABLE_KEY ||
			m_state == AUTH_FAILED )
		{
			MyLog::log->error( "IP:[%s] does not pass auth and connect for a long time, close socket!!!", get_remote_address_string().c_str() );
			this->close();
		}
	}
	*/
	if( m_connecttime && is_connected() && g_UNIXTIME - m_connecttime > 5 && !m_is_valid_connection )
	{
		boost::mutex::scoped_lock lock( m_father->m_proc_mutex );
		std::map<unsigned int, unsigned int>::iterator it = m_father->m_idleip.find( this->get_remote_address_ui() );
		if( it != m_father->m_idleip.end() )
		{
			if( ++it->second > 50 )
			{
				m_father->m_idleip.erase( it );
				m_father->add_ban_ip( this->get_remote_address_ui(), 3600, net_global::BAN_REASON_TOO_MANY_IDLE_CONNECTION );
			}
		}
		else
			m_father->m_idleip[this->get_remote_address_ui()] = 1;

		this->close();
		net_global::write_close_log( "IP:[%s] no login req in 5 seconds, m_recv_size = %d", this->get_remote_address_string().c_str(), this->m_recv_size );
	}
}

void user_session::send_packet( const PakHead& msg )
{
	CPacketSn cps;
	cps << msg;
	cps.SetProLen();
	send_message( cps.GetBuf(), cps.GetSize() );
}
