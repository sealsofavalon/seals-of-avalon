#ifndef _USER_SESSION_HEAD
#define _USER_SESSION_HEAD

class user_session : public tcp_session
{
public:
	enum auth_state_t
	{
		JUST_CONNECTED,
		SENT_TABLE_KEY,
		AUTH_FAILED,
		AUTH_PASS,
		SELECTING_GROUP
	};
	user_session(void);
	~user_session(void);

public:
	virtual void on_accept( tcp_server* p );
	virtual void on_close( const boost::system::error_code& error );
	virtual void proc_message( const message_t& msg );
	virtual void reset();
	virtual void run();

	void send_packet( const PakHead& msg );

	void auth_req( const std::string& acct, const std::string& pwd );

	auth_state_t m_state;
	uint32 m_sessionid;
	uint32 m_tablekey;
	int m_accid;

private:
	uint32 m_connecttime;
	uint32 m_is_valid_connection;
};

#endif 