// SerialGiftTool.cpp : 定义控制台应用程序的入口点。
//
#include "stdafx.h"
#include "SerialSystem.h"
#include <iostream>
#include "TestQueue.h"

static SerialSystem* sm_SerialSystem  = NULL ;
static clock_t start = 0;
static unsigned int stcount = 10;
static unsigned int sttype = 0 ;
static unsigned int curindex = 0 ;
static bool bCreateEnd = false ;
static bool bSaveed = false ;
static bool Quit = false ;

bool init()
{
	
	printf("初始化。。。\n");
	sm_SerialSystem = new SerialSystem;
	bool bInit = sm_SerialSystem->InitSystem();
	if (!bInit)
	{
		return false ;
	}
	printf("初始化OK........ \n");

	return true ;
	
}


void doAskCreate()
{
	if (constbegin)
	{
		return ;
	}
	sm_SerialSystem->ShowPrizeType();
	printf("请输入创建数量命令:Create type count\n例如需要创建1000个1类型激活码：Create 1 1000 \n");
	std::string input , strtype, strcount;
	std::cin>> input >> strtype >> strcount ;
	if (_stricmp(input.c_str(), "create") == 0)
	{
		sttype = atoi(strtype.c_str());
		stcount= atoi(strcount.c_str());

		if (sttype < Gold_N || sttype >= Gold_Max || !sm_SerialSystem->FindPrizeType(sttype))
		{
			printf("\n\n\n\n\n类型不合法\n\n");
			return ;
		}

		if (stcount <= 0 && stcount > 10000)
		{
			printf("\n\n\n\n\n输入创建数量不合法 %u,~%u\n", 0, 10000);
			return ;
		}

		printf("开始生成激活码\n");
		constbegin = true;
		sm_SerialSystem->CreateSerial(stcount,sttype);
	}
	
}
bool doCreate()
{
	while(constbegin)
	{
		clock_t time = clock();

		if (time  - start >= 200)
		{
			//printf("正在生成%u--%u...... \n", curindex* OnceCount ,curindex * OnceCount + OnceCount);
			printf("正在生成激活码.........\n");
			start = time;
			if (sm_SerialSystem->BeginCreate())
			{
				sm_SerialSystem->GetSerial();
				curindex++;

				if (sm_SerialSystem->EndCreate())
				{
					printf("生成结束...... \n");
					constbegin = false ;
					return true ;
				}

			}
		}
	}

	return false ;
}

void doAskSave()
{
	printf("是否保存到数据库Y/N\n");
	std::string input;
	std::cin>> input ;

	if (_stricmp(input.c_str(), "Y") == 0)
	{
		sm_SerialSystem->SaveAllSerial();
		bCreateEnd = false ;
	} 
	if(_stricmp(input.c_str(), "N") == 0)
	{
		printf("即将销毁数据\n\n\n\n");
		sm_SerialSystem->DeleteCreate();
		bCreateEnd = false;
	}
}

// for test

static TestQueue* pktestqueue = NULL;

void doAakQueue()
{
	printf("input queue num .");
	std::string input;
	std::cin>> input;
	ui32 stcount= atoi(input.c_str());
	if (pktestqueue)
	{
	}

}
int main(int argc, char* argv[])
{
	/*if (!sm_SerialSystem )
	{
		if (!init())
		{
			return 0 ;
		}
	}*/

	/*while(true)
	{
		doAskCreate();
		
		if (!bCreateEnd)
		{
			bCreateEnd = doCreate();
		}
		if (bCreateEnd)
		{
			doAskSave();
		}
		if (Quit)
		{
			sm_SerialSystem->ShutDown();
		}else
		{
			sm_SerialSystem->Run();
		}
	}*/
	if (!pktestqueue)
	{
		pktestqueue = new TestQueue;
	}

	while(true)
	{
		clock_t time = clock();
		if (time  - start >= 10)
		{
			start = time;
			if (pktestqueue)
			{
				pktestqueue->addteam();
			}
		}
	}

	//测试排队
	return 0 ;
}