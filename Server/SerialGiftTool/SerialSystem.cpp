#include "stdafx.h"
#include "SerialSystem.h"


Database* g_Realm_db = NULL;
unsigned int SerialSystem::m_type = 0;
unsigned int SerialSystem::m_count = 0;
SerialSystem::SerialSystem()
{

}
SerialSystem::~SerialSystem()
{

}
bool SerialSystem::InitSystem()
{
	FILE*fp = fopen("serial.conf", "r");
	if (!fp)
	{
		return false ;
	}
	char key[32];
	char value[32];
	std::string  ip, username, password, dataname;
	unsigned int port ;

	fscanf( fp, "%s %s\n", key, value );
	ip = value;
	fscanf( fp, "%s %s\n", key, value );
	dataname = value;
	fscanf( fp, "%s %s\n", key, value );
	username = value;
	fscanf( fp, "%s %s\n", key, value );
	password = value;
	fscanf( fp, "%s %u\n", key, &port );
	fclose( fp );

	MyLog::Init();
	unsigned int mysqlthreadsafe = mysql_thread_safe();
	if( !mysqlthreadsafe )
	{
		MyLog::log->error( "mysql lib is not a thread safe mode!!!!!!!!!" );
		return false;
	}
	Database::StartThread();
	g_Realm_db = new MySQLDatabase();

	//连接数据库 
	printf("连接数据库ip:%s\n", ip.c_str());
	if (!g_Realm_db->Initialize(ip.c_str(), port, username.c_str(), password.c_str(), dataname.c_str(), 5, 16384))
	{
		printf("连接数据库失败\n");
		return false ;
	}
	printf("连接数据库成功\n");
	if (!LoadSerial())
	{
		return false;
	}

	if (!LoadPrize())
	{
		return false ;
	}

	
	return true ;
}
bool SerialSystem::FindPrizeType(unsigned int t)
{
	std::map<unsigned __int32, Prize*>::iterator it = m_PrizeMap.find((unsigned __int32)t);
	if (it != m_PrizeMap.end())
	{
		return true ;
	}

	return false ;
}
bool SerialSystem::LoadPrize()
{
	printf("加载奖励类型....\n");
	QueryResult* qr = g_Realm_db->Query( "select * from cardserialprizelist" );
	if (qr)
	{
		do
		{
			Field* f = qr->Fetch();
			Prize*  pkPrize = new Prize;
			pkPrize->PrizeID = f[0].GetUInt32();
			pkPrize->Item[0] = f[1].GetUInt32();
			pkPrize->ItemCount[0] = f[2].GetUInt32();

			pkPrize->Item[1] = f[3].GetUInt32();
			pkPrize->ItemCount[1] = f[4].GetUInt32();

			pkPrize->Item[2] = f[5].GetUInt32();
			pkPrize->ItemCount[2] = f[6].GetUInt32();

			pkPrize->Item[3] = f[7].GetUInt32();
			pkPrize->ItemCount[3] = f[8].GetUInt32();

			pkPrize->des = f[9].GetString();
			m_PrizeMap.insert(std::map<ui32, Prize*>::value_type(pkPrize->PrizeID , pkPrize));
		}
		while( qr->NextRow() );

		delete qr;
		printf("加载奖励类型成功....\n");
		return true ;
	}
	return false ;
}
bool SerialSystem::LoadSerial()
{
	//
	printf("加载现有激活码列表...\n");
	QueryResult* qr = g_Realm_db->Query( "select * from cardserial" );
	if (qr)
	{
		do 
		{
			Field* f = qr->Fetch();
			Serial* pkSerial = new Serial;
			pkSerial->PrizeID = f[1].GetUInt32();
			pkSerial->IsUsed = f[2].GetBool();
			pkSerial->serial = f[3].GetString();

			mSerialMap.insert(std::map<std::string , Serial*>::value_type(pkSerial->serial, pkSerial));
			
		} while (qr->NextRow());

		printf("加载成功....\n");
		delete qr;
		return true ;
	}
	return false ;
}
void SerialSystem::ShowPrizeType()
{
	std::map<unsigned __int32, Prize*>::iterator it = m_PrizeMap.begin();
	printf("........................\n");
	printf("当前可用奖励类型：\n");
	
	for (it ; it != m_PrizeMap.end(); ++it)
	{
		if (it->second)
		{
			printf("%s:type = %u \n",it->second->des.c_str(), it->second->PrizeID);
		}
	}
	printf("........................\n\n");
}
bool SerialSystem::CreateSerial(unsigned int Count, unsigned int Type)
{
	if (Type >= Gold_Max)
	{
		printf("错误参数.... \n");
		return false ;
	}

	printf("生成激活码...... \n");
	
	
	m_type = Type;
	m_count = Count;
	return true ;
}
bool SerialSystem::Checkok(char* buf)
{
	//简单的限制下激活码
	//连续有5个字符一样去除
	//相同的字符有8个的去除
	unsigned int lianxucharcount = 0;
	unsigned int samechar = 0;
	for (int i = 1; i < SerialLen; i++)
	{
		samechar = 0;
		if (i < SerialLen -1)
		{
			if (buf[i] == buf[i+1])
			{
				lianxucharcount++; 
			}else
			{
				lianxucharcount = 0;
			}
		}

		for (int j = i; j < SerialLen; j++)
		{
			if (buf[i] == buf[j])
			{
				samechar ++ ;
			}
		}

		
		if (samechar >= 8)
		{
			return false ;
		}
		
	}

	if (lianxucharcount >= 7 )
	{
		return false ;
	}
	return true;
}
bool SerialSystem::Create()
{
	static char buf[SerialLen];
	for (int j = 0; j < SerialLen; j++)
	{
		if (j == 0)
		{
			buf[0] = Headstr[m_type];
		}else
		{
			srand(time(NULL) * rand());
			unsigned int index = rand() % 10000;
			buf[j] = m_Serialchar[index];
		}
	}
	buf[SerialLen] = 0;
	
	static std::string str;
	str.clear();
	str = buf ;
	std::map<std::string, Serial*>::iterator it0 = mCreateMap.end();
	std::map<std::string, Serial*>::iterator it1 = mSerialMap.end();

	it0 = mCreateMap.find(str);
	it1 = mSerialMap.find(str);


	if (it0 != mCreateMap.end() ||  it1 != mSerialMap.end() || !Checkok(buf))
	{
		return false ;
	}

	Serial* pkSerial = new Serial;
	pkSerial->IsUsed = 0;
	pkSerial->serial = str;
	pkSerial->PrizeID = m_type;
	mCreateMap.insert(std::map<std::string, Serial*>::value_type(pkSerial->serial, pkSerial));
	return true;
}
bool SerialSystem::EndCreate()
{
	//printf("生成%u \n", mCreateMap.size());

	if (mCreateMap.size() < m_count)
	{
		return false ;
	}
	printf("生成完毕..... \n");
	return true ;
}
bool SerialSystem::SaveToFile()
{
	time_t curTime;
	curTime = time(NULL);
	tm localTime = *localtime(&curTime);


	char savefile[256];
	sprintf(savefile, "Serial_Create_By_%dY%dM%dD%dH%dM%dS", 1900 + localTime.tm_year, localTime.tm_mon,localTime.tm_mday, localTime.tm_hour, localTime.tm_min, localTime.tm_sec);
	FILE* fp = fopen(savefile, "w+");
	if (fp)
	{
		std::string str ;
		str.reserve((SerialLen + 2) *  m_count + 40);
		std::map<unsigned __int32, Prize*>::iterator pit = m_PrizeMap.find(m_type);
		if (pit != m_PrizeMap.end())
		{
			if (pit->second)
			{
				str = pit->second->des + "\n";
			}
		}
		std::map<std::string, Serial*>::iterator it = mCreateMap.begin();
		for (it; it != mCreateMap.end(); ++it)
		{
			if (it->second)
			{
				str += it->second->serial + "\n";
			}
		}
		fwrite(str.c_str(),sizeof(char), str.length(),fp);

		fclose(fp);
	}

	return true ;
}
bool SerialSystem::GetSerial()
{
	for ( int i = 0 ; i < OnceCount; i++)
	{
		bool create = true;
		while(create)
		{
			create = !Create();
		}
	}

	return true ;
}
unsigned int SerialSystem::RandIndex()
{
	srand(time(NULL) *  rand());
	unsigned int index = rand() % strlen(randstr);
	return index ;
}
bool SerialSystem::BeginCreate()
{
	unsigned int count = 0;
	unsigned int lastindex = 0;
	for (int i = 0 ; i < 10000 ; i++)
	{
		unsigned int index = RandIndex();

		while(lastindex == index)
		{
			index = RandIndex() ;
			count ++ ;

			if (count >= 100)
			{
				return false ;
			}
		}
		lastindex = index ;
		count = 0;

		m_Serialchar[i] = randstr[index]; 
	}
	m_Serialchar[10000] = 0;

	return true ;
}
bool SerialSystem::DeleteCreate()
{
	std::map<std::string, Serial*>::iterator it = mCreateMap.begin();
	for (it; it != mCreateMap.end(); ++it)
	{
		Serial* pkSerial = it->second;
		if (pkSerial)
		{
			delete pkSerial;
			pkSerial = NULL ;
		}
	}

	mCreateMap.clear();

	return true ;
}
bool SerialSystem::SaveAllSerial()
{
	printf("保存数据到数据库.....\n");
	std::map<std::string, Serial*>::iterator it = mCreateMap.begin();
	for (it; it != mCreateMap.end(); ++it)
	{
		if (it->second)
		{
			g_Realm_db->Execute("insert into cardserial (PrizeID, IsUsed, CardSerial) values(%u, %u, '%s')", it->second->PrizeID, it->second->IsUsed, it->second->serial.c_str());
		}
		mSerialMap.insert(std::map<std::string, Serial*>::value_type(it->first, it->second));
	}

	SaveToFile();
	DeleteCreate();
	printf("保存完毕.....\n\n\n\n");
	return true ;
}
void SerialSystem::Run()
{
	g_Realm_db->QueryTaskRun();
}
void SerialSystem::ShutDown()
{
	std::map<std::string, Serial*>::iterator it = mCreateMap.begin();
	for (it; it != mCreateMap.end(); ++it)
	{
		Serial* pkSerial = it->second;
		if (pkSerial)
		{
			delete pkSerial;
			pkSerial = NULL ;
		}
	}

	it = mSerialMap.begin();
	for (it; it != mSerialMap.end(); ++it)
	{
		Serial* pkSerial = it->second;
		if (pkSerial)
		{
			delete pkSerial;
			pkSerial = NULL ;
		}
	}

	g_Realm_db->Shutdown();
	delete g_Realm_db;
	MyLog::Release();
}
