#pragma once
enum 
{
	Gold_N = 1,  //普通激活码
	Gold_Y,      //黄金激活码
	Gold_W,		 //白金激活码
	//....
	Gold_Max = 24,
};
const static char* randstr = "23456789ABCDEFGHJKLMNPQRSTUVWXYZ";
const static int SerialLen = 12;
const static char* Headstr = "0ABCDEFGHJKLMNPQRSTUVWXYZ"; // 0不使用
static bool constbegin = false ;
const static unsigned int OnceCount = 10; //单次抓取的数量

class SerialSystem
{
public:
	SerialSystem();
	~SerialSystem();
	bool InitSystem();
	void ShutDown();
	void Run();

	void ShowPrizeType();

	bool CreateSerial(unsigned int Count, unsigned int Type);
	bool BeginCreate();
	bool GetSerial();
	bool EndCreate();
	bool SaveAllSerial();
	bool DeleteCreate();
	bool FindPrizeType(unsigned int t);
protected:
	bool LoadSerial(); //加载现有的激活码
	bool LoadPrize();
	bool Create();
	bool Checkok(char* buf); //检验激活码是否合法
	unsigned int RandIndex();
	bool SaveToFile();
	
private:
	char m_Serialchar[10000];
	static unsigned int m_type ;
	static unsigned int m_count ;
	std::map<std::string, Serial*> mCreateMap; //生成的
	std::map<std::string, Serial*> mSerialMap; //已经存在的
	std::map<unsigned __int32, Prize*> m_PrizeMap; // 奖励的类型
};