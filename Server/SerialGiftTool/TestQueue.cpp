#include "stdafx.h"
#include "TestQueue.h"

TestQueue::TestQueue()
{
	m_testlist.clear();
	m_resultlist.clear();
}
TestQueue::~TestQueue()
{

}
void TestQueue::addteam()
{
	srand(get_tick_count());
	ui32 num = rand() % 5;
	m_testlist.push_back(num + 1);
	if (GetFire(10))
		PrintQueue();
	
}
void TestQueue::Erasefromlist(ui32 num)
{
	std::list<ui32>::iterator it = m_testlist.begin() ;
	for (it; it != m_testlist.end(); ++it)
	{
		if (*it == num)
		{
			m_testlist.erase(it);
			return;
		}
	}

	
}
void TestQueue::PrintQueue()
{
	std::list<ui32>::iterator it = m_resultlist.begin() ;
	ui32 brnume = 1;
	printf("\nbegin queue number. \n");
	for (it; it != m_resultlist.end(); ++it)
	{
		Erasefromlist(*it);
		printf("%d  ", *it);
	}
	printf("\nqueue number end.\n");

	HANDLE stdOut = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(stdOut, FOREGROUND_RED | FOREGROUND_INTENSITY  );
	std::list<ui32>::iterator it2 = m_testlist.begin() ;
	for (it2; it2 != m_testlist.end(); ++it2)
		printf("%d ", *it2);

	SetConsoleTextAttribute(stdOut, FOREGROUND_GREEN | FOREGROUND_INTENSITY  );
}
bool TestQueue::CheckCanFind(const std::set<ui32>& checklist, ui32 number)
{
	return checklist.find(number) == checklist.end();
}
bool TestQueue::CheckFireTeam(std::list<ui32>& mother, int count, std::list<ui32>& reslut)
{
	std::list<ui32> n_mother;
	std::list<ui32> n_result;

	if (!mother.size())
	{
		return false;
	}
	ui32 team_head= *mother.begin();
	if (team_head == count)
	{
		reslut.push_back(team_head);
		return true;
	}else
	{
		std::list<ui32>::iterator it = mother.begin();
		std::list<ui32>::iterator end_it = mother.end();
		std::list<ui32>::iterator it2 ;
		for (it; it != end_it; ++it)
		{
			n_mother.clear();
			n_result.clear();

			if ((*it) > count)
			{
				continue ;
			}else
			{
				if ((*it) == count)
				{
					reslut.push_back(*it);
					return true;
				}
			}
			for (it2 = it ; it2 != end_it; ++it2)
			{
				if (it2 != it && (*it2) <= count - (*it))
					n_mother.push_back(*it2);
			}

			if (CheckFireTeam(n_mother, count - (*it), n_result))
			{
				reslut.push_back(*it);
				for (std::list<ui32>::iterator it_result = n_result.begin(); it_result != n_result.end(); ++it_result)
					reslut.push_back( * it_result);
				return true;
			}
		}
	}
	return false;
}
bool TestQueue::GetFire(ui32 num)
{
	m_resultlist.clear();
	int max = num;
	std::list<ui32>::iterator it = m_testlist.begin();
	std::list<ui32>::iterator it2;
	std::list<ui32>::iterator end_it = m_testlist.end();

	std::list<ui32> mother;
	std::list<ui32> result;
	std::set<ui32> check;

	for (it; it != end_it; ++it)
	{
		if (check.size() && !CheckCanFind(check, *it))
			continue ;

		// ���ƥ��
		if (*it == max)
		{
			m_resultlist.push_back(*it);
			return true;
		}
		mother.clear();
		result.clear();

		for (it2 = it ; it2 != end_it; ++it2)
		{
			if (it2 != it && (*it2) <= max - (*it))
				mother.push_back(*it2);
		}

		if (CheckFireTeam(mother, max - *it, result))
		{
			m_resultlist.push_back(*it);
			for (std::list<ui32>::iterator it_result = result.begin(); it_result != result.end(); ++ it_result)
				m_resultlist.push_back(*it_result);

			return true;
		}else
			check.insert(*it);
	}

	m_resultlist.clear();
	return false;
}