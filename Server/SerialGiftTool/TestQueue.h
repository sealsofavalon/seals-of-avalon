class TestQueue
{
public:
	TestQueue();
	~TestQueue();
	bool CheckCanFind(const std::set<ui32>& checklist, ui32 number);
	bool CheckFireTeam(std::list<ui32>& mother, int count, std::list<ui32>& reslut);
	bool GetFire(ui32 num);
	void PrintQueue();
	void Erasefromlist(ui32 num);
	void addteam();
protected:
	std::list<ui32> m_testlist;
	std::list<ui32> m_resultlist;
private:
	
};