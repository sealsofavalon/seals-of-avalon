#include "stdafx.h"
#include "creaturemgr.h"
#include "creature.h"
#include "User.h"

CreatureMgr::~CreatureMgr()
{
	while(m_mapCreatures.size())
	{
		delete m_mapCreatures.begin()->second;
		m_mapCreatures.erase( m_mapCreatures.begin() );
	}
}

void CreatureMgr::AddCreature( uint64 guid, Creature* c )
{
	m_mapCreatures.insert( std::map<uint64, Creature*>::value_type( guid, c ) );
}

void CreatureMgr::RemoveCreature( uint64 id )
{
	std::map<uint64, Creature*>::iterator it = m_mapCreatures.find( id );
	if( it != m_mapCreatures.end() )
	{
		delete it->second;
		m_mapCreatures.erase( it );
	}
}

Creature* CreatureMgr::FindNearestCreature( User* p )
{
	float distance = 1000000.f;
	Creature* sel = NULL;
	for( std::map<uint64, Creature*>::iterator it = m_mapCreatures.begin(); it != m_mapCreatures.end(); ++it )
	{
		Creature* c = it->second;
		float temp = (p->x - c->m_positionX) * (p->x - c->m_positionX) + (p->y - c->m_positionY) * (p->y - c->m_positionY) + (p->z - c->m_positionZ) * (p->z - c->m_positionZ);
		if( temp < distance )
		{
			distance = temp;
			sel = c;
		}
	}
	return sel;
}

Creature* CreatureMgr::CreateCreature()
{
	Creature* c = new Creature;
	return c;
}

Creature* CreatureMgr::GetCreature( uint64 guid )
{
	std::map<uint64, Creature*>::iterator it = m_mapCreatures.find( guid );
	if( it != m_mapCreatures.end() )
		return it->second;
	else
		return NULL;
}

UpdateObj* CreatureMgr::GetObj(uint64 id)
{
	std::map<uint64, UpdateObj*>::iterator it = m_mapObjs.find( id );
	if( it != m_mapObjs.end() )
	{
		return it->second;
	}
	return NULL;
}

void CreatureMgr::AddObj(UpdateObj* pObj)
{
	uint64 guid = pObj->GetUInt64Value(OBJECT_FIELD_GUID);
	m_mapObjs.insert( std::map<uint64, UpdateObj*>::value_type( guid, pObj ) );
}

void CreatureMgr::RemoveObj( uint64 id )
{
	std::map<uint64, UpdateObj*>::iterator it = m_mapObjs.find( id );
	if( it != m_mapObjs.end() )
	{
		//delete it->second;
		m_mapObjs.erase( it );
	}
}