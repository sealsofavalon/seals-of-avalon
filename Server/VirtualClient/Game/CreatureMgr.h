#pragma once

class Creature;
class UpdateObj;
class User;

class CreatureMgr
{
public:
	~CreatureMgr();
	void AddCreature( uint64 guid, Creature* c );
	void RemoveCreature( uint64 id );
	Creature* FindNearestCreature( User* p );
	Creature* CreateCreature();
	Creature* GetCreature( uint64 guid );

	UpdateObj* GetObj(uint64 id);
	void AddObj(UpdateObj* pObj);
	void RemoveObj(uint64 id);
private:
	std::map<uint64, Creature*> m_mapCreatures;
	std::map<uint64, UpdateObj*> m_mapObjs;
};