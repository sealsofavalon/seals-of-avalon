#include "StdAfx.h"
#include "Group.h"
#include "UserMgr.h"
#include "User.h"



SubGroup::SubGroup()
{

	for (int i = 0; i < MAXMEMBER; i ++)
	{
		m_Member[i].guid = 0;
	}
	m_leader = 0;
}

SubGroup::~SubGroup()
{

}




bool SubGroup::HaveMember(ui64 guid)
{
	for (int i = 0 ; i < MAXMEMBER; i ++)
	{

		if (m_Member[i].guid == guid)
		{
			return true;
		}
			
		
	}

	return false;
}




int SubGroup::GetNoBodyAdder()
{
	for(int i = 0; i < MAXMEMBER; i ++)
	{
		if(m_Member[i].guid == 0)
		{
			return i;
		}

	}

	return -1;
}

bool SubGroup::IsFull()
{
	int i = GetNoBodyAdder();
	if (i == -1)
	{
		return true;
	}

	return false;
}

bool SubGroup::IsEmpty()
{
	for (int i = 0 ; i < MAXMEMBER; i ++)
	{
		if (m_Member[i].guid != 0)
		{
			return false;
		}
	}


	return true;
}

bool SubGroup::AddMember(MSG_S2C::stGroupList::stMember Member)
{
	if (!HaveMember(Member.guid))
	{
		for (int i = 0; i < MAXMEMBER; i ++)
		{
			if (m_Member[i].guid == 0)
			{
				m_Member[i] = Member;
				return true;
			}
		}
	}



	return false;

}




bool SubGroup::RemoveMember(ui64 guid)
{
	for (int i = 0; i < MAXMEMBER; i ++)
	{
		if (m_Member[i].guid == guid)
		{
			m_Member[i].guid = 0;
			return true;
		}
	}
	return false;


}



void SubGroup::Clear()
{
	m_leader = 0;
	for (int i = 0; i < MAXMEMBER; i ++)
	{
		m_Member[i].guid = 0;
	}
}

bool SubGroup::SetLeader(ui64 guidSette)
{

	if (HaveMember(guidSette))
	{
		m_leader = guidSette;
		return true;
	}

	return false;

}

const char* SubGroup::GetMemberName(ui64 guid)
{
	for (int i = 0; i < MAXMEMBER; i ++)
	{
		if (m_Member[i].guid == guid)
		{
			return m_Member[i].name.c_str();
		}
	}


	return NULL;

}

Group::Group(void)
{
	for (int i = 0; i < MAXSUBGROUP; i ++)
	{
		m_pSubGroup[i] = NULL;
	}

	m_pGroupLeader = NULL;
}

Group::~Group(void)
{

}


void Group::SwepMember(ui64 guiddes, ui64 guidsrc)
{
	bool havedes = false;
	bool havesrc = false;
	int ndes = -1;
	int nsrc = -1;

	MSG_S2C::stGroupList::stMember membersec;
	MSG_S2C::stGroupList::stMember memberdes;
	for (int i = 0; i < MAXSUBGROUP; i ++)
	{
		for (int j = 0; j < MAXMEMBER; j ++)
		{
			if (ndes == -1)
			{
				if (m_pSubGroup[i]->m_Member[j].guid == guiddes)
				{
					ndes = i;
					memberdes  = m_pSubGroup[i]->m_Member[j];
				}
			}

			if (nsrc == -1)
			{
				if (m_pSubGroup[i]->m_Member[j].guid ==  guidsrc)
				{
					havesrc = i;
					membersec = m_pSubGroup[i]->m_Member[j];
				}
			}
		}


	}

	if (ndes != -1 && nsrc != -1)
	{
		m_pSubGroup[ndes]->RemoveMember(guiddes);
		m_pSubGroup[nsrc]->RemoveMember(guidsrc);
		m_pSubGroup[nsrc]->AddMember(memberdes);
		m_pSubGroup[ndes]->AddMember(membersec);

	}

	//GetTeamDlg()->RefreshTeamMemberList(this);
}


const char* Group::GetMemberName(ui64 guid)
{

	const char* sz = NULL;
	for (int i = 0; i < MAXSUBGROUP; i ++)
	{
		if (!m_pSubGroup[i])
		{
			continue;
		}
		sz = m_pSubGroup[i]->GetMemberName(guid);

		if (sz)
		{
			return sz;
		}
	}

	return sz;

}

//User* Group::FindMember(ui64 guid)
//{
//	User* pFinder;
//	for (int i = 0 ; i < MAXSUBGROUP; i ++)
//	{
//		if (!m_pSubGroup[i])
//		{
//			continue;
//		}
//
//		pFinder = m_pSubGroup[i]->FindMember(guid);
//		if (pFinder)
//		{
//			return pFinder;
//		}
//
//	}
//
//	return NULL;
//}

bool Group::HaveMember(MSG_S2C::stGroupList::stMember member)
{
	for (int i = 0; i < MAXSUBGROUP; i ++)
	{
		if (!m_pSubGroup[i])
		{
			continue;
		}

		if (m_pSubGroup[i]->HaveMember(member.guid))
		{
			return true;
		}
	}

	return false;
}



void Group::ChangeSubgroup(ui64 guid, ui8 newTeam)
{
	for (int i = 0; i < MAXSUBGROUP; i ++)
	{
		if (m_pSubGroup[i])
		{
			for (int j = 0; j < MAXMEMBER; j ++)
			{
				if (m_pSubGroup[i]->m_Member[j].guid == guid)
				{
					if (m_pSubGroup[newTeam])
					{
						if (!m_pSubGroup[newTeam]->IsFull())
						{
							m_pSubGroup[newTeam]->AddMember(m_pSubGroup[i]->m_Member[j]);
							m_pSubGroup[i]->m_Member[j].guid = 0;
							return;
						}

					}
				}
			}

		}
	}
}

bool Group::RemoveMember(ui64 guid)
{


	for (int i = 0; i < MAXSUBGROUP; i  ++)
	{
		if (m_pSubGroup[i]->RemoveMember(guid))
		{
			return true;
		}
	}


	return false;
}





bool Group::AddMember(int i, MSG_S2C::stGroupList::stMember member)
{


	if (m_pSubGroup[i])
	{
		if (!m_pSubGroup[i]->HaveMember(member.guid))
		{
			return m_pSubGroup[i]->AddMember(member);
		}
		
	}

	return false;
	
}

bool Group::Create(MSG_S2C::stGroupList::stMember member, GroupType en)
{
	m_enType = en;
	m_pGroupLeader = member.guid;
	switch(m_enType)
	{
	case Group_Raid:
		{

		}
		break;

	case Group_Team:
		{
			
		}
		break;
	}


	return true;
}


bool Group::ChangeToRaid()
{
	if (m_enType == Group_Raid)
	{
		return false;
	}

	m_enType = Group_Raid;

	for (int i = 0;  i < MAXSUBGROUP;  i ++)
	{
		if (!m_pSubGroup[i])
		{
			m_pSubGroup[i] = new SubGroup;
		}

	}
	return true;

}


bool Group::ChangeToTeam()
{
	if (m_enType == Group_Team)
	{
		return false;
	}

	m_enType = Group_Team;

	return true;
}

void Group::SetGroupLeader(ui64 guid)
{
	if (m_enType == Group_Team)
	{
		m_pGroupLeader = guid;
		m_pSubGroup[0]->SetLeader(guid);
	}
	else
	{
		m_pGroupLeader = guid;
	}


}

void Group::SetIcon(ui64 guid, ui64 Icon)
{
	m_mapIcon[guid] = Icon;
}

void Group::SetGroupLeader(const char* sz)
{	
	if (m_enType == Group_Team)
	{
		if (!m_pSubGroup[0])
		{
			return;
		}
		for (int i = 0; i < MAXMEMBER; i ++)
		{
			if (m_pSubGroup[0]->m_Member[i].name == sz)
			{
				m_pGroupLeader = m_pSubGroup[0]->m_Member[i].guid;
			}
		}
	}

}

bool Group::ClearSubgoup(int i)
{
	if (m_pSubGroup[i])
	{
		m_pSubGroup[i]->Clear();
	}

	return true;
}

bool Group::Clear()
{
	//m_pGroupLeader = 0;
	for (int i = 0; i < MAXSUBGROUP; i ++)
	{
		if (m_pSubGroup[i])
		{
			m_pSubGroup[i]->Clear();
		}
	}

	return true;
}

