#pragma once
#include "..\..\SDBase\Protocol\S2C_Group.h"
#define MAXMEMBER 5
#define MAXSUBGROUP 8
class User;
class Group;



enum GroupType
{
	Group_Team,
	Group_Raid
};

class SubGroup
{
public:
	SubGroup();
	virtual ~SubGroup();
public:

	bool AddMember(MSG_S2C::stGroupList::stMember member);
	bool RemoveMember(ui64 guid);
	bool HaveMember(ui64 guid);
	int GetNoBodyAdder();
	bool IsEmpty();
	bool IsFull();
	const char* GetMemberName(ui64);
	

	bool SetLeader(ui64 guidSett);

	void Clear();



public:
	MSG_S2C::stGroupList::stMember m_Member[MAXMEMBER];
	ui64 m_leader;
	Group* m_Father;



};


class Group
{
public:
	Group(void);
	virtual ~Group(void);
public:
	typedef std::map<uint64, uint64> MAPICON;
public:
	bool Create(MSG_S2C::stGroupList::stMember member , GroupType en);
	bool ChangeToRaid();
	bool ChangeToTeam();
	bool AddMember(MSG_S2C::stGroupList::stMember member);
	bool AddMember(ui64 guid);


	bool AddMember(int i, MSG_S2C::stGroupList::stMember member);
	//bool AddMember(int i, ui64 guid);
	//bool RemoveMember(User* p);
	bool RemoveMember(ui64 guid);

	void ChangeSubgroup(ui64 guid, ui8 newTeam);


	//User* FindMember(ui64 guid);
	bool HaveMember(MSG_S2C::stGroupList::stMember member);



	void SetSubGroupLeader(ui64 guid);
	void SetGroupLeader(const char* sz);
	void SetGroupLeader(ui64 guid);
	void SetIcon(ui64 guid, ui64 Icon);

	bool ClearSubgoup(int i);
	bool Clear();

	void SwepMember(ui64 guiddes, ui64 guidsrc);
	//void ChangeSubgroup(ui64 )

	const char* GetMemberName(ui64 guid);






public:
	ui64 m_pGroupLeader;
	SubGroup* m_pSubGroup[MAXSUBGROUP];
	GroupType m_enType;
	uint64 m_targetIcons[8];
	ui64 m_GroupId;
	MAPICON m_mapIcon;

};
