#include "StdAfx.h"
#include "GroupManager.h"
#include "group.h"

GroupManager::GroupManager(void)
{
}

GroupManager::~GroupManager(void)
{
}


Group* GroupManager::CreateGroup(ui64 groupid, ui64 guid, GroupType en)
{
	Group* pGroup = NULL;

	pGroup = new Group;
	pGroup->m_GroupId = groupid;
	pGroup->m_enType = en;
	switch (en)
	{
	case Group_Team:
		{
			if (!pGroup->m_pSubGroup[0])
			{
				pGroup->m_pSubGroup[0] = new SubGroup;
			}

		}
		break;
	case Group_Raid:
		{

			for (int i = 0; i < MAXSUBGROUP; i ++)
			{
				if (!pGroup->m_pSubGroup[i] )
				{
					pGroup->m_pSubGroup[i] =  new SubGroup;
				}

			}

		}
		break;
	
	}
	m_mapTrueGroup.insert(MAPGROUP::value_type(guid, pGroup));
	return pGroup;



}
GroupManager* g_pGroupManager;
