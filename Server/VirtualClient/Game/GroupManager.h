#pragma once
#include "Group.h"
#include <vector>
#include <map>

typedef std::map<ui64, Group*> MAPGROUP;
typedef std::vector<Group*> VCGROUP;

class GroupManager
{
public:
	GroupManager(void);
	virtual ~GroupManager(void);
	Group* CreateGroup(ui64 groupid, ui64 guid, GroupType en);
	Group* FindGroup(ui64 guid);


protected:
	VCGROUP   m_vcGroup;
	MAPGROUP   m_mapTrueGroup;

};

extern GroupManager* g_pGroupManager;