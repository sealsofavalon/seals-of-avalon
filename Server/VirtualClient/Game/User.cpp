#include "StdAfx.h"
#include "User.h"
#include "UserMgr.h"
#include "..\net\GTSocket.h"
#include "..\net\LSSocket.h"
#include "..\..\SDBase\public\UnitDef.h"
#include "..\..\SDBase\Protocol\C2S.h"
#include "..\..\SDBase\Protocol\C2S_Move.h"
#include "..\..\SDBase\Protocol\C2S_Chat.h"
#include "..\..\SDBase\Protocol\C2S_Group.h"
#include "../Common/share/Auth/MD5.h"
#include "Creature.h"
#include "CreatureMgr.h"
#include "../../SDBase/Protocol/C2S_Spell.h"
#include "../../new_common/Source/ssl/MD5.h"
#include "../resource.h"
#include "../TeamDlg.h"
#include "groupManager.h"
#include "group.h"

User::User(void) : m_pGTSocket( NULL ), m_pLSSocket( NULL ), m_nAccountID( 0 ), m_nSessionID( 0 ), m_movementflags( 0 ), m_bmove( false ), m_delete( false ),
m_lastPingTime(0),m_pGroup(NULL),m_PinIndex(0),m_strGroupState("空闲")
{
	m_creatureMgr = new CreatureMgr;
	
}

User::~User(void)
{
	if( m_pGTSocket )
	{
		m_pGTSocket->m_pUser = NULL;
		m_pGTSocket->close();
	}
	if( m_pLSSocket )
	{
		m_pLSSocket->m_pUser = NULL;
		m_pLSSocket->close();
	}
	delete m_creatureMgr;
}

bool User::Init( const char* szAccount, const char* szPassword, const char* IP, uint16 Port )
{
	m_strAccount = szAccount;
	m_strPassword = szPassword;

	m_pLSSocket = new LSSocket;
	m_pLSSocket->m_pUser = this;

	m_pLSSocket->connect( IP, Port );
	return true;
}

void User::SendMsg2GT( const PakHead& msg )
{
	m_pGTSocket->PostSend( msg );
}

void User::SendMsg2Login( const PakHead& msg )
{
	m_pLSSocket->PostSend( msg );
}

/*
void User::LoginAuth( const std::string& strRsaKey )
{
	string strRsaEncoded = CRsaDesManager::RSAPubEncode( strRsaKey, m_strDesKey );
	MSG_C2LS::stLoginReq req;
	req.strDstKey_RsaEncode	= strRsaEncoded;
	req.strAccount			= CRsaDesManager::DESEncode( m_strDesKey, m_strAccount );
	req.strPass				= CRsaDesManager::DESEncode( m_strDesKey, m_strPassword );
	SendMsg2Login( req );
}
*/


void User::AskTele()
{



	MSG_C2S::stMove_Teleport_Ack stTeleAck;
	stTeleAck.guid = m_role.guid;
	if( m_pGTSocket )
		m_pGTSocket->PostSend( stTeleAck );
	MSG_C2S::stSet_Target settarget;
	settarget.guid = m_role.guid;
	SendMsg2GT( settarget );

	MSG_C2S::stSet_Selection setselection;
	setselection.guid = m_role.guid;
	setselection.ToAttack = true;
	SendMsg2GT( setselection );


}

void User::LongReq( uint32 tablekey )
{
	MSG_C2LS::stLoginReq req;
	req.account = m_strAccount;
	std::string md5pwd = MD5Hash::_make_md5_pwd( m_strPassword.c_str() );
	encrypt_string( md5pwd, req.password, tablekey );
	OutputMessage( "Login req user:%s password:%s", m_strAccount.c_str(), m_strPassword.c_str() );
	SendMsg2Login( req );
}

void User::SelectGroup( uint32 AccountID, uint32 SessionID, const std::vector<MSG_LS2C::stLoginAck::tagGroup>& vGroups )
{
	m_nAccountID = AccountID;
	m_nSessionID = SessionID;
	if( vGroups.size() > 0 )
	{
		MSG_C2LS::stGroupSelReq req;
		const MSG_LS2C::stLoginAck::tagGroup& group = vGroups[2];
		req.strGroupName = group.strGroupName;
		SendMsg2Login( req );
		//OutputMessage( "user(%s) selecting group", GetAccount() );
	}
	else
	{
		//OutputMessage( "user(%s) not found server group", GetAccount() );
		m_delete = true;
	}
}

void User::ConnectGate( uint32 IP, uint16 Port )
{
	m_pGTSocket = new GTSocket;
	m_pGTSocket->m_pUser = this;

	m_pGTSocket->connect( IP, Port );
	m_lastPingTime = UNIXTIME;
}

void User::LoginGame( uint8 error )
{
	//ErrCode:0=成功，1＝账号不存在，2＝sessionID验证失败，3＝ip验证失败,4=账号已经在线
	switch( error )
	{
	case 0:
		{
//			MSG_C2S::stRoleListReq req;
//			req.nAccountID = m_nAccountID;
//			SendMsg2GT( req );
		}
		break;
	case 1:
		OutputMessage( "user(%s) account not found", GetAccount() );
		break;
	case 2:
		OutputMessage( "user(%s) wrong sessionID", GetAccount() );
		break;
	case 3:
		OutputMessage( "user(%s) IP auth failed", GetAccount() );
		break;
	case 4:
		OutputMessage( "user(%s) is already online", GetAccount() );
		break;
	default:
		assert( 0 );
		break;
	}
}

void User::RoleListAck( uint8 error, const std::vector<MSG_S2C::RoleInfo>& vRoles )
{
	if( vRoles.size() == 0 )
	{
		MSG_C2S::stCreateRoleReq req;
		req.nAccountID = m_nAccountID;
		req.Name = m_strAccount + "A";
		req.nSex = rand() % 2;
//		req.nRace = rand() % 3 + 1;
//		req.nClass = rand() % 6 + 1;
		req.nRace = (rand() % 3) + 1;
		req.nClass = 1;
		SendMsg2GT( req );
	}
	else
	{
		m_role = *vRoles.begin();
		EnterGameReq();
	}
}

void User::CreateRoleAck( uint8 error, const MSG_S2C::RoleInfo& role )
{
	switch( error )
	{
	case 0:
		OutputMessage( "user(%s) create role success, name:%s", GetAccount(), role.name.c_str() );
		break;
	case MSG_S2C::stCreateRoleAck::NOT_LOGON:
		OutputMessage( "user(%s) not login", GetAccount() );
		return;
	case MSG_S2C::stCreateRoleAck::ROLE_IS_FULL:
		OutputMessage( "user(%s) role is full", GetAccount() );
		return;
	case MSG_S2C::stCreateRoleAck::BANNED_NAME:
		OutputMessage( "user(%s) banned name", GetAccount() );
		return;
	case MSG_S2C::stCreateRoleAck::NAME_IS_USED:
		OutputMessage( "user(%s) is used", GetAccount() );
		return;
	case MSG_S2C::stCreateRoleAck::NO_SUCH_RACECLASS:
		OutputMessage( "user(%s) no such raceclass", GetAccount() );
		return;
	case MSG_S2C::stCreateRoleAck::GENERAL_ERROR:
		OutputMessage( "user(%s) general error", GetAccount() );
		return;
	case MSG_S2C::stCreateRoleAck::NAME_IS_TOO_LONG:
		OutputMessage( "user(%s) create role name is too long", GetAccount() );
		return;
	default:
		assert( 0 );
		break;
	}
	m_role = role;
	EnterGameReq();
}

void User::EnterGameReq()
{
	MSG_C2S::stEnterGameReq req;
	req.guid = m_role.guid;
	req.strRole = m_role.name;
	req.AccountID = m_nAccountID;
	req.AccountName = m_strAccount;
	SendMsg2GT( req );
	//OutputMessage( "user(%s) send enter game request", GetAccount() );
}

void User::EnterGame( const MSG_S2C::stEnterGameAck& ack )
{
	nCurExp = ack.nCurExp;
	nMaxExp = ack.nMaxExp;
	nMoney = ack.nMoney;
	nCurHP = ack.nCurHP;
	nMaxHP = ack.nMaxHP;
	nCurMP = ack.nCurMP;
	nMaxMP = ack.nMaxMP;

	x = ack.x;
	y = ack.y;
	z = ack.z;
	dir = ack.dir;
	dir = 0;

	MSG_C2S::stLoadingOK msg;
	SendMsg2GT( msg );
	
	/*
	int id;
	float x, y, z;
	if( g_pUserMgr->GetRandomPosition( id, x, y, z ) )
	{
		Teleport( id, x, y, z );
	}

	SendGMCommand( ".modify level 200" );
	*/
	g_pUserMgr->AddUser( this );
	m_fLastBeatTime = GetCurrentTimeInSec();

	/*
	srand( GetTickCount() );

	if( this->m_role.nRace == 1 )
	{
		switch( rand() % 4 )
		{
		case 1:
			SendGMCommand( ".worldport 1 4591.8 3034.5 9.9" );
			break;
		case 2:
			SendGMCommand( ".worldport 1 4860.1 3281.1 3.6" );
			break;
		case 3:
			SendGMCommand( ".worldport 1 4824.8 3028.4 2.8" );
			break;
		}
	}
	else if( this->m_role.nRace == 2 )
	{
		switch( rand() % 4 )
		{
		case 1:
			SendGMCommand( ".worldport 2 4760.6 3507.3 16.1" );
			break;
		case 2:
			SendGMCommand( ".worldport 2 4718.9 3294.2 20.3" );
			break;
		case 3:
			SendGMCommand( ".worldport 2 4932.1 3552.0 14.4" );
			break;
		}
	}
	else
	{
		switch( rand() % 4 )
		{
		case 1:
			SendGMCommand( ".worldport 3 4944.5 2973.8 0.2" );
			break;
		case 2:
			SendGMCommand( ".worldport 3 5051.7 3218.9 -0.9" );
			break;
		case 3:
			SendGMCommand( ".worldport 3 4869.7 3370.0 11.7" );
			break;
		}
	}

	if( m_pGTSocket)
		m_pGTSocket->delay_close( 12 );
	*/
}

void User::HeartBeat()
{


	if( !m_bmove )
		return;
	float nowtime = GetCurrentTimeInSec();
	float deltatime = nowtime - m_fLastBeatTime;
	if( deltatime >= 1.f )
	{
		if( ++m_movecount % 16 == 0 )
		{
			if( m_movementflags & MOVEFLAG_MOVE_FORWARD )
			{
				m_movementflags &= ~MOVEFLAG_MOVING_MASK;
				m_movementflags |= MOVEFLAG_MOVE_BACKWARD;
			}
			else
			{
				m_movementflags &= ~MOVEFLAG_MOVING_MASK;
				m_movementflags |= MOVEFLAG_MOVE_FORWARD;
			}
		}

		MSG_C2S::stMove_OP msg;
		memset(&msg.move, 0, sizeof(msg.move));

		msg.moveOP = MOVE_HEARTBEAT;
		msg.move.flags = m_movementflags;

		
		msg.move.time = uint32( deltatime * 1000.f );
		if( m_movementflags & MOVEFLAG_MOVE_FORWARD )
		{
			y -= 3.f * deltatime;
			msg.move.orientation = 0;
		}
		else
		{
			y += 3.f * deltatime;
			msg.move.orientation = 3.1415927;
		}
		msg.move.x = x;
		msg.move.y = y;
		msg.move.z = z;

		SendMsg2GT( msg );
		m_fLastBeatTime = GetCurrentTimeInSec();

		//OutputMessage( "[%s] moveop x = %.2f, y = %.2f", this->GetName(), x, y );
	}
}

void User::Run()
{
	if( m_pLSSocket )
	{
//		m_pLSSocket->thread_run_asio();
		m_pLSSocket->run_no_wait();
	}
	if( m_pGTSocket )
	{
//		m_pGTSocket->thread_run_asio();
		if( (uint32)UNIXTIME - m_lastPingTime > 15 )
		{

			SendPing(UNIXTIME);
			//MSG_C2S::stPing msg;
			//m_pGTSocket->PostSend( msg );
			//m_lastPingTime = (uint32)UNIXTIME;
			OutputMessage( "player:%s send ping msg", m_role.name.c_str() );

			/*
			static int s_index = 0;
			MSG_C2S::stPing msg;
			msg.index = ++s_index;
			msg.timestamp = (uint32)UNIXTIME;
			m_pGTSocket->PostSend( msg );
			m_lastPingTime = (uint32)UNIXTIME;
			*/
			//OutputMessage( "player:%s send ping msg", m_role.name.c_str() );
		}
		m_pGTSocket->run_no_wait();
	}
}

void User::Teleport( int id, int xmin, int xmax, int ymin, int ymax )
{
	x = xmin + rand() % ( xmax - xmin + 1 );
	y = ymin + rand() % ( ymax - ymin + 1 );
	z = 0;

	MSG_C2S::stChat_Message msg;
	msg.type = 1;
	char str[256];
	sprintf( str, ".worldport %d %.2f %.2f 0.00", id, x, y );
	msg.txt = str;
	if( m_pGTSocket )
	{
		m_pGTSocket->PostSend( msg );
		m_role.nMapID = id;
	}

	MSG_C2S::stMove_Teleport_Ack ack;
	ack.guid = this->GetID();
	if( m_pGTSocket )
		m_pGTSocket->PostSend( ack );
}

void User::Teleport( int id, float x, float y, float z )
{
	MSG_C2S::stChat_Message msg;
	msg.type = 1;
	char str[256];
	sprintf( str, ".worldport %d %.2f %.2f %.2f", id, x, y, z );
	msg.txt = str;
	if( m_pGTSocket )
	{
		m_pGTSocket->PostSend( msg );
		m_role.nMapID = id;
		this->x = x;
		this->y = y;
		this->z = z;

		//OutputMessage( "user(%s) teleport to %d %.2f %.2f %.2f", GetAccount(), id, x, y, z );
	}
	else
		return;

	MSG_C2S::stMove_Teleport_Ack ack;
	ack.guid = this->GetID();
	if( m_pGTSocket )
		m_pGTSocket->PostSend( ack );


	MSG_C2S::stSet_Target settarget;
	settarget.guid = m_role.guid;
	SendMsg2GT( settarget );

	MSG_C2S::stSet_Selection setselection;
	setselection.guid = m_role.guid;
	setselection.ToAttack = false;
	SendMsg2GT( setselection );

	SendGMCommand( ".modify hp 90000000 90000000" );
}

void User::SendGMCommand( const char* cmd )
{
	if( m_pGTSocket )
	{
		MSG_C2S::stChat_Message msg;
		msg.type = 1;
		msg.txt = cmd;
		m_pGTSocket->PostSend( msg );
	}
}

void User::StartMove()
{
	m_bmove = true;
}

void User::StopMove()
{
	m_bmove = false;
}

void User::SpawnCreature()
{
	char str[256];
	sprintf( str, ".npc spawn %d", 110000011 + rand() % 3 );
	SendGMCommand( str );

	SendGMCommand( ".modify hp 90000000 90000000" );
}

float GetCurrentTimeInSec()
{
	static bool bFirst = true;
	static LARGE_INTEGER freq;
	static LARGE_INTEGER initial;

	if (bFirst)
	{
		QueryPerformanceFrequency(&freq);
		QueryPerformanceCounter(&initial);
		bFirst = false;
	}

	LARGE_INTEGER counter;
	QueryPerformanceCounter(&counter);
	return (float)((long double)
		(counter.QuadPart - initial.QuadPart) / 
		(long double) freq.QuadPart);
}

void User::UpdateObject( MSG_S2C::stObjectUpdate& msg )
{
	ByteBuffer& data = msg.buffer;

	ui32 nUpdateCount = 0;ui8 temp;
	data >> nUpdateCount >> temp;
	while( nUpdateCount-- )
	{
		ui8	updatetype;
		data >> updatetype;

		ui32*	values = NULL;
		ui64 guid;
		if( updatetype == UPDATETYPE_CREATE_OBJECT || updatetype == UPDATETYPE_CREATE_YOURSELF )
		{
			ui8 type;ui8 update_flags;

			data >> guid >>  type >> update_flags;

			UpdateObj* pObj = NULL;
			static Creature c;
			switch(type)
			{
			case TYPEID_ITEM:
				{
					static Item item;
					pObj = &item;
				}
				break;
			case TYPEID_CONTAINER:
				{
					static Bag bag;
					pObj = &bag;
				}
				break;
			case TYPEID_PLAYER:
				{
					static Player player;
					pObj = &player;
				}
				break;
			case TYPEID_UNIT:
				{
					pObj = new Creature;
					m_creatureMgr->AddCreature( guid, (Creature*)pObj );
				}
				break;
			case TYPEID_GAMEOBJECT:
				{
					static GameObj gameobj;
					pObj = &gameobj;
				}
				break;
			default:
				return;
			}

			pObj->OnCreateMsg(&data, update_flags);
			m_creatureMgr->AddObj(pObj);
						
		}
		else if(updatetype == UPDATETYPE_VALUES)
		{
			data >> guid;
 			UpdateObj* c = m_creatureMgr->GetObj(guid);
 			if( c )
 			{
 				//values = pObj->m_uint32Values;
 				c->OnUpdateMsg(&data);
 			}

		}
		else if(updatetype == UPDATETYPE_OUT_OF_RANGE_OBJECTS )
		{
			ui32 mOutOfRangeIdCount = 0;
			data >> mOutOfRangeIdCount;
			for( ui32 i = 0; i < mOutOfRangeIdCount; i++ )
			{
				ui64 guid;
				data >> guid;
				m_creatureMgr->RemoveObj( guid );
				m_creatureMgr->RemoveCreature(guid);
			}
		}
	}
}

void User::AttackCreature()
{
	if( !m_pGTSocket ) return;

	Creature* c = m_creatureMgr->FindNearestCreature( this );
	if( c )
	{
		char cmd[256];
		sprintf( cmd, ".worldport %d %.2f %.2f %.2f", this->m_role.nMapID, c->m_positionX, c->m_positionY + .5f, c->m_positionZ );
		SendGMCommand( cmd );

		MSG_C2S::stMove_Teleport_Ack ack;
		ack.guid = this->GetID();
		SendMsg2GT( ack );

		MSG_C2S::stSet_Target settarget;
		settarget.guid = c->GetUInt64Value( OBJECT_FIELD_GUID );
		SendMsg2GT( settarget );

		MSG_C2S::stSet_Selection setselection;
		setselection.guid = c->GetUInt64Value( OBJECT_FIELD_GUID );
		setselection.ToAttack = true;
		SendMsg2GT( setselection );

		SendGMCommand( ".modify hp 90000000 90000000" );

		MSG_C2S::stAttack_Swing attackswing;
		attackswing.guid = c->GetUInt64Value( OBJECT_FIELD_GUID );
		SendMsg2GT( attackswing );
	}
}


void User::MoveOP( MSG_S2C::stMove_OP& msg )
{
// 	Creature* c = this->m_creatureMgr->GetCreature( msg.guid );
// 	if( c )
// 	{
// 		c->m_positionX = msg.move.x;
// 		c->m_positionY = msg.move.y;
// 		c->m_positionZ = msg.move.z;
// 	}
}


void User::SendPing(uint32 t)
{
	MSG_C2S::stPing Msg;

	Msg.index = m_PinIndex;
	Msg.timestamp = t;
	m_PinIndex ++;
	m_lastPingTime = t;
	m_pGTSocket->PostSend( Msg );
}


void User::GroupInvite(std::string str)
{
	MSG_C2S::stGroupInvite Msg;
	Msg.membername = str;

	SendMsg2GT(Msg);


}



void User::AcceptIvite()
{
	MSG_C2S::stGroupAccept Msg;
	SendMsg2GT(Msg);

	m_strGroupState = "接受邀请";
	GetTeamDlg()->RefreshUserItemOutTeam(this);

}

void User::RefuseInvite()
{

	MSG_C2S::stGroupDecline Msg;
	SendMsg2GT(Msg);


	m_strGroupState = "接受邀请";

	GetTeamDlg()->RefreshUserItemOutTeam(this);

}

void User::UnIvite()
{
	m_strGroupState = "空闲";
}

void User::BeDecline()
{
	//m_strGroupState = "请求被拒绝";
	GetTeamDlg()->RefreshUserItemOutTeam(this);
}

void User::ChangeLeader(MSG_C2S::stGroupSetLeader MsgRecv)
{
	if(m_pGroup)
	{
		SendMsg2GT(MsgRecv);
		//User* pUser = g_pUserMgr->FindUser(MsgRecv.name.c_str);
		//m_pGroup->SetGroupLeader(pUser.guid);
	}
}

void User::GroupToRaid()
{
	if (m_pGroup->m_pGroupLeader == m_role.guid)
	{
		MSG_C2S::stGroup_Raid_Convert msg;
		SendMsg2GT(msg);

	}

}

void User::SwepMember(MSG_S2C::stGroupSwep msg)
{
	if(m_pGroup)
	{
		m_pGroup->SwepMember(msg.guiddes, msg.guidsrc);
		GetTeamDlg()->RefreshTeamMemberList(m_pGroup);
	}
}

void User::ChangeLeader(MSG_S2C::stGroupSetLeader msg)
{


	const char* sz = m_pGroup->GetMemberName(m_pGroup->m_pGroupLeader);
	if (!sz)
	{

		GetTeamDlg()->DelTeamLeader(m_pGroup->m_pGroupLeader);	

	}
	

    m_pGroup->SetGroupLeader(msg.name.c_str());
	if (msg.name == m_role.name)
	{

		GetTeamDlg()->AddTeamLeader(this);	
	}

	//msg.name
}

void User::UpdateMemeberState(MSG_S2C::stPartyMemberStat msg)
{

	if (m_pGroup)
	{
		for (int i = 0; i < MAXSUBGROUP; i ++)
		{

			if (!m_pGroup->m_pSubGroup[i])
			{
				continue;
			}

			for (int j = 0; j < MAXMEMBER; j ++)
			{

					if (m_pGroup->m_pSubGroup[i]->m_Member[j].guid == msg.guid)
					{

					}
	
				
			}
		}
		
		
	}
}

void User::SendRemovePlayer(const char* szName)
{
	MSG_C2S::stGroupUnviteName Msg;
	Msg.membername = szName;
	char sz[512];
	sprintf(sz, "移除玩家 %s", szName);
	m_strGroupState = sz;
	SendMsg2GT(Msg);
	GetTeamDlg()->RefreshUserItemOutTeam(this);
}

void User::SendRemovePlayer(ui64 guid)
{
	char sz[512];
	MSG_C2S::stGroupUnviteGuid Msg;
	Msg.guid = guid;

	sprintf(sz, "移除玩家 %d", guid);
	m_strGroupState = sz;
	SendMsg2GT(Msg);
	GetTeamDlg()->RefreshUserItemOutTeam(this);
}


void User::SendGroupDisbandMsg()
{
	MSG_C2S::stGroupDisband Msg;
	m_strGroupState = "离开队伍";
	SendMsg2GT(Msg);
	GetTeamDlg()->RefreshUserItemOutTeam(this);
}

void User::SendReSetGroupLeaderMsg(ui64 guid)
{
	MSG_C2S::stGroupSetLeader Msg;
	Msg.guid = guid;
	SendMsg2GT(Msg);
	m_strGroupState = "更改队长";
	GetTeamDlg()->RefreshUserItemOutTeam(this);
	
}

void User::ChangeSubGroup(MSG_C2S::stGroup_Change_Sub_Group msg)
{
	SendMsg2GT(msg);

}

void User::SwepMemberGroup(MSG_C2S::stGroupSwepName msg)
{
	SendMsg2GT(msg);
}

void User::ReadyCheck(MSG_C2S::stRaid_ReadyCheck msg)
{
	SendMsg2GT(msg);
}

void User::ReadyCheck(MSG_S2C::stRaid_ReadyCheck msg)
{
	if (!msg.ready)
	{
		m_strGroupState = "确认就位";
		GetTeamDlg()->RefreshUserItemOutTeam(this);
	}
	else
	{
		if (msg.ready == 1)
		{
			User* pUser =  g_pUserMgr->FindUser(msg.player_guid);
			if (pUser)
			{
				pUser->m_strGroupState = "准备好了";
				GetTeamDlg()->RefreshUserItemOutTeam(pUser);
			}
		}
	}

}

void User::DestroyGroup(MSG_S2C::stGroupDestroyed msg)
{
	if (!m_pGroup)
	{
		return;
	}

	if (m_pGroup->m_pGroupLeader == m_role.guid)
	{
		GetTeamDlg()->DelTeamLeader(this);
	}

	
	delete m_pGroup;
	m_pGroup = NULL;
}

void User::GroupAddMember(MSG_S2C::stGroupAddMember msg)
{
	MSG_S2C::stGroupList::stMember member;
	member.Class = msg.NewMember.Class;
	member.Race = msg.NewMember.Race;
	member.name = msg.NewMember.name;
	member.guid = msg.NewMember.guid;
	m_pGroup->m_pSubGroup[msg.SubTeam]->AddMember(member);
	//m_pGroup->AddMember(msg.SubTeam, msg.NewMember)
	GetTeamDlg()->RefreshTeamMemberList(m_pGroup);
}

void User::GroupSetIcon(MSG_S2C::stGroupSetPlayerIcon msg)
{
	if (!m_pGroup)
	{
		return;
	}
	
	for (int i = 0; i < 8; i ++)
	{
		m_pGroup->m_targetIcons[i] = msg.m_targetIcons[i];
	}


	m_pGroup->SetIcon(msg.guid, msg.icon);
	


}

void User::GroupSetIcon(MSG_C2S::stGroupSetPlayerIcon msg)
{
	if (!m_pGroup)
	{
		return;
	}

	SendMsg2GT(msg);
}



void User::RecvGroupList(MSG_S2C::stGroupList Msg)
{

	if (!m_pGroup)
	{

		GroupType en;
		if (Msg.subgroupcount == 1)
		{
			en =  Group_Team;
		}
		else
		{
			en = Group_Raid;
		}
		m_pGroup = g_pGroupManager->CreateGroup(Msg.groupid, m_role.guid, en);
		if (Msg.leaderguid == m_role.guid)
		{
			m_pGroup->SetGroupLeader(Msg.leaderguid);

			GetTeamDlg()->AddTeamLeader(this);
		}

	}


	if (Msg.subgroupcount > 1)
	{
		if (m_pGroup->m_enType == Group_Team)
		{
			m_pGroup->ChangeToRaid();
		}
	}
	if (m_pGroup)
	{

		m_pGroup->Clear();


		if (!Msg.vSubGroups.empty())
		{


			for (int i = 0; i < Msg.subgroupcount; i ++ )
			{
				std::vector<MSG_S2C::stGroupList::stMember>::iterator it= Msg.vSubGroups[i].vMembers.begin();
				std::vector<MSG_S2C::stGroupList::stMember>::iterator itEnd= Msg.vSubGroups[i].vMembers.end();
				for (; it != itEnd; it ++)
				{
					m_pGroup->AddMember(i, (*it));
				}

			}

			bool EmptyGroup = true;
			for (int i = 0; i < Msg.subgroupcount ; i ++)
			{
				if (!Msg.vSubGroups[i].vMembers.empty())
				{
					EmptyGroup = false;
				}
			}


			if (EmptyGroup)
			{
				m_pGroup->SetGroupLeader(Msg.leaderguid);

				if (m_pGroup->m_pGroupLeader == m_role.guid)
				{

					GetTeamDlg()->DelTeamLeader(this);
				}

				delete m_pGroup;
				m_pGroup = NULL;
			}
			else
			{

			}



		}

		if (Msg.vSubGroups.empty())
		{
			GetTeamDlg()->DelTeamLeader(m_pGroup->m_pGroupLeader);

			delete m_pGroup;
			m_pGroup = NULL;
		}

		GetTeamDlg()->RefreshUserItemOutTeam(this);




	}
}