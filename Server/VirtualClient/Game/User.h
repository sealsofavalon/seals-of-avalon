#pragma once
#include "../../SDBase/Protocol/S2C_Move.h"
#include "../../SDBase/Protocol/C2S_Move.h"
#include "../../SDBase/protocol/S2C_Group.h"
#include "../../SDBase/protocol/c2s_Group.h"

class GTSocket;
class LSSocket;
class CreatureMgr;
class Group;

class User
{
public:
	User(void);
	~User(void);

public:
	bool Init( const char* szAccount, const char* szPassword, const char* IP, uint16 Port );
	void SendMsg2GT( const PakHead& msg );
	void SendMsg2Login( const PakHead& msg );

public:
	void LongReq( uint32 tablekey );
//	void LoginAuth( const std::string& strRsaKey );
	void SelectGroup( uint32 AccountID, uint32 SessionID, const std::vector<MSG_LS2C::stLoginAck::tagGroup>& vGroups );
	void ConnectGate( uint32 IP, uint16 Port );
	void LoginGame( uint8 error );
	void RoleListAck( uint8 error, const std::vector<MSG_S2C::RoleInfo>& vRoles );
	void CreateRoleAck( uint8 error, const MSG_S2C::RoleInfo& role );
	void EnterGameReq();
	void EnterGame( const MSG_S2C::stEnterGameAck& ack );
	void HeartBeat();
	void Run();
	void Teleport( int id, int xmin, int xmax, int ymin, int ymax );
	void Teleport( int id, float x, float y, float z );
	void SendGMCommand( const char* cmd );
	void StartMove();
	void StopMove();
	void SpawnCreature();
	void UpdateObject( MSG_S2C::stObjectUpdate& msg );
	void AttackCreature();
	void MoveOP( MSG_S2C::stMove_OP& msg );
	void AskTele();
	void SendPing(uint32 t);

public:
	inline const char* GetAccount() const { return m_strAccount.c_str(); }
	inline const char* GetName() const { return m_role.name.c_str(); }
	inline uint64 GetID() const { return m_role.guid; }


//////////////////////////////////////////////////////////////////////////���////////////////////////////////////////////////////////////////////////////
	void GroupInvite(std::string str);
	void AcceptIvite();
	void RefuseInvite();
	void UnIvite();
	void BeDecline();
	void ChangeLeader(MSG_C2S::stGroupSetLeader MsgRecv);
	void ChangeLeader(MSG_S2C::stGroupSetLeader msg);
	void SwepMember(MSG_S2C::stGroupSwep msg);
	void RecvGroupList(MSG_S2C::stGroupList Msg);
	void SendRemovePlayer(const char* szName);
	void SendRemovePlayer(ui64 guid);
	void SendGroupDisbandMsg();
	void SendReSetGroupLeaderMsg(ui64 guid);
	void UpdateMemeberState(MSG_S2C::stPartyMemberStat msg);
	void ChangeSubGroup(MSG_C2S::stGroup_Change_Sub_Group msg);
	void SwepMemberGroup(MSG_C2S::stGroupSwepName msg);
	void ReadyCheck(MSG_C2S::stRaid_ReadyCheck msg);
	void ReadyCheck(MSG_S2C::stRaid_ReadyCheck msg);
	void DestroyGroup(MSG_S2C::stGroupDestroyed msg);
	void GroupToRaid();
	void GroupSetIcon(MSG_C2S::stGroupSetPlayerIcon msg);
	void GroupSetIcon(MSG_S2C::stGroupSetPlayerIcon msg);
	void GroupAddMember(MSG_S2C::stGroupAddMember msg);

public:
	bool m_delete;
	GTSocket* m_pGTSocket;
	LSSocket* m_pLSSocket;
	std::string m_strAccount;
	std::string m_strPassword;
	uint32 m_nAccountID;
	uint32 m_nSessionID;
	CreatureMgr* m_creatureMgr;

public:
	MSG_S2C::RoleInfo m_role;
	ui32		nCurExp;
	ui32		nMaxExp;
	ui32		nMoney;
	ui16		nCurHP;
	ui16		nMaxHP;
	ui16		nCurMP;
	ui16		nMaxMP;
	string		strMap;
	float		x;
	float		y;
	float		z;
	float		dir;
	uint32		m_movementflags;
	float		m_fLastBeatTime;
	uint32		m_movecount;
	bool		m_bmove;
	uint32		m_lastPingTime;
	Group*      m_pGroup;

	uint32      m_PinIndex;

	std::string m_strGroupState;

};


float GetCurrentTimeInSec();
