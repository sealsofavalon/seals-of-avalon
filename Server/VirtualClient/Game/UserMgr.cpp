#include "StdAfx.h"
#include "UserMgr.h"
#include "User.h"
#include "../Net/GTSocket.h"
#include <boost/bind.hpp>
#include "../resource.h"
#include "../VirtualClientDlg.h"

UserMgr* g_pUserMgr = NULL;

UserMgr::UserMgr(void) : m_lastcaltime( 0 ), m_lastcalsize( 0.0 ), m_recvsize( 0.0 )
{
	ReadRandomPosition();
}

UserMgr::~UserMgr(void)
{
	for( std::set<User*>::iterator it = m_users.begin(); it != m_users.end(); ++it )
		delete *it;
}

void UserMgr::GenerateUser( const char* szAccountBegin, uint32 nAccountCount, const char* szIP, uint16 Port )
{
	char buff[128];
	for( uint32 i = 0; i < nAccountCount; ++i )
	{
		int length = strlen( szAccountBegin );
		int n = 0;
		while( --length > 0 )
		{
			if( szAccountBegin[length] < '0' || szAccountBegin[length] > '9' )
			{
				sscanf( &szAccountBegin[length+1], "%d", &n );
				break;
			}
		}
		strncpy( buff, szAccountBegin, length+1 );
		buff[length+1] = 0;
		std::string account( buff );
		sprintf( buff, "%d", n + i + 1 );
		account += buff;
		User* p = new User;
		if( !p->Init( account.c_str(), account.c_str(), szIP, Port ) )
		//if( !p->Init( "ivtest2", "0zc4cj59", szIP, Port ) )
		{
			delete p;
			break;
		}
		m_users.insert( p );
		//return;
	}
}

void UserMgr::ReadUserListFile( const char* szAccountFile, const char* szIP, uint16 Port )
{
	/*
	FILE* fp = fopen( szAccountFile, "r" );
	if( !fp )
	{
		AfxMessageBox( "cannot open file stress_test_accounts.txt" );
		return;
	}
<<<<<<< .mine
	
=======

>>>>>>> .r14426
	int i = 0;
	m_account_pwd.clear();
	while( !feof( fp ) )
	{
		char account[64] = { 0 };
		char password[64] = { 0 };
		fscanf( fp, "%s\r%s\n", account, password );
		++i;
		m_account_pwd[std::string(account)] = std::string( password );
		//if( i >= 1000 )
			//break;
	}
	fclose( fp );
	*/

	//for( int i = 0; i < 4000; ++i )
	//{
	//	char tmp[64];
	//	sprintf( tmp, "t%d", i + 1001 );
	//	m_account_pwd[std::string(tmp)] = std::string("1");
	//}

	///*
	int n = atoi( ((CVirtualClientDlg*)AfxGetMainWnd())->m_strAccountBegin );
	int count = ((CVirtualClientDlg*)AfxGetMainWnd())->m_nAccountCount;

	for( int i = n; i < count + n; ++i )
	{
		char tmp[64];
		sprintf( tmp, "t%d", i + 1 );
		m_account_pwd[std::string(tmp)] = std::string("1");
	}
	//*/
}

User* UserMgr::FindUser( uint64 ID )
{
	user_hash_map::iterator it = m_hmUsers.find( ID );
	if( it != m_hmUsers.end() )
		return it->second;
	else
		return NULL;
}

User* UserMgr::FindUser(const char* szName)
{
	user_hash_map::iterator it = m_hmUsers.begin();
	for (; it != m_hmUsers.end(); it ++)
	{
		if (it->second->GetName() == szName)
		{
			return it->second;
		}

	}

	return NULL;
}

void UserMgr::DelUser( User* p )
{
	if( p->GetID() )
	{
		user_hash_map::iterator it = m_hmUsers.find( p->GetID() );
		if( it != m_hmUsers.end() )
		{
			User* p = it->second;
			m_hmUsers.erase( it );
			//OutputMessage( "user(%s) leave the game", p->GetAccount() );
			ListenerForEach( &IUserListener::OnDelUser, p );
		}
	}
	delete p;
	m_users.erase( p );
}

void UserMgr::AddUser( User* p )
{
	if( m_hmUsers.insert( user_hash_map::value_type( p->GetID(), p ) ).second )
	{
		//OutputMessage( "user(%s) join the game", p->GetAccount() );
		ListenerForEach( &IUserListener::OnAddUser, p );
	}
}

void UserMgr::OnRefreshUser( User* p )
{
	ListenerForEach( &IUserListener::OnRefreshUser, p );
}

void UserMgr::AddUserListener( UserMgr::IUserListener* p )
{
	m_listUserListeners.push_back( p );
}

void UserMgr::ListenerForEach( void (IUserListener::*pfn)( const User* p ), User* pUser )
{
	std::for_each( m_listUserListeners.begin(), m_listUserListeners.end(), std::bind2nd( std::mem_fun( pfn ), pUser ) );
}

void UserMgr::HeartBeat()
{
	for( stdext::hash_map<uint64, User*>::iterator it = m_hmUsers.begin(); it != m_hmUsers.end(); ++it )
	{
		User* p = it->second;
		if(!p->m_delete)
		p->HeartBeat();
	}
}

void UserMgr::Run()
{
	UNIXTIME = time( NULL );
	std::set<User*> temp( m_users );
	for( user_set::iterator it = temp.begin(); it != temp.end(); ++it )
	{
		User* p = *it;
		if( !p->m_delete )
			p->Run();
		else
		{
			delete p;
			m_users.erase( p );
		}
	}
	
	uint32 now = get_tick_count();
	if( m_lastcaltime == 0 )
		m_lastcaltime = now;
	else if( now - m_lastcaltime >= 500 )
	{
		float rate = ( m_recvsize - m_lastcalsize ) / 1024.0 * ( 1000.0 / (double)(now - m_lastcaltime) );
		UpdateNetFlowRate( m_recvsize / 1024.0, rate );
		m_lastcaltime = now;
		m_lastcalsize = m_recvsize;
	}

}

void UserMgr::Teleport()
{
	for( user_set::iterator it = m_users.begin(); it != m_users.end(); ++it )
	{
		User* p = *it;
		const position_t& pos = m_vPositions[rand()%m_vPositions.size()];
		p->Teleport( pos.id, pos.x, pos.y, pos.z );
	}
}

void UserMgr::Teleport( int id, int xmin, int xmax, int ymin, int ymax )
{
	for( user_set::iterator it = m_users.begin(); it != m_users.end(); ++it )
	{
		User* p = *it;
		p->Teleport( id, xmin, xmax, ymin, ymax );
	}
}

void UserMgr::SendGMCommand( const char* cmd )
{
	std::for_each( m_users.begin(), m_users.end(), boost::bind( &User::SendGMCommand, _1, cmd ) );
}

void UserMgr::StartMove()
{
	for( user_set::iterator it = m_users.begin(); it != m_users.end(); ++it )
	{
		User* p = *it;
		p->StartMove();
	}
}

void UserMgr::StopMove()
{
	for( user_set::iterator it = m_users.begin(); it != m_users.end(); ++it )
	{
		User* p = *it;
		p->StopMove();
	}
}

void UserMgr::SpawnCreature()
{
	for( user_set::iterator it = m_users.begin(); it != m_users.end(); ++it )
	{
		User* p = *it;
		p->SpawnCreature();
	}
}

void UserMgr::AttackNearest()
{
	for( user_set::iterator it = m_users.begin(); it != m_users.end(); ++it )
	{
		User* p = *it;
		p->AttackCreature();
	}
}

void UserMgr::RecvMessage( uint16 size )
{
	m_recvsize += (double)size;
}

bool UserMgr::GetRandomPosition( int& id, float& x, float& y, float& z )
{
	if( m_vPositions.size() == 0 ) return false;

	const position_t& pos = m_vPositions[rand() % m_vPositions.size()];
	id = pos.id;
	x = pos.x;
	y = pos.y;
	z = pos.z;
	return true;
}

void UserMgr::ReadRandomPosition()
{
	FILE* fp = fopen( "robot_position.txt", "r" );
	if( !fp )
	{
		AfxMessageBox( "cannot open file robot_position.txt" );
		return;
	}
	while( !feof( fp ) )
	{
		position_t pos;
		fscanf( fp, "%d %f %f %f\n", &pos.id, &pos.x, &pos.y, &pos.z );
		m_vPositions.push_back( pos );
	}
	fclose( fp );
}

void UserMgr::logoff_some(int cnt)
{
	int curcnt = g_Config[g_nCurConfig].m_NumsPerLogin;
	while(curcnt-- && m_users.size())
	{
		user_set::iterator itr = m_users.begin();
		User* p = *itr;
		if(!p->m_delete)
		{
			m_account_pwd[p->m_strAccount] = p->m_strPassword;
			if(p->m_pGTSocket)
				p->m_pGTSocket->close();
			p->m_pGTSocket = NULL;
			//p->m_delete = true;
		}
	}
}

void UserMgr::Login( const char* szIP, uint16 Port )
{
	int i = 0;
	if(m_account_pwd.size() == 0)
		AfxMessageBox("�˺�������");

	//logoff_some(0);

	for( std::map<std::string, std::string>::iterator it = m_account_pwd.begin(); it != m_account_pwd.end(); )
	{
		std::map<std::string, std::string>::iterator it2 = it;
		++it;

		/*
		if( m_users.size() )
		{
			User* u = *m_users.begin();
			if( u && u->m_pGTSocket )
			{
				u->m_pGTSocket->close();
				m_users.erase( m_users.begin() );
			}
		}
		*/

		User* p = new User;
		if( !p->Init( it2->first.c_str(), it2->second.c_str(), szIP, Port ) )
		{
			delete p;
			break;
		}
		else
			m_users.insert( p );
		m_account_pwd.erase( it2 );
		++i;
		if( i >= g_Config[g_nCurConfig].m_NumsPerLogin )
			break;
	}
}
