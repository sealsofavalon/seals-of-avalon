#pragma once

#include <hash_map>
class User;

class UserMgr
{
public:
	UserMgr(void);
	~UserMgr(void);

public:
	class IUserListener
	{
	public:
		virtual ~IUserListener(){}
		virtual void OnAddUser( const User* p ) = 0;
		virtual void OnDelUser( const User* p ) = 0;
		virtual void OnRefreshUser( const User* p ) = 0;
	};

public:
	void GenerateUser( const char* szAccountBegin, uint32 nAccountCount, const char* szIP, uint16 Port );
	void ReadUserListFile( const char* szAccountFile, const char* szIP, uint16 Port );
	User* FindUser( uint64 ID );
	User* FindUser(const char* szName);
	void DelUser( User* p );
	void AddUser( User* p );
	void OnRefreshUser( User* p );
	void AddUserListener( IUserListener* p );
	void HeartBeat();
	void Run();
	void Teleport();
	void Teleport( int id, int xmin, int xmax, int ymin, int ymax );
	void SendGMCommand( const char* cmd );
	void StartMove();
	void StopMove();
	void SpawnCreature();
	void AttackNearest();
	void RecvMessage( uint16 size );

	bool GetRandomPosition( int& id, float& x, float& y, float& z );
	void ReadRandomPosition();
	void Login( const char* szIP, uint16 Port );
	void logoff_some(int cnt);

public:
	inline size_t GetOnlineNum() const { return m_hmUsers.size(); }

private:
	struct position_t
	{
		int id;
		float x;
		float y;
		float z;
	};
	std::vector<position_t> m_vPositions;
	void ListenerForEach( void (IUserListener::*pfn)( const User* p ), User* pUser );
	std::list<IUserListener*> m_listUserListeners;
	typedef stdext::hash_map<uint64, User*> user_hash_map;
	user_hash_map m_hmUsers;
	typedef std::set<User*> user_set;
	user_set m_users;
	double m_recvsize;
	double m_lastcalsize;
	uint32 m_lastcaltime;
	std::map<std::string, std::string> m_account_pwd;
	uint32 m_loginCount;
};

extern UserMgr* g_pUserMgr;