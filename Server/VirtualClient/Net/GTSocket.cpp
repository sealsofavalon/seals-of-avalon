#include "StdAfx.h"
#include ".\gtsocket.h"
#include "..\game\UserMgr.h"
#include "..\game\User.h"
#include "GateMsgParser.h"

GTSocket::GTSocket() : tcp_client( *net_global::get_io_service() ), m_pUser( NULL )
{
}

GTSocket::~GTSocket(void)
{
	if( m_pUser )
		m_pUser->m_pGTSocket = NULL;
}

void GTSocket::on_close( const boost::system::error_code& error )
{
	tcp_client::on_close( error );
	g_pUserMgr->DelUser( m_pUser );
}

void GTSocket::on_connect()
{
	//OutputMessage( "connect gate server success, send loginreq message. user(%s)", m_pUser->GetAccount() );
	MSG_C2S::stLoginReq req;
	req.nSession = m_pUser->m_nSessionID;
	req.nAccountID = m_pUser->m_nAccountID;
	req.nIP = this->get_remote_address_ui();
	PostSend( req );
	tcp_client::on_connect();
}

void GTSocket::on_connect_failed( boost::system::error_code error )
{
	//OutputMessage( "connect gate server failed, error message:%s", error.message().c_str() );
}

void GTSocket::proc_message( const message_t& msg )
{
	g_GateMsgParser.ParsePacket( this, (char*)msg.data, msg.len );
}

void GTSocket::PostSend(const char *pszBuf, int nLen)
{
	send_message( pszBuf, nLen );
}

void GTSocket::PostSend(const PakHead& pak)
{
	CPacketSn cps;
	cps << pak;
	cps.SetProLen();
	send_message( cps.GetBuf(), cps.GetSize() );
}