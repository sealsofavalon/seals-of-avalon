#include "StdAfx.h"
#include "GateMsgParser.h"
#include "..\Game\User.h"
#include "..\Game\UserMgr.h"
#include "GTSocket.h"
#include "..\..\..\SDBase\Protocol\C2S_Move.h"
#include "..\..\..\SDBase\Protocol\S2C_Group.h"
#include "..\resource.h"

#include "..\TeamDlg.h"
#include "..\Game\Group.h"

GateMsgParser g_GateMsgParser;

GateMsgParser::GateMsgParser(void)
{
}

GateMsgParser::~GateMsgParser(void)
{
}

void GateMsgParser::ParsePacket( GTSocket* pSocket, char* buff, uint16 size )
{
	CPacketUsn pakTool(buff, size);
	User* pUser = pSocket->m_pUser;
	char szTemp[512];
	if( !pUser )
		return;
	g_pUserMgr->RecvMessage( size );
	switch(pakTool.GetProNo())
	{
	case MSG_S2C::LOGIN_ACK:
		{
			MSG_S2C::stLoginAck msg;
			pakTool >> msg;
			pUser->LoginGame( msg.nError );
		}
		break;
	case MSG_S2C::ENTERGAME_ACK:
		{
			MSG_S2C::stEnterGameAck msg;
			pakTool >> msg;
			pUser->EnterGame( msg );
		}
		break;
	case MSG_S2C::CREATEROLE_ACK:
		{
			MSG_S2C::stCreateRoleAck msg;
			pakTool >> msg;
			pUser->CreateRoleAck( msg.nError, msg.role );
		}
		break;
	case MSG_S2C::ROLELIST_ACK:
		{
			MSG_S2C::stRoleListAck msg;
			pakTool >> msg;
			pUser->RoleListAck( msg.nError, msg.vRoles );
		}
		break;
	case MSG_S2C::UPDATE_OBJECT:
		{
			MSG_S2C::stObjectUpdate msg;
			pakTool >> msg;
			pUser->UpdateObject( msg );
		}
		break;
	case MSG_S2C::MOVE_OP:
		{
			MSG_S2C::stMove_OP msg;
			pakTool >> msg;
			pUser->MoveOP( msg );
		}
		break;
	case MSG_S2C::TRANSFER_PENDING:
		{

			pUser->AskTele();



		}
		break;
	case MSG_S2C::MOVE_TELEPORT_ACK:
		{
			MSG_S2C::stMove_Teleport_Ack msg;
			pakTool >> msg;
			pUser->x = msg.x;
			pUser->y = msg.y;
			pUser->z = msg.z;
			pUser->AskTele();

		}
		break;
	case MSG_S2C::MOVE_NEW_WORLD:
		{

			MSG_S2C::stMove_New_World msg;
			pakTool >> msg;
			pUser->x = msg.pos_x;
			pUser->y = msg.pos_y;
			pUser->z = msg.pos_z;
			pUser->AskTele();
		}
		break;
	case MSG_S2C::GROUP_INVITE:
		{
			MSG_S2C::stGroupInvite msg;
			pakTool >> msg;
			pUser->m_strGroupState = msg.name + "邀请组队";
			GetTeamDlg()->RefreshUserItemOutTeam(pUser);


		}
		break;
	case MSG_S2C::MSG_DUNGEON_DIFFICULTY:
		{
			MSG_S2C::stMsg_Dungeon_Difficulty msg;
			pakTool >> msg;
			pUser->m_strGroupState;
			

		}
		break;

	case MSG_S2C::PARTY_MEMBER_STATS:
		{
			MSG_S2C::stPartyMemberStat msg;
			pakTool >> msg;
			pUser->UpdateMemeberState(msg);

		}
		break;

	case MSG_S2C::GROUP_LIST:
		{
			MSG_S2C::stGroupList msg;
			pakTool >> msg;
			pUser->RecvGroupList(msg);


		}
		break;
	case MSG_S2C::GROUP_UNINVITE:
		{

		}
		break;

	case MSG_S2C::GROUP_DECLINE:
		{
			MSG_S2C::stGroupDecline MsgRecv;
			pakTool>>MsgRecv;
			sprintf(szTemp, "%s,拒绝", MsgRecv.name.c_str());
			pUser->m_strGroupState = szTemp;
			pUser->BeDecline();
		}
		break;

	case MSG_S2C::GROUP_SET_LEADER:
		{
			MSG_S2C::stGroupSetLeader MsgRecv;
			pakTool>>MsgRecv;
			pUser->ChangeLeader(MsgRecv);
		}
		break;


	case  MSG_S2C::MSG_GROUP_SWEP:
		{
			MSG_S2C::stGroupSwep Msg;
			pakTool >> Msg;
			pUser->SwepMember(Msg);

		}
		break;

	case MSG_S2C::RAID_READYCHECK:
		{
			MSG_S2C::stRaid_ReadyCheck MsgRecv;
			pakTool >> MsgRecv;
			pUser->ReadyCheck(MsgRecv);
		}
		break;

	case MSG_S2C::GROUP_DESTROYED:
		{
			MSG_S2C::stGroupDestroyed Msg;
			pakTool >> Msg;
			pUser->DestroyGroup(Msg);
		}
		break;
	case  MSG_S2C::PARTY_COMMAND_RESULT:
		{

		}
		break;

	case MSG_S2C::GROUP_SET_PLAYER_ICON:
		{
			MSG_S2C::stGroupSetPlayerIcon Msg;
			pakTool >> Msg;
			pUser->GroupSetIcon(Msg);
		
		}
		break;

	case MSG_S2C::MSG_ADD_MEMBER:
		{
			MSG_S2C::stGroupAddMember Msg;
			pakTool >> Msg;

			int n = 0;
			n ++;





			pUser->GroupAddMember(Msg);
		}
		break;

	case  MSG_S2C::MSG_REMOVE_MEMBER:
		{
			MSG_S2C::stGroupRemoveMember Msg;
			pakTool >> Msg;

			pUser->m_pGroup->RemoveMember(Msg.guid);
			GetTeamDlg()->RefreshTeamMemberList(pUser->m_pGroup);
		}
		break;

	case  MSG_S2C::MSG_CHANGE_SUBGROUP:
		{
			MSG_S2C::stGroupChangeSubgroup Msg;
			pakTool >> Msg;
			pUser->m_pGroup->ChangeSubgroup(Msg.guid, Msg.team);
			GetTeamDlg()->RefreshTeamMemberList(pUser->m_pGroup);

		}
		break;;
	default:
//		OutputMessage( "LoginMsgParser::ParsePacket, 发现未知协议号:%x", pakTool.GetProNo() );
		break;
	}
}