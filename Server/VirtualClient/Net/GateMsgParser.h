#pragma once

class GTSocket;

class GateMsgParser
{
public:
	GateMsgParser(void);
	~GateMsgParser(void);

public:
	void ParsePacket( GTSocket* pSocket, char* buff, uint16 size );
};

extern GateMsgParser g_GateMsgParser;