#include "StdAfx.h"
#include ".\lssocket.h"
#include "..\Game\User.h"
#include "LoginMsgParser.h"

extern long g_nVersion;

LSSocket::LSSocket() : tcp_client( *net_global::get_io_service() ), m_pUser( NULL )
{
}


LSSocket::~LSSocket(void)
{
	if( m_pUser )
		m_pUser->m_pLSSocket = NULL;
}

void LSSocket::on_close( const boost::system::error_code& error )
{
	//OutputMessage( "disconnected from login server, user(%s) error[%s]", m_pUser->GetAccount(), error.message().c_str() );
	//delete this;
}

void LSSocket::on_connect()
{
	//OutputMessage( "connect login server success, send version req message. user(%s)", m_pUser->GetAccount() );
	MSG_C2LS::stVersionReq req;
	//req.nVersion = 091022;
	req.nVersion = g_nVersion; //g_Config[g_nCurConfig].m_nVersion;
	PostSend( req );
	tcp_client::on_connect();
}

void LSSocket::on_connect_failed( boost::system::error_code error )
{
	//OutputMessage( "connect login server failed, error message:%s", error.message().c_str() );
}

void LSSocket::proc_message( const message_t& msg )
{
	g_LoginMsgParser.ParsePacket( this, (char*)msg.data, msg.len );
}

void LSSocket::PostSend(const char *pszBuf, int nLen)
{
	send_message( pszBuf, nLen );
}

void LSSocket::PostSend(const PakHead& pak)
{
	CPacketSn cps;
	cps << pak;
	cps.SetProLen();
	send_message( cps.GetBuf(), cps.GetSize() );
}