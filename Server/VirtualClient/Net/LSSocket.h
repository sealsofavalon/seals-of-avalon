#pragma once
#include "..\..\new_common\Source\Net\tcpclient.h"

class User;

class LSSocket :
	public tcp_client
{
public:
	LSSocket();
	~LSSocket(void);

	virtual void on_close( const boost::system::error_code& error );
	virtual void on_connect();
	virtual void on_connect_failed( boost::system::error_code error );
	virtual void proc_message( const message_t& msg );
	void PostSend(const char *pszBuf, int nLen);
	void PostSend(const PakHead& pak);

	User* m_pUser;
};
