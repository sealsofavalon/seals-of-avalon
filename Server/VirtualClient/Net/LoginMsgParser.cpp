#include "StdAfx.h"
#include "LoginMsgParser.h"
#include "..\Game\User.h"
#include "LSSocket.h"
#include "../Game/UserMgr.h"

LoginMsgParser g_LoginMsgParser;

LoginMsgParser::LoginMsgParser(void)
{
}

LoginMsgParser::~LoginMsgParser(void)
{
}

void LoginMsgParser::ParsePacket( LSSocket* pSocket, char* buff, uint16 size )
{
	CPacketUsn pakTool(buff, size);
	User* pUser = pSocket->m_pUser;
	switch(pakTool.GetProNo())
	{
	case MSG_LS2C::VERSION_ACK:
		{
			MSG_LS2C::stVersionAck msg;
			pakTool >> msg;
			pUser->LongReq( msg.encrypt_table );
		}
		break;
	case MSG_LS2C::LOGIN_ACK:
		{
			MSG_LS2C::stLoginAck msg;
			pakTool >> msg;
			switch( msg.nStat )
			{
			case MSG_LS2C::stLoginAck::LOGIN_OK:					//成功
				pUser->SelectGroup( msg.nAccountId, msg.nSessionID, msg.vGroup );
				break;
			case MSG_LS2C::stLoginAck::LOGIN_USER_ONLINE:			//当前用户在线
				OutputMessage( "user(%s) is already online", pUser->GetAccount() );
				break;
			case MSG_LS2C::stLoginAck::LOGIN_PASSWORD_ERROR:		//密码错误
				OutputMessage( "user(%s) password wrong", pUser->GetAccount() );
				break;
			case MSG_LS2C::stLoginAck::LOGIN_USER_NOT_FOUND:		//用户名不存在
				OutputMessage( "user(%s) not found", pUser->GetAccount() );
				break;
			case MSG_LS2C::stLoginAck::LOGIN_DATABASE_EXCEPTION:	//数据库异常
				OutputMessage( "user(%s) database exception", pUser->GetAccount() );
				break;
			case MSG_LS2C::stLoginAck::LOGIN_NOT_ENOUGH_POINT_OR_EXPIRE:	// 点卡不够或包月到期
				OutputMessage( "user(%s) account expire", pUser->GetAccount() );
				break;
			case MSG_LS2C::stLoginAck::LOGIN_VERSION_NOT_MATCH:
				OutputMessage( "version not match" );
				break;
			default:
				assert( 0 );
				break;
			}
		}
		break;
	case MSG_LS2C::GROUP_SEL_ACK:
		{
			MSG_LS2C::stGroupSelAck msg;
			pakTool >> msg;
			if( msg.nStat == 1 )
				pUser->ConnectGate( msg.IP, msg.port );
			else
			{
				OutputMessage( "user(%s) no gate server dispatched!!!", pUser->GetAccount() );
				//g_pUserMgr->DelUser( pUser );
			}
		}
		break;
	default:
//		OutputMessage( "LoginMsgParser::ParsePacket, 发现未知协议号:%x", pakTool.GetProNo() );
		break;
	}
}