#pragma once

class LSSocket;

class LoginMsgParser
{
public:
	LoginMsgParser(void);
	~LoginMsgParser(void);

	void ParsePacket( LSSocket* pSocket, char* buff, uint16 size );
};

extern LoginMsgParser g_LoginMsgParser;