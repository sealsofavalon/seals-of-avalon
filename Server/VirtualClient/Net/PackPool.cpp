#include "stdafx.h"
#include "Common/Net/PakPool.h"
#include "GateMsgParser.h"
#include "LoginMsgParser.h"

void CPakPool::ParseAllPackages()
{
	PTNETPACKAGE pPkg = NULL;

	GateMsgParser gp;
	LoginMsgParser lp;
	while (pPkg = PopPak())
	{
		switch(pPkg->m_eType)
		{
		case eFromGT:
			{
				gp.ParsePacket((GTSocket*)(pPkg->m_pSocket), pPkg->m_szPackage, pPkg->m_iPkgLen);
			}
			break;
		case eFromLS:
			{
				lp.ParsePacket((LSSocket*)(pPkg->m_pSocket), pPkg->m_szPackage, pPkg->m_iPkgLen);
			}
			break;
		default:
			{
				sLog.outError("CPackagePool::ProccessPackages(), 发现未知数据来源类型(%u)", pPkg->m_eType);
			}
			break;
		}
		FreePak(pPkg);
	}
}