// TeamDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "VirtualClient.h"
#include "TeamDlg.h"
#include "Game/User.h"
#include "Game/Group.h"


// CTeamDlg 对话框

IMPLEMENT_DYNAMIC(CTeamDlg, CDialog)

CTeamDlg::CTeamDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CTeamDlg::IDD, pParent),
	m_pCurSelect(NULL)
{

}

CTeamDlg::~CTeamDlg()
{
}

void CTeamDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_Group2, m_listUser);
	DDX_Control(pDX, IDC_LISTLEADER, m_listLeader);
	DDX_Control(pDX, IDC_LIST_Group, m_listTeamMember);
}


BOOL CTeamDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	m_listUser.InsertColumn( 0, "GUID", LVCFMT_LEFT, 50, 0 );
	m_listUser.InsertColumn( 1, "Name", LVCFMT_LEFT, 80, 1 );
	m_listUser.InsertColumn( 2, "状态", LVCFMT_LEFT, 80, 2 );
	m_listUser.InsertColumn( 3, "队伍状态", LVCFMT_LEFT, 30, 3);
	m_listUser.InsertColumn(4, "种族", LVCFMT_LEFT, 80, 4);

	m_listTeamMember.InsertColumn( 0, "GUID", LVCFMT_LEFT, 50, 0 );
	m_listTeamMember.InsertColumn( 1, "Name", LVCFMT_LEFT, 80, 1 );
	m_listTeamMember.InsertColumn( 2, "title", LVCFMT_LEFT, 80, 1 );
	m_listTeamMember.InsertColumn(3, "小队", LVCFMT_LEFT, 80, 1);
	m_listTeamMember.InsertColumn(4, "标记", LVCFMT_LEFT, 80, 1);
	//m_listTeamMember.InsertColumn( 2, "", LVCFMT_LEFT, 80, 1 );


	::SendMessage(m_listTeamMember.m_hWnd, LVM_SETEXTENDEDLISTVIEWSTYLE, LVS_EX_FULLROWSELECT, LVS_EX_FULLROWSELECT );

	::SendMessage(m_listUser.m_hWnd, LVM_SETEXTENDEDLISTVIEWSTYLE, LVS_EX_FULLROWSELECT, LVS_EX_FULLROWSELECT );
	return TRUE;

}

void CTeamDlg::AddTeamLeader(const User* pUser)
{
	int n = m_listLeader.GetCount();

	User* TempUser = NULL;
	for (int i = 0; i < n; i ++)
	{
		char sz[512];
		TempUser = (User*)m_listLeader.GetItemDataPtr(i);
		if (TempUser== pUser)
		{
			return;
		}
	}
	m_listLeader.InsertString(n,pUser->GetName());
	m_listLeader.SetItemDataPtr(n, (void*)pUser);


}

void CTeamDlg::DelTeamLeader(const User* pUser)
{
	User* pTem = NULL;
	int n = m_listLeader.GetCount();
	for (int i = 0; i < n; i ++)
	{
		pTem = (User*)m_listLeader.GetItemDataPtr(i);
		if (pTem && pUser)
		{
			if (pTem->m_role.name == pUser->m_role.name)
			{
				m_listLeader.DeleteString(i);
			}
		}

	}

}

void CTeamDlg::DelTeamLeader(ui64 guid)
{
	User* pTem = NULL;
	int n = m_listLeader.GetCount();
	for (int i = 0; i < n; i ++)
	{
		pTem = (User*)m_listLeader.GetItemDataPtr(i);
		if (pTem)
		{
			if (pTem->m_role.guid ==guid)
			{
				m_listLeader.DeleteString(i);

				ClearTeamMemberList();
				return;
			}
		}

	}

}

void CTeamDlg::DelMemberTeamList(MSG_S2C::stGroupList::stMember* pUser)
{
	int count = m_listTeamMember.GetItemCount();
	for( int i = 0; i < count; ++i )
	{
		if( m_listTeamMember.GetItemData( i ) == (DWORD_PTR)pUser )
		{
			m_listTeamMember.DeleteItem( i );
			break;
		}
	}
}



void CTeamDlg::AddMemberTeamList(MSG_S2C::stGroupList::stMember* pUser, const char* szTitle, int nSub, uint64 Icon)
{

	char buff[128];
	sprintf( buff, "%u", pUser->guid );
	int n = m_listTeamMember.InsertItem( LVIF_TEXT, buff );
	m_listTeamMember.SetItemText(n , 0, buff);
	m_listTeamMember.SetItemText(n, 1, pUser->name.c_str());
	m_listTeamMember.SetItemText(n, 2, szTitle);
	sprintf( buff, "%u", nSub);
	m_listTeamMember.SetItemText(n, 3, buff);
	m_listTeamMember.SetItemData( n, (DWORD_PTR)pUser );
	sprintf( buff, "%u", Icon);
	m_listTeamMember.SetItemText(n, 4, buff);
	
}


void CTeamDlg::ClearTeamMemberList()
{
	m_listTeamMember.DeleteAllItems();
}
void CTeamDlg::RefreshTeamMemberList(const Group* pGroup)
{
	ClearTeamMemberList();
	if (!pGroup)
	{

		return;
	}

	SubGroup* pCurSub;
	ui64 LeaderId;
	ui64 TeamLeader;

	std::string str;
	LeaderId = pGroup->m_pGroupLeader;
	for (int i = 0; i < MAXSUBGROUP; i ++)
	{
		pCurSub = pGroup->m_pSubGroup[i];
		if (!pCurSub)
		{
			continue;
		}
		TeamLeader = pCurSub->m_leader;
		str = "普通";

		for (int j  = 0; j < MAXMEMBER; j ++)
		{
			if(pCurSub->m_Member[j].guid == 0)
			{
				continue;
			}
			str = "普通";
			ui64 Memberguid = pCurSub->m_Member[j].guid;

			if (  Memberguid == LeaderId)
			{
				str = "领袖";
			}
			else if (Memberguid == TeamLeader)
			{
				str = "队长";
			}

			Group::MAPICON::const_iterator it  = pGroup->m_mapIcon.begin();
			uint64 uiIcon = 0;
			for (; it != pGroup->m_mapIcon.end();  it ++)
			{
				if (Memberguid == it->first)
				{
					uiIcon = it->second;
				}
			}

			AddMemberTeamList(&(pCurSub->m_Member[j]), str.c_str(), i, uiIcon);


		}
	}


}

void CTeamDlg::DelUserOutTeam(const User* pUser)
{
	int count = m_listUser.GetItemCount();
	for( int i = 0; i < count; ++i )
	{
		if( m_listUser.GetItemData( i ) == (DWORD_PTR)pUser )
		{
			m_listUser.DeleteItem( i );
			break;
		}
	}

}


int CTeamDlg::FindUserOutTeam(const User* pUser)
{
	User* TempUser = NULL;
	int n = m_listUser.GetItemCount();
	for (int i = 0; i < n; i ++)
	{
		if (m_listUser.GetItemData(i))
		{
			TempUser = (User*)m_listUser.GetItemData(i);
			if (TempUser->GetID() == pUser->GetID())
			{
				return i;
			}
		}
	}

	return -1;
}

void CTeamDlg::RefreshUserItemOutTeam(int n, const User* pUser)
{
	char buff[128];
	m_listUser.SetItemText( n, 1, pUser->m_role.name.c_str() );

	m_listUser.SetItemText( n, 2, pUser->m_strGroupState.c_str() );
	std::string str = "普通";
	if (pUser->m_pGroup)
	{
		ui64 guid = pUser->m_role.guid;
		bool bFind = false;
		str = "成员";
	
		for(int i = 0; i < MAXSUBGROUP; i ++)
		{
			if (bFind)
			{
				break;
			}
			if (!pUser->m_pGroup->m_pSubGroup[i])
			{
				continue;;
			}

			if (pUser->m_pGroup->m_pSubGroup[i]->m_leader == guid)
			{
				str = "队长";
				break;
			}

			for (int j = 0; j <MAXMEMBER; j ++)
			{

				if (!pUser->m_pGroup->m_pSubGroup[i])
				{
					continue;
				}
				if (pUser->m_pGroup->m_pSubGroup[i]->m_Member[j].guid == guid)
				{
					str = "成员";
					bFind = true;
					break;
				}
			}
		}

		if (pUser->m_pGroup->m_pGroupLeader == pUser->m_role.guid)
		{
			str = "领袖";
		}

	
	}
	m_listUser.SetItemText(n, 3, str.c_str());
	char sz[125];
	sprintf(sz, "%d", pUser->m_role.nRace);
	m_listUser.SetItemText(n, 4, sz);

	//sprintf( buff, "%.2f", p->x );
	//m_listUsers.SetItemText( nItem, 3, buff );
	//sprintf( buff, "%.2f", p->y );
	//m_listUsers.SetItemText( nItem, 4, buff );
}

void CTeamDlg::RefreshUserItemOutTeam(const User* pUser)
{
	int n = FindUserOutTeam(pUser);
	if(n != -1)
	{
		RefreshUserItemOutTeam(n, pUser);
	}
}

void CTeamDlg::AddUserOutTeam(const User* pUser)
{

	char buff[128];
	sprintf( buff, "%u", pUser->GetID() );
	int n = m_listUser.InsertItem( LVIF_TEXT, buff );
	m_listUser.SetItemData( n, (DWORD_PTR)pUser );
	RefreshUserItemOutTeam(n, pUser);

}

BEGIN_MESSAGE_MAP(CTeamDlg, CDialog)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST_Group2, &CTeamDlg::OnLvnItemchangedListGroup2)
	ON_NOTIFY(NM_CLICK, IDC_LIST_Group2, &CTeamDlg::OnNMClickListGroup2)
	ON_LBN_SELCHANGE(IDC_LISTLEADER, &CTeamDlg::OnLbnSelchangeListleader)
	ON_BN_CLICKED(IDC_BTNINVIDE, &CTeamDlg::OnBnClickedBtninvide)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_Group2, &CTeamDlg::OnNMDblclkListGroup2)
	ON_LBN_DBLCLK(IDC_LISTLEADER, &CTeamDlg::OnLbnDblclkListleader)
	ON_BN_CLICKED(IDC_BUTTON6, &CTeamDlg::OnBnClickedButton6)
	ON_BN_CLICKED(IDC_BTNREFUSE, &CTeamDlg::OnBnClickedBtnrefuse)
	ON_BN_CLICKED(IDC_BUTTON7, &CTeamDlg::OnBnClickedButton7)
	ON_NOTIFY(NM_CLICK, IDC_LIST_Group, &CTeamDlg::OnNMClickListGroup)
	ON_BN_CLICKED(IDC_BTNSETLEADER, &CTeamDlg::OnBnClickedBtnsetleader)
	ON_BN_CLICKED(IDC_BUTTON8, &CTeamDlg::OnBnClickedButton8)
	ON_BN_CLICKED(IDC_BTNCHANGETEAM, &CTeamDlg::OnBnClickedBtnchangeteam)
	ON_BN_CLICKED(IDC_SwepTeam, &CTeamDlg::OnBnClickedSwepteam)
	ON_BN_CLICKED(IDC_BTNREADY, &CTeamDlg::OnBnClickedBtnready)
	ON_BN_CLICKED(IDC_BTNFLG, &CTeamDlg::OnBnClickedBtnflg)
END_MESSAGE_MAP()


// CTeamDlg 消息处理程序

void CTeamDlg::OnLvnItemchangedListGroup2(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);

	pNMLV->iItem;


	// TODO: 在此添加控件通知处理程序代码
	*pResult = 0;
}

void CTeamDlg::OnNMClickListGroup2(NMHDR *pNMHDR, LRESULT *pResult)
{

	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	pNMItemActivate->iItem;
	if (pNMItemActivate->iItem == -1)
	{
		return;
	}



	User* pUser = (User*)m_listUser.GetItemData(pNMItemActivate->iItem);
	if (pUser)
	{
		m_select.strName = pUser->GetName();
		m_select.guid = pUser->m_role.guid;
		SetDlgItemText(IDC_EDITNAME, m_select.strName.c_str());
	}

	*pResult = 0;
}

void CTeamDlg::OnLbnSelchangeListleader()
{
	int n = m_listLeader.GetCurSel();
	User* pUser = (User*)m_listLeader.GetItemDataPtr(n);
	if (pUser)
	{
		m_select.strName = pUser->GetName();
		m_select.guid = pUser->m_role.guid;

		SetDlgItemText(IDC_SELECTNAME, m_select.strName.c_str());

		if (pUser->m_pGroup)
		{
			RefreshTeamMemberList(pUser->m_pGroup);
		}
		
	}

	

	
	// TODO: 在此添加控件通知处理程序代码
}

void CTeamDlg::OnBnClickedBtninvide()
{

	if (m_pCurSelect && !m_select.strName.empty())
	{
		m_pCurSelect->GroupInvite( m_select.strName);
	}


	// TODO: 在此添加控件通知处理程序代码
}

void CTeamDlg::OnNMDblclkListGroup2(NMHDR *pNMHDR, LRESULT *pResult)
{

	// TODO: 在此添加控件通知处理程序代码

	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	pNMItemActivate->iItem;
	if (pNMItemActivate->iItem == -1)
	{
		return;
	}



	User* pUser = (User*)m_listUser.GetItemData(pNMItemActivate->iItem);
	if (pUser)
	{
		m_pCurSelect = pUser;
		SetDlgItemText(IDC_SELECTNAME, m_pCurSelect->GetName());


		if (pUser->m_pGroup)
		{
			RefreshTeamMemberList(pUser->m_pGroup);
		}
	}



	*pResult = 0;
}

void CTeamDlg::OnLbnDblclkListleader()
{
	int n = m_listLeader.GetCurSel();
	User* pUser = (User*)m_listLeader.GetItemDataPtr(n);
	if (pUser)
	{
		m_pCurSelect = pUser;

		SetDlgItemText(IDC_SELECTNAME, m_pCurSelect->GetName());
	
		
	}
	// TODO: 在此添加控件通知处理程序代码
}

///同意组队//////
void CTeamDlg::OnBnClickedButton6()
{
	if (m_pCurSelect)
	{
		m_pCurSelect->AcceptIvite();
	}
	// TODO: 在此添加控件通知处理程序代码
}

void CTeamDlg::OnBnClickedBtnrefuse()
{

	if (m_pCurSelect)
	{
		m_pCurSelect->RefuseInvite();
	}
	// TODO: 在此添加控件通知处理程序代码
}


//////////////////////////////////////////////////////////////////////////移除队员//////////////////////////////////////////////////////////////////////////////

void CTeamDlg::OnBnClickedButton7()
{

	if (!m_pCurSelect)
	{
	
		return;
	}

	if (!m_pCurSelect->m_pGroup) 
	{
		return;
	}

	if (m_pCurSelect->m_role.name == m_select.strName)
	{

		m_pCurSelect->SendGroupDisbandMsg();
	}
	else
	if (m_pCurSelect->m_pGroup->m_pGroupLeader == m_pCurSelect->GetID())
	{
		m_pCurSelect->SendRemovePlayer(m_select.guid);
		
	}

	
	// TODO: 在此添加控件通知处理程序代码
}

void CTeamDlg::OnNMClickListGroup(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	if (!pNMItemActivate)
	{
		return;
	}
	char sz[512];
	sz[0] = 0;
	int nLen = 0;

	LPTSTR cp = NULL;
	ui64 guid;
	m_listTeamMember.GetItemText(pNMItemActivate->iItem, 1, sz, 512);
	m_select.strName = sz;
	m_listTeamMember.GetItemText(pNMItemActivate->iItem, 0, sz, 512);
	guid = atoi(sz);
	m_select.guid = guid;
	

	SetDlgItemText(IDC_EDITNAME, m_select.strName.c_str());

	// TODO: 在此添加控件通知处理程序代码
	*pResult = 0;
}

void CTeamDlg::OnBnClickedBtnsetleader()
{

	if (m_pCurSelect)
	{
		if (m_select.guid != m_pCurSelect->m_role.guid)
		{
			MSG_C2S::stGroupSetLeader msg;
			msg.guid = m_select.guid;
			m_pCurSelect->ChangeLeader(msg);
		}


	}
	// TODO: 在此添加控件通知处理程序代码
}

void CTeamDlg::OnBnClickedButton8()
{
	if (m_pCurSelect)
	{
		if (m_pCurSelect->m_pGroup)
		{
			if (m_pCurSelect->m_pGroup->m_pGroupLeader == m_pCurSelect->m_role.guid)
			{
				m_pCurSelect->GroupToRaid();


			}
		}
	}
	// TODO: 在此添加控件通知处理程序代码
}

void CTeamDlg::OnBnClickedBtnchangeteam()
{

	if (m_pCurSelect)
	{
		if (m_pCurSelect->m_pGroup)
		{
			if (m_pCurSelect->m_pGroup->m_pGroupLeader == m_pCurSelect->m_role.guid)
			{
				int n = GetDlgItemInt(IDC_EDTTEAMID);
				MSG_C2S::stGroup_Change_Sub_Group msg;
				msg.name = m_select.strName;
				msg.subGroup = n;
				m_pCurSelect->ChangeSubGroup(msg);

			}
		}
	}

	// TODO: 在此添加控件通知处理程序代码
}

void CTeamDlg::OnBnClickedSwepteam()
{


	if (m_pCurSelect)
	{
		if (m_pCurSelect->m_pGroup)
		{
			if (m_pCurSelect->m_pGroup->m_pGroupLeader == m_pCurSelect->m_role.guid)
			{
				CString str1;
				GetDlgItemText(IDC_EDTSWEP1, str1);
				CString str2;
				GetDlgItemText(IDC_EDTSWEP2, str2);

				if (!str1.IsEmpty() && !str2.IsEmpty())
				{
					MSG_C2S::stGroupSwepName Msg;
					Msg.des = str1;
					Msg.src = str2;
					m_pCurSelect->SwepMemberGroup(Msg);
				}

			}
		}
	}
	// TODO: 在此添加控件通知处理程序代码
}

void CTeamDlg::OnBnClickedBtnready()
{

	if (m_pCurSelect)
	{
		if (m_pCurSelect->m_pGroup)
		{

			MSG_C2S::stRaid_ReadyCheck msg;
			if (m_pCurSelect->m_pGroup->m_pGroupLeader == m_pCurSelect->m_role.guid)
			{



				msg.ready = 0;


			}
			else
			{
				msg.ready = 1;
			}
			m_pCurSelect->ReadyCheck(msg);
		}

	}
	// TODO: 在此添加控件通知处理程序代码
}

void CTeamDlg::OnBnClickedBtnflg()
{


	if (m_pCurSelect)
	{
		if (m_pCurSelect->m_pGroup)
		{
			if (m_pCurSelect->m_pGroup->m_pGroupLeader == m_pCurSelect->m_role.guid)
			{
				MSG_C2S::stGroupSetPlayerIcon msg;
				msg.guid = m_select.guid;
				msg.icon = 1;
				m_pCurSelect->GroupSetIcon(msg);
			}
		}
	}
	// TODO: 在此添加控件通知处理程序代码
}
