#pragma once
#include "afxcmn.h"
#include "afxwin.h"
#include "..\..\SDBase\Protocol\S2C_Group.h"

class User;
// CTeamDlg 对话框
class Group;

struct tgSelectInfo
{
	std::string strName;
	ui64         guid;
};
class CTeamDlg : public CDialog
{
	DECLARE_DYNAMIC(CTeamDlg)

public:
	CTeamDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CTeamDlg();

// 对话框数据
	enum { IDD = IDD_DLGGROUP };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持
	virtual BOOL OnInitDialog();
	
public:
	void AddUserOutTeam(const User* pUser);
	void DelUserOutTeam(const User* pUser);
	int FindUserOutTeam(const User* pUser);

	void RefreshUserItemOutTeam(int n, const User* pUser);
	void RefreshUserItemOutTeam(const User* pUser);

	void AddTeamLeader(const User* pUser);
	void DelTeamLeader(const User* pUser);
	void DelTeamLeader(ui64 guid);
	

	void RefreshTeamMemberList(const Group* pGroup);

	void AddMemberTeamList(MSG_S2C::stGroupList::stMember* pUser, const char* szTitle, int nSub, ui64 Icon = 0);
	void DelMemberTeamList(MSG_S2C::stGroupList::stMember* pUser);

	void ClearTeamMemberList();

protected:

	DECLARE_MESSAGE_MAP()
public:
	CListCtrl m_listUser;
	CListBox m_listLeader;
	CListCtrl m_listTeamMember;
	User*        m_pCurSelect;
	
	tgSelectInfo m_select;
	afx_msg void OnLvnItemchangedListGroup2(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMClickListGroup2(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnLbnSelchangeListleader();
	afx_msg void OnBnClickedBtninvide();
	afx_msg void OnNMDblclkListGroup2(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnLbnDblclkListleader();
	afx_msg void OnBnClickedButton6();
	afx_msg void OnBnClickedBtnrefuse();
	afx_msg void OnBnClickedButton7();
	afx_msg void OnNMClickListGroup(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedBtnsetleader();
	afx_msg void OnBnClickedButton8();
	afx_msg void OnBnClickedBtnchangeteam();
	afx_msg void OnBnClickedSwepteam();
	afx_msg void OnBnClickedBtnready();
	afx_msg void OnBnClickedBtnflg();
};
