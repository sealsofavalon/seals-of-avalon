// VirtualClientDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "VirtualClient.h"
#include "VirtualClientDlg.h"
#include "game\UserMgr.h"
#include "game\User.h"
#include <time.h>
#include "TeamDlg.h"
#include "../../new_common/Source/new_common.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

class UserListener : public UserMgr::IUserListener
{
public:
	virtual void OnAddUser( const User* p )
	{
		((CVirtualClientDlg*)AfxGetMainWnd())->AddUser( p );
	}
	virtual void OnDelUser( const User* p )
	{
		((CVirtualClientDlg*)AfxGetMainWnd())->DelUser( p );
	}
	virtual void OnRefreshUser( const User* p )
	{
		((CVirtualClientDlg*)AfxGetMainWnd())->RefreshUser( p );
	}
};

zip_compress_strategy cs;

void OutputMessage( const char* szMessage, ... )
{
	va_list ap;
	va_start(ap, szMessage);
	char out[32768];

	time_t t = time(NULL);
	tm* aTm = localtime(&t);
	sprintf(out, "%02d:%02d:%02d]",aTm->tm_hour,aTm->tm_min,aTm->tm_sec);
	size_t l = strlen(out);
	vsnprintf(&out[l], 32768 - l, szMessage, ap);

	int n = ((CVirtualClientDlg*)AfxGetMainWnd())->m_listboxLog.InsertString( -1, out );
	((CVirtualClientDlg*)AfxGetMainWnd())->m_listboxLog.SetCurSel( n );

	va_end(ap);
}

CTeamDlg* GetTeamDlg()
{
	return ((CVirtualClientDlg*)AfxGetMainWnd())->m_pTeamDlg;
}

void UpdateNetFlowRate( float rsize, float rate )
{
	char strsize[64] = { 0 };
	char strrate[64] = { 0 };
	if( rsize < 1024.f )
		sprintf( strsize, "%.3f kb", rsize );
	else
		sprintf( strsize, "%.3f mb", rsize / 1024.f );

	if( rate < 1024.f )
		sprintf( strrate, "%.3f kb", rate );
	else
		sprintf( strrate, "%.3f mb", rate / 1024.f );

	((CVirtualClientDlg*)AfxGetMainWnd())->m_edtRecvSize.SetWindowText( strsize );
	((CVirtualClientDlg*)AfxGetMainWnd())->m_edtRecvRate.SetWindowText( strrate );
}

// 用于应用程序“关于”菜单项的 CAboutDlg 对话框


class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CVirtualClientDlg 对话框




CVirtualClientDlg::CVirtualClientDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CVirtualClientDlg::IDD, pParent)
	, m_strAccountBegin(_T("1"))
	, m_nAccountCount(1000)
	, m_teleportmapid(6)
	, m_xmin(1100)
	, m_xmax(1500)
	, m_ymin(1100)
	, m_ymax(1500)
	, m_gmcmd(_T(""))
	, m_nVersion( 2000 )
	, m_pTeamDlg(NULL)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CVirtualClientDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_ACCOUNTBEGIN, m_strAccountBegin);
	DDX_Text(pDX, IDC_EDIT_ACCOUNTCOUNT, m_nAccountCount);
	DDV_MinMaxInt(pDX, m_nAccountCount, 0, 50000);
	DDX_Control(pDX, IDC_LIST_USERS, m_listUsers);
	DDX_Control(pDX, IDC_LIST_LOG, m_listboxLog);
	DDX_Control(pDX, IDC_EDIT_ONLINE_NUM, m_edtOnlineNum);
	DDX_Text(pDX, IDC_EDIT1, m_teleportmapid);
	DDX_Text(pDX, IDC_EDIT2, m_xmin);
	DDX_Text(pDX, IDC_EDIT4, m_xmax);
	DDX_Text(pDX, IDC_EDIT3, m_ymin);
	DDX_Text(pDX, IDC_EDIT5, m_ymax);
	DDX_Text(pDX, IDC_EDIT6, m_gmcmd);
	DDX_Control(pDX, IDC_EDIT_ONLINE_NUM3, m_edtRecvSize);
	DDX_Control(pDX, IDC_EDIT_ONLINE_NUM2, m_edtRecvRate);
	DDX_Control(pDX, IDC_COMBO_CONFIG, m_Combo_Config);
	DDX_Text(pDX, IDC_EDIT_VERSION, m_nVersion );
}

BEGIN_MESSAGE_MAP(CVirtualClientDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
//	ON_EN_CHANGE(IDC_EDIT4, &CVirtualClientDlg::OnEnChangeEdit4)
ON_BN_CLICKED(IDC_BUTTON_LOGIN, &CVirtualClientDlg::OnBnClickedButtonLogin)
ON_WM_TIMER()
ON_BN_CLICKED(IDC_BUTTON_CLEAR_LOG, &CVirtualClientDlg::OnBnClickedButtonClearLog)
ON_BN_CLICKED(IDC_BUTTON1, &CVirtualClientDlg::OnBnClickedButton1)
ON_BN_CLICKED(IDC_BUTTON2, &CVirtualClientDlg::OnBnClickedButton2)
ON_BN_CLICKED(IDC_BUTTON3, &CVirtualClientDlg::OnBnClickedButton3)
ON_BN_CLICKED(IDC_BUTTON4, &CVirtualClientDlg::OnBnClickedButton4)
ON_BN_CLICKED(IDC_BUTTON5, &CVirtualClientDlg::OnBnClickedButton5)
ON_BN_CLICKED(IDC_BUTTON6, &CVirtualClientDlg::OnBnClickedButton6)
ON_BN_CLICKED(IDC_BUTTON7, &CVirtualClientDlg::OnBnClickedButton7)
ON_CBN_SELCHANGE(IDC_COMBO_CONFIG, &CVirtualClientDlg::OnCbnSelchangeComboConfig)
ON_BN_CLICKED(IDC_BUTTON8, &CVirtualClientDlg::OnBnClickedButton8)
ON_BN_CLICKED(IDC_BtnTeamDlg, &CVirtualClientDlg::OnBnClickedBtnteamdlg)
END_MESSAGE_MAP()


// CVirtualClientDlg 消息处理程序
long g_nVersion = 0;

BOOL CVirtualClientDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	if (!m_pTeamDlg)
	{
		m_pTeamDlg = new CTeamDlg;
		m_pTeamDlg->Create(IDD_DLGGROUP, NULL);
		
	
	}
	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

//	ShowWindow(SW_MINIMIZE);

	// TODO: 在此添加额外的初始化代码
	m_listUsers.InsertColumn( 0, "GUID", LVCFMT_LEFT, 50, 0 );
	m_listUsers.InsertColumn( 1, "Name", LVCFMT_LEFT, 80, 1 );
	m_listUsers.InsertColumn( 2, "Map", LVCFMT_LEFT, 80, 2 );
	m_listUsers.InsertColumn( 3, "x", LVCFMT_LEFT, 25, 3 );
	m_listUsers.InsertColumn( 4, "y", LVCFMT_LEFT, 25, 4 );

	net_global::init_net_service( 4, 20, &cs, true, 1000 );
	
	::SendMessage(m_listUsers.m_hWnd, LVM_SETEXTENDEDLISTVIEWSTYLE, LVS_EX_FULLROWSELECT, LVS_EX_FULLROWSELECT );
	g_pUserMgr->AddUserListener( new UserListener );

	g_Config[0].m_strServerIP = "192.168.1.5";
	g_Config[0].m_nServerPort = 7000;
	g_Config[0].m_NumsPerLogin = 30;
	g_Config[0].m_strAccountList = "192.168.1.5.txt";
	g_Config[0].name = "192.168.1.5";
	g_Config[0].m_nVersion = 1026;

	g_Config[1].m_strServerIP = "192.168.1.253";
	g_Config[1].m_nServerPort = 7000;
	g_Config[1].m_NumsPerLogin = 30;
	g_Config[1].m_strAccountList = "192.168.1.253.txt";
	g_Config[1].name = "192.168.1.253";
	g_Config[1].m_nVersion = 1033;

	g_Config[2].m_strServerIP = "58.215.240.4";
	g_Config[2].m_nServerPort = 7000;
	g_Config[2].m_NumsPerLogin = 100;
	g_Config[2].m_strAccountList = "58.215.240.4.txt";
	g_Config[2].name = "58.215.240.4:per 100";
	g_Config[2].m_nVersion = 91022;

	g_Config[3].m_strServerIP = "58.215.241.135";
	g_Config[3].m_nServerPort = 7000;
	g_Config[3].m_NumsPerLogin = 50;
	g_Config[3].m_strAccountList = "58.215.241.135.txt";
	g_Config[3].name = "58.215.241.135:per 50";
	g_Config[3].m_nVersion = 30;

	g_Config[4].m_strServerIP = "61.128.110.48";
	g_Config[4].m_nServerPort = 9002;
	g_Config[4].m_NumsPerLogin = 30;
	g_Config[4].m_strAccountList = "新疆外网.txt";
	g_Config[4].name = "61.128.110.48:per 200";
	g_Config[4].m_nVersion = 1001;

	g_Config[5].m_strServerIP = "192.168.1.23";
	g_Config[5].m_nServerPort = 7000;
	g_Config[5].m_NumsPerLogin = 30;
	g_Config[5].m_strAccountList = "192.168.1.23.txt";
	g_Config[5].name = "192.168.1.23";
	g_Config[5].m_nVersion = 1026;

	for(int i = 0; i < 6; i++)
		m_Combo_Config.AddString(g_Config[i].name);

	srand( (uint32)time( NULL ) );

	int n = sizeof( tagTradeItem );
	uint32 nd = make_unix_time( 2011, 1, 1, 1, 1, 1 );
	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CVirtualClientDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CVirtualClientDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CVirtualClientDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


//void CVirtualClientDlg::OnEnChangeEdit4()
//{
//	// TODO:  如果该控件是 RICHEDIT 控件，则它将不会
//	// 发送该通知，除非重写 CDialog::OnInitDialog()
//	// 函数并调用 CRichEditCtrl().SetEventMask()，
//	// 同时将 ENM_CHANGE 标志“或”运算到掩码中。
//
//	// TODO:  在此添加控件通知处理程序代码
//}

void CVirtualClientDlg::OnBnClickedButtonLogin()
{
	if( UpdateData() )
	{
		g_nVersion = m_nVersion;
		//g_pUserMgr->GenerateUser( m_strAccountBegin, m_nAccountCount, m_strServerIP, m_nPort );
		g_pUserMgr->Login( g_Config[g_nCurConfig].m_strServerIP, g_Config[g_nCurConfig].m_nServerPort );
	}
}

void CVirtualClientDlg::AddUser( const User* p )
{
	char buff[128];
	sprintf( buff, "%u", p->GetID() );
	int n = m_listUsers.InsertItem( LVIF_TEXT, buff );
	m_listUsers.SetItemData( n, (DWORD_PTR)p );
	RefreshUserItem( p, n );

	sprintf( buff, "%u", g_pUserMgr->GetOnlineNum() );
	m_edtOnlineNum.SetWindowText( buff );

	m_pTeamDlg->AddUserOutTeam(p);
}

void CVirtualClientDlg::DelUser( const User* p )
{
	int count = m_listUsers.GetItemCount();
	for( int i = 0; i < count; ++i )
	{
		if( m_listUsers.GetItemData( i ) == (DWORD_PTR)p )
		{
			m_listUsers.DeleteItem( i );
			break;
		}
	}

	char buff[128];
	sprintf( buff, "%u", g_pUserMgr->GetOnlineNum() );
	m_edtOnlineNum.SetWindowText( buff );
	m_pTeamDlg->DelUserOutTeam(p);
}

void CVirtualClientDlg::RefreshUser( const User* p )
{
	int count = m_listUsers.GetItemCount();
	for( int i = 0; i < count; ++i )
	{
		if( m_listUsers.GetItemData( i ) == (DWORD_PTR)p )
		{
			RefreshUserItem( p, i );
			break;
		}
	}
}

void CVirtualClientDlg::RefreshUserItem( const User* p, int nItem )
{
	char buff[128];
	m_listUsers.SetItemText( nItem, 1, p->m_role.name.c_str() );
	m_listUsers.SetItemText( nItem, 2, p->strMap.c_str() );
	sprintf( buff, "%.2f", p->x );
	m_listUsers.SetItemText( nItem, 3, buff );
	sprintf( buff, "%.2f", p->y );
	m_listUsers.SetItemText( nItem, 4, buff );
}

void CVirtualClientDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	switch( nIDEvent )
	{
	case 0:
		//get_io_service()->poll();
		g_pUserMgr->Run();
		g_pUserMgr->HeartBeat();
		break;
	case 1:
		{
			g_pUserMgr->logoff_some(0);
			OnBnClickedButtonLogin();
		}
	default:
		break;
	}
	CDialog::OnTimer(nIDEvent);
}

void CVirtualClientDlg::OnBnClickedButtonClearLog()
{
	m_listboxLog.ResetContent();
}

void CVirtualClientDlg::OnBnClickedButton1()
{
	if( UpdateData( true ) )
		g_pUserMgr->Teleport();
}

void CVirtualClientDlg::OnBnClickedButton2()
{
	g_pUserMgr->StartMove();
}

void CVirtualClientDlg::OnBnClickedButton3()
{
	g_pUserMgr->StopMove();
}

void CVirtualClientDlg::OnBnClickedButton4()
{
	g_pUserMgr->SpawnCreature();
}

void CVirtualClientDlg::OnBnClickedButton5()
{
	if( UpdateData() )
	{
		if( m_gmcmd.GetLength() )
			g_pUserMgr->SendGMCommand( m_gmcmd );
	}
}

void CVirtualClientDlg::OnBnClickedButton6()
{
	g_pUserMgr->AttackNearest();
}

void CVirtualClientDlg::OnBnClickedButton7()
{
	// TODO: 在此添加控件通知处理程序代码
	if( UpdateData() )
	{
		g_pUserMgr->ReadUserListFile( g_Config[g_nCurConfig].m_strAccountList, g_Config[g_nCurConfig].m_strServerIP, g_Config[g_nCurConfig].m_nServerPort );
		SetTimer( 0, 10, NULL );
	}
}

void CVirtualClientDlg::OnCbnSelchangeComboConfig()
{
	// TODO: 在此添加控件通知处理程序代码
	g_nCurConfig = m_Combo_Config.GetCurSel();
}

stConfig g_Config[10];

void CVirtualClientDlg::OnBnClickedButton8()
{
	// TODO: 在此添加控件通知处理程序代码
	SetTimer( 1, 2000, NULL);
}

void CVirtualClientDlg::OnBnClickedBtnteamdlg()
{

	m_pTeamDlg->ShowWindow(1);
	// TODO: 在此添加控件通知处理程序代码
}
