// VirtualClientDlg.h : 头文件
//

#pragma once
#include "afxcmn.h"
#include "afxwin.h"

class User;
class CTeamDlg;

// CVirtualClientDlg 对话框
class CVirtualClientDlg : public CDialog
{
// 构造
public:
	CVirtualClientDlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
	enum { IDD = IDD_VIRTUALCLIENT_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

	void RefreshUserItem( const User* p, int nItem );
public:
	void AddUser( const User* p );
	void DelUser( const User* p );
	void RefreshUser( const User* p );

//	afx_msg void OnEnChangeEdit4();
	CString m_strAccountBegin;
	int m_nAccountCount;
	afx_msg void OnBnClickedButtonLogin();
	CListCtrl m_listUsers;
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	CListBox m_listboxLog;
	afx_msg void OnBnClickedButtonClearLog();
	CEdit m_edtOnlineNum;
	int m_teleportmapid;
	int m_xmin;
	int m_xmax;
	int m_ymin;
	int m_ymax;
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton2();
	afx_msg void OnBnClickedButton3();
	afx_msg void OnBnClickedButton4();
	CString m_gmcmd;
	afx_msg void OnBnClickedButton5();
	CEdit m_edtRecvSize;
	CEdit m_edtRecvRate;
	afx_msg void OnBnClickedButton6();
	afx_msg void OnBnClickedButton7();
	CComboBox m_Combo_Config;
	afx_msg void OnCbnSelchangeComboConfig();

	afx_msg void OnBnClickedButton8();
	long m_nVersion;
	CTeamDlg* m_pTeamDlg;
	afx_msg void OnBnClickedBtnteamdlg();
};
