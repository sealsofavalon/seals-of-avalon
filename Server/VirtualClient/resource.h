//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by VirtualClient.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_VIRTUALCLIENT_DIALOG        102
#define IDR_MAINFRAME                   128
#define IDD_DLGGROUP                    129
#define IDC_EDIT_SERVERIP               1000
#define IDC_EDIT_SERVERPORT             1001
#define IDC_LIST_USERS                  1002
#define IDC_EDIT_ACCOUNTBEGIN           1003
#define IDC_EDIT_ACCOUNTCOUNT           1004
#define IDC_BUTTON_LOGIN                1005
#define IDC_LIST_LOG                    1008
#define IDC_BUTTON_CLEAR_LOG            1009
#define IDC_EDIT_ONLINE_NUM             1010
#define IDC_EDIT1                       1011
#define IDC_EDIT2                       1012
#define IDC_EDIT3                       1013
#define IDC_EDIT4                       1014
#define IDC_EDIT5                       1015
#define IDC_BUTTON1                     1016
#define IDC_BUTTON2                     1017
#define IDC_BUTTON3                     1018
#define IDC_BUTTON4                     1019
#define IDC_EDIT6                       1020
#define IDC_BUTTON5                     1021
#define IDC_EDIT_ONLINE_NUM2            1022
#define IDC_EDIT_ONLINE_NUM3            1023
#define IDC_BUTTON6                     1024
#define IDC_BUTTON7                     1025
#define IDC_COMBO_CONFIG                1026
#define IDC_BUTTON8                     1027
#define IDC_BUTTON_LOADCONFIG           1027
#define IDC_EDIT7                       1028
#define IDC_EDIT_VERSION                1028
#define IDC_LIST_Group                  1032
#define IDC_BTNSETLEADER                1033
#define IDC_BTNTEAMLEADER               1034
#define IDC_BTNINVIDE                   1035
#define IDC_LIST_Group2                 1036
#define IDC_LISTLEADER                  1037
#define IDC_BtnTeamDlg                  1038
#define IDC_SELECTNAME                  1039
#define IDC_EDITNAME                    1040
#define IDC_BTNREFUSE                   1041
#define IDC_BTNCHANGETEAM               1042
#define IDC_EDTTEAMID                   1043
#define IDC_SwepTeam                    1044
#define IDC_EDTSWEP1                    1045
#define IDC_EDTSWEP2                    1046
#define IDC_BTNREADY                    1047
#define IDC_BTNFLG                      1048

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1049
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
