// stdafx.h : 标准系统包含文件的包含文件，
// 或是经常使用但不常更改的
// 特定于项目的包含文件

#pragma once

#ifndef _SECURE_ATL
#define _SECURE_ATL 1
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN            // 从 Windows 头中排除极少使用的资料
#endif

#include "targetver.h"

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS      // 某些 CString 构造函数将是显式的

// 关闭 MFC 对某些常见但经常可放心忽略的警告消息的隐藏
#define _AFX_ALL_WARNINGS

#include <afxwin.h>         // MFC 核心组件和标准组件
#include <afxext.h>         // MFC 扩展


#include <afxdisp.h>        // MFC 自动化类



#ifndef _AFX_NO_OLE_SUPPORT
#include <afxdtctl.h>           // MFC 对 Internet Explorer 4 公共控件的支持
#endif
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>                     // MFC 对 Windows 公共控件的支持
#endif // _AFX_NO_AFXCMN_SUPPORT

//#include "../../Common/Com/Exception.h"
#include "../../../SDBase/Public/TypeDef.h"
#include "../../../SDBase/Public/DBCStores.h"
#include "../../../SDBase/Public/SkillDef.h"
#include "../../../SDBase/Public/GossipDef.h"
#include "../../../SDBase/Public/QuestDef.h"
#include "../../../SDBase/Public/CreatureDef.h"
#include "../../../SDBase/Public/UnitDef.h"
#include "../../../SDBase/Public/PlayerDef.h"
#include "../../../SDBase/Public/SpellDef.h"
#include "../../../SDBase/Public/MapDef.h"
#include "../../../SDBase/Public/ItemPrototype.h"
#include "../../../SDBase/Public/Timer.h"
#include "../../../SDBase/Public/UpdateMask.h"
#include "../../../SDBase/Public/UpdateFields.h"
#include "../../../SDBase/Public/ByteBuffer.h"
//#include "../../../SDBase/Protocol/Opcodes.h"

#include "../../Common/share/config/config.h"
//#include "../../Common/share/AuthCodes.h"
#include "../../Common/share/svn_revision.h"
#include "../../../new_common/Source/ssl/MD5.h"
#include "../../Common/share/zlib/zlib.h"
#include "../../Common/share/crc32.h"
#include "../../Common/share/LocationVector.h"
#include "../../Common/share/CallBack.h"
#include "../../Common/share/Common.h"
#include "../../Common/share/Threading/Mutex.h"
#include "../../Common/share/MersenneTwister.h"
#include "../../Common/encrypt/Base64.h"
#include "../../Common/encrypt/RsaDesManager.h"

#include "..\..\..\SDBase\Protocol\LS2C.h"
#include "..\..\..\SDBase\Protocol\C2LS.h"

#include "..\..\SDBase\Protocol\C2S.h"
#include "..\..\SDBase\Protocol\s2c.h"

class CTeamDlg;
extern void OutputMessage( const char* szMessage, ... );
extern void UpdateNetFlowRate( float rsize, float rate );
extern CTeamDlg* GetTeamDlg();

extern time_t UNIXTIME;

struct stConfig
{
	CString name;
	CString m_strServerIP;
	int m_nServerPort;
	CString m_strAccountList;
	int m_NumsPerLogin;
	int m_nVersion;
};
extern stConfig g_Config[10];
extern int g_nCurConfig;