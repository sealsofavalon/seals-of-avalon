// ac_client.cpp : 定义控制台应用程序的入口点。
//
#include "stdafx.h"
#if 0
#include "ac_client_interface.h"
#include "../../new_common/Source/utilities.h"


#include <cstdlib>
#include <cstdio>
#include <algorithm>
#include <functional>

long exec_count = 0;
bool s_exit = false;
int thread_count = 0;
void ac_thread_test()
{

	ac_account_t acc;
	while( !s_exit )
	{
		ac_client_auth( "test1", "test1", &acc );
		interlocked_increment( &exec_count );
	}
}
boost::thread* s_threads[100] = { NULL };
int begin_time = 0;
void start_ac_thread_test( int tc )
{
	if( thread_count )
	{
		printf( "some threads is already running\n" );
		return;
	}
	s_exit = false;
	begin_time = get_tick_count();
	exec_count = 0;
	thread_count = tc;
	for( int i = 0; i < tc; ++i )
		s_threads[i] = new boost::thread( &ac_thread_test );
	printf( "%d threads has been started\n", tc );
}
void stop_ac_thread_test()
{
	if( !thread_count )
	{
		printf( "no thread running\n" );
		return;
	}
	int end_time = get_tick_count();
	s_exit = true;
	for( int i = 0; i < thread_count; ++i )
	{
		s_threads[i]->join();
		delete s_threads[i];
	}
	thread_count = 0;
	printf( "exec count:%d, spend time:%d, cps:%.2f\n", exec_count, end_time - begin_time, (float)exec_count / ( (float)(end_time - begin_time) / 1000.f ) );
}

int main()
{
	const char* defaultip = "192.168.1.43";
	printf( "please input account center ip, type 1 and enter to connect default ip:[%s]...", defaultip );
	char ip[32];
	scanf( "%s", ip );
	if( strlen( ip ) < 5 )
		strcpy( ip, defaultip );
	if( AC_CLIENT_OK == ac_client_init( ip, 7500 ) )
		printf( "ac_client_init succeed\n" );
	else
	{
		printf( "ac_client_init failed\n" );
		return -1;
	}
	const char AC_COMMAND_STRING[] = "\tauth [account] [password]\n\tmodp [account] [oldpassword] [newpassword]\n\tcreate [account] [password] [source]\n\tbatchcreate [startname] [count]\n\tbatchauth [startname] [count]\n\tlicense [user] [password]\n\tgencards [batch_number(4-digit)] [type] [points] [count] [description]\n\ttopupcard [account] [serial] [password] [realm]\n\tstartmt [threadcount]\n\tstopmt\n\texit";
	printf( "please input command to test ac_client\ncommand:\n%s\n", AC_COMMAND_STRING );
	while( true )
	{
		std::string input;
		std::cin >> input;
		if( 0 == strnicmp( input.c_str(), "auth", 4 ) )
		{
			std::string account, pwd;
			std::cin >> account;
			std::cin >> pwd;
			ac_account_t acc;
			printf( "authing acc:%s pwd:%s\n", account.c_str(), pwd.c_str() );
			uint32 n = get_tick_count();
			int ret = ac_client_auth( account.c_str(), pwd.c_str(), &acc );
			printf( "auth finished, ret:%s accountid:%d, gm_flag:%d, spend time:%dms\n", ac_client_get_error_string( ret ), acc.id, acc.gm_flag, get_tick_count() - n );
		}
		else if( 0 == strnicmp( input.c_str(), "modp", 4 ) )
		{
			std::string account, oldpwd, newpwd;
			std::cin >> account >> oldpwd >> newpwd;

			int accountid = 0;
			printf( "modifying acc:%s oldpwd:%s newpwd:%s\n", account.c_str(), oldpwd.c_str(), newpwd.c_str() );
			uint32 n = get_tick_count();
			int ret = ac_client_modify_password( account.c_str(), oldpwd.c_str(), newpwd.c_str(), &accountid );
			printf( "modify acc finished, ret:%s accountid:%d, spend time:%dms\n", ac_client_get_error_string( ret ), accountid, get_tick_count() - n );
		}
		else if( 0 == strnicmp( input.c_str(), "create", 6 ) )
		{
			ac_account_t acc;
			std::string pwd, pwdanswer( "kyo" );
			std::cin >> acc.name >> pwd;
			int accountid = 0;
			strcpy( acc.pwdquestion, "who is winner?" );
			strcpy( acc.nationality, "USA" );
			strcpy( acc.province, "New York" );
			strcpy( acc.city, "New York" );
			acc.birth_year = 1982;
			acc.birth_month = 9;
			acc.birth_day = 29;
			strcpy( acc.description, "The king of fighters" );
			strcpy( acc.email, "pylonsprotoss@hotmail.com" );
			strcpy( acc.source, "auto generated" );
			printf( "creating acc:%s pwd:%s source:%s\n", acc.name, pwd.c_str(), acc.source );
			uint32 n = get_tick_count();
			int ret = ac_client_create_account( &acc, pwd.c_str(), pwdanswer.c_str(), &accountid );
			printf( "create acc finished, ret:%s accountid:%d, spend time:%dms\n", ac_client_get_error_string( ret ), accountid, get_tick_count() - n );
		}
		else if( 0 == strnicmp( input.c_str(), "exit", 4 ) )
		{
			break;
		}
		else if( 0 == strnicmp( input.c_str(), "batchcreate", 11 ) )
		{
			uint32 count = 0;
			std::string account;
			std::cin >> account >> count;
			uint32 n = get_tick_count();

			for( uint32 i = 0; i < count; ++i )
			{
				std::string temp, pwd, source("batchcreate");
				char buff[32] = { 0 };
				temp = account + itoa( i, buff, 10 );
				pwd = temp;
				ac_account_t acc;
				int accountid = 0;
				strcpy( acc.name, temp.c_str() );
				strcpy( acc.pwdquestion, "who is winner?" );
				std::string pwdanswer = "kyo";
				strcpy( acc.nationality, "USA" );
				strcpy( acc.province, "New York" );
				strcpy( acc.city, "New York" );
				acc.birth_year = 1982;
				acc.birth_month = 9;
				acc.birth_day = 29;
				strcpy( acc.description, "The king of fighters" );
				strcpy( acc.email, "pylonsprotoss@hotmail.com" );
				strcpy( acc.source, "batchcreate" );
				printf( "creating acc:%s pwd:%s source:%s\n", acc.name, pwd.c_str(), acc.source );
				uint32 n = get_tick_count();
				int ret = ac_client_create_account( &acc, pwd.c_str(), pwdanswer.c_str(), &accountid );
				printf( "create acc finished, ret:%s accountid:%d\n", ac_client_get_error_string( ret ), accountid );
			}
			printf( "batchcreate finished, spend time:%d\n", get_tick_count() - n );
		}
		else if( 0 == strnicmp( input.c_str(), "batchauth", 9 ) )
		{
			uint32 count = 0;
			ac_account_t acc;
			std::string account;
			std::cin >> account >> count;
			uint32 n = get_tick_count();

			for( uint32 i = 0; i < count; ++i )
			{
				std::string temp, pwd;
				char buff[32] = { 0 };
				temp = account + itoa( i, buff, 10 );
				pwd = temp;
				printf( "authing acc:%s pwd:%s\n", temp.c_str(), pwd.c_str() );
				int ret = ac_client_auth( temp.c_str(), pwd.c_str(), &acc );
				printf( "auth acc finished, ret:%s accountid:%d, gm_flag:%d\n", ac_client_get_error_string( ret ), acc.id, acc.gm_flag );
			}
			printf( "batchauth finished, spend time:%d\n", get_tick_count() - n );
		}
		else if( 0 == strnicmp( input.c_str(), "query", 5 ) )
		{
			std::string account;
			std::cin >> account;

			ac_account_t acc;
			memset( &acc, 0, sizeof( acc ) );
			printf( "querying acc:%s\n", account.c_str() );
			uint32 n = get_tick_count();
			int ret = ac_client_query_account( account.c_str(), &acc );
			printf( "query acc finished, ret:%s id:%d\n/*************************\nname:%s\nsource:%s\npwdquestion:%s\nemail:%s\nnationality:%s\nprovince:%s\ncity:%s\nbirth_year:%u\nbirth_month:%u\nbirth_day:%u\ndescription:%s\n*************************/\nspend time:%dms\n",
				ac_client_get_error_string( ret ), acc.id, acc.name, acc.source, acc.pwdquestion, acc.email,
				acc.nationality, acc.province, acc.city, acc.birth_year, acc.birth_month, acc.birth_day,
				acc.description, get_tick_count() - n );
		}
		else if( 0 == strnicmp( input.c_str(), "fetchpwd", 8 ) )
		{
			std::string account, pwdanswer;
			std::cin >> account >> pwdanswer;

			char newpwd[32] = { 0 };
			printf( "fetching password, acc:%s pwdanswer:%s\n", account.c_str(), pwdanswer.c_str() );
			uint32 n = get_tick_count();
			int ret = ac_client_fetch_pwd( account.c_str(), pwdanswer.c_str(), newpwd );
			printf( "fetch password finished, ret:%s the new password is:%s, spend time:%dms\n", ac_client_get_error_string( ret ), newpwd, get_tick_count() - n );
		}
		else if( 0 == strnicmp( input.c_str(), "topupcard", 9 ) )
		{
			std::string account, card_serial, card_password;
			int realm_id;
			std::cin >> account >> card_serial >> card_password >> realm_id;
			printf( "topping up card, acc:%s card_serial:%s card_password:%s realm_id:%d\n", account.c_str(), card_serial.c_str(), card_password.c_str(), realm_id );
			uint32 n = get_tick_count();
			int ret = ac_client_top_up_card( account.c_str(), card_serial.c_str(), card_password.c_str(), realm_id, "auto top up" );
			printf( "top up card finished, ret:%s, spend time:%dms\n", ac_client_get_error_string( ret ), get_tick_count() - n );
		}
		else if( 0 == strnicmp( input.c_str(), "license", 7 ) )
		{
			std::string user, password;
			std::cin >> user >> password;
			printf( "getting generate license, user:%s password:%s\n", user.c_str(), password.c_str() );
			uint32 n = get_tick_count();
			int ret = ac_client_get_generate_license( user.c_str(), password.c_str() );
			printf( "get generate license finished, ret:%s, spend time:%dms\n", ac_client_get_error_string( ret ), get_tick_count() - n );
			printf( "please generate cards in 60 seconds, or the license will expire!\n" );
			printf( "max count per generate is [1000]\n" );
		}
		else if( 0 == strnicmp( input.c_str(), "gencard", 7 ) )
		{
			std::string batch_number, description;
			int type, points, count;
			std::cin >> batch_number >> type >> points >> count >> description;
			printf( "generating cards, batch_number = %s type = %d points = %d count = %d\n", batch_number.c_str(), type, points, count );

			char file_name[64] = { 0 };
			sprintf( file_name, "batch_number_%s.card", batch_number.c_str() );
			FILE* fp = fopen( file_name, "w" );
			if( !fp )
			{
				printf( "cannot open file: [%s]!!!\n", file_name );
				continue;
			}
			ac_rechargeable_card_t* cards = new ac_rechargeable_card_t[count];
			int n = get_tick_count();
			int ret = ac_client_generate_card( batch_number.c_str(), type, points, count, description.c_str(), cards );
			if( ret == AC_CLIENT_OK )
			{
				int y, m, d, h, mi, s;
				ac_client_get_time( (uint32)time( NULL ), &y, &m, &d, &h, &mi, &s );
				fprintf( fp, "[%d-%d-%d %d:%d:%d] generate cards...\n", y, m, d, h, mi, s );
				fprintf( fp, "batch number:[%s] type:[%d] points[%d] count[%d] description:[%s]\n", batch_number.c_str(), type, points, count, description.c_str() );
				for( int i = 0; i < count; ++i )
				{
					char card_info[128] = { 0 };
					sprintf( card_info, "[%04d] ==> card serial = %s,  card password = %s\n", i + 1, cards[i].serial, cards[i].password );
					printf( card_info );
					fprintf( fp, card_info );
				}
			}
			fclose( fp );
			delete[] cards;
			printf( "generate cards finished, ret:%s, spend time:%dms, [%s] file was saved\n", ac_client_get_error_string( ret ), get_tick_count() - n, file_name );
		}
		else if( 0 == strnicmp( input.c_str(), "startmt", 6 ) )
		{
			int tc;
			std::cin >> tc;
			start_ac_thread_test( tc );
		}
		else if( 0 == strnicmp( input.c_str(), "stopmt", 6 ) )
		{
			stop_ac_thread_test();
		}
		else
			printf( "bad command!!!\ncommand:\n%s\n", AC_COMMAND_STRING );
	}
	ac_client_release();
	return 0;
}
#endif