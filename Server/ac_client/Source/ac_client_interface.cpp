#include "stdafx.h"
#include "ac_client_interface.h"
#include "ac_net_client.h"
#include "../../../new_common/Source/ssl/MD5.h"
//#include "../../../new_common/Source/utilities.h"

boost::asio::io_service* _g_ac_io_service = NULL;
static ac_net_client* g_net_client = NULL;
static boost::thread* g_thread[2] = { NULL };
static net_global::asio_run_timer* g_run_timer = NULL;
static volatile bool g_exit = false;
static bool g_inited = false;

void _ac_client_copy_info( ac_account_t* a, const account_info_t* b )
{
	a->id = b->id;
	strcpy( a->name, b->name.c_str() );
	strcpy( a->source, b->source.c_str() );
	strcpy(a->gm_flag, b->gm_flag.c_str());
	strcpy( a->email, b->email.c_str() );
	a->question[0] = b->question[0];
	a->question[1] = b->question[1];
	strcpy( a->nationality, b->nationality.c_str() );
	strcpy( a->province, b->province.c_str() );
	strcpy( a->city, b->city.c_str() );
	a->birth_year = b->birth_year;
	a->birth_month = b->birth_month;
	a->birth_day = b->birth_day;
	strcpy( a->id_card, b->id_card.c_str() );
	strcpy( a->description, b->description.c_str() );
	a->reg_time = b->reg_time;
	strcpy( a->safe_lock_pwd, b->safe_lock_pwd.c_str() );
	a->safe_lock_flag = b->safe_lock_flag;
	a->occupation = b->occupation;
	a->education = b->education;
	strcpy( a->qq, b->qq.c_str() );
	strcpy( a->msn, b->msn.c_str() );
	strcpy( a->telephone, b->telephone.c_str() );
	strcpy( a->zipcode, b->zipcode.c_str() );
	strcpy( a->address, b->address.c_str() );
	a->gender = b->gender;
	strcpy( a->real_name, b->real_name.c_str() );
}

void _ac_client_copy_info( account_info_t* a, const ac_account_t* b )
{
	a->id = b->id;
	a->name = b->name;
	a->gm_flag = b->gm_flag;
	a->source = b->source;
	a->email = b->email;
	a->question[0] = b->question[0];
	a->question[1] = b->question[1];
	a->nationality = b->nationality;
	a->province = b->province;
	a->city = b->city;
	a->birth_year = b->birth_year;
	a->birth_month = b->birth_month;
	a->birth_day = b->birth_day;
	a->id_card = b->id_card;
	a->description = b->description;
	a->reg_time = b->reg_time;
	a->safe_lock_pwd = b->safe_lock_pwd;
	a->safe_lock_flag = b->safe_lock_flag;
	a->occupation = b->occupation;
	a->education = b->education;
	a->qq = b->qq;
	a->msn = b->msn;
	a->telephone = b->telephone;
	a->zipcode = b->zipcode;
	a->address = b->address;
	a->gender = b->gender;
	a->real_name = b->real_name;
}

ac_result_t _ac_client_check_valid()
{
	if( !g_inited )
		return AC_CLIENT_ERROR_UNINITIALIZED;
	if( !g_net_client->is_connected() )
		return AC_CLIENT_ERROR_DISCONNECTED;
	return AC_CLIENT_OK;
}

ac_result_t _ac_client_check_string( const char* s )
{
	int i = 0;
	while( s[i] != 0 )
	{
		if( ( s[i] >= 'a' && s[i] <= 'z' ) || ( s[i] >= 'A' && s[i] <= 'Z' ) || ( s[i] >= '0' && s[i] <= '9' ) )
		{
			++i;
			continue;
		}
		else
			return AC_CLIENT_ERROR_INVALID_CHARACTER;
	}
	return AC_CLIENT_OK;
}

ac_result_t _ac_client_check_chinese_string( const char* s )
{
	if( !is_valid_string( s ) )
		return AC_CLIENT_ERROR_INVALID_CHARACTER;
	else
		return AC_CLIENT_OK;
}

class acc_run_timer : public net_global::asio_run_timer
{
public:
	acc_run_timer( boost::asio::io_service& io, int interval ) : asio_run_timer( io, interval )
	{
	}

	virtual void v_call()
	{
		g_net_client->thread_run_asio();
		g_net_client->run();
	}
};

void ac_main_loop()
{
	while( !g_exit )
	{
		//wait_message();
		cpu_wait();
		g_net_client->run();
	}
}

ac_result_t ac_client_init( const char* ip, unsigned short port )
{
	ssl_init_dictionary();
	if( g_inited )
		return AC_CLIENT_ERROR_ALREADY_INITED;

	static zip_compress_strategy _ac_cs;
	net_global::set_compress_strategy( &_ac_cs );
	/*
	if( !_g_ac_io_service )
	{
		_g_ac_io_service = new boost::asio::io_service;
		g_run_timer = new acc_run_timer( *_g_ac_io_service, 1 );
	}
	*/
	net_global::init_net_service( 2, 1, &_ac_cs, true, 1024 );
	if( !g_net_client )
	{
		g_net_client = new ac_net_client;
		//g_thread[0] = new boost::thread( boost::bind( &boost::asio::io_service::run, _g_ac_io_service ) );
		g_thread[0] = new boost::thread( &ac_main_loop );
	}
	
	bool bcon = g_net_client->wait_connect( ip, port );
	g_net_client->set_reconnect( true );

	g_inited = true;
	if( bcon )
		return AC_CLIENT_OK;
	else
		return AC_CLIENT_ERROR_CONNECT_FAILED;
}

ac_result_t ac_client_release()
{
	if( g_run_timer )
	{
		delete g_run_timer;
		g_run_timer = NULL;
	}
	_g_ac_io_service->stop();
	g_exit = true;
	for( int i = 0; i < 2; ++i )
		if( g_thread[i] )
		{
			g_thread[i]->join();
			delete g_thread[i];
			g_thread[i] = NULL;
		}
	if( g_net_client )
	{
		delete g_net_client;
		g_net_client = NULL;
	}
	if( _g_ac_io_service )
	{
		delete _g_ac_io_service;
		_g_ac_io_service = NULL;
	}
	g_inited = false;

	return AC_CLIENT_OK;
}

ac_result_t ac_client_auth( const char* account, const char* pwd, ac_account_t* outacc )
{
	ac_result_t cr = _ac_client_check_valid();
	if( cr != AC_CLIENT_OK )
		return cr;

	cr = _ac_client_check_string( account );
	if( cr != AC_CLIENT_OK )
		return cr;


	MSG_CLIENT2AC::stAuthReq req;
	req.name = account;
	req.md5pwd = MD5Hash::_make_md5_pwd( pwd );
	req.nTransID = g_net_client->fetch_wait_obj();

	account_info_t* info = NULL;
	uint8 ret = g_net_client->wait( req.nTransID, req, (uint32&)info );
	switch( ret )
	{
	case MSG_AC2CLIENT::stAuthAck::RET_SUCCESS:
		{
			_ac_client_copy_info( outacc, info );
			delete info;
			return AC_CLIENT_OK;
		}
	case MSG_AC2CLIENT::stAuthAck::RET_WRONG_ACC: return AC_CLIENT_ERROR_WRONG_ACC;
	case MSG_AC2CLIENT::stAuthAck::RET_WRONG_PWD: return AC_CLIENT_ERROR_WRONG_PWD;
	case AC_CLIENT_ERROR_DISCONNECTED: return AC_CLIENT_ERROR_DISCONNECTED;
	default: break;
	}
	return AC_CLIENT_ERROR_UNKNOWN;
}

ac_result_t ac_client_md5auth( const char* account, const char* md5pwd, ac_account_t* outacc )
{
	ac_result_t cr = _ac_client_check_valid();
	if( cr != AC_CLIENT_OK )
		return cr;

	cr = _ac_client_check_string( account );
	if( cr != AC_CLIENT_OK )
		return cr;

	MSG_CLIENT2AC::stAuthReq req;
	req.name = account;
	req.md5pwd = md5pwd;
	req.nTransID = g_net_client->fetch_wait_obj();

	account_info_t* info = NULL;
	uint8 ret = g_net_client->wait( req.nTransID, req, (uint32&)info );
	switch( ret )
	{
	case MSG_AC2CLIENT::stAuthAck::RET_SUCCESS:
		{
			_ac_client_copy_info( outacc, info );
			delete info;
			return AC_CLIENT_OK;
		}
	case MSG_AC2CLIENT::stAuthAck::RET_WRONG_ACC: return AC_CLIENT_ERROR_WRONG_ACC;
	case MSG_AC2CLIENT::stAuthAck::RET_WRONG_PWD: return AC_CLIENT_ERROR_WRONG_PWD;
	case AC_CLIENT_ERROR_DISCONNECTED: return AC_CLIENT_ERROR_DISCONNECTED;
	default: break;
	}
	return AC_CLIENT_ERROR_UNKNOWN;
}



ac_result_t ac_client_modify_password( const char* account, const char* oldpwd, const char* newpwd, int* outid )
{
	ac_result_t cr = _ac_client_check_valid();
	if( cr != AC_CLIENT_OK )
		return cr;

	cr = _ac_client_check_string( account );
	if( cr != AC_CLIENT_OK )
		return cr;

	cr = _ac_client_check_string( newpwd );
	if( cr != AC_CLIENT_OK )
		return cr;

	MSG_CLIENT2AC::stModPwdReq req;
	req.name = account;
	req.oldmd5pwd = MD5Hash::_make_md5_pwd( oldpwd );
	req.newmd5pwd = MD5Hash::_make_md5_pwd( newpwd );
	req.nTransID = g_net_client->fetch_wait_obj();
	uint8 ret = g_net_client->wait( req.nTransID, req, (uint32&)*outid );
	switch( ret )
	{
	case MSG_AC2CLIENT::stModPwdAck::RET_SUCCESS: return AC_CLIENT_OK;
	case MSG_AC2CLIENT::stModPwdAck::RET_WRONG_ACC: return AC_CLIENT_ERROR_WRONG_ACC;
	case MSG_AC2CLIENT::stModPwdAck::RET_WRONG_PWD: return AC_CLIENT_ERROR_WRONG_PWD;
	case AC_CLIENT_ERROR_DISCONNECTED: return AC_CLIENT_ERROR_DISCONNECTED;
	default: break;
	}
	return AC_CLIENT_ERROR_UNKNOWN;
}

ac_result_t ac_client_modify_information( ac_account_t* info, const char* answer1, const char* answer2 )
{
	ac_result_t cr = _ac_client_check_valid();
	if( cr != AC_CLIENT_OK )
		return cr;

	cr = _ac_client_check_chinese_string( answer1 );
	if( cr != AC_CLIENT_OK )
		return cr;

	cr = _ac_client_check_chinese_string( answer2 );
	if( cr != AC_CLIENT_OK )
		return cr;

	cr = _ac_client_check_chinese_string( info->real_name );
	if( cr != AC_CLIENT_OK )
		return cr;

	cr = _ac_client_check_string( info->safe_lock_pwd );
	if( cr != AC_CLIENT_OK )
		return cr;


	MSG_CLIENT2AC::stModInformationReq req;
	req.nTransID = g_net_client->fetch_wait_obj();
	req.answer1 = answer1;
	req.answer2 = answer2;
	_ac_client_copy_info( &req.info, info );
	uint32 temp;
	uint8 ret = g_net_client->wait( req.nTransID, req, temp );
	switch( ret )
	{
	case MSG_AC2CLIENT::stModInformationAck::RET_SUCCESS: return AC_CLIENT_OK;
	case MSG_AC2CLIENT::stModInformationAck::RET_WRONG_ACC: return AC_CLIENT_ERROR_WRONG_ACC;
	case MSG_AC2CLIENT::stModInformationAck::RET_DB_ERROR: return AC_CLIENT_ERROR_DB;
	case AC_CLIENT_ERROR_DISCONNECTED: return AC_CLIENT_ERROR_DISCONNECTED;
	default: break;
	}
	return AC_CLIENT_ERROR_UNKNOWN;
}

ac_result_t ac_client_modify_password_by_qa( const char* account, int question, const char* answer, const char* newpwd )
{
	ac_result_t cr = _ac_client_check_valid();
	if( cr != AC_CLIENT_OK )
		return cr;

	cr = _ac_client_check_string( account );
	if( cr != AC_CLIENT_OK )
		return cr;

	MSG_CLIENT2AC::stModPwdByQaReq req;
	req.nTransID = g_net_client->fetch_wait_obj();
	req.account = account;
	req.newpwd = newpwd;
	req.question = question;
	req.answer = answer;
	uint32 temp;
	uint8 ret = g_net_client->wait( req.nTransID, req, temp );
	switch( ret )
	{
	case MSG_AC2CLIENT::stModPwdByQaAck::RET_SUCCESS: return AC_CLIENT_OK;
	case MSG_AC2CLIENT::stModPwdByQaAck::RET_WRONG_ACC: return AC_CLIENT_ERROR_WRONG_ACC;
	case MSG_AC2CLIENT::stModPwdByQaAck::RET_WRONG_ANSWER: return AC_CLIENT_ERROR_WRONG_ANSWER;
	case MSG_AC2CLIENT::stModPwdByQaAck::RET_DB_ERROR: return AC_CLIENT_ERROR_DB;
	case AC_CLIENT_ERROR_DISCONNECTED: return AC_CLIENT_ERROR_DISCONNECTED;
	default: break;
	}
	return AC_CLIENT_ERROR_UNKNOWN;
}

ac_result_t ac_client_create_account( const ac_account_t* acc, const char* pwd, const char* answer1, const char* answer2, int* outid )
{
	ac_result_t cr = _ac_client_check_valid();
	if( cr != AC_CLIENT_OK )
		return cr;

	cr = _ac_client_check_string( acc->name );
	if( cr != AC_CLIENT_OK )
		return cr;

	cr = _ac_client_check_string( pwd );
	if( cr != AC_CLIENT_OK )
		return cr;

	cr = _ac_client_check_string( answer1 );
	if( cr != AC_CLIENT_OK )
		return cr;

	cr = _ac_client_check_string( answer2 );
	if( cr != AC_CLIENT_OK )
		return cr;

	MSG_CLIENT2AC::stCreateAccReq req;
	req.md5pwd = MD5Hash::_make_md5_pwd( pwd );
	req.answer1 = answer1;
	req.answer2 = answer2;
	_ac_client_copy_info( &req.acc, acc );
	req.nTransID = g_net_client->fetch_wait_obj();
	uint8 ret = g_net_client->wait( req.nTransID, req, (uint32&)*outid );
	switch( ret )
	{
	case MSG_AC2CLIENT::stCreateAccAck::RET_SUCCESS: return AC_CLIENT_OK;
	case MSG_AC2CLIENT::stCreateAccAck::RET_ALREADY_EXIST: return AC_CLIENT_ERROR_ALREADY_EXIST;
	case MSG_AC2CLIENT::stCreateAccAck::RET_DB_ERROR: return AC_CLIENT_ERROR_DB;
	case MSG_AC2CLIENT::stCreateAccAck::RET_ID_CARD_ALREADY_EXIST: return AC_CLIENT_ERROR_ID_CARD_ALREADY_EXIST;
	case AC_CLIENT_ERROR_DISCONNECTED: return AC_CLIENT_ERROR_DISCONNECTED;
	default: break;
	}
	return AC_CLIENT_ERROR_UNKNOWN;
}

ac_result_t ac_client_query_account( const char* account, ac_account_t* outacc )
{
	ac_result_t cr = _ac_client_check_valid();
	if( cr != AC_CLIENT_OK )
		return cr;

	cr = _ac_client_check_string( account );
		if( cr != AC_CLIENT_OK )
			return cr;

	MSG_CLIENT2AC::stQueryAccReq req;
	req.name = account;
	req.nTransID = g_net_client->fetch_wait_obj();
	account_info_t* info = NULL;
	uint8 ret = g_net_client->wait( req.nTransID, req, (uint32&)info );
	switch( ret )
	{
	case MSG_AC2CLIENT::stQueryAccAck::RET_SUCCESS:
		{
			_ac_client_copy_info( outacc, info );
			delete info;
			return AC_CLIENT_OK;
		}
	case MSG_AC2CLIENT::stQueryAccAck::RET_WRONG_ACC: return AC_CLIENT_ERROR_WRONG_ACC;
	case AC_CLIENT_ERROR_DISCONNECTED: return AC_CLIENT_ERROR_DISCONNECTED;
	default: break;
	}
	return AC_CLIENT_ERROR_UNKNOWN;
}

ac_result_t ac_client_fetch_pwd( const char* account, int question, const char* answer, char* outnewpwd )
{
	ac_result_t cr = _ac_client_check_valid();
	if( cr != AC_CLIENT_OK )
		return cr;

	cr = _ac_client_check_string( account );
	if( cr != AC_CLIENT_OK )
		return cr;

	MSG_CLIENT2AC::stFetchPwdReq req;
	req.name = account;
	req.question = question;
	req.answer = answer;
	req.nTransID = g_net_client->fetch_wait_obj();
	char* newpwd = NULL;
	uint8 ret = g_net_client->wait( req.nTransID, req, (uint32&)newpwd );
	switch( ret )
	{
	case MSG_AC2CLIENT::stFetchPwdAck::RET_SUCCESS:
		{
			strcpy( outnewpwd, newpwd );
			free( newpwd );
			return AC_CLIENT_OK;
		}
	case MSG_AC2CLIENT::stFetchPwdAck::RET_WRONG_ACC: return AC_CLIENT_ERROR_WRONG_ACC;
	case MSG_AC2CLIENT::stFetchPwdAck::RET_WRONG_ANSWER: return AC_CLIENT_ERROR_WRONG_ANSWER;
	case AC_CLIENT_ERROR_DISCONNECTED: return AC_CLIENT_ERROR_DISCONNECTED;
	default: break;
	}
	return AC_CLIENT_ERROR_UNKNOWN;
}

ac_result_t ac_client_top_up_card( const char* account, const char* serial, const char* password, int realm_id, const char* description )
{
	ac_result_t cr = _ac_client_check_valid();
	if( cr != AC_CLIENT_OK )
		return cr;

	cr = _ac_client_check_string( account );
	if( cr != AC_CLIENT_OK )
		return cr;
	cr = _ac_client_check_string( serial );
	if( cr != AC_CLIENT_OK )
		return cr;
	cr = _ac_client_check_string( password );
	if( cr != AC_CLIENT_OK )
		return cr;

	MSG_CLIENT2AC::stTopUpCardReq req;
	req.name = account;
	req.serial = serial;
	req.password = password;
	req.realm_id = realm_id;
	req.description = description;
	req.nTransID = g_net_client->fetch_wait_obj();
	char* newpwd = NULL;
	uint8 ret = g_net_client->wait( req.nTransID, req, (uint32&)newpwd );
	switch( ret )
	{
	case MSG_AC2CLIENT::stTopUpCardAck::RET_SUCCESS:
		{
			return AC_CLIENT_OK;
		}
	case MSG_AC2CLIENT::stTopUpCardAck::RET_WRONG_ACC: return AC_CLIENT_ERROR_WRONG_ACC;
	case MSG_AC2CLIENT::stTopUpCardAck::RET_WRONG_CARD: return AC_CLIENT_ERROR_WRONG_CARD;
	case MSG_AC2CLIENT::stTopUpCardAck::RET_DB_ERROR: return AC_CLIENT_ERROR_DB;
	case AC_CLIENT_ERROR_DISCONNECTED: return AC_CLIENT_ERROR_DISCONNECTED;
	default: break;
	}
	return AC_CLIENT_ERROR_UNKNOWN;
}

ac_result_t ac_client_get_generate_license( const char* user, const char* password )
{
	ac_result_t cr = _ac_client_check_valid();
	if( cr != AC_CLIENT_OK )
		return cr;

	cr = _ac_client_check_string( user );
	if( cr != AC_CLIENT_OK )
		return cr;

	MSG_CLIENT2AC::stVipAccessReq req;
	req.user = user;
	req.password = password;
	req.nTransID = g_net_client->fetch_wait_obj();

	uint32 temp;
	uint8 ret = g_net_client->wait( req.nTransID, req, temp );
	switch( ret )
	{
	case MSG_AC2CLIENT::stVipAccessAck::RET_SUCCESS:
		{
			return AC_CLIENT_OK;
		}
	case MSG_AC2CLIENT::stVipAccessAck::RET_REFUSE: return AC_CLIENT_ERROR_WRONG_PWD;
	case AC_CLIENT_ERROR_DISCONNECTED: return AC_CLIENT_ERROR_DISCONNECTED;
	default: break;
	}
	return AC_CLIENT_ERROR_UNKNOWN;
}

ac_result_t ac_client_generate_card( const char* batch_number, int type, int points, int count, const char* description, ac_rechargeable_card_t* card_array )
{
	ac_result_t cr = _ac_client_check_valid();
	if( cr != AC_CLIENT_OK )
		return cr;

	MSG_CLIENT2AC::stGenerateCardReq req;
	req.batch_number = batch_number;
	req.type = type;
	req.points = points;
	req.count = count;
	req.description = description;
	req.nTransID = g_net_client->fetch_wait_obj();

	ac_rechargeable_card_t* p = NULL;
	uint8 ret = g_net_client->wait( req.nTransID, req, (uint32&)p );
	switch( ret )
	{
	case MSG_AC2CLIENT::stGenerateCardAck::RET_SUCCESS:
		{
			memcpy( card_array, p, sizeof( ac_rechargeable_card_t ) * count );
			delete[] p;
			return AC_CLIENT_OK;
		}
	case MSG_AC2CLIENT::stGenerateCardAck::RET_NO_LICENSE: return AC_CLIENT_ERROR_NO_LICENSE;
	case MSG_AC2CLIENT::stGenerateCardAck::RET_DB_ERROR: return AC_CLIENT_ERROR_DB;
	case AC_CLIENT_ERROR_DISCONNECTED: return AC_CLIENT_ERROR_DISCONNECTED;
	default: break;
	}
	return AC_CLIENT_ERROR_UNKNOWN;
}

ac_result_t ac_client_query_top_up_card_history( int id, ac_top_up_card_history_t** history, int* count )
{
	ac_result_t cr = _ac_client_check_valid();
	if( cr != AC_CLIENT_OK )
		return cr;

	MSG_CLIENT2AC::stQueryTopUpCardHistoryReq req;
	req.id = id;
	req.nTransID = g_net_client->fetch_wait_obj();

	ac_top_up_card_history_t* p = NULL;
	uint32 recordsize = 0;
	uint8 ret = g_net_client->wait( req.nTransID, req, (uint32&)p, &recordsize );
	switch( ret )
	{
	case MSG_AC2CLIENT::stQueryTopUpCardHistoryAck::RET_SUCCESS:
		{
			*history = new ac_top_up_card_history_t[recordsize];
			memcpy( *history, p, sizeof( ac_top_up_card_history_t ) * recordsize );
			delete[] p;
			*count = recordsize;
			return AC_CLIENT_OK;
		}
	case MSG_AC2CLIENT::stQueryTopUpCardHistoryAck::RET_NO_RECORD:
		{
			*count = 0;
			return AC_CLIENT_OK;
		}
	case MSG_AC2CLIENT::stQueryTopUpCardHistoryAck::RET_DB_ERROR: return AC_CLIENT_ERROR_DB;
	case AC_CLIENT_ERROR_DISCONNECTED: return AC_CLIENT_ERROR_DISCONNECTED;
	default: break;
	}
	return AC_CLIENT_ERROR_UNKNOWN;
}

ac_result_t ac_client_query_remain_points( int id, int realm, int* outpt )
{
	ac_result_t cr = _ac_client_check_valid();
	if( cr != AC_CLIENT_OK )
		return cr;

	MSG_CLIENT2AC::stQueryRemainPointsReq req;
	req.id = id;
	req.realm = realm;
	req.nTransID = g_net_client->fetch_wait_obj();

	uint8 ret = g_net_client->wait( req.nTransID, req, (uint32&)(*outpt) );
	switch( ret )
	{
	case MSG_AC2CLIENT::stQueryRemainPointsAck::RET_SUCCESS:
		{
			return AC_CLIENT_OK;
		}
	case MSG_AC2CLIENT::stQueryRemainPointsAck::RET_DB_ERROR: return AC_CLIENT_ERROR_DB;
	case AC_CLIENT_ERROR_DISCONNECTED: return AC_CLIENT_ERROR_DISCONNECTED;
	default: break;
	}
	return AC_CLIENT_ERROR_UNKNOWN;
}

ac_result_t ac_client_rmb_direct_charge( const char* account, int realm, int type, int* outpt )
{
	ac_result_t cr = _ac_client_check_valid();
	if( cr != AC_CLIENT_OK )
		return cr;

	MSG_CLIENT2AC::stRMBDirectChargeReq req;
	req.user = account;
	req.target_realm = realm;
	req.card_type = type;
	req.nTransID = g_net_client->fetch_wait_obj();

	uint8 ret = g_net_client->wait( req.nTransID, req, (uint32&)(*outpt) );
	switch( ret )
	{
	case MSG_AC2CLIENT::stRMBDirectChargeAck::RET_SUCCESS:
		{
			return AC_CLIENT_OK;
		}
	case MSG_AC2CLIENT::stRMBDirectChargeAck::RET_DB_ERROR: return AC_CLIENT_ERROR_DB;
	case MSG_AC2CLIENT::stRMBDirectChargeAck::RET_WRONG_ACCOUNT: return AC_CLIENT_ERROR_WRONG_ACC;
	case MSG_AC2CLIENT::stRMBDirectChargeAck::RET_NO_MATCH_CARD: return AC_CLIENT_ERROR_WRONG_CARD;
	case AC_CLIENT_ERROR_DISCONNECTED: return AC_CLIENT_ERROR_DISCONNECTED;
	default: break;
	}
	return AC_CLIENT_ERROR_UNKNOWN;
}

ac_result_t ac_client_cleanup_account( const char* account )
{
	ac_result_t cr = _ac_client_check_valid();
	if( cr != AC_CLIENT_OK )
		return cr;

	MSG_CLIENT2AC::stCleanupAccountReq req;
	req.account = account;
	req.nTransID = g_net_client->fetch_wait_obj();

	uint32 temp;
	uint8 ret = g_net_client->wait( req.nTransID, req, temp );
	switch( ret )
	{
	case MSG_AC2CLIENT::stCleanupAccountAck::RET_SUCCESS:
		{
			return AC_CLIENT_OK;
		}
	case MSG_AC2CLIENT::stCleanupAccountAck::RET_NOT_FOUND: return AC_CLIENT_ERROR_WRONG_ACC;
	case AC_CLIENT_ERROR_DISCONNECTED: return AC_CLIENT_ERROR_DISCONNECTED;
	default: break;
	}
	return AC_CLIENT_ERROR_UNKNOWN;
}

ac_result_t ac_client_safe_lock( const char* account, const char* password, unsigned char is_lock )
{
	ac_result_t cr = _ac_client_check_valid();
	if( cr != AC_CLIENT_OK )
		return cr;

	MSG_CLIENT2AC::stSafeLockReq req;
	req.account = account;
	req.password = password;
	req.is_lock = is_lock;
	req.nTransID = g_net_client->fetch_wait_obj();

	uint32 temp;
	uint8 ret = g_net_client->wait( req.nTransID, req, temp );
	switch( ret )
	{
	case MSG_AC2CLIENT::stSafeLockAck::RET_SUCCESS:
		{
			return AC_CLIENT_OK;
		}
	case MSG_AC2CLIENT::stSafeLockAck::RET_WRONG_ACC: return AC_CLIENT_ERROR_WRONG_ACC;
	case MSG_AC2CLIENT::stSafeLockAck::RET_DB_ERROR: return AC_CLIENT_ERROR_DB;
	case AC_CLIENT_ERROR_DISCONNECTED: return AC_CLIENT_ERROR_DISCONNECTED;
	default: break;
	}
	return AC_CLIENT_ERROR_UNKNOWN;
}

const char* ac_client_get_error_string( ac_result_t r )
{
	switch( r )
	{
	case AC_CLIENT_OK: return "AC_CLIENT_OK";
	case AC_CLIENT_ERROR_UNKNOWN: return "AC_CLIENT_ERROR_UNKNOWN";
	case AC_CLIENT_ERROR_CONNECT_FAILED: return "AC_CLIENT_ERROR_CONNECT_FAILED";
	case AC_CLIENT_ERROR_WRONG_ACC: return "AC_CLIENT_ERROR_WRONG_ACC";
	case AC_CLIENT_ERROR_WRONG_PWD: return "AC_CLIENT_ERROR_WRONG_PWD";
	case AC_CLIENT_ERROR_ALREADY_EXIST: return "AC_CLIENT_ERROR_ALREADY_EXIST";
	case AC_CLIENT_ERROR_DB: return "AC_CLIENT_ERROR_DB";
	case AC_CLIENT_ERROR_ALREADY_INITED: return "AC_CLIENT_ERROR_ALREADY_INITED";
	case AC_CLIENT_ERROR_DISCONNECTED: return "AC_CLIENT_ERROR_DISCONNECTED";
	case AC_CLIENT_ERROR_UNINITIALIZED: return "AC_CLIENT_ERROR_UNINITIALIZED";
	case AC_CLIENT_ERROR_WRONG_ANSWER: return "AC_CLIENT_ERROR_WRONG_ANSWER";
	case AC_CLIENT_ERROR_WRONG_CARD: return "AC_CLIENT_ERROR_WRONG_CARD";
	case AC_CLIENT_ERROR_INVALID_HANDLE: return "AC_CLIENT_ERROR_INVALID_HANDLE";
	case AC_CLIENT_ERROR_STRING_OVERFLOW: return "AC_CLIENT_ERROR_STRING_OVERFLOW";
	case AC_CLIENT_ERROR_NO_LICENSE: return "AC_CLIENT_ERROR_NO_LICENSE";
	case AC_CLIENT_ERROR_INVALID_CHARACTER: return "AC_CLIENT_ERROR_INVALID_CHARACTER";
	case AC_CLIENT_ERROR_SET_LOCALE: return "AC_CLIENT_ERROR_SET_LOCALE";
	case AC_CLIENT_ERROR_ID_CARD_ALREADY_EXIST: return "AC_CLIENT_ERROR_ID_CARD_ALREADY_EXIST";
	default:break;
	}
	return "AC_CLIENT_ERROR_UNKNOWN";
}

void ac_client_get_time( unsigned int t, int* outyear, int* outmonth, int* outday, int* outhour, int* outminute, int* outsecond )
{
	time_t _t = t;
	tm* p = localtime( &_t );
	*outyear = p->tm_year + 1900;
	*outmonth = p->tm_mon + 1;
	*outday = p->tm_mday;
	*outhour = p->tm_hour;
	*outminute = p->tm_min;
	*outsecond = p->tm_sec;
}
