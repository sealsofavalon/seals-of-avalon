#ifndef _AC_CLIENT_INTERFACE_HEAD
#define _AC_CLIENT_INTERFACE_HEAD

/************************************************************************
Account center client application interfaces for servers and webservices.

Nathan Gui.
27/03/2009
************************************************************************/


#ifdef __cplusplus
extern "C" {
#endif

#define AC_CLIENT_OK							0
#define AC_CLIENT_ERROR_UNKNOWN					1
#define AC_CLIENT_ERROR_CONNECT_FAILED			12
#define AC_CLIENT_ERROR_WRONG_ACC				13
#define AC_CLIENT_ERROR_WRONG_PWD				14
#define AC_CLIENT_ERROR_ALREADY_EXIST			15
#define AC_CLIENT_ERROR_DB						16
#define AC_CLIENT_ERROR_ALREADY_INITED			17
#define AC_CLIENT_ERROR_DISCONNECTED			18
#define AC_CLIENT_ERROR_UNINITIALIZED			19
#define AC_CLIENT_ERROR_WRONG_ANSWER			20
#define AC_CLIENT_ERROR_WRONG_CARD				21
#define AC_CLIENT_ERROR_INVALID_HANDLE			22
#define AC_CLIENT_ERROR_STRING_OVERFLOW			23
#define AC_CLIENT_ERROR_NO_LICENSE				24
#define AC_CLIENT_ERROR_INVALID_CHARACTER		25
#define AC_CLIENT_ERROR_SET_LOCALE				26
#define AC_CLIENT_ERROR_ID_CARD_ALREADY_EXIST	27

typedef int ac_result_t;

struct ac_account_t
{
	int id;
	char name[32];
	char gm_flag[32];
	char source[32];
	char email[64];
	int question[2];
	char nationality[32];
	char province[32];
	char city[32];
	unsigned short birth_year;
	unsigned char birth_month;
	unsigned char birth_day;
	char id_card[20];
	char description[128];
	unsigned int reg_time;
	char safe_lock_pwd[20];
	unsigned int safe_lock_flag;
	unsigned int occupation;
	unsigned int education;
	char qq[20];
	char msn[64];
	char telephone[20];
	char zipcode[20];
	char address[64];
	unsigned char gender;
	char real_name[64];
};

struct ac_rechargeable_card_t
{
	char serial[23];
	char password[17];
};

struct ac_top_up_card_history_t
{
	int points;
	int remain_points;
	int type;
	int realm_id;
	unsigned int unixtime;
};

extern ac_result_t ac_client_init( const char* ip, unsigned short port );
extern ac_result_t ac_client_release();
extern ac_result_t ac_client_auth( const char* account, const char* pwd, ac_account_t* outacc );
extern ac_result_t ac_client_md5auth( const char* account, const char* md5pwd, ac_account_t* outacc );
extern ac_result_t ac_client_modify_password( const char* account, const char* oldpwd, const char* newpwd, int* outid );
extern ac_result_t ac_client_modify_password_by_qa( const char* account, int question, const char* answer, const char* newpwd );
extern ac_result_t ac_client_modify_information( ac_account_t* info, const char* answer1, const char* answer2 );

extern ac_result_t ac_client_create_account( const ac_account_t* acc, const char* pwd, const char* answer1, const char* answer2, int* outid );
extern ac_result_t ac_client_query_account( const char* account, ac_account_t* outacc );
extern ac_result_t ac_client_fetch_pwd( const char* account, int question, const char* answer, char* outnewpwd );
extern ac_result_t ac_client_top_up_card( const char* account, const char* serial, const char* password, int realm_id, const char* description );
extern ac_result_t ac_client_get_generate_license( const char* user, const char* password );
extern ac_result_t ac_client_generate_card( const char* batch_number, int type, int points, int count, const char* description, ac_rechargeable_card_t* card_array );
extern ac_result_t ac_client_query_top_up_card_history( int id, ac_top_up_card_history_t** history, int* count );
extern ac_result_t ac_client_query_remain_points( int id, int realm, int* outpt );
extern ac_result_t ac_client_rmb_direct_charge( const char* account, int realm, int type, int* outpt );
extern ac_result_t ac_client_cleanup_account( const char* account );
extern ac_result_t ac_client_safe_lock( const char* account, const char* password, unsigned char is_lock );

// utilities
extern const char* ac_client_get_error_string( ac_result_t r );
extern void ac_client_get_time( unsigned int t, int* outyear, int* outmonth, int* outday, int* outhour, int* outminute, int* outsecond );

#ifdef __cplusplus
}
#endif

#endif // _AC_CLIENT_INTERFACE_HEAD