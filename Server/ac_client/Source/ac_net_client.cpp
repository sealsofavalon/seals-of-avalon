#include "stdafx.h"
#include "ac_net_client.h"
#include "ac_client_interface.h"

extern boost::asio::io_service* _g_ac_io_service;

ac_net_client::mt_wait_t::mt_wait_t() : used( 0 ) {}

ac_net_client::ac_net_client(void) : tcp_client( *net_global::get_io_service() ), m_lasthb( 0 )
{
}

ac_net_client::~ac_net_client(void)
{
}

void ac_net_client::on_close( const boost::system::error_code& error )
{
	tcp_client::on_close( error );
	_notify_all();
}

void ac_net_client::on_connect()
{
//	boost::mutex::scoped_lock lock( m_connect_mutex );
//	m_connect_cond.notify_one();
	tcp_client::on_connect();
	m_lasthb = (uint32)time( NULL );
}

void ac_net_client::on_connect_failed( boost::system::error_code error )
{
//	boost::mutex::scoped_lock lock( m_connect_mutex );
//	m_connect_cond.notify_one();
}

void ac_net_client::push_message( message_t* msg )
{
	proc_message( *msg );
}

void ac_net_client::proc_message( const message_t& msg )
{
	CPacketUsn pak( (char*)msg.data, msg.len );

	if( pak.GetTransID() >= POOL_COUNT )
	{
#ifdef _DEBUG
		printf( "Fatal error!!! ack serial invalid" );
#endif
		return;
	}
	switch( pak.GetProNo() )
	{
	case MSG_AC2CLIENT::AUTH_ACK:
		{
			MSG_AC2CLIENT::stAuthAck msg;
			pak >> msg;

			boost::mutex::scoped_lock lock( m_waitobj[msg.nTransID].mutex );
			m_waitobj[msg.nTransID].ret = msg.ret;
			if( msg.ret == MSG_AC2CLIENT::stAuthAck::RET_SUCCESS )
			{
				account_info_t* info = new account_info_t( msg.acc );
				m_waitobj[msg.nTransID].attach = (uint32)info;
			}
			else
				m_waitobj[msg.nTransID].attach = 0;
			m_waitobj[msg.nTransID].cond.notify_one();
		}
		break;
	case MSG_AC2CLIENT::MOD_PWD_ACK:
		{
			MSG_AC2CLIENT::stModPwdAck msg;
			pak >> msg;

			boost::mutex::scoped_lock lock( m_waitobj[msg.nTransID].mutex );
			m_waitobj[msg.nTransID].ret = msg.ret;
			m_waitobj[msg.nTransID].attach = msg.id;
			m_waitobj[msg.nTransID].cond.notify_one();
		}
		break;
	case MSG_AC2CLIENT::CREATE_ACC_ACK:
		{
			MSG_AC2CLIENT::stCreateAccAck msg;
			pak >> msg;

			boost::mutex::scoped_lock lock( m_waitobj[msg.nTransID].mutex );
			m_waitobj[msg.nTransID].ret = msg.ret;
			m_waitobj[msg.nTransID].attach = msg.id;
			m_waitobj[msg.nTransID].cond.notify_one();
		}
		break;
	case MSG_AC2CLIENT::QUERY_ACC_ACK:
		{
			MSG_AC2CLIENT::stQueryAccAck msg;
			pak >> msg;


			boost::mutex::scoped_lock lock( m_waitobj[msg.nTransID].mutex );
			m_waitobj[msg.nTransID].ret = msg.ret;
			if( msg.ret == MSG_AC2CLIENT::stQueryAccAck::RET_SUCCESS )
			{
				account_info_t* info = new account_info_t( msg.acc );
				m_waitobj[msg.nTransID].attach = (uint32)info;
			}
			m_waitobj[msg.nTransID].cond.notify_one();
		}
		break;
	case MSG_AC2CLIENT::FETCH_PWD_ACK:
		{
			MSG_AC2CLIENT::stFetchPwdAck msg;
			pak >> msg;

			boost::mutex::scoped_lock lock( m_waitobj[msg.nTransID].mutex );
			m_waitobj[msg.nTransID].ret = msg.ret;
			if( msg.ret == MSG_AC2CLIENT::stFetchPwdAck::RET_SUCCESS )
			{
				char* newpwd = strdup( msg.newpwd.c_str() );
				m_waitobj[msg.nTransID].attach = (uint32)newpwd;
			}
			m_waitobj[msg.nTransID].cond.notify_one();
		}
		break;
	case MSG_AC2CLIENT::TOP_UP_CARD_ACK:
		{
			MSG_AC2CLIENT::stTopUpCardAck msg;
			pak >> msg;

			boost::mutex::scoped_lock lock( m_waitobj[msg.nTransID].mutex );
			m_waitobj[msg.nTransID].ret = msg.ret;
			m_waitobj[msg.nTransID].cond.notify_one();
		}
		break;
	case MSG_AC2CLIENT::VIP_ACCESS_ACK:
		{
			MSG_AC2CLIENT::stVipAccessAck msg;
			pak >> msg;

			boost::mutex::scoped_lock lock( m_waitobj[msg.nTransID].mutex );
			m_waitobj[msg.nTransID].ret = msg.ret;
			m_waitobj[msg.nTransID].cond.notify_one();
		}
		break;
	case MSG_AC2CLIENT::GENERATE_CARD_ACK:
		{
			MSG_AC2CLIENT::stGenerateCardAck msg;
			pak >> msg;

			boost::mutex::scoped_lock lock( m_waitobj[msg.nTransID].mutex );
			m_waitobj[msg.nTransID].ret = msg.ret;
			if( msg.ret == MSG_AC2CLIENT::stGenerateCardAck::RET_SUCCESS )
			{
				ac_rechargeable_card_t* p = new ac_rechargeable_card_t[msg.cards.size()];
				for( size_t i = 0; i < msg.cards.size(); ++i )
				{
					strncpy( p[i].serial, msg.cards[i].serial.c_str(), sizeof( p[i].serial ) );
					strncpy( p[i].password, msg.cards[i].password.c_str(), sizeof( p[i].password ) );
				}
				m_waitobj[msg.nTransID].attach = (uint32)p;
			}
			m_waitobj[msg.nTransID].cond.notify_one();
		}
		break;
	case MSG_AC2CLIENT::QUERY_TOP_UP_CARD_HISTORY_ACK:
		{
			MSG_AC2CLIENT::stQueryTopUpCardHistoryAck msg;
			pak >> msg;
			boost::mutex::scoped_lock lock( m_waitobj[msg.nTransID].mutex );
			m_waitobj[msg.nTransID].ret = msg.ret;
			if( msg.ret == MSG_AC2CLIENT::stQueryTopUpCardHistoryAck::RET_SUCCESS )
			{
				ac_top_up_card_history_t* p = new ac_top_up_card_history_t[msg.historys.size()];
				for( size_t i = 0; i < msg.historys.size(); ++i )
				{
					const MSG_AC2CLIENT::stQueryTopUpCardHistoryAck::history_t& his = msg.historys[i];
					p[i].points = his.points;
					p[i].remain_points = his.remain_points;
					p[i].type = his.type;
					p[i].realm_id = his.realm;
					p[i].unixtime = his.time;
				}
				m_waitobj[msg.nTransID].attach = (uint32)p;
				m_waitobj[msg.nTransID].attach2 = msg.historys.size();
			}
			m_waitobj[msg.nTransID].cond.notify_one();
		}
		break;
	case MSG_AC2CLIENT::QUERY_REMAIN_POINTS_ACK:
		{
			MSG_AC2CLIENT::stQueryRemainPointsAck msg;
			pak >> msg;
			boost::mutex::scoped_lock lock( m_waitobj[msg.nTransID].mutex );
			m_waitobj[msg.nTransID].ret = msg.ret;
			m_waitobj[msg.nTransID].attach = (uint32)msg.remain_points;
			m_waitobj[msg.nTransID].cond.notify_one();
		}
		break;
	case MSG_AC2CLIENT::RMB_DIRECT_CHARGE_ACK:
		{
			MSG_AC2CLIENT::stRMBDirectChargeAck msg;
			pak >> msg;
			boost::mutex::scoped_lock lock( m_waitobj[msg.nTransID].mutex );
			m_waitobj[msg.nTransID].ret = msg.ret;
			m_waitobj[msg.nTransID].attach = msg.points;
			m_waitobj[msg.nTransID].cond.notify_one();
		}
		break;
	case MSG_AC2CLIENT::CLEANUP_ACCOUNT_ACK:
		{
			MSG_AC2CLIENT::stCleanupAccountAck msg;
			pak >> msg;
			boost::mutex::scoped_lock lock( m_waitobj[msg.nTransID].mutex );
			m_waitobj[msg.nTransID].ret = msg.ret;
			m_waitobj[msg.nTransID].attach = 0;
			m_waitobj[msg.nTransID].cond.notify_one();
		}
		break;
	case MSG_AC2CLIENT::SAFE_LOCK_ACK:
		{
			MSG_AC2CLIENT::stSafeLockAck msg;
			pak >> msg;
			boost::mutex::scoped_lock lock ( m_waitobj[msg.nTransID].mutex );
			m_waitobj[msg.nTransID].ret = msg.ret;
			m_waitobj[msg.nTransID].attach = 0;
			m_waitobj[msg.nTransID].cond.notify_one();
		}
		break;
	case MSG_AC2CLIENT::MOD_INFORMATION_ACK:
		{
			MSG_AC2CLIENT::stModInformationAck msg;
			pak >> msg;
			boost::mutex::scoped_lock lock ( m_waitobj[msg.nTransID].mutex );
			m_waitobj[msg.nTransID].ret = msg.ret;
			m_waitobj[msg.nTransID].attach = 0;
			m_waitobj[msg.nTransID].cond.notify_one();
		}
		break;
	case MSG_AC2CLIENT::MOD_PWD_BY_QA_ACK:
		{
			MSG_AC2CLIENT::stModPwdByQaAck msg;
			pak >> msg;
			boost::mutex::scoped_lock lock ( m_waitobj[msg.nTransID].mutex );
			m_waitobj[msg.nTransID].ret = msg.ret;
			m_waitobj[msg.nTransID].attach = 0;
			m_waitobj[msg.nTransID].cond.notify_one();
		}
		break;
	default:
#ifdef _DEBUG
		printf( "recv unknown message from server, message id : [%d]", pak.GetProNo() );
#endif
		break;
	}
}

void ac_net_client::run()
{
	_get_cb_mgr()->poll();
	heart_beat();
}

void ac_net_client::heart_beat()
{
	if( is_connected() )
	{
		uint32 now = (uint32)time( NULL );
		if( now - m_lasthb >= 25 )
		{
			MSG_CLIENT2AC::stHeartBeatReq req;
			send_packet( req );
			m_lasthb = now;
		}
	}
}

void ac_net_client::send_packet( const PakHead& pak )
{
	CPacketSn cps;
	cps << pak;
	cps.SetProLen();
	send_message( cps.GetBuf(), cps.GetSize() );
}

uint32 ac_net_client::fetch_wait_obj()
{
	while( true )
	{
		for( int i = 0; i < POOL_COUNT; ++i )
		{
			if( !interlocked_compare_exchange( &m_waitobj[i].used, 1, 0 ) )
			{
				return i;
			}
		}
		Sleep( 1 );
	}
	return 0;
}

uint8 ac_net_client::wait( uint32 waitid, const PakHead& pak, uint32& output, uint32* attach )
{
	boost::mutex::scoped_lock lock( m_waitobj[waitid].mutex );
	send_packet( pak );
	m_waitobj[waitid].cond.wait( lock );
	output = m_waitobj[waitid].attach;
	if( attach )
		*attach = m_waitobj[waitid].attach2;
	uint8 ret = m_waitobj[waitid].ret;
	interlocked_write( &m_waitobj[waitid].used, 0 );
	return ret;
}

// bool ac_net_client::wait_connect( const char* ip, unsigned short port )
// {
//	boost::mutex::scoped_lock lock( m_connect_mutex );
//	this->connect( ip, port );
// 	m_connect_cond.wait( lock );
// 	return is_connected();
// }

void ac_net_client::_notify_all()
{
	for( int i = 0; i < POOL_COUNT; ++i )
	{
		if( m_waitobj[i].used )
		{
			m_waitobj[i].ret = AC_CLIENT_ERROR_DISCONNECTED;
			m_waitobj[i].attach = 0;
			m_waitobj[i].cond.notify_one();
			interlocked_write( &m_waitobj[i].used, 0 );
		}
	}
}
