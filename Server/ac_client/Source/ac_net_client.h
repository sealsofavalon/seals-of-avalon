#ifndef _AC_NET_CLIENT_HEAD
#define _AC_NET_CLIENT_HEAD

#include <boost/thread/condition.hpp>

class ac_net_client : public tcp_client
{
	enum { POOL_COUNT = 20 };
	struct mt_wait_t
	{
		boost::mutex mutex;
		boost::condition cond;
		uint8 ret;
		uint32 attach;
		uint32 attach2;
		volatile boost::uint32_t used;
		mt_wait_t();
	};
public:
	ac_net_client(void);
	~ac_net_client(void);

public:
	virtual void on_close( const boost::system::error_code& error );
	virtual void on_connect();
	virtual void on_connect_failed( boost::system::error_code error );
	virtual void push_message( message_t* msg );
	virtual void proc_message( const message_t& msg );
	virtual void run();
	void send_packet( const PakHead& pak );

	uint32 fetch_wait_obj();
	uint8 wait( uint32 waitid, const PakHead& pak, uint32& output, uint32* attach = NULL );
	//bool wait_connect( const char* ip, unsigned short port );
	void heart_beat();

private:
	void _notify_all();
	//boost::condition m_connect_cond;
	//boost::mutex m_connect_mutex;
	mt_wait_t m_waitobj[POOL_COUNT];
	uint32 m_lasthb;
};


#endif // _AC_NET_CLIENT_HEAD
