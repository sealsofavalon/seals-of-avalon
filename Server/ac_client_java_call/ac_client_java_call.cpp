// ac_client_java_call.cpp : 定义 DLL 应用程序的导出函数。
//

#include "stdafx.h"
#include "ac_client_java_call.h"
#include <map>
#include <vector>
#include <cstdio>
#include <locale.h>
#include <string>

jstring NewJString( JNIEnv* env, const char* str )
{
	jchar dest[4096];
	size_t n = mbstowcs( (wchar_t*)dest, str, 4096 );
	if( n >= 4096 || n == 0 )
		return jstring();

	dest[n] = 0;
#ifdef _WIN32
	return env->NewString( dest, n );
#else
	return env->NewString( dest, n*2 );
#endif
}

std::string GetJString( JNIEnv* env, jbyteArray str, jint length )
{
	if( length == 0 ) return std::string( "" );

	jbyte* jb = env->GetByteArrayElements( str, 0 );
	char temp[1024] = { 0 };
	memcpy( temp, jb, length );
	env->ReleaseByteArrayElements( str, jb, 0 );
	return std::string( temp );
}

void CopyJString( JNIEnv* env, jbyteArray str, jint length, char* dest )
{
	jbyte* jb = env->GetByteArrayElements( str, 0 );
	memcpy( dest, jb, length );
	dest[length] = 0;
	env->ReleaseByteArrayElements( str, jb, 0 );
}

struct handle
{
	handle()
	{
		memset( this, 0, sizeof( handle ) );
	}
	~handle()
	{
		if( phis )
			delete[] phis;
	}
	void release()
	{
		delete this;
	}

	java_handle_t h;
	ac_result_t result;
	ac_account_t acc;
	char newpwd[32];
	ac_top_up_card_history_t* phis;
	int his_count;
	int points;
};

class handle_manager
{
public:
	handle_manager() : m_current( 0 ) {}
	~handle_manager()
	{
		for( std::map<java_handle_t, handle*>::iterator it = m_handles.begin(); it != m_handles.end(); ++it )
			it->second->release();
	}
	handle* find( java_handle_t h )
	{
		std::map<java_handle_t, handle*>::iterator it = m_handles.find( h );
		if( it != m_handles.end() )
			return it->second;
		else
			return NULL;
	}
	handle* create_handle()
	{
		handle* p = new handle;
		p->h = ++m_current;
		m_handles[p->h] = p;
		return p;
	}
	void close_handle( java_handle_t h )
	{
		std::map<java_handle_t, handle*>::iterator it = m_handles.find( h );
		if( it != m_handles.end() )
		{
			it->second->release();
			m_handles.erase( it );
		}
	}

private:
	java_handle_t m_current;
	std::map<java_handle_t, handle*> m_handles;
};

handle_manager* g_hmgr = NULL;

JNIEXPORT java_handle_t	JNICALL Java_com_sunyou_account_interfacec_accjc_createHandle(JNIEnv *, jclass)
{
	if( g_hmgr )
	{
		handle* p = g_hmgr->create_handle();
		return p->h;
	}
	else
		return 0;
}

JNIEXPORT jint			JNICALL Java_com_sunyou_account_interfacec_accjc_closeHandle(JNIEnv *, jclass, java_handle_t h )
{
	if( g_hmgr )
	{
		g_hmgr->close_handle( h );
		return AC_CLIENT_OK;
	}
	else
		return AC_CLIENT_ERROR_UNINITIALIZED;
}

JNIEXPORT java_result_t	JNICALL Java_com_sunyou_account_interfacec_accjc_handleReadResult(JNIEnv *, jclass, java_handle_t h )
{
	if( !g_hmgr ) return AC_CLIENT_ERROR_UNINITIALIZED;

	handle* p = g_hmgr->find( h );
	if( !p ) return AC_CLIENT_ERROR_INVALID_HANDLE;

	return p->result;
}

JNIEXPORT jint			JNICALL Java_com_sunyou_account_interfacec_accjc_handleReadAccountId(JNIEnv *, jclass, java_handle_t h )
{
	if( !g_hmgr ) return AC_CLIENT_ERROR_UNINITIALIZED;

	handle* p = g_hmgr->find( h );
	if( !p ) return AC_CLIENT_ERROR_INVALID_HANDLE;

	return p->acc.id;
}

JNIEXPORT jstring		JNICALL Java_com_sunyou_account_interfacec_accjc_handleReadAccountName( JNIEnv* env, jclass, java_handle_t h )
{
	if( !g_hmgr ) return NewJString( env, "" );

	handle* p = g_hmgr->find( h );
	if( !p ) return NewJString( env, "" );

	return NewJString( env, p->acc.name );
}

JNIEXPORT jstring		JNICALL Java_com_sunyou_account_interfacec_accjc_handleReadAccountGmflag(JNIEnv *env, jclass, java_handle_t h)
{
	if( !g_hmgr ) return NewJString( env, "" );

	handle* p = g_hmgr->find( h );
	if( !p ) return NewJString( env, "" );

	return NewJString( env, p->acc.gm_flag );
}

JNIEXPORT jstring		JNICALL Java_com_sunyou_account_interfacec_accjc_handleReadAccountSource( JNIEnv* env, jclass, java_handle_t h )
{
	if( !g_hmgr ) return NewJString( env, "" );

	handle* p = g_hmgr->find( h );
	if( !p ) return NewJString( env, "" );

	return NewJString( env, p->acc.source );
}

JNIEXPORT jstring		JNICALL Java_com_sunyou_account_interfacec_accjc_handleReadAccountEmail( JNIEnv* env, jclass, java_handle_t h )
{
	if( !g_hmgr ) return NewJString( env, "" );

	handle* p = g_hmgr->find( h );
	if( !p ) return NewJString( env, "" );

	return NewJString( env, p->acc.email );
}

JNIEXPORT jint			JNICALL Java_com_sunyou_account_interfacec_accjc_handleReadAccountQuestion( JNIEnv* env, jclass, java_handle_t h, jint index )
{
	if( index < 0 || index > 1 ) return 0;

	if( !g_hmgr ) return 0;

	handle* p = g_hmgr->find( h );
	if( !p ) return 0;

	return p->acc.question[index];
}

JNIEXPORT jstring		JNICALL Java_com_sunyou_account_interfacec_accjc_handleReadAccountNationality( JNIEnv* env, jclass, java_handle_t h )
{
	if( !g_hmgr ) return NewJString( env, "" );

	handle* p = g_hmgr->find( h );
	if( !p ) return NewJString( env, "" );

	return NewJString( env, p->acc.nationality );
}

JNIEXPORT jstring		JNICALL Java_com_sunyou_account_interfacec_accjc_handleReadAccountProvince( JNIEnv* env, jclass, java_handle_t h )
{
	if( !g_hmgr ) return NewJString( env, "" );

	handle* p = g_hmgr->find( h );
	if( !p ) return NewJString( env, "" );

	return NewJString( env, p->acc.province );
}

JNIEXPORT jstring		JNICALL Java_com_sunyou_account_interfacec_accjc_handleReadAccountCity( JNIEnv* env, jclass, java_handle_t h )
{
	if( !g_hmgr ) return NewJString( env, "" );

	handle* p = g_hmgr->find( h );
	if( !p ) return NewJString( env, "" );

	return NewJString( env, p->acc.city );
}

JNIEXPORT jint			JNICALL Java_com_sunyou_account_interfacec_accjc_handleReadAccountBirthyear(JNIEnv *, jclass, java_handle_t h )
{
	if( !g_hmgr ) return 0;

	handle* p = g_hmgr->find( h );
	if( !p ) return 0;

	return p->acc.birth_year;
}

JNIEXPORT jint			JNICALL Java_com_sunyou_account_interfacec_accjc_handleReadAccountBirthmonth( JNIEnv *, jclass, java_handle_t h )
{
	if( !g_hmgr ) return 0;

	handle* p = g_hmgr->find( h );
	if( !p ) return 0;

	return p->acc.birth_month;
}

JNIEXPORT jint			JNICALL Java_com_sunyou_account_interfacec_accjc_handleReadAccountBirthday( JNIEnv *, jclass, java_handle_t h )
{
	if( !g_hmgr ) return 0;

	handle* p = g_hmgr->find( h );
	if( !p ) return 0;

	return p->acc.birth_day;
}

JNIEXPORT jstring		JNICALL Java_com_sunyou_account_interfacec_accjc_handleReadAccountIdcard( JNIEnv* env, jclass, java_handle_t h )
{
	if( !g_hmgr ) return NewJString( env, "" );

	handle* p = g_hmgr->find( h );
	if( !p ) return NewJString( env, "" );

	return NewJString( env, p->acc.id_card );
}

JNIEXPORT jstring		JNICALL Java_com_sunyou_account_interfacec_accjc_handleReadAccountDescription( JNIEnv* env, jclass, java_handle_t h )
{
	if( !g_hmgr ) return NewJString( env, "" );

	handle* p = g_hmgr->find( h );
	if( !p ) return NewJString( env, "" );

	return NewJString( env, p->acc.description );
}

JNIEXPORT jint			JNICALL Java_com_sunyou_account_interfacec_accjc_handleReadAccountRegTime( JNIEnv* env, jclass, java_handle_t h )
{
	if( !g_hmgr ) return 0;

	handle* p = g_hmgr->find( h );
	if( !p ) return 0;

	return p->acc.reg_time;
}

JNIEXPORT jint			JNICALL Java_com_sunyou_account_interfacec_accjc_handleReadAccountSafeLockFlag( JNIEnv *, jclass, java_handle_t h )
{
	if( !g_hmgr ) return 0;

	handle* p = g_hmgr->find( h );
	if( !p ) return 0;

	return p->acc.safe_lock_flag;
}

JNIEXPORT jint			JNICALL Java_com_sunyou_account_interfacec_accjc_handleReadAccountOccupation( JNIEnv* env, jclass, java_handle_t h )
{
	if( !g_hmgr ) return 0;

	handle* p = g_hmgr->find( h );
	if( !p ) return 0;

	return p->acc.occupation;
}

JNIEXPORT jint			JNICALL Java_com_sunyou_account_interfacec_accjc_handleReadAccountEducation( JNIEnv* env, jclass, java_handle_t h )
{
	if( !g_hmgr ) return 0;

	handle* p = g_hmgr->find( h );
	if( !p ) return 0;

	return p->acc.education;
}

JNIEXPORT jstring		JNICALL Java_com_sunyou_account_interfacec_accjc_handleReadAccountQQ( JNIEnv* env, jclass, java_handle_t h )
{
	if( !g_hmgr ) return NewJString( env, "" );

	handle* p = g_hmgr->find( h );
	if( !p ) return NewJString( env, "" );

	return NewJString( env, p->acc.qq );
}

JNIEXPORT jstring		JNICALL Java_com_sunyou_account_interfacec_accjc_handleReadAccountMsn( JNIEnv* env, jclass, java_handle_t h )
{
	if( !g_hmgr ) return NewJString( env, "" );

	handle* p = g_hmgr->find( h );
	if( !p ) return NewJString( env, "" );

	return NewJString( env, p->acc.msn );
}

JNIEXPORT jstring		JNICALL Java_com_sunyou_account_interfacec_accjc_handleReadAccountTelephone( JNIEnv* env, jclass, java_handle_t h )
{
	if( !g_hmgr ) return NewJString( env, "" );

	handle* p = g_hmgr->find( h );
	if( !p ) return NewJString( env, "" );

	return NewJString( env, p->acc.telephone );
}

JNIEXPORT jstring		JNICALL Java_com_sunyou_account_interfacec_accjc_handleReadAccountZipcode( JNIEnv* env, jclass, java_handle_t h )
{
	if( !g_hmgr ) return NewJString( env, "" );

	handle* p = g_hmgr->find( h );
	if( !p ) return NewJString( env, "" );

	return NewJString( env, p->acc.zipcode );
}

JNIEXPORT jstring		JNICALL Java_com_sunyou_account_interfacec_accjc_handleReadAccountAddress( JNIEnv* env, jclass, java_handle_t h )
{
	if( !g_hmgr ) return NewJString( env, "" );

	handle* p = g_hmgr->find( h );
	if( !p ) return NewJString( env, "" );

	return NewJString( env, p->acc.address );
}

JNIEXPORT jint			JNICALL Java_com_sunyou_account_interfacec_accjc_handleReadAccountGender(JNIEnv *, jclass, java_handle_t h)
{
	if( !g_hmgr ) return 0;

	handle* p = g_hmgr->find( h );
	if( !p ) return 0;

	return p->acc.gender;
}

JNIEXPORT jstring		JNICALL Java_com_sunyou_account_interfacec_accjc_handleReadAccountRealName(JNIEnv * env, jclass, java_handle_t h)
{
	if( !g_hmgr ) return NewJString( env, "" );

	handle* p = g_hmgr->find( h );
	if( !p ) return NewJString( env, "" );

	return NewJString( env, p->acc.real_name );
}

JNIEXPORT jstring		JNICALL Java_com_sunyou_account_interfacec_accjc_handleReadNewpwd( JNIEnv* env, jclass, java_handle_t h )
{
	if( !g_hmgr ) return NewJString( env, "" );

	handle* p = g_hmgr->find( h );
	if( !p ) return NewJString( env, "" );

	return NewJString( env, p->newpwd );
}

JNIEXPORT jint			JNICALL Java_com_sunyou_account_interfacec_accjc_handleReadTopUpCardHistoryRecordCount( JNIEnv* env, jclass, java_handle_t h )
{
	if( !g_hmgr ) return 0;

	handle* p = g_hmgr->find( h );
	if( !p ) return 0;

	return p->his_count;
}

JNIEXPORT jint			JNICALL Java_com_sunyou_account_interfacec_accjc_handleReadTopUpCardHistoryInitPoints( JNIEnv* env, jclass, java_handle_t h, jint index )
{
	if( !g_hmgr ) return 0;

	handle* p = g_hmgr->find( h );
	if( !p ) return 0;

	if( index >= p->his_count || !p->phis ) return 0;
	return p->phis[index].points;
}

JNIEXPORT jint			JNICALL Java_com_sunyou_account_interfacec_accjc_handleReadTopUpCardHistoryRemainPoints(JNIEnv *, jclass, java_handle_t h, jint index)
{
	if( !g_hmgr ) return 0;

	handle* p = g_hmgr->find( h );
	if( !p ) return 0;

	if( index >= p->his_count || !p->phis ) return 0;
	return p->phis[index].remain_points;
}

JNIEXPORT jint			JNICALL Java_com_sunyou_account_interfacec_accjc_handleReadTopUpCardHistoryType( JNIEnv* env, jclass, java_handle_t h, jint index )
{
	if( !g_hmgr ) return 0;

	handle* p = g_hmgr->find( h );
	if( !p ) return 0;

	if( index >= p->his_count || !p->phis ) return 0;
	return p->phis[index].type;
}

JNIEXPORT jint			JNICALL Java_com_sunyou_account_interfacec_accjc_handleReadTopUpCardHistoryRealm( JNIEnv* env, jclass, java_handle_t h, jint index )
{
	if( !g_hmgr ) return 0;

	handle* p = g_hmgr->find( h );
	if( !p ) return 0;

	if( index >= p->his_count || !p->phis ) return 0;
	return p->phis[index].realm_id;
}

JNIEXPORT jint			JNICALL Java_com_sunyou_account_interfacec_accjc_handleReadTopUpCardHistoryTime( JNIEnv* env, jclass, java_handle_t h, jint index )
{
	if( !g_hmgr ) return 0;

	handle* p = g_hmgr->find( h );
	if( !p ) return 0;
	if( index >= p->his_count || !p->phis ) return 0;

	return p->phis[index].unixtime;
}

JNIEXPORT jint			JNICALL Java_com_sunyou_account_interfacec_accjc_handleReadRMBDirectChargePoints(JNIEnv *, jclass, java_handle_t h)
{
	if( !g_hmgr ) return 0;

	handle* p = g_hmgr->find( h );
	if( !p ) return 0;
	return p->points;
}

JNIEXPORT java_result_t JNICALL Java_com_sunyou_account_interfacec_accjc_handleWriteAccountId(JNIEnv *, jclass, java_handle_t h, jint i)
{
	if( !g_hmgr ) return AC_CLIENT_ERROR_UNINITIALIZED;

	handle* p = g_hmgr->find( h );
	if( !p ) return AC_CLIENT_ERROR_INVALID_HANDLE;

	p->acc.id = i;
	return AC_CLIENT_OK;
}

JNIEXPORT java_result_t JNICALL Java_com_sunyou_account_interfacec_accjc_handleWriteAccountName( JNIEnv* env, jclass, java_handle_t h, jbyteArray str, jint str_len )
{
	if( !g_hmgr ) return AC_CLIENT_ERROR_UNINITIALIZED;

	handle* p = g_hmgr->find( h );
	if( !p ) return AC_CLIENT_ERROR_INVALID_HANDLE;
	if( str_len >= sizeof( p->acc.name ) )
		return AC_CLIENT_ERROR_STRING_OVERFLOW;

	std::string accname = GetJString( env, str, str_len );
	strcpy( p->acc.name, accname.c_str() );
	return AC_CLIENT_OK;
}

JNIEXPORT java_result_t	JNICALL Java_com_sunyou_account_interfacec_accjc_handleWriteAccountGmflag(JNIEnv *env, jclass, java_handle_t h, jbyteArray str, jint str_len)
{
	if( !g_hmgr ) return AC_CLIENT_ERROR_UNINITIALIZED;

	handle* p = g_hmgr->find( h );
	if( !p ) return AC_CLIENT_ERROR_INVALID_HANDLE;
	if( str_len >= sizeof( p->acc.gm_flag ) )
		return AC_CLIENT_ERROR_STRING_OVERFLOW;

	CopyJString( env, str, str_len, p->acc.gm_flag );
	return AC_CLIENT_OK;
}

JNIEXPORT java_result_t JNICALL Java_com_sunyou_account_interfacec_accjc_handleWriteAccountSource( JNIEnv* env, jclass, java_handle_t h, jbyteArray str, jint str_len )
{
	if( !g_hmgr ) return AC_CLIENT_ERROR_UNINITIALIZED;

	handle* p = g_hmgr->find( h );
	if( !p ) return AC_CLIENT_ERROR_INVALID_HANDLE;
	if( str_len >= sizeof( p->acc.source ) )
		return AC_CLIENT_ERROR_STRING_OVERFLOW;

	CopyJString( env, str, str_len, p->acc.source );
	return AC_CLIENT_OK;
}

JNIEXPORT java_result_t JNICALL Java_com_sunyou_account_interfacec_accjc_handleWriteAccountEmail( JNIEnv* env, jclass, java_handle_t h, jbyteArray str, jint str_len )
{
	if( !g_hmgr ) return AC_CLIENT_ERROR_UNINITIALIZED;

	handle* p = g_hmgr->find( h );
	if( !p ) return AC_CLIENT_ERROR_INVALID_HANDLE;
	if( str_len >= sizeof( p->acc.email ) )
		return AC_CLIENT_ERROR_STRING_OVERFLOW;

	CopyJString( env, str, str_len, p->acc.email );
	return AC_CLIENT_OK;
}

JNIEXPORT java_result_t JNICALL Java_com_sunyou_account_interfacec_accjc_handleWriteAccountQuestion( JNIEnv* env, jclass, java_handle_t h, jint index, jint question )
{
	if( index < 0 || index > 1 ) return AC_CLIENT_ERROR_UNKNOWN;

	if( !g_hmgr ) return AC_CLIENT_ERROR_UNINITIALIZED;

	handle* p = g_hmgr->find( h );
	if( !p ) return AC_CLIENT_ERROR_INVALID_HANDLE;
	p->acc.question[index] = question;
	return AC_CLIENT_OK;
}

JNIEXPORT java_result_t JNICALL Java_com_sunyou_account_interfacec_accjc_handleWriteAccountNationality( JNIEnv* env, jclass, java_handle_t h, jbyteArray str, jint str_len )
{
	if( !g_hmgr ) return AC_CLIENT_ERROR_UNINITIALIZED;

	handle* p = g_hmgr->find( h );
	if( !p ) return AC_CLIENT_ERROR_INVALID_HANDLE;
	if( str_len >= sizeof( p->acc.nationality ) )
		return AC_CLIENT_ERROR_STRING_OVERFLOW;

	CopyJString( env, str, str_len, p->acc.nationality );
	return AC_CLIENT_OK;
}

JNIEXPORT java_result_t JNICALL Java_com_sunyou_account_interfacec_accjc_handleWriteAccountProvince( JNIEnv* env, jclass, java_handle_t h, jbyteArray str, jint str_len )
{
	if( !g_hmgr ) return AC_CLIENT_ERROR_UNINITIALIZED;

	handle* p = g_hmgr->find( h );
	if( !p ) return AC_CLIENT_ERROR_INVALID_HANDLE;
	if( str_len >= sizeof( p->acc.province ) )
		return AC_CLIENT_ERROR_STRING_OVERFLOW;

	CopyJString( env, str, str_len, p->acc.province );
	return AC_CLIENT_OK;
}

JNIEXPORT java_result_t JNICALL Java_com_sunyou_account_interfacec_accjc_handleWriteAccountCity( JNIEnv* env, jclass, java_handle_t h, jbyteArray str, jint str_len )
{
	if( !g_hmgr ) return AC_CLIENT_ERROR_UNINITIALIZED;

	handle* p = g_hmgr->find( h );
	if( !p ) return AC_CLIENT_ERROR_INVALID_HANDLE;
	if( str_len >= sizeof( p->acc.city ) )
		return AC_CLIENT_ERROR_STRING_OVERFLOW;

	CopyJString( env, str, str_len, p->acc.city );
	return AC_CLIENT_OK;
}

JNIEXPORT java_result_t JNICALL Java_com_sunyou_account_interfacec_accjc_handleWriteAccountBirthyear( JNIEnv* env, jclass, java_handle_t h, jint i )
{
	if( !g_hmgr ) return AC_CLIENT_ERROR_UNINITIALIZED;

	handle* p = g_hmgr->find( h );
	if( !p ) return AC_CLIENT_ERROR_INVALID_HANDLE;

	p->acc.birth_year = i;
	return AC_CLIENT_OK;
}

JNIEXPORT java_result_t JNICALL Java_com_sunyou_account_interfacec_accjc_handleWriteAccountBirthmonth( JNIEnv* env, jclass, java_handle_t h, jint i)
{
	if( !g_hmgr ) return AC_CLIENT_ERROR_UNINITIALIZED;

	handle* p = g_hmgr->find( h );
	if( !p ) return AC_CLIENT_ERROR_INVALID_HANDLE;

	p->acc.birth_month = i;
	return AC_CLIENT_OK;
}

JNIEXPORT java_result_t JNICALL Java_com_sunyou_account_interfacec_accjc_handleWriteAccountBirthday( JNIEnv* env, jclass, java_handle_t h, jint i)
{
	if( !g_hmgr ) return AC_CLIENT_ERROR_UNINITIALIZED;

	handle* p = g_hmgr->find( h );
	if( !p ) return AC_CLIENT_ERROR_INVALID_HANDLE;

	p->acc.birth_day = i;
	return AC_CLIENT_OK;
}

JNIEXPORT java_result_t JNICALL Java_com_sunyou_account_interfacec_accjc_handleWriteAccountIdcard( JNIEnv* env, jclass, java_handle_t h, jbyteArray str, jint str_len )
{
	if( !g_hmgr ) return AC_CLIENT_ERROR_UNINITIALIZED;

	handle* p = g_hmgr->find( h );
	if( !p ) return AC_CLIENT_ERROR_INVALID_HANDLE;
	if( str_len >= sizeof( p->acc.id_card ) )
		return AC_CLIENT_ERROR_STRING_OVERFLOW;

	CopyJString( env, str, str_len, p->acc.id_card );
	return AC_CLIENT_OK;
}

JNIEXPORT java_result_t JNICALL Java_com_sunyou_account_interfacec_accjc_handleWriteAccountDescription( JNIEnv* env, jclass, java_handle_t h, jbyteArray str, jint str_len )
{
	if( !g_hmgr ) return AC_CLIENT_ERROR_UNINITIALIZED;

	handle* p = g_hmgr->find( h );
	if( !p ) return AC_CLIENT_ERROR_INVALID_HANDLE;
	if( str_len >= sizeof( p->acc.description ) )
		return AC_CLIENT_ERROR_STRING_OVERFLOW;

	CopyJString( env, str, str_len, p->acc.description );
	return AC_CLIENT_OK;
}

JNIEXPORT java_result_t	JNICALL Java_com_sunyou_account_interfacec_accjc_handleWriteAccountSafeLockPassword(JNIEnv * env, jclass, java_handle_t h, jbyteArray oldpwd, jint op_len, jbyteArray newpwd, jint np_len )
{
	if( !g_hmgr ) return AC_CLIENT_ERROR_UNINITIALIZED;

	handle* p = g_hmgr->find( h );
	if( !p ) return AC_CLIENT_ERROR_INVALID_HANDLE;
	if( np_len >= sizeof( p->acc.safe_lock_pwd ) )
		return AC_CLIENT_ERROR_STRING_OVERFLOW;

	std::string coldpwd = GetJString( env, oldpwd, op_len );
	std::string cnewpwd = GetJString( env, newpwd, np_len );
	if( coldpwd.length() == 0 || coldpwd == p->acc.safe_lock_pwd )
	{
		CopyJString( env, newpwd, np_len, p->acc.safe_lock_pwd );
		return AC_CLIENT_OK;
	}
	else
		return AC_CLIENT_ERROR_WRONG_PWD;
}

JNIEXPORT java_result_t	JNICALL Java_com_sunyou_account_interfacec_accjc_handleWriteAccountOccupation(JNIEnv *, jclass, java_handle_t h, jint i)
{
	if( !g_hmgr ) return AC_CLIENT_ERROR_UNINITIALIZED;

	handle* p = g_hmgr->find( h );
	if( !p ) return AC_CLIENT_ERROR_INVALID_HANDLE;

	p->acc.occupation = i;
	return AC_CLIENT_OK;
}

JNIEXPORT java_result_t	JNICALL Java_com_sunyou_account_interfacec_accjc_handleWriteAccountEducation(JNIEnv *, jclass, java_handle_t h, jint i)
{
	if( !g_hmgr ) return AC_CLIENT_ERROR_UNINITIALIZED;

	handle* p = g_hmgr->find( h );
	if( !p ) return AC_CLIENT_ERROR_INVALID_HANDLE;

	p->acc.education = i;
	return AC_CLIENT_OK;
}

JNIEXPORT java_result_t	JNICALL Java_com_sunyou_account_interfacec_accjc_handleWriteAccountQQ( JNIEnv* env, jclass, java_handle_t h, jbyteArray str, jint str_len )
{
	if( !g_hmgr ) return AC_CLIENT_ERROR_UNINITIALIZED;

	handle* p = g_hmgr->find( h );
	if( !p ) return AC_CLIENT_ERROR_INVALID_HANDLE;
	if( str_len >= sizeof( p->acc.qq ) )
		return AC_CLIENT_ERROR_STRING_OVERFLOW;

	CopyJString( env, str, str_len, p->acc.qq );
	return AC_CLIENT_OK;
}

JNIEXPORT java_result_t	JNICALL Java_com_sunyou_account_interfacec_accjc_handleWriteAccountMsn( JNIEnv* env, jclass, java_handle_t h, jbyteArray str, jint str_len )
{
	if( !g_hmgr ) return AC_CLIENT_ERROR_UNINITIALIZED;

	handle* p = g_hmgr->find( h );
	if( !p ) return AC_CLIENT_ERROR_INVALID_HANDLE;
	if( str_len >= sizeof( p->acc.msn ) )
		return AC_CLIENT_ERROR_STRING_OVERFLOW;

	CopyJString( env, str, str_len, p->acc.msn );
	return AC_CLIENT_OK;
}

JNIEXPORT java_result_t	JNICALL Java_com_sunyou_account_interfacec_accjc_handleWriteAccountTelephone( JNIEnv* env, jclass, java_handle_t h, jbyteArray str, jint str_len )
{
	if( !g_hmgr ) return AC_CLIENT_ERROR_UNINITIALIZED;

	handle* p = g_hmgr->find( h );
	if( !p ) return AC_CLIENT_ERROR_INVALID_HANDLE;
	if( str_len >= sizeof( p->acc.telephone ) )
		return AC_CLIENT_ERROR_STRING_OVERFLOW;

	CopyJString( env, str, str_len, p->acc.telephone );
	return AC_CLIENT_OK;
}

JNIEXPORT java_result_t	JNICALL Java_com_sunyou_account_interfacec_accjc_handleWriteAccountZipcode( JNIEnv* env, jclass, java_handle_t h, jbyteArray str, jint str_len )
{
	if( !g_hmgr ) return AC_CLIENT_ERROR_UNINITIALIZED;

	handle* p = g_hmgr->find( h );
	if( !p ) return AC_CLIENT_ERROR_INVALID_HANDLE;
	if( str_len >= sizeof( p->acc.zipcode ) )
		return AC_CLIENT_ERROR_STRING_OVERFLOW;

	CopyJString( env, str, str_len, p->acc.zipcode );
	return AC_CLIENT_OK;
}

JNIEXPORT java_result_t	JNICALL Java_com_sunyou_account_interfacec_accjc_handleWriteAccountAddress( JNIEnv* env, jclass, java_handle_t h, jbyteArray str, jint str_len )
{
	if( !g_hmgr ) return AC_CLIENT_ERROR_UNINITIALIZED;

	handle* p = g_hmgr->find( h );
	if( !p ) return AC_CLIENT_ERROR_INVALID_HANDLE;
	if( str_len >= sizeof( p->acc.address ) )
		return AC_CLIENT_ERROR_STRING_OVERFLOW;

	CopyJString( env, str, str_len, p->acc.address );
	return AC_CLIENT_OK;
}

JNIEXPORT java_result_t	JNICALL Java_com_sunyou_account_interfacec_accjc_handleWriteAccountGender(JNIEnv *, jclass, java_handle_t h, jint i)
{
	if( !g_hmgr ) return AC_CLIENT_ERROR_UNINITIALIZED;

	handle* p = g_hmgr->find( h );
	if( !p ) return AC_CLIENT_ERROR_INVALID_HANDLE;

	p->acc.gender = (unsigned char)i;
	return AC_CLIENT_OK;
}

JNIEXPORT java_result_t	JNICALL Java_com_sunyou_account_interfacec_accjc_handleWriteAccountRealName(JNIEnv * env, jclass, java_handle_t h, jbyteArray str, jint str_len)
{
	if( !g_hmgr ) return AC_CLIENT_ERROR_UNINITIALIZED;

	handle* p = g_hmgr->find( h );
	if( !p ) return AC_CLIENT_ERROR_INVALID_HANDLE;
	if( str_len >= sizeof( p->acc.real_name ) )
		return AC_CLIENT_ERROR_STRING_OVERFLOW;

	CopyJString( env, str, str_len, p->acc.real_name );
	return AC_CLIENT_OK;
}

JNIEXPORT java_result_t JNICALL Java_com_sunyou_account_interfacec_accjc_init( JNIEnv* env, jclass, jbyteArray ip, jint ip_len, jint port )
{
	if( g_hmgr )
		return AC_CLIENT_ERROR_ALREADY_INITED;

#ifdef _WIN32
	const char* p = setlocale( LC_ALL, "chs" );
#else
	const char* p = setlocale( LC_ALL, "zh_CN.gbk" );
#endif

	if( p == NULL || p[0] == 0 )
		return AC_CLIENT_ERROR_SET_LOCALE;

	g_hmgr = new handle_manager;

	std::string cip = GetJString( env, ip, ip_len );
	printf( "welcome to use ac_client_java_call version 1.0\n" );
	return ac_client_init( cip.c_str(), (unsigned short)port );
}

JNIEXPORT java_result_t JNICALL Java_com_sunyou_account_interfacec_accjc_release(JNIEnv *, jclass)
{
	if( !g_hmgr )
		return AC_CLIENT_ERROR_UNINITIALIZED;
	delete g_hmgr;

	return ac_client_release();
}

JNIEXPORT java_handle_t JNICALL Java_com_sunyou_account_interfacec_accjc_auth( JNIEnv* env, jclass, jbyteArray account, jint acc_len, jbyteArray pwd, jint pwd_len )
{
	if( !g_hmgr ) return AC_CLIENT_ERROR_UNINITIALIZED;

	std::string caccount = GetJString( env, account, acc_len );

	std::string cpwd = GetJString( env, pwd, pwd_len );

	handle* p = g_hmgr->create_handle();
	p->result = ac_client_auth( caccount.c_str(), cpwd.c_str(), &p->acc );
	return p->h;
}

JNIEXPORT java_handle_t JNICALL Java_com_sunyou_account_interfacec_accjc_modifyPassword( JNIEnv* env, jclass, jbyteArray account, jint acc_len, jbyteArray oldpwd, jint op_len, jbyteArray newpwd, jint np_len )
{
	if( !g_hmgr ) return AC_CLIENT_ERROR_UNINITIALIZED;

	std::string caccount = GetJString( env, account, acc_len );
	std::string coldpwd = GetJString( env, oldpwd, op_len );
	std::string cnewpwd = GetJString( env, newpwd, np_len );

	handle* p = g_hmgr->create_handle();
	p->result = ac_client_modify_password( caccount.c_str(), coldpwd.c_str(), cnewpwd.c_str(), &p->acc.id );
	return p->h;
}

JNIEXPORT java_handle_t JNICALL Java_com_sunyou_account_interfacec_accjc_createAccount( JNIEnv * env, jclass, java_handle_t hacc, jbyteArray pwd, jint pwd_len, jbyteArray answer1, jint a1_len, jbyteArray answer2, jint a2_len )
{
	if( !g_hmgr ) return AC_CLIENT_ERROR_UNINITIALIZED;

	std::string cpwd = GetJString( env, pwd, pwd_len );
	std::string canswer1 = GetJString( env, answer1, a1_len );
	std::string canswer2 = GetJString( env, answer2, a2_len );

	handle* p = g_hmgr->find( hacc );
	if( !p ) return AC_CLIENT_ERROR_INVALID_HANDLE;

	handle* np = g_hmgr->create_handle();
	np->result = ac_client_create_account( &p->acc, cpwd.c_str(), canswer1.c_str(), canswer2.c_str(), &np->acc.id );
	return np->h;
}

JNIEXPORT java_handle_t JNICALL Java_com_sunyou_account_interfacec_accjc_queryAccount( JNIEnv* env, jclass, jbyteArray account, jint acc_len )
{
	if( !g_hmgr ) return AC_CLIENT_ERROR_UNINITIALIZED;

	std::string caccount = GetJString( env, account, acc_len );
	handle* p = g_hmgr->create_handle();
	p->result = ac_client_query_account( caccount.c_str(), &p->acc );
	printf( "for liujunxiang: email:%s\n", p->acc.email );
	return p->h;
}

JNIEXPORT java_handle_t JNICALL Java_com_sunyou_account_interfacec_accjc_fetchPwd( JNIEnv * env, jclass, jbyteArray account, jint acc_len, jint question, jbyteArray answer, jint a_len )
{
	if( !g_hmgr ) return AC_CLIENT_ERROR_UNINITIALIZED;

	std::string caccount = GetJString( env, account, acc_len );
	std::string canswer = GetJString( env, answer, a_len );

	handle* p = g_hmgr->create_handle();
	p->result = ac_client_fetch_pwd( caccount.c_str(), question, canswer.c_str(), p->newpwd );
	return p->h;
}

JNIEXPORT java_result_t JNICALL Java_com_sunyou_account_interfacec_accjc_topUpCard( JNIEnv* env, jclass, jbyteArray account, jint acc_len, jbyteArray serial, jint s_len, jbyteArray password, jint p_len, jint realm_id, jbyteArray description, jint de_len )
{
	if( !g_hmgr ) return AC_CLIENT_ERROR_UNINITIALIZED;

	std::string caccount = GetJString( env, account, acc_len );
	std::string cserial = GetJString( env, serial, s_len );
	std::string cpassword = GetJString( env, password, p_len );
	std::string cdescription = GetJString( env, description, de_len );
	return ac_client_top_up_card( caccount.c_str(), cserial.c_str(), cpassword.c_str(), realm_id, cdescription.c_str() );
}

JNIEXPORT java_handle_t JNICALL Java_com_sunyou_account_interfacec_accjc_queryTopUpCardHistory( JNIEnv* env, jclass, jint id )
{
	if( !g_hmgr ) return AC_CLIENT_ERROR_UNINITIALIZED;

	handle* p = g_hmgr->create_handle();
	p->result = ac_client_query_top_up_card_history( id, &p->phis, &p->his_count );
	return p->h;
}

JNIEXPORT jint			JNICALL Java_com_sunyou_account_interfacec_accjc_queryRemainPoints( JNIEnv *, jclass, jint id, jint realm )
{
	if( !g_hmgr ) return AC_CLIENT_ERROR_UNINITIALIZED;

	int remain_points = 0;
	ac_client_query_remain_points( id, realm, &remain_points );
	return remain_points;
}

JNIEXPORT java_handle_t JNICALL Java_com_sunyou_account_interfacec_accjc_rmbDirectCharge( JNIEnv* env, jclass, jbyteArray account, jint acc_len, jint realm, jint type )
{
	if( !g_hmgr ) return AC_CLIENT_ERROR_UNINITIALIZED;

	handle* p = g_hmgr->create_handle();
	std::string caccount = GetJString( env, account, acc_len );
	p->result = ac_client_rmb_direct_charge( caccount.c_str(), realm, type, &p->points );
	return p->h;
}

JNIEXPORT java_result_t JNICALL Java_com_sunyou_account_interfacec_accjc_modifyInformation( JNIEnv* env, jclass, java_handle_t hacc, jbyteArray answer1, jint a1_len, jbyteArray answer2, jint a2_len )
{
	if( !g_hmgr ) return AC_CLIENT_ERROR_UNINITIALIZED;

	handle* pacc = g_hmgr->find( hacc );
	if( !pacc ) return AC_CLIENT_ERROR_INVALID_HANDLE;

	std::string canswer1 = GetJString( env, answer1, a1_len );
	std::string canswer2 = GetJString( env, answer2, a2_len );
	return ac_client_modify_information( &pacc->acc, canswer1.c_str(), canswer2.c_str() );
}

JNIEXPORT java_handle_t JNICALL Java_com_sunyou_account_interfacec_accjc_modifyPasswordByQA( JNIEnv* env, jclass, jbyteArray account, jint acc_len, jint question, jbyteArray answer, jint a_len, jbyteArray newpwd, jint np_len )
{
	if( !g_hmgr ) return AC_CLIENT_ERROR_UNINITIALIZED;

	handle* p = g_hmgr->create_handle();

	std::string caccount = GetJString( env, account, acc_len );
	std::string canswer = GetJString( env, answer, a_len );
	std::string cnewpwd = GetJString( env, newpwd, np_len );
	p->result = ac_client_modify_password_by_qa( caccount.c_str(), question, canswer.c_str(), cnewpwd.c_str() );
	return p->h;
}

JNIEXPORT java_handle_t JNICALL Java_com_sunyou_account_interfacec_accjc_safeLock( JNIEnv* env, jclass, jbyteArray account, jint acc_len, jbyteArray password, jint pwd_len, jint is_lock )
{
	if( !g_hmgr ) return AC_CLIENT_ERROR_UNINITIALIZED;

	handle* p = g_hmgr->create_handle();

	std::string caccount = GetJString( env, account, acc_len );
	std::string cpassword = GetJString( env, password, pwd_len );
	p->result = ac_client_safe_lock( caccount.c_str(), cpassword.c_str(), (unsigned char)is_lock );
	return p->h;
}

JNIEXPORT jstring		JNICALL Java_com_sunyou_account_interfacec_accjc_getErrorString( JNIEnv* env, jclass, java_result_t r )
{
	return NewJString( env, ac_client_get_error_string( r ) );
}

JNIEXPORT jstring		JNICALL Java_com_sunyou_account_interfacec_accjc_getTimeString( JNIEnv* env, jclass, jint t )
{
	int y, mon, d, h, min, s;
	ac_client_get_time( t, &y, &mon, &d, &h, &min, &s );
	char temp[32] = { 0 };
	sprintf( temp, "%02d-%02d-%02d %02d:%02d:%02d", y, mon, d, h, min, s );
	return NewJString( env, temp );
}
