// ac_client.cpp : 定义控制台应用程序的入口点。
//
#include "stdafx.h"
#include "../ac_client/Source/ac_client_interface.h"
#include "../../new_common/Source/utilities/utilities.h"

#define AC_OPERATION_CONSOLE_VERSION_A
//#define AC_OPERATION_CONSOLE_VERSION_F

#include <cstdlib>
#include <cstdio>
#include <algorithm>
#include <functional>

boost::uint32_t exec_count = 0;
bool s_exit = false;
int thread_count = 0;
void ac_thread_test()
{

	ac_account_t acc;
	while( !s_exit )
	{
		ac_client_auth( "test1", "test1", &acc );
		interlocked_increment( &exec_count );
	}
}
boost::thread* s_threads[100] = { NULL };
int begin_time = 0;
void start_ac_thread_test( int tc )
{
	if( thread_count )
	{
		printf( "some threads is already running\n" );
		return;
	}
	s_exit = false;
	begin_time = get_tick_count();
	exec_count = 0;
	thread_count = tc;
	for( int i = 0; i < tc; ++i )
		s_threads[i] = new boost::thread( &ac_thread_test );
	printf( "%d threads has been started\n", tc );
}
void stop_ac_thread_test()
{
	if( !thread_count )
	{
		printf( "no thread running\n" );
		return;
	}
	int end_time = get_tick_count();
	s_exit = true;
	for( int i = 0; i < thread_count; ++i )
	{
		s_threads[i]->join();
		delete s_threads[i];
	}
	thread_count = 0;
	printf( "exec count:%d, spend time:%d, cps:%.2f\n", exec_count, end_time - begin_time, (float)exec_count / ( (float)(end_time - begin_time) / 1000.f ) );
}

void create_account( ac_account_t& acc, uint32 count, const char* namehead, FILE* fp )
{
	//static const char s_table[] = "abcdefghijklmnopqrstuvwxyz0123456789";
	for( uint32 i = 0; i < count; ++i )
	{
		std::string temp;
		char buff[32] = { 0 };
		temp = std::string( namehead ) + itoa( i + 1, buff, 10 );
		strcpy( acc.name, temp.c_str() );
		//char pwd[5];
		//for( int i = 0; i < 4; ++i )
		//	pwd[i] = s_table[rand() % 36];
		//pwd[4] = 0;
		printf( "creating acc:%s pwd:%s source:%s\n", acc.name, "1", acc.source );
		int accountid = 0;
		int ret = ac_client_create_account( &acc, "1", "", "", &accountid );
		printf( "create acc finished, ret:%s accountid:%d\n", ac_client_get_error_string( ret ), accountid );
		if( ret == AC_CLIENT_OK && fp )
			fprintf( fp, "%s %s\n", acc.name, "1" );
	}
}

int main()
{
	const char* defaultip = "142.54.187.194";
#ifdef AC_OPERATION_CONSOLE_VERSION_F
	printf( "welcome to use account center operation console finance version!\n" );
#else
	printf( "welcome to use account center operation console administrator version!\n" );
#endif

	printf( "please input account center ip\ntype 1 and press enter to connect default ip:[%s]...", defaultip );
	char ip[32];
	scanf( "%s", ip );
	if( strlen( ip ) < 5 )
		strcpy( ip, defaultip );
	if( AC_CLIENT_OK == ac_client_init( ip, 7500 ) )
		printf( "ac_client_init succeed\n" );
	else
	{
		printf( "ac_client_init failed, please wait for reconnection...\n" );
	}
#ifndef AC_OPERATION_CONSOLE_VERSION_F
	const char AC_COMMAND_STRING[] = "\tauth [account] [password]\n\tmodp [account] [oldpassword] [newpassword]\n\tfetchpwd [account] [pwdanswer]\n\tcreate [account] [password]\n\tstestcreateacc\n\tbatchcreatebytxt [filename]\n\tbatchauth [startname] [count]\n\tlicense [user] [password]\n\tgencards [batch_number(6-bytes)] [type] [points] [count] [description]\n\ttopupcard [account] [serial] [password] [realm]\n\tqueryhistory [account]\n\tremainpt [account] [realm]\n\trmbcharge [account] [realm] [card type]\n\tstartmt [threadcount]\n\tstopmt\n\tcleanup [account]\n\tmodemail [account] [password] [new email]\n\tmodpwdbyqa [account] [question id] [answer] [new password]\n\tlock [account]\n\tunlock [account]\n\thelp\n\texit";
#else
	const char AC_COMMAND_STRING[] = "\tlicense [user] [password]\n\tgencards [batch_number(6-bytes)] [type] [points] [count] [description]\n\thelp\n\texit";
#endif
	printf( "please input command to test ac_client\ncommand:\n%s\n", AC_COMMAND_STRING );
	while( true )
	{
		std::string input;
		std::cin >> input;
#ifndef AC_OPERATION_CONSOLE_VERSION_F
		if( input.length() == 4 && 0 == strnicmp( input.c_str(), "auth", 4 ) )
		{
			std::string account, pwd;
			std::cin >> account;
			std::cin >> pwd;
			ac_account_t acc;
			int count = 0;
			//while( true )
			{
				printf( "authing acc:%s pwd:%s\n", account.c_str(), pwd.c_str() );
				uint32 n = get_tick_count();
				int ret = ac_client_auth( account.c_str(), pwd.c_str(), &acc );
				printf( "%d auth finished, ret:%s accountid:%d, gm_flag:%s, spend time:%dms\n", ++count, ac_client_get_error_string( ret ), acc.id, acc.gm_flag, get_tick_count() - n );
			}
		}
		else if( input.length() == 4 && 0 == strnicmp( input.c_str(), "modp", 4 ) )
		{
			std::string account, oldpwd, newpwd;
			std::cin >> account >> oldpwd >> newpwd;

			int accountid = 0;
			printf( "modifying acc:%s oldpwd:%s newpwd:%s\n", account.c_str(), oldpwd.c_str(), newpwd.c_str() );
			uint32 n = get_tick_count();
			int ret = ac_client_modify_password( account.c_str(), oldpwd.c_str(), newpwd.c_str(), &accountid );
			printf( "modify acc finished, ret:%s accountid:%d, spend time:%dms\n", ac_client_get_error_string( ret ), accountid, get_tick_count() - n );
		}
		else if( input.length() == 6 && 0 == strnicmp( input.c_str(), "create", 6 ) )
		{
			ac_account_t acc;
			memset( &acc, 0, sizeof( acc ) );
			std::string pwd, pwdanswer( "kyo" );
			std::cin >> acc.name >> pwd;
			int accountid = 0;
			strcpy( acc.gm_flag, "a" );
			strcpy( acc.nationality, "USA" );
			strcpy( acc.province, "New York" );
			strcpy( acc.city, "New York" );
			acc.birth_year = 1970;
			acc.birth_month = 1;
			acc.birth_day = 1;
			strcpy( acc.id_card, "310109198002251087" );
			strcpy( acc.description, "Game Master" );
			strcpy( acc.email, "jtx_gm@fzycube.com" );
			strcpy( acc.source, "generated by ac_operation_console" );
			//strcpy( acc.id_card, "310112345678901" );
			printf( "creating acc:%s pwd:%s source:%s\n", acc.name, pwd.c_str(), acc.source );
			uint32 n = get_tick_count();
			int ret = ac_client_create_account( &acc, pwd.c_str(), "", "", &accountid );
			printf( "create acc finished, ret:%s accountid:%d, spend time:%dms\n", ac_client_get_error_string( ret ), accountid, get_tick_count() - n );
		}
		else if( input.length() == 14 && 0 == strnicmp( input.c_str(), "stestcreateacc", 14 ) )
		{
			uint32 n = get_tick_count();
			ac_account_t acc;
			memset( &acc, 0, sizeof( acc ) );
			std::string pwdanswer = "kyo";
			strcpy( acc.nationality, "USA" );
			strcpy( acc.province, "New York" );
			strcpy( acc.city, "New York" );
			acc.birth_year = 1982;
			acc.birth_month = 9;
			acc.birth_day = 29;
			strcpy( acc.description, "The king of fighters" );
			strcpy( acc.email, "guixiaoyu@sunyou.com" );
			strcpy( acc.source, "stress_test" );
			strcpy( acc.gm_flag, "a" );

			srand( 123 );
			create_account( acc, 5000, "t", NULL );

			printf( "stress test account create finished, spend time:%d\n", get_tick_count() - n );
		}
		else if( input.length() == 16 && 0 == strnicmp( input.c_str(), "batchcreatebytxt", 16 ) )
		{
			uint32 count = 0;
			std::string file;
			std::string account;
			std::cin >> file;
			uint32 n = get_tick_count();

			static char s_table[] = "0123456789";
			srand( (uint32)time( NULL ) );
			FILE* fp = fopen( "castletest_account.txt", "w" );
			if( !fp )
			{
				printf( "cannot open file stress_test_accounts.txt\n" );
				continue;
			}

			FILE* fptxt = fopen(file.c_str(), "r");
			if( !fptxt )
			{
				printf( "cannot read file[%s]\n", file.c_str() );
				continue;
			}

			while(!feof(fptxt))
			{
				std::string temp;
				char buff[32] = { 0 };
				fscanf(fptxt, "%s\n", buff);
				account = buff;
				char pwd[9];
				for( int i = 0; i < 3; ++i )
					pwd[i] = s_table[rand() % 10];
				pwd[3] = 0;

				ac_account_t acc;
				memset( &acc, 0, sizeof( acc ) );
				int accountid = 0;
				strcpy( acc.name, account.c_str() );
				strcpy( acc.gm_flag, "a" );
				std::string pwdanswer = "kyo";
				strcpy( acc.nationality, "USA" );
				strcpy( acc.province, "New York" );
				strcpy( acc.city, "New York" );
				acc.birth_year = 1982;
				acc.birth_month = 9;
				acc.birth_day = 29;
				strcpy( acc.description, "The king of fighters" );
				strcpy( acc.email, "pylonsprotoss@hotmail.com" );
				strcpy( acc.source, "stress_test" );
				printf( "creating acc:%s pwd:%s source:%s\n", acc.name, pwd, acc.source );
				uint32 n = get_tick_count();
				int ret = ac_client_create_account( &acc, pwd, "", "", &accountid );
				printf( "create acc finished, ret:%s accountid:%d\n", ac_client_get_error_string( ret ), accountid );
				if( ret == AC_CLIENT_OK )
					fprintf( fp, "%s %s\n", acc.name, pwd );
			}
			fclose( fp );
			printf( "batchcreate finished, spend time:%d\n", get_tick_count() - n );
		}
		else if( input.length() == 9 && 0 == strnicmp( input.c_str(), "batchauth", 9 ) )
		{
			uint32 count = 0;
			ac_account_t acc;
			std::string account;
			std::cin >> account >> count;
			uint32 n = get_tick_count();

			for( uint32 i = 0; i < count; ++i )
			{
				std::string temp, pwd;
				char buff[32] = { 0 };
				temp = account + itoa( i, buff, 10 );
				pwd = temp;
				printf( "authing acc:%s pwd:%s\n", temp.c_str(), pwd.c_str() );
				int ret = ac_client_auth( temp.c_str(), pwd.c_str(), &acc );
				printf( "auth acc finished, ret:%s accountid:%d, gm_flag:%s\n", ac_client_get_error_string( ret ), acc.id, acc.gm_flag );
			}
			printf( "batchauth finished, spend time:%d\n", get_tick_count() - n );
		}
		else if( input.length() == 5 && 0 == strnicmp( input.c_str(), "query", 5 ) )
		{
			std::string account;
			std::cin >> account;

			ac_account_t acc;
			memset( &acc, 0, sizeof( acc ) );
			printf( "querying acc:%s\n", account.c_str() );
			uint32 n = get_tick_count();
			int ret = ac_client_query_account( account.c_str(), &acc );
			printf( "query acc finished, ret:%s id:%d\n/*************************\nname:%s\nsource:%s\nemail:%s\nnationality:%s\nprovince:%s\ncity:%s\nbirth_year:%u\nbirth_month:%u\nbirth_day:%u\ndescription:%s\noccupation:%d\neducation:%d\nqq:%s\nmsn:%s\ntelephone:%s\nzipcode:%s\naddress:%s\n*************************/\nspend time:%dms\n",
				ac_client_get_error_string( ret ), acc.id, acc.name, acc.source, acc.email,
				acc.nationality, acc.province, acc.city, acc.birth_year, acc.birth_month, acc.birth_day,
				acc.description, acc.occupation, acc.education, acc.qq, acc.msn, acc.telephone, acc.zipcode, acc.address, get_tick_count() - n );
		}
		else if( input.length() == 8 && 0 == strnicmp( input.c_str(), "fetchpwd", 8 ) )
		{
			int question = 0;
			std::string account, answer;
			std::cin >> account >> question >> answer;

			char newpwd[32] = { 0 };
			printf( "fetching password, acc:%s question:%d answer:%s\n", account.c_str(), question, answer.c_str() );
			uint32 n = get_tick_count();
			int ret = ac_client_fetch_pwd( account.c_str(), question, answer.c_str(), newpwd );
			printf( "fetch password finished, ret:%s the new password is:%s, spend time:%dms\n", ac_client_get_error_string( ret ), newpwd, get_tick_count() - n );
		}
		else if( input.length() == 9 && 0 == strnicmp( input.c_str(), "topupcard", 9 ) )
		{
			std::string account, card_serial, card_password;
			int realm_id;
			std::cin >> account >> card_serial >> card_password >> realm_id;
			printf( "topping up card, acc:%s card_serial:%s card_password:%s realm_id:%d\n", account.c_str(), card_serial.c_str(), card_password.c_str(), realm_id );
			uint32 n = get_tick_count();
			int ret = ac_client_top_up_card( account.c_str(), card_serial.c_str(), card_password.c_str(), realm_id, "auto top up" );
			printf( "top up card finished, ret:%s, spend time:%dms\n", ac_client_get_error_string( ret ), get_tick_count() - n );
		}
		else if( input.length() == 12 && 0 == strnicmp( input.c_str(), "queryhistory", 12 ) )
		{
			std::string account;
			ac_account_t acc;
			std::cin >> account;
			printf( "getting account id, acc:%s\n", account.c_str() );
			uint32 n = get_tick_count();
			int ret = ac_client_query_account( account.c_str(), &acc );
			if( ret == AC_CLIENT_OK )
			{
				printf( "querying top up card history, id:%d\n", acc.id );
				int count = 0;
				ac_top_up_card_history_t* p;
				ret = ac_client_query_top_up_card_history( acc.id, &p, &count );
				if( ret == AC_CLIENT_OK )
				{
					for( int i = 0; i < count; ++i )
					{
						int y, m, d, h, mi, s;
						ac_client_get_time( p[i].unixtime, &y, &m, &d, &h, &mi, &s );
						printf( "%d->init_pt:%d remain_pt:%d type:%d realm:%d date:%02d-%02d-%02d %02d:%02d:%02d\n", i + 1, p[i].points, p[i].remain_points, p[i].type, p[i].realm_id, y, m, d, h, mi, s );
					}
					
					if( count == 0 )
						printf( "no history record\n" );
					else
						delete[] p;
				}
				else
					printf( "no history record\n" );
			}
			else
				printf( "user not found!\n" );
		}
		else if( input.length() == 6 && 0 == strnicmp( input.c_str(), "startmt", 6 ) )
		{
			int tc;
			std::cin >> tc;
			start_ac_thread_test( tc );
		}
		else if( input.length() == 6 && 0 == strnicmp( input.c_str(), "stopmt", 6 ) )
		{
			stop_ac_thread_test();
		}
		else if( input.length() == 8 && 0 == strnicmp( input.c_str(), "remainpt", 8 ) )
		{
			std::string account;
			int realm;
			std::cin >> account >> realm;
			int remain_points = 0;
			printf( "getting account id, acc:%s\n", account.c_str() );
			uint32 n = get_tick_count();
			ac_account_t acc;
			int ret = ac_client_query_account( account.c_str(), &acc );
			if( ret == AC_CLIENT_OK )
			{
				printf( "querying remain points, id:%d realm:%d\n", acc.id, realm );
				ret = ac_client_query_remain_points( acc.id, realm, &remain_points );
				if( ret == AC_CLIENT_OK )
				{
					printf( "remain points = %d\n", remain_points );
				}
				else
					printf( "database error\n" );
			}
			else
				printf( "user not found!\n" );

			printf( "query remain points finished, ret:%s, spend time:%dms\n", ac_client_get_error_string( ret ), get_tick_count() - n );
		}
		else if( input.length() == 9 && 0 == strnicmp( input.c_str(), "rmbcharge", 9 ) )
		{
			std::string account;
			int realm, type;
			std::cin >> account >> realm >> type;
			int points = 0;
			printf( "rmb direct charge....\n" );
			int n = get_tick_count();
			int ret = ac_client_rmb_direct_charge( account.c_str(), realm, type, &points );
			printf( "finished!, ret:%s points:%d, spend time:%dms\n", ac_client_get_error_string( ret ), points, get_tick_count() - n );
		}
		else if( input.length() == 4 && 0 == strnicmp( input.c_str(), "lock", 4 ) )
		{
			std::string account, password;
			std::cin >> account >> password;
			printf( "lock account....\n" );
			int n = get_tick_count();
			int ret = ac_client_safe_lock( account.c_str(), password.c_str(), 1 );
			printf( "finished!, ret:%s spend time:%dms\n", ac_client_get_error_string( ret ), get_tick_count() - n );
		}
		else if( input.length() == 6 && 0 == strnicmp( input.c_str(), "unlock", 6 ) )
		{
			std::string account, password;
			std::cin >> account >> password;
			printf( "unlock account....\n" );
			int n = get_tick_count();
			int ret = ac_client_safe_lock( account.c_str(), password.c_str(), 0 );
			printf( "finished!, ret:%s spend time:%dms\n", ac_client_get_error_string( ret ), get_tick_count() - n );
		}
		else if( input.length() == 10 && 0 == strnicmp( input.c_str(), "modpwdbyqa", 10 ) )
		{
			std::string account, answer, newpwd;
			int question;
			std::cin >> account >> question >> answer >> newpwd;
			printf( "modify password by question and answer....\n" );
			int n = get_tick_count();
			int ret = ac_client_modify_password_by_qa( account.c_str(), question, answer.c_str(), newpwd.c_str() );
			printf( "finished!, ret:%s spend time:%dms\n", ac_client_get_error_string( ret ), get_tick_count() - n );
		}
		else if( input.length() == 8 && 0 == strnicmp( input.c_str(), "modemail", 8 ) )
		{
			std::string account, pwd, email;
			std::cin >> account >> pwd >> email;
			printf( "modify email....\n" );
			ac_account_t acc;
			if( AC_CLIENT_OK == ac_client_auth( account.c_str(), pwd.c_str(), &acc ) )
			{
				int n = get_tick_count();
				strcpy( acc.email, email.c_str() );
				int ret = ac_client_modify_information( &acc, "", "" );
				printf( "finished!, ret:%s spend time:%dms\n", ac_client_get_error_string( ret ), get_tick_count() - n );
			}
			else
				printf( "auth account failed\n" );
		}
		else if( input.length() == 14 && 0 == strnicmp( input.c_str(), "modsafelockpwd", 14 ) )
		{
		}
		else if( input.length() == 7 && 0 == strnicmp( input.c_str(), "cleanup", 7 ) )
		{
			std::string account;
			std::cin >> account;
			printf( "cleanup account....\n" );
			int n = get_tick_count();
			int ret = ac_client_cleanup_account( account.c_str() );
			printf( "finished!, ret:%s spend time:%dms\n", ac_client_get_error_string( ret ), get_tick_count() - n );
		}
		else
#endif
		if( input.length() == 4 && 0 == strnicmp( input.c_str(), "help", 4 ) )
		{
			printf( "%s\n", AC_COMMAND_STRING );
		}
		else if( input.length() == 4 && 0 == strnicmp( input.c_str(), "exit", 4 ) )
		{
			break;
		}
		else if( input.length() == 7 && 0 == strnicmp( input.c_str(), "license", 7 ) )
		{
			std::string user, password;
			std::cin >> user >> password;
			printf( "getting generate license, user:%s password:%s\n", user.c_str(), password.c_str() );
			uint32 n = get_tick_count();
			int ret = ac_client_get_generate_license( user.c_str(), password.c_str() );
			printf( "get generate license finished, ret:%s, spend time:%dms\n", ac_client_get_error_string( ret ), get_tick_count() - n );
			printf( "please generate cards in 60 seconds, or the license will expire!\n" );
			printf( "max count per generate is [1000]\n" );
		}
		else if( input.length() == 8 && 0 == strnicmp( input.c_str(), "gencards", 8 ) )
		{
			std::string batch_number, description;
			int type, points, count;
			std::cin >> batch_number >> type >> points >> count >> description;
			printf( "generating cards, batch_number = %s type = %d points = %d count = %d\n", batch_number.c_str(), type, points, count );
			printf( "please wait for a while...\n" );

			char file_name[64] = { 0 };
			sprintf( file_name, "batch_number_%s.card", batch_number.c_str() );
			FILE* fp = fopen( file_name, "w" );
			if( !fp )
			{
				printf( "cannot open file: [%s]!!!\n", file_name );
				continue;
			}
			ac_rechargeable_card_t* cards = new ac_rechargeable_card_t[count];
			int n = get_tick_count();
			int ret = ac_client_generate_card( batch_number.c_str(), type, points, count, description.c_str(), cards );
			if( ret == AC_CLIENT_OK )
			{
				int y, m, d, h, mi, s;
				ac_client_get_time( (uint32)time( NULL ), &y, &m, &d, &h, &mi, &s );
				fprintf( fp, "[%d-%d-%d %d:%d:%d] generate cards...\n", y, m, d, h, mi, s );
				fprintf( fp, "batch number:[%s] type:[%d] points[%d] count[%d] description:[%s]\n", batch_number.c_str(), type, points, count, description.c_str() );
				for( int i = 0; i < count; ++i )
				{
					char card_info[128] = { 0 };
					sprintf( card_info, "[%04d] ==> card serial = %s,  card password = %s\n", i + 1, cards[i].serial, cards[i].password );
					printf( card_info );
					fprintf( fp, card_info );
				}
			}
			fclose( fp );
			delete[] cards;
			printf( "generate cards finished, ret:%s, spend time:%dms\n[%s] file was saved\n", ac_client_get_error_string( ret ), get_tick_count() - n, file_name );
		}
		else
			//printf( "bad command!!!\ncommand:\n%s\n", AC_COMMAND_STRING );
		{
			printf( "bad command! please type help to get more information!\n" );
		}
	}
	ac_client_release();
	return 0;
}
