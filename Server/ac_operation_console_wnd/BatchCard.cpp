// BatchCard.cpp : 实现文件
//

#include "stdafx.h"
#include "../ac_client/Source/ac_client_interface.h"
#include "../../new_common/Source/utilities.h"
#include "ac_operation_console_wnd.h"
#include "BatchCard.h"
#include "../../SDBase/Public/TypeDef.h"
#include "../../new_common/Source/new_common.h"


struct DESC
{
	int card_type;
	int points;
	char sz[128];
};

DESC g_cardtypes[10] = 
{
	1, 	1000, "实卡1000点",
	2,  3000, "实卡3000点",
	3,  5000, "实卡5000点",
	4,  10000, "实卡10000点",
	5,  50000, "实卡50000点",
	6, 	1000, "虚卡1000点",
	7,  3000, "虚卡3000点",
	8,  5000, "虚卡5000点",
	9,  10000, "虚卡10000点",
	10,  50000, "虚卡50000点",
};
const int TABLE_SIZE= 360;
char m_table[TABLE_SIZE + 1];
// CBatchCard 对话框

IMPLEMENT_DYNAMIC(CBatchCard, CDialog)

CBatchCard::CBatchCard(CWnd* pParent /*=NULL*/)
	: CDialog(CBatchCard::IDD, pParent)
	, m_batchnum("")
	, m_points(0)
	, m_count(0)
	, m_desc(_T(""))
	, m_nCurType(-1)
	, m_bCrypt(TRUE)
{

}

CBatchCard::~CBatchCard()
{
}

void CBatchCard::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, m_batchnum);
	DDX_Control(pDX, IDC_COMBO1, m_types);
	DDX_Text(pDX, IDC_EDIT2, m_points);
	DDX_Text(pDX, IDC_EDIT3, m_count);
	DDX_Text(pDX, IDC_EDIT4, m_desc);
	DDX_Check(pDX, IDC_CHECK1, m_bCrypt);
}


BEGIN_MESSAGE_MAP(CBatchCard, CDialog)
	ON_CBN_SELCHANGE(IDC_COMBO1, &CBatchCard::OnCbnSelchangeCombo1)
	ON_BN_CLICKED(IDOK, &CBatchCard::OnBnClickedOk)
END_MESSAGE_MAP()


// CBatchCard 消息处理程序

BOOL CBatchCard::OnInitDialog()
{
	CDialog::OnInitDialog();

	const char* p = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	for( int i = 0; i < TABLE_SIZE; ++i )
		m_table[i] = p[i % 36];
	m_table[TABLE_SIZE] = 0;

#ifdef _WIN32
	std::random_shuffle( &m_table[0], &m_table[TABLE_SIZE]/*, pointer_to_unary_function<int, int>( Rand )*/ );
#else
	std::random_shuffle( &m_table[0], &m_table[TABLE_SIZE], Rand );
#endif

	// TODO:  在此添加额外的初始化
	for (int i = 0; i < sizeof(g_cardtypes)/sizeof(DESC); i++)
	{
		m_types.InsertString(i, g_cardtypes[i].sz);
		m_types.SetItemData(i, g_cardtypes[i].card_type);
	}
	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}

void CBatchCard::OnCbnSelchangeCombo1()
{
	int nsel = m_types.GetCurSel();
	m_nCurType = m_types.GetItemData(nsel);
	if(m_nCurType <= 0 && m_nCurType >= 10)
	{
		AfxMessageBox("未知的点卡类型");
		return;
	}
	m_points = g_cardtypes[m_nCurType-1].points;
	UpdateData(FALSE);
	// TODO: 在此添加控件通知处理程序代码
}

void CBatchCard::OnBnClickedOk()
{
	UpdateData();
	if(m_nCurType == -1)
		AfxMessageBox("未选择点卡类型");
	// TODO: 在此添加控件通知处理程序代码
	char file_name[64] = { 0 };
	sprintf( file_name, "batch_number_%s.card", m_batchnum.GetString() );
	FILE* fp = fopen( file_name, "w" );
	if( !fp )
	{
		char szMsg[128];
		sprintf(szMsg, "cannot open file: [%s]!!!\n", file_name);
		AfxMessageBox( szMsg );
		return;;
	}
	ac_rechargeable_card_t* cards = new ac_rechargeable_card_t[m_count];
	int ret = ac_client_generate_card( m_batchnum.GetString(), m_nCurType, m_points, m_count, m_desc.GetString(), cards );
// 	if( ret == AC_CLIENT_OK )
// 	{
// 		int y, m, d, h, mi, s;
// 		ac_client_get_time( (uint32)time( NULL ), &y, &m, &d, &h, &mi, &s );
// 		fprintf( fp, "[%d-%d-%d %d:%d:%d] generate cards...\n", y, m, d, h, mi, s );
// 		fprintf( fp, "batch number:[%s] type:[%d] points[%d] count[%d] description:[%s]\n", m_batchnum.GetString(), m_nCurType, m_points, m_count, m_desc.GetString() );
// 		for( int i = 0; i < m_count; ++i )
// 		{
// 			char card_info[128] = { 0 };
// 			sprintf( card_info, "[%04d] ==> card serial = %s,  card password = %s\n", i + 1, cards[i].serial, cards[i].password );
// 			printf( card_info );
// 			fprintf( fp, card_info );
// 		}
// 	}
	if( ret == AC_CLIENT_OK )
	{
		int y, m, d, h, mi, s;
		ac_client_get_time( (uint32)time( NULL ), &y, &m, &d, &h, &mi, &s );
		fprintf( fp, "[%d-%d-%d %d:%d:%d] generate cards...\n", y, m, d, h, mi, s );
		fprintf( fp, "batch number:[%s] type:[%d] points[%d] count[%d] description:[%s]\n", m_batchnum.GetString(), m_nCurType, m_points, m_count, m_desc.GetString() );
		fprintf(fp, "id,card sn,password\n");
		for( int i = 0; i < m_count; ++i )
		{
			char card_info[128] = { 0 };
			sprintf( card_info, "%d,%s,%s\n", i + 1, cards[i].serial, cards[i].password );
			fprintf( fp, card_info );
		}
	}
	fclose( fp );
	delete[] cards;

	if(m_bCrypt)
	{
		char szPassword[7] = {0};
		srand( time(NULL) );

		sprintf(szPassword, "%c%c%c%c%c%c", m_table[rand() % TABLE_SIZE], m_table[rand() % TABLE_SIZE], m_table[rand() % TABLE_SIZE]
		, m_table[rand() % TABLE_SIZE], m_table[rand() % TABLE_SIZE], m_table[rand() % TABLE_SIZE]);
		
		char szCmd[128];

		FILE* fpassword;
		char szfpassword[128];
		sprintf(szfpassword, "%s.txt", file_name);
		fpassword = fopen(szfpassword, "w+");
		fwrite(szPassword, 6, 1, fpassword);
		fclose(fpassword);

		sprintf(szCmd, "\"winrar.exe\" m -p%s %s.rar %s", szPassword, file_name, file_name);
		WinExec(szCmd, SW_HIDE);
	}

	AfxMessageBox("生成成功");
}
