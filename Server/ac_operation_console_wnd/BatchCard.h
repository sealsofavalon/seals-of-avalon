#pragma once
#include "afxwin.h"


// CBatchCard 对话框

class CBatchCard : public CDialog
{
	DECLARE_DYNAMIC(CBatchCard)

public:
	CBatchCard(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CBatchCard();

// 对话框数据
	enum { IDD = IDD_DIALOG2 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	CString m_batchnum;
	CComboBox m_types;
	UINT m_points;
	UINT m_count;
	CString m_desc;
	virtual BOOL OnInitDialog();
	afx_msg void OnCbnSelchangeCombo1();
	afx_msg void OnBnClickedOk();

	int m_nCurType;
	BOOL m_bCrypt;
};
