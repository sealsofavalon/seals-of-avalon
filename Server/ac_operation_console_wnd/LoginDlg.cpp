// LoginDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "ac_operation_console_wnd.h"
#include "../ac_client/Source/ac_client_interface.h"
#include "LoginDlg.h"


// CLoginDlg 对话框

IMPLEMENT_DYNAMIC(CLoginDlg, CDialog)

CLoginDlg::CLoginDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CLoginDlg::IDD, pParent)
	, m_AcctName(_T(""))
	, m_AcctPass(_T(""))
{

}

CLoginDlg::~CLoginDlg()
{
}

void CLoginDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, m_AcctName);
	DDX_Text(pDX, IDC_EDIT2, m_AcctPass);
}


BEGIN_MESSAGE_MAP(CLoginDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CLoginDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// CLoginDlg 消息处理程序

void CLoginDlg::OnBnClickedOk()
{
	// TODO: 在此添加控件通知处理程序代码
	UpdateData(TRUE);
	int ret = ac_client_get_generate_license( m_AcctName.GetString(), m_AcctPass.GetString() );
	if(ret != AC_CLIENT_OK)
		AfxMessageBox("用户或密码错误");
	else
		OnOK();
}