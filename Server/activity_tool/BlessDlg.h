#pragma once


// CBlessDlg 对话框

class CBlessDlg : public CDialog
{
	DECLARE_DYNAMIC(CBlessDlg)

public:
	CBlessDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CBlessDlg();

// 对话框数据
	enum { IDD = IDD_DIALOG_ACT_BLESS };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
};
