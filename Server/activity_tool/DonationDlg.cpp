// DonationDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "activity_tool.h"
#include "DonationDlg.h"

BOOL CPicture::UpdatePic(CDC* pDC)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	CRect kRect;
	this->GetClientRect(&kRect);
	CDC kMemDC;
	CBitmap* pOldBitmap;
	kMemDC.CreateCompatibleDC(pDC);
	pOldBitmap = kMemDC.SelectObject(m_pkBitMap);
	pDC->BitBlt(0, 0, kRect.Width(), kRect.Height(), &kMemDC, 0, 0, SRCCOPY);
	kMemDC.SelectObject(pOldBitmap);

	UpdateWindow();

	return TRUE;
}
CPicture::CPicture()
{
	CStatic::CStatic();
	m_pkBitMap = NULL ;
}
CPicture::~CPicture()
{
	if (m_pkBitMap)
	{
		delete m_pkBitMap;
		m_pkBitMap = NULL;
	}
	CStatic::~CStatic();
}
BOOL CPicture::ChangeBitMap(const char*  str)
{
	if ( m_pkBitMap )
	{
		delete m_pkBitMap;
		m_pkBitMap = NULL;
	}

	

	m_pkBitMap = new CBitmap;
	m_pkBitMap->LoadBitmap(str);

	return TRUE; 
}
// CDonationDlg 对话框

IMPLEMENT_DYNAMIC(CDonationDlg, CDialog)

CDonationDlg::CDonationDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CDonationDlg::IDD, pParent)
{
	m_EnventId = 0;
	m_Event_photo.clear();
	//m_Event_des.clear();
	m_NumPhoto.clear();

	char Tem[_MAX_PATH];
	DWORD Error = GetCurrentDirectory(_MAX_PATH,Tem);
	m_CurFilePath = Tem ;
	m_bLoadAllPhoto = FALSE ;
	m_bNewEvent = TRUE ;
	m_InitPhtoMax = 0;

	m_bHadLoad = NULL ;
	
}

CDonationDlg::~CDonationDlg()
{
	if (!m_lppi)
	{
		(m_lppi)->Release();
	}
}

#define  PHOTOFILE "\\data\\Photo\\"
#define  DANATIONFILE "\\data\\Donation\\"

BOOL CDonationDlg::LoadJPG(const char* str, LPPICTURE *lppi)
{
	HANDLE hfile = CreateFile(str,GENERIC_READ,0,NULL,OPEN_EXISTING,0,NULL);
	if (hfile == INVALID_HANDLE_VALUE)
	{
		return FALSE ;
	}
	DWORD DwFileSize = GetFileSize(hfile, NULL);
	if ((DWORD) -1 == DwFileSize)
	{
		CloseHandle(hfile);
		return FALSE;
	}

	LPVOID pvData ;

	HGLOBAL hGlobal=GlobalAlloc(GMEM_MOVEABLE, DwFileSize );
	if (!hGlobal)
	{
		GlobalUnlock(hGlobal);
		CloseHandle(hfile);
		return FALSE ;
	}
	pvData=GlobalLock(hGlobal);

	if (!pvData)
	{
		GlobalUnlock(hGlobal);
		CloseHandle(hfile);

		return FALSE ;
	}
	
	DWORD dwFileRead=0 ;
	BOOL bRead = ReadFile(hfile,pvData,DwFileSize, &dwFileRead,NULL);

	GlobalUnlock(hGlobal);
	CloseHandle(hfile);

	if (!bRead)
	{
		return FALSE;
	}

	LPSTREAM pstm=NULL;
	HRESULT hr=CreateStreamOnHGlobal(hGlobal,TRUE, &pstm);
	if (!SUCCEEDED(hr))
	{
		if (pstm)
		{
			pstm->Release();
		}
		return FALSE;
	}

	if (!*lppi)
	{
		(*lppi)->Release();
	}


	hr = OleLoadPicture(pstm , DwFileSize, FALSE, IID_IPicture, (LPVOID*)(lppi));
	pstm->Release();
	if (!SUCCEEDED(hr))
	{
		return FALSE;
	}

	if (*lppi == NULL)
	{
		return FALSE ;
	}

	return TRUE;
}
BOOL CDonationDlg::AddBitMapList(const std::string strFile ,const char* str, CListBox& stList, std::vector<PhotoData>& vec)
{
	//加载图片目录
	
	char buf[_MAX_PATH];
	std::string FileName = m_CurFilePath + strFile ;
	sprintf_s(buf,"%s%s",FileName.c_str() ,str);


	WIN32_FIND_DATA wfd;
	HANDLE File = NULL;
	CFileFind pkFileFind;
	BOOL find =  pkFileFind.FindFile(buf);
	File = FindFirstFile(buf,  &wfd);
	DWORD Res = GetLastError();
	if (find)
	{
		stList.AddString(wfd.cFileName);
		
		sprintf_s(buf,"%s%s", FileName.c_str(),wfd.cFileName);
		
		PhotoData phData ;
		phData.PhotoDir = buf;
		vec.push_back(phData);


		while (FindNextFile(File, &wfd))
		{
			//添加到目录
			stList.AddString(wfd.cFileName);
			sprintf_s(buf,"%s%s",FileName.c_str(), wfd.cFileName);
			
			PhotoData phtData ;
			phtData.PhotoDir = buf;

			vec.push_back(phtData);
		}

		FindClose(File);
		return TRUE;
	}
	return FALSE;
}
BOOL CDonationDlg::AddImageAndDes(const char* str)
{
	for (int i =0; i < m_NumPhoto.size(); i++)
	{
		std::string strDir = m_NumPhoto[i].PhotoDir ; 
		int len = strDir.length() ;
		if (len< strlen(str))
		{
			continue ;
		}
		std::string strT = strDir.substr(len - strlen(str) ,strlen(str));
		if (_stricmp(strT.c_str(), str) == 0)
		{
			//std::string Tem = strDir.substr(strDir.find_last_of(str));
			PhotoData phData ;
			phData.PhotoDir = m_NumPhoto[i].PhotoDir;
			phData.PhotoDes = m_CurPhotoDes ;
			
			m_CurPhotoDes = "";
			SetDlgItemText(IDC_EDIT3, "");
			m_CurPhotoList.AddString(str);
			m_Event_photo.push_back(phData);
			m_CurPhotoList.SetCurSel(-1);

			return TRUE ;
		}
	}

	return FALSE;
}
BOOL CDonationDlg::DelImageAndDes(int index)
{
	char* str ;
	CString Cstr ;
	m_CurPhotoList.GetText(index,Cstr);
	str = Cstr.GetBuffer();
	std::string SaveFileName = m_CurFilePath + DANATIONFILE;

	for ( int i = 0; i < m_Event_photo.size(); i++)
	{
		std::string strDir = m_Event_photo[i].PhotoDir;
		int len = strDir.length() ;
		if (len< strlen(str))
		{
			continue ;
		}
		std::string strT = strDir.substr(len - strlen(str) ,strlen(str));

		if (_stricmp(strT.c_str(), str) == 0)
		{
			/*if (!m_bNewEvent)
			{
				CFileFind pkFileFind;
				char buf[_MAX_PATH];
				sprintf_s(buf, _MAX_PATH, "%sDonation_%d//%s" ,SaveFileName.c_str(),m_EnventId,str);

				BOOL find =  pkFileFind.FindFile(buf);
				if (find)
				{
					::DeleteFile(buf);
				}
				sprintf_s(buf, _MAX_PATH, "%sDonation_%d//Event_des%d.txt",SaveFileName.c_str(),i);
				find = pkFileFind.FindFile(buf);

				if (find )
				{
					::DeleteFile(buf);
				}
			}*/
			
			m_Event_photo.erase(m_Event_photo.begin() + i);
			m_CurPhotoList.DeleteString(index);
			m_CurPhotoList.SetCurSel(-1);
			return TRUE;
		}
	}

	return FALSE ;
}
BOOL CDonationDlg::ShowLoadJPG(const char* str, BOOL  bFromResList)
{
	if (bFromResList)
	{
		for (int i =0; i < m_NumPhoto.size(); i++)
		{
			std::string strDir = m_NumPhoto[i].PhotoDir ; 
			int len = strDir.length() ;
			if (len< strlen(str))
			{
				continue ;
			}
			std::string strT = strDir.substr(len - strlen(str) ,strlen(str));
			if (_stricmp(strT.c_str(), str) == 0)
			{
				m_bHadLoad = LoadJPG(strDir.c_str(), &m_lppi);
				OnPaint();
				return m_bHadLoad;
			}
		}
	}else
	{
		for ( int i = 0; i < m_Event_photo.size(); i++)
		{
			std::string strDir = m_Event_photo[i].PhotoDir;
			int len = strDir.length() ;
			if (len< strlen(str))
			{
				continue ;
			}
			std::string strT = strDir.substr(len - strlen(str) ,strlen(str));

			if (_stricmp(strT.c_str(), str) == 0)
			{
				m_bHadLoad = LoadJPG(strDir.c_str(), &m_lppi);
				OnPaint();
				return m_bHadLoad;
			}
		}
	}

	return FALSE ;
}
BOOL CDonationDlg::UpdataImageDes(int index)
{
	char* str ;
	CString Cstr ;
	m_CurPhotoList.GetText(index,Cstr);
	str = Cstr.GetBuffer();
	for ( int i = 0; i < m_Event_photo.size(); i++)
	{
		std::string strDir = m_Event_photo[i].PhotoDir;
		int len = strDir.length() ;
		if (len< strlen(str))
		{
			continue ;
		}
		std::string strT = strDir.substr(len - strlen(str) ,strlen(str));

		if (_stricmp(strT.c_str(), str) == 0)
		{
			m_Event_photo[i].PhotoDes = m_CurPhotoDes ;
			m_CurPhotoList.SetCurSel(-1);
			m_CurPhotoDes = "";
			SetDlgItemText(IDC_EDIT3, "");
			return TRUE;
		}
	}

	return FALSE ;
}
BOOL CDonationDlg::InitPhotoList()
{
	int Count = m_PhotoList.GetCount();
	for (int i = 0; i < Count; i++)
	{
		m_PhotoList.DeleteString(i);
	}
	std::string str = PHOTOFILE;
	AddBitMapList(str, "*.jpg", m_PhotoList, m_NumPhoto);
	//AddBitMapList(str, "*.bmp", m_PhotoList, m_NumPhoto);
	//AddBitMapList(str, "*.png", m_PhotoList, m_NumPhoto);
	return TRUE;
}
BOOL CDonationDlg::RemoveDonation(activity_mgr::donation_event_config_t* sp)
{
	if (sp)
	{
		char DirBuf[1024];
		std::string dir = m_CurFilePath + DANATIONFILE;
		sprintf_s(DirBuf, 1024, "%sDonation_%d", dir.c_str(), sp->event_id);

		//CFileFind pkFileFind;
		//BOOL find =  pkFileFind.FindFile(DirBuf);
		//if (find)
		//{
		return ::RemoveDirectory(DirBuf);
		//}
		
	}
	return FALSE;
}
BOOL CDonationDlg::InitDonationData(activity_mgr::donation_event_config_t* sp)
{
	if (sp)
	{ 
		m_bNewEvent = FALSE ;
		CWnd* pkWnd = GetDlgItem(IDC_EDIT1);
		if (pkWnd)
		{
			pkWnd->EnableWindow(FALSE);
		}
		m_EnventId = sp->event_id;
		m_EventTitle = sp->event_title.c_str();
		m_InitPhtoMax =  sp->event_photo_count;

		m_Event_photo.clear();

		//清楚数据
		m_CurPhotoList.ResetContent();
		/*for ( int i = 0 ; i < m_CurPhotoList.GetCount(); i++)
		{
			m_CurPhotoList.DeleteString(i);
		}*/

		char buf[_MAX_PATH];
		sprintf_s(buf,_MAX_PATH,"%s%sDonation_%d",m_CurFilePath.c_str(),DANATIONFILE,m_EnventId);
		
		CFileFind pkFileFind;
		BOOL find =  pkFileFind.FindFile(buf);
		if (!find)
		{
			MessageBox("没有找到资源","操作失败",MB_OK);
			return FALSE ;
		}else
		{
			pkFileFind.FindNextFile();
			BOOL bDirectoryFind = pkFileFind.IsDirectory();
			if (!bDirectoryFind)
			{
				MessageBox("没有找到事件对面资源目录","操作失败",MB_OK);
				return FALSE ;
			}
		}

		
		//AddBitMapList(buf, "Event_photo_*.jpg", m_CurPhotoList, m_Event_photo);
		//AddBitMapList(buf, "Event_photo_*.bmp", m_CurPhotoList, m_Event_photo);
		//AddBitMapList(buf, "Event_photo_*.png", m_CurPhotoList, m_Event_photo);
		

		//读取描述资源
		for (int i = 0; i< m_InitPhtoMax ; i++)
		{
			char buf2[_MAX_PATH];
			sprintf_s(buf,_MAX_PATH,"%sDonation_%d\\",DANATIONFILE,m_EnventId);
			sprintf_s(buf2,_MAX_PATH,"Event_photo_%d.jpg",i);
			AddBitMapList(buf, buf2, m_CurPhotoList, m_Event_photo);

			sprintf_s(buf,_MAX_PATH,"%s%sDonation_%d\\Event_des%d.txt",m_CurFilePath.c_str(),DANATIONFILE,m_EnventId,i);
			FILE* pkFile = fopen(buf, "rb");
			if (pkFile)
			{
				char fileStr[10*1024];
				int  len = fread(fileStr,sizeof(char),10 * 1024 ,pkFile);
				fileStr[len] = '\0' ;
				
				m_Event_photo[i].PhotoDes = fileStr;
				fclose(pkFile);
			}
		}

		

		sprintf_s(buf,_MAX_PATH,"%s%sDonation_%d\\URL.txt",m_CurFilePath.c_str(),DANATIONFILE,m_EnventId);
		FILE* pkFile = fopen(buf, "rb");
		if (pkFile)
		{
			char fileStr[10*1024];
			int  len = fread(fileStr,sizeof(char),10 * 1024 ,pkFile);
			fileStr[len] = '\0' ;
			m_Event_weblink = fileStr;
			fclose(pkFile);
		}


		UpdateData(false);

	}else
	{
		m_bNewEvent = TRUE ;
		m_InitPhtoMax = 0;
		CWnd* pkWnd = GetDlgItem(IDC_EDIT1);
		if (pkWnd)
		{
			pkWnd->EnableWindow(TRUE);
		}
		SetDlgItemText(IDC_EDIT1,"");
		SetDlgItemText(IDC_EDIT2,"");
		SetDlgItemText(IDC_EDIT3,"");
		SetDlgItemText(IDC_EDIT7,"");
		m_Event_photo.clear();
		//清楚数据
		m_CurPhotoList.ResetContent();
	}

	return TRUE ;
}
BOOL CDonationDlg::OnInitDialog()
{
	if (!CDialog::OnInitDialog())
	{
		return FALSE ;
	}

	return TRUE ;
}
void CDonationDlg::OnDestroy()
{
	CDialog::OnDestroy();
	(m_lppi)->Release();
}

BOOL CDonationDlg::SaveData()
{
	//目录设置好。
	//如果目录不存在. 那么创建.
	//
	std::string SaveFileName = m_CurFilePath + DANATIONFILE;
	char buf[_MAX_PATH];
	sprintf_s(buf, _MAX_PATH, "%sDonation_%d" ,SaveFileName.c_str(),m_EnventId);

	CFileFind pkFileFind;
	BOOL find =  pkFileFind.FindFile(buf);
	if (!find)
	{
		//如果没找到
		if (!CreateDirectory(buf, NULL))
		{
			MessageBox("创建目录失败","操作错误",MB_OK);
			return FALSE ;
		}
	}else
	{
		pkFileFind.FindNextFile();
		BOOL bDirectoryFind = pkFileFind.IsDirectory();
		if (!bDirectoryFind)
		{
			//
			if (DeleteFile(buf))
			{
				if (!CreateDirectory(buf, NULL))
				{
					MessageBox("创建目录失败","操作错误",MB_OK);
					return FALSE ;
				}
			}else
			{
				MessageBox("已经存在文件和目录名字一样","操作错误",MB_OK);
				return FALSE ;
			}	 
		}	
	}
	
	SaveFileName = buf ;
	SaveFileName += "\\";

	
	//移动文件
	for (int i = 0; i < m_Event_photo.size() ; i++)
	{
		std::string str = m_Event_photo[i].PhotoDir;
		std::string strType = str.substr(str.length() - 4,4);

		if (_stricmp(".jpg",strType.c_str()) == 0)
		{
			sprintf_s(buf, _MAX_PATH, "%sEvent_photo_%d.jpg",SaveFileName.c_str(),i);
		}else
		{
			//if (_stricmp(".bmp",strType.c_str()) == 0)
			//{
			//	sprintf_s(buf, _MAX_PATH, "%sEvent_photo_%d.bmp",SaveFileName.c_str(),i);
			//}else
			//{
			//	if (_stricmp(".png",strType.c_str()) == 0)
			//	{
			//		sprintf_s(buf, _MAX_PATH, "%sEvent_photo_%d.png",SaveFileName.c_str(),i);
			//	}
			//}
		}
		
		if (_stricmp(str.c_str(),buf) == 0)
		{

		}else
		{
			if (!CopyFile(str.c_str(), buf, FALSE))
			{
				MessageBox("复制图片失败","操作错误",MB_OK);
				return FALSE ;
			}
		}
		
		

		sprintf_s(buf, _MAX_PATH, "%sEvent_des%d.txt",SaveFileName.c_str(),i);
		FILE* pkFile = fopen(buf,"w+");
		if (pkFile)
		{
			std::string stra =  m_Event_photo[i].PhotoDes;
			fwrite(stra.c_str(),1,stra.length(), pkFile);
			fclose(pkFile);
		}
		
	}

	//如果是修改。
	//先需要在当前目录删除旧的数据
	if (!m_bNewEvent)
	{
		int DelCount = m_InitPhtoMax > m_Event_photo.size();
		if (DelCount > 0)
		{
			for (int i =0; i < m_InitPhtoMax ; i++)
			{
				sprintf_s(buf, _MAX_PATH, "%sEvent_photo_%d.jpg",SaveFileName.c_str(),i + m_Event_photo.size());
				::DeleteFile(buf);
				sprintf_s(buf, _MAX_PATH, "%sEvent_des%d.txt",SaveFileName.c_str(),i + m_Event_photo.size());
				::DeleteFile(buf);
			}
		}
	}

	std::string webLink = m_Event_weblink;
	if (webLink.length())
	{
		sprintf_s(buf, _MAX_PATH, "%sURL.txt",SaveFileName.c_str());
		FILE* pkFile = fopen(buf,"w+");
		if (pkFile)
		{
			fwrite(webLink.c_str(),1,webLink.length(), pkFile);
			fclose(pkFile);
		}
	}

	std::string Title = m_EventTitle;
	if (Title.length())
	{
		sprintf_s(buf, _MAX_PATH, "%sTitle.txt",SaveFileName.c_str());
		FILE* pkFile = fopen(buf,"w+");
		if (pkFile)
		{
			fwrite(Title.c_str(),1,Title.length(), pkFile);
			fclose(pkFile);
		}
	}
	return TRUE; 
}
void CDonationDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, m_EnventId);
	DDX_Text(pDX, IDC_EDIT2, m_EventTitle);
	DDX_Text(pDX, IDC_EDIT3, m_CurPhotoDes);
	DDX_Text(pDX, IDC_EDIT7, m_Event_weblink);
	DDX_Control(pDX,IDC_LIST1, m_PhotoList);
	DDX_Control(pDX,IDC_LIST2, m_CurPhotoList);
	//DDX_Control(pDX, IDC_EDIT3,m_PhotoDesCtrl);
	//DDX_Control(pDX,IDC_STATIC, m_Picture);

	//加载所有图片资源 
	if (!m_bLoadAllPhoto)
	{
		InitPhotoList();
		m_bLoadAllPhoto = TRUE ;
	}

}


BEGIN_MESSAGE_MAP(CDonationDlg, CDialog)
	ON_BN_CLICKED(IDC_BUTTON1, &CDonationDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON4, &CDonationDlg::OnBnClickedButton4)
	ON_LBN_SELCHANGE(IDC_LIST1, &CDonationDlg::OnLbnSelchangeList1)
	ON_LBN_SELCHANGE(IDC_LIST2, &CDonationDlg::OnLbnSelchangeList2)
	ON_WM_ERASEBKGND()
	ON_BN_CLICKED(IDC_BUTTON2, &CDonationDlg::OnBnClickedButton2)
	ON_WM_PAINT()
END_MESSAGE_MAP()

void CDonationDlg::OnBnClickedButton1()
{
	if( !UpdateData() )
		return;

	// TODO: 在此添加控件通知处理程序代码
	std::string str = m_CurPhotoDes ;
	
	if (str.length())
	{
		int index  = m_PhotoList.GetCurSel();
		if (index > -1 &&  index < m_PhotoList.GetCount())
		{
			CString text ; 
			m_PhotoList.GetText(index,text);
			if (!AddImageAndDes(text))
			{
				MessageBox("添加失败","添加失败", MB_OK);
			}
			m_PhotoList.SetCurSel(-1);
		}else
		{
			MessageBox("请选择图片","erro", MB_OK);
			return ;
		}
	}else
	{
		MessageBox("请添加图片描述","erro", MB_OK);
		return ;
	}
}

void CDonationDlg::OnBnClickedButton4()
{
	// TODO: 在此添加控件通知处理程序代码
	//

	if( !UpdateData() )
		return;

	int index  = m_CurPhotoList.GetCurSel();
	if (index > -1 &&  index < m_CurPhotoList.GetCount())
	{
		if (index < m_Event_photo.size())
		{
			//还需要删除对应的文件夹的文件
			if (!DelImageAndDes(index))
			{
				MessageBox("删除失败","删除失败", MB_OK);
			}
			
		}else
		{
			MessageBox("删除失败","删除失败", MB_OK);
		}
	}
}

void CDonationDlg::OnLbnSelchangeList1()
{
	if (!UpdateData())
	{
		return ;
	}
	
	int index  = m_PhotoList.GetCurSel();
	if (index > -1 &&  index < m_PhotoList.GetCount())
	{
		CString text ; 
		m_PhotoList.GetText(index,text);
		ShowLoadJPG(text.GetBuffer());
	}
	
	// TODO: 在此添加控件通知处理程序代码
		//m_Picture.ChangeBitMap(m_NumPhoto[2]);
	

	//
}

void CDonationDlg::OnLbnSelchangeList2()
{
	// TODO: 在此添加控件通知处理程序代码
	int index  = m_CurPhotoList.GetCurSel();
	if (index > -1 && index < m_CurPhotoList.GetCount())
	{
		CString Cstr ;
		m_CurPhotoList.GetText(index,Cstr);
	
		
		ShowLoadJPG(Cstr.GetBuffer(), m_bNewEvent);
		
		
		for ( int i = 0; i < m_Event_photo.size(); i++)
		{
			std::string strDir = m_Event_photo[i].PhotoDir;
			int len = strDir.length() ;
			if (len< Cstr.GetLength())
			{
				continue ;
			}
			std::string strT = strDir.substr(len - Cstr.GetLength() ,Cstr.GetLength());

			if (_stricmp(strT.c_str(), Cstr) == 0)
			{
				m_CurPhotoDes = m_Event_photo[i].PhotoDes;
				SetDlgItemText(IDC_EDIT3, m_CurPhotoDes);
				break ;
			}
		}
	}
}

BOOL CDonationDlg::OnEraseBkgnd(CDC* pDC)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值

	

	return TRUE;
}
void CDonationDlg::OnBnClickedButton2()
{
	// TODO: 在此添加控件通知处理程序代码

	if( !UpdateData() )
		return;

	int index  = m_CurPhotoList.GetCurSel();
	if (index > -1 &&  index < m_CurPhotoList.GetCount())
	{
		if (index < m_Event_photo.size())
		{
			//还需要删除对应的文件夹的文件
			if (!UpdataImageDes(index))
			{
				MessageBox("更新失败","更新失败", MB_OK);
			}

		}else
		{
			MessageBox("更新失败","更新失败", MB_OK);
		}
	}else
	{
		MessageBox("请选择更新的图片","请选择更新的图片", MB_OK);
	}
}

void CDonationDlg::OnPaint()
{
	CPaintDC dc(this); // device context for painting

	if (m_bHadLoad)
	{
		
		CRect rc;
		long hmWidth = 0;
		long hmHeight = 0;

		m_lppi->get_Height(&hmHeight);
		m_lppi->get_Width(&hmWidth);

		GetDlgItem(IDC_STATIC5)->GetClientRect(&rc);
		CDC* pDC = GetDlgItem(IDC_STATIC5)->GetDC();

		int nW ;
		int nH ; 
		nW = rc.Width();
		nH = rc.Height();

		HRESULT hr = m_lppi->Render(pDC->m_hDC,nW,0,-nW,nH,hmWidth,hmHeight,-hmWidth,-hmHeight,&rc);
		if (!SUCCEEDED(hr))
		{
			return ;
		}
	}

	return ;
	
}
