#pragma once

#include <OCIdl.h>

// CDonationDlg 对话框

class  CPicture : public CStatic
{
public:
	CPicture();
	~CPicture();
	BOOL ChangeBitMap(const char*  str);
	BOOL UpdatePic(CDC* pDC);
private:
	CBitmap* m_pkBitMap ;
};
class CDonationDlg : public CDialog
{
	DECLARE_DYNAMIC(CDonationDlg)

public:
	CDonationDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CDonationDlg();

// 对话框数据
	enum { IDD = IDD_DIALOG_ACT_DONATION };

	struct PhotoData
	{
		CString PhotoDir;  //目录
		CString PhotoDes;  //描述

	};

public:
protected:
	virtual BOOL OnInitDialog();
	virtual void OnDestroy();
	//读取目录文件图片资源接口
	BOOL AddBitMapList(const std::string strFile, const char* str,  CListBox& stList, std::vector<PhotoData>& vec);  //添加图片资源
//
public:
	// 数据操作
	BOOL SaveData();
	//初始化数据
	BOOL InitDonationData(activity_mgr::donation_event_config_t* sp);
	BOOL RemoveDonation(activity_mgr::donation_event_config_t* sp);
	// 读取整个图片资源
	BOOL InitPhotoList(); 
	//添加新的文件
	BOOL AddImageAndDes(const char* str);
	//删除文件
	BOOL DelImageAndDes(int index); // 删除一个图片资源和文字描述
	BOOL UpdataImageDes(int index); //
	BOOL ShowLoadJPG(const char* str, BOOL  bFromResList = TRUE);
protected:
	BOOL LoadJPG(const char* str, LPPICTURE *lppi);
	LPPICTURE m_lppi;//加载图像文件的流
	BOOL m_bHadLoad;//已经加载了背景图像

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()

public:
	
	int m_InitPhtoMax ;          //获得的当前的图片的数量。 
	int m_EnventId;
	CString m_EventTitle ;

	BOOL m_bNewEvent ;					//标示是不是新创建的（没保存之前都算新创建的）
	CListBox m_CurPhotoList;			//当前事件的图片的LIST 
	std::vector<PhotoData> m_Event_photo; //当前事件的图片  

	CString m_Event_weblink;            //当前事件的URL
	CString m_CurPhotoDes;              //事件描述编辑器文字


	std::vector<PhotoData> m_NumPhoto;    //整个目录的图片资源
	CListBox m_PhotoList;               //整个目录图片资源的LIST
	
	std::string m_CurFilePath ;         //整个事件管理器的资源完整目录
	
	CPicture m_Picture ;                //用于显示当前图片
	BOOL m_bLoadAllPhoto ;						//是否已经加载整个图片资源



	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton4();
	afx_msg void OnLbnSelchangeList1(); // 显示整理图片资源库德单个资源
	afx_msg void OnLbnSelchangeList2();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnBnClickedButton2();  //更新说明按钮
	//afx_msg void OnPaint();
	afx_msg void OnPaint();
	afx_msg void OnCbnSelchangeCombo2();
};
