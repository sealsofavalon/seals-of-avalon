// ExpDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "activity_tool.h"
#include "ExpDlg.h"


// CExpDlg 对话框

IMPLEMENT_DYNAMIC(CExpDlg, CDialog)

CExpDlg::CExpDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CExpDlg::IDD, pParent)
	, m_rate(1.f)
{

}

CExpDlg::~CExpDlg()
{
}

void CExpDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBO2, m_comboType);
	DDX_Text(pDX, IDC_EDIT2, m_rate);
}


BEGIN_MESSAGE_MAP(CExpDlg, CDialog)
END_MESSAGE_MAP()

