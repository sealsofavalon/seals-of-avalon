#pragma once
#include "afxwin.h"


// CExpDlg 对话框

class CExpDlg : public CDialog
{
	DECLARE_DYNAMIC(CExpDlg)

public:
	CExpDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CExpDlg();

// 对话框数据
	enum { IDD = IDD_DIALOG_ACT_EXP };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	CComboBox m_comboType;
	float m_rate;
};
