// ShopDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "activity_tool.h"
#include "ShopDlg.h"




// CShopDlg 消息处理程序
// Shopdlg.cpp : 实现文件
//

#include "stdafx.h"
#include "activity_tool.h"
#include "Shopdlg.h"


// CShopdlg 对话框

IMPLEMENT_DYNAMIC(CShopdlg, CDialog)

CShopdlg::CShopdlg(CWnd* pParent /*=NULL*/)
	: CDialog(CShopdlg::IDD, pParent)
{

}

CShopdlg::~CShopdlg()
{
}

void CShopdlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX,IDC_COMBO_CATEGORY ,m_ShopItem_Category);
	DDX_Control(pDX,IDC_COMBO_DISCOUNT ,m_DisCount);
}


BEGIN_MESSAGE_MAP(CShopdlg, CDialog)
END_MESSAGE_MAP()


// CShopdlg 消息处理程序
