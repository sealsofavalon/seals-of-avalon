#pragma once


// CShopdlg 对话框

class CShopdlg : public CDialog
{
	DECLARE_DYNAMIC(CShopdlg)

public:
	CShopdlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CShopdlg();

// 对话框数据
	enum { IDD = IDD_DIALOG_SHOP };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()

public:
	CComboBox m_ShopItem_Category;
	CComboBox m_DisCount ;
};
