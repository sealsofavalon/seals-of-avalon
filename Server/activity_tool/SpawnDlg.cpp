// SpawnDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "activity_tool.h"
#include "SpawnDlg.h"


// CSpawnDlg 对话框

IMPLEMENT_DYNAMIC(CSpawnDlg, CDialog)

CSpawnDlg::CSpawnDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSpawnDlg::IDD, pParent)
	, m_checkRespawn(FALSE)
	, m_mapid(0)
	, m_spawnid(0)
	, m_checkIsCreature(FALSE)
{

}

CSpawnDlg::~CSpawnDlg()
{
}

void CSpawnDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Check(pDX, IDC_CHECK1, m_checkRespawn);
	DDX_Text(pDX, IDC_EDIT5, m_mapid);
	DDX_Text(pDX, IDC_EDIT2, m_spawnid);
	DDX_Check(pDX, IDC_CHECK_IS_CREATURE, m_checkIsCreature);
}


BEGIN_MESSAGE_MAP(CSpawnDlg, CDialog)
END_MESSAGE_MAP()


// CSpawnDlg 消息处理程序

BOOL CSpawnDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	return TRUE;  // return TRUE unless you set the focus to a control
}

void CSpawnDlg::OnBnClickedButtonAdd()
{
	// TODO: 在此添加控件通知处理程序代码
}

void CSpawnDlg::OnBnClickedButtonRemove()
{
	// TODO: 在此添加控件通知处理程序代码
}
