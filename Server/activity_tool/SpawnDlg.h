#pragma once
#include "afxcmn.h"


// CSpawnDlg 对话框

class CSpawnDlg : public CDialog
{
	DECLARE_DYNAMIC(CSpawnDlg)

public:
	CSpawnDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CSpawnDlg();

// 对话框数据
	enum { IDD = IDD_DIALOG_ACT_SPAWN };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	BOOL m_checkRespawn;
	int m_mapid;
	UINT m_spawnid;
	BOOL m_checkIsCreature;
	afx_msg void OnBnClickedButtonAdd();
	afx_msg void OnBnClickedButtonRemove();
};
