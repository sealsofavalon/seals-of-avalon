// SrvshutdownDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "activity_tool.h"
#include "SrvshutdownDlg.h"


// CSrvshutdownDlg 对话框

IMPLEMENT_DYNAMIC(CSrvshutdownDlg, CDialog)

CSrvshutdownDlg::CSrvshutdownDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSrvshutdownDlg::IDD, pParent)
	, m_seconds(0)
{

}

CSrvshutdownDlg::~CSrvshutdownDlg()
{
}

void CSrvshutdownDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, m_seconds);
}


BEGIN_MESSAGE_MAP(CSrvshutdownDlg, CDialog)
END_MESSAGE_MAP()


// CSrvshutdownDlg 消息处理程序
