#pragma once


// CSrvshutdownDlg 对话框

class CSrvshutdownDlg : public CDialog
{
	DECLARE_DYNAMIC(CSrvshutdownDlg)

public:
	CSrvshutdownDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CSrvshutdownDlg();

// 对话框数据
	enum { IDD = IDD_DIALOG_ACT_SRVSHUTDOWN };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	UINT m_seconds;
};
