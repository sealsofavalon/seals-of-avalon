// TreasureDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "activity_tool.h"
#include "TreasureDlg.h"


// CTreasureDlg 对话框

IMPLEMENT_DYNAMIC(CTreasureDlg, CDialog)

CTreasureDlg::CTreasureDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CTreasureDlg::IDD, pParent)
{
	m_TreasureCount = 0;
	m_TreasurePosStart = 0;
	m_TreasurePosEnd = 0;
	m_TreasureID = 0;
	m_TreasureTime = 0;
	m_TreasureItemCount = 1;
	m_Treasurescale = 1.0f;
}

string str1 = "原地刷新:刷新次数没限制，刷新时间按照游戏编辑器里填写时间,固定位置刷新。";
string str2 = "随即刷新:完全按照本编辑器里填写的刷新时间和刷新数量随机刷新位置。";

CTreasureDlg::~CTreasureDlg()
{
}

BOOL CTreasureDlg::OnInitDialog()
{
	if (!CDialog::OnInitDialog())
	{
		return  FALSE ;
	}

	m_comboType.InsertString(0, "原地刷新");
	m_comboType.InsertString(1, "随即刷新");
	
	m_comboType.SetCurSel(0);
	
	SetDlgItemText(IDC_STATIC_SHOW, str1.c_str());

	CWnd* pkWnd = GetDlgItem(IDC_EDIT_ITEMCOUNT); //刷新次数
	CWnd* pkStac1 = GetDlgItem(IDC_STATIC_1);
	CWnd* pkStac2 = GetDlgItem(IDC_STATIC_2);
	CWnd* pkStac3 = GetDlgItem(IDC_STATIC_3);
	CWnd* pkStac6 = GetDlgItem(IDC_STATIC_TIPS);
	CWnd* pkWnd2 = GetDlgItem(IDC_EDIT_TIME);//刷新数量

	SetDlgItemText(IDC_EDIT_ITEMCOUNT,"0");
	pkWnd->ShowWindow(SW_HIDE);
	SetDlgItemText(IDC_EDIT_TIME, "0");
	pkWnd2->ShowWindow(SW_HIDE);
	pkStac1->ShowWindow(SW_HIDE);
	pkStac2->ShowWindow(SW_HIDE);
	pkStac3->ShowWindow(SW_HIDE);
	pkStac6->ShowWindow(SW_HIDE);

	return TRUE;
}
void CTreasureDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Text(pDX, IDC_EDIT_COUNT, m_TreasureCount);
	DDX_Text(pDX, IDC_EDIT_POSSTART, m_TreasurePosStart);
	DDX_Text(pDX, IDC_EDIT_POSEND,m_TreasurePosEnd);
	DDX_Text(pDX, IDC_EDIT_ZUJIANID, m_TreasureID);

	DDX_Text(pDX, IDC_EDIT_TIME, m_TreasureTime);
	DDX_Text(pDX, IDC_EDIT_ITEMCOUNT, m_TreasureItemCount);
	DDX_Text(pDX, IDC_EDIT_SCALE, m_Treasurescale);
	
	DDX_Control(pDX, IDC_COMBO_TYPE, m_comboType);
	
}





// CTreasureDlg 消息处理程序


BEGIN_MESSAGE_MAP(CTreasureDlg, CDialog)
	ON_CBN_SELCHANGE(IDC_COMBO_TYPE, &CTreasureDlg::OnCbnSelchangeComboType)
END_MESSAGE_MAP()

void CTreasureDlg::OnCbnSelchangeComboType()
{
	int index = m_comboType.GetCurSel();
	CWnd* pkWnd = GetDlgItem(IDC_EDIT_ITEMCOUNT); //刷新次数
	CWnd* pkStac1 = GetDlgItem(IDC_STATIC_1);
	CWnd* pkStac2 = GetDlgItem(IDC_STATIC_2);
	CWnd* pkStac3 = GetDlgItem(IDC_STATIC_3);
	CWnd* pkStac6 = GetDlgItem(IDC_STATIC_TIPS);

	CWnd* pkWnd2 = GetDlgItem(IDC_EDIT_TIME);//刷新数量

	if (index == 0)
	{
		//原地刷新
		
		SetDlgItemText(IDC_EDIT_ITEMCOUNT,"0");
		pkWnd->ShowWindow(SW_HIDE);
		SetDlgItemText(IDC_EDIT_TIME, "0");
		pkWnd2->ShowWindow(SW_HIDE);
		pkStac1->ShowWindow(SW_HIDE);
		pkStac2->ShowWindow(SW_HIDE);
		pkStac3->ShowWindow(SW_HIDE);
		pkStac6->ShowWindow(SW_HIDE);
		SetDlgItemText(IDC_STATIC_SHOW, str1.c_str());
	}
	if (index == 1)
	{
		//随即刷新
		
		//SetDlgItemText(IDC_EDIT_ITEMCOUNT,m_TreasureItemCount);
		pkWnd->ShowWindow(SW_SHOW);
	//	SetDlgItemText(IDC_EDIT_TIME, m_TreasureTime);
		pkWnd2->ShowWindow(SW_SHOW);
		
		pkStac1->ShowWindow(SW_SHOW);
		pkStac2->ShowWindow(SW_SHOW);
		pkStac3->ShowWindow(SW_SHOW);
		pkStac6->ShowWindow(SW_SHOW);

		SetDlgItemText(IDC_STATIC_SHOW, str2.c_str());
	}
	// TODO: 在此添加控件通知处理程序代码
}
