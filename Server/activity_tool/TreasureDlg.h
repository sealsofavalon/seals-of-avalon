#pragma once
#include "afxwin.h"

// CTreasureDlg 对话框

class CTreasureDlg : public CDialog
{
	DECLARE_DYNAMIC(CTreasureDlg)

public:
	CTreasureDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CTreasureDlg();

// 对话框数据
	enum { IDD = IDD_DIALOG_ACT_TREASURE };
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持
	// 生成的消息映射函数
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()
public:
	int m_TreasureCount;
	int m_TreasurePosStart;
	int m_TreasurePosEnd;
	int m_TreasureID;
	int m_TreasureTime;
	int m_TreasureItemCount;
	CComboBox m_comboType;
	float m_Treasurescale;
	afx_msg void OnCbnSelchangeComboType();
};
