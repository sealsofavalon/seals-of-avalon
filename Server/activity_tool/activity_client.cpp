#include "StdAfx.h"
#include "activity_client.h"
#include "activity_toolDlg.h"

activity_client* g_activity_client = NULL;

activity_client::activity_client() : m_lastpingtime( 0 ), m_servertime( 0 ), m_maxid( 1 )
{
	static zip_compress_strategy zip;
	net_global::init_net_service( 2, 30, &zip, true, 100 );

	m_listener = new listener;
	m_network = new net;
}

activity_client::~activity_client()
{
	net_global::free_net_service();

	delete m_listener;
	delete m_network;
}
bool activity_client::listener::on_upate(activity_mgr::activity_base_t* p)
{
	return true;
}
bool activity_client::listener::on_add_activity( activity_mgr::activity_base_t* p, bool fromdb )
{
	char itemtext[1024];
	sprintf( itemtext, "[%u]-(%s)", p->id, UTF8ToAnis(p->comment).c_str() );
	p->attach = (uint32)g_maindlg->m_treeAct.InsertItem( itemtext, g_maindlg->m_hTreeRoot[p->category] );
	g_maindlg->OnRefreshActivity( p );
	g_maindlg->m_treeAct.SetItemData( (HTREEITEM)p->attach, (DWORD_PTR)p );
	g_maindlg->m_treeAct.SelectSetFirstVisible( (HTREEITEM)p->attach );
	return true;
}

bool activity_client::listener::on_remove_activity( activity_mgr::activity_base_t* p )
{
	if( p->attach )
	{
		if (p->category == activity_mgr::ACTIVITY_CATEGORY_DONATION_EVENT )
		{
			activity_mgr::donation_event_config_t* evt = static_cast<activity_mgr::donation_event_config_t*>( p );
			g_maindlg->m_dlgDonation.RemoveDonation(evt);
		}

		g_maindlg->m_treeAct.DeleteItem( (HTREEITEM)p->attach );
		return true;
	}
	else
		return false;
}

void activity_client::listener::on_duty_change( activity_mgr::activity_base_t* p )
{
	if( p && p->attach )
	{
		g_maindlg->m_treeAct.SetCheck( (HTREEITEM)p->attach, p->on_duty );
		g_maindlg->m_on_duty = p->on_duty;
		g_maindlg->UpdateData( false );
	}
}

void activity_client::listener::on_upate()
{
	g_activity_client->m_unixtime = (uint32)time( NULL );
	g_activity_client->m_network->run_no_wait();
	if( g_activity_client->m_unixtime - g_activity_client->m_lastpingtime >= 5 )
	{
		MSG_C2S::stPing ping;
		g_activity_client->m_network->send_packet( ping );
		g_activity_client->m_lastpingtime = g_activity_client->m_unixtime;
	}
}

void activity_client::listener::on_duty_update( activity_mgr::activity_base_t* p )
{
	
}

//////////////////////////////////////////////////////////////////////////

activity_client::net::net() : tcp_client( *net_global::get_io_service() )
{

}

activity_client::net::~net()
{

}

void activity_client::net::on_close( const boost::system::error_code& error )
{
	tcp_client::on_close( error );

	AfxMessageBox( "disconnected from server, click ok to quit this program!" );
	g_maindlg->EndDialog(0);
}

void activity_client::net::on_connect()
{
	tcp_client::on_connect();
}

void activity_client::net::on_connect_failed( boost::system::error_code error )
{
}

void activity_client::net::proc_message( const message_t& msg )
{
	CPacketUsn pakTool( (char*)msg.data, msg.len );
	switch( pakTool.GetProNo() )
	{
	case MSG_S2C::MSG_ACT_TOOL_ACCESS_ACK:
		{
			MSG_S2C::stActToolAccessAck ack;
			pakTool >> ack;
			g_maindlg->KillTimer( 1 );
			if( ack.pass )
			{
				//MessageBox( ::GetActiveWindow(), "Login Succeed!", "", MB_OK );
				g_maindlg->EnableWindow( true );
				char titleinfo[128];
				int y, m, d, h, min, s;
				convert_unix_time( ack.servertime, &y, &m, &d, &h, &min, &s );
				sprintf( titleinfo, "%02d-%02d-%02d %02d:%02d:%02d", y, m, d, h, min, s );
				g_maindlg->SetWindowText( titleinfo );

				g_activity_client->m_servertime = ack.servertime;
				g_maindlg->SetTimer( 2, 1000, NULL );
				MSG_C2S::stQueryActivityList msg1;
				send_packet( msg1 );
			}
			else
			{
				//MessageBox( ::GetActiveWindow(), "Login Failed!", "Warning!", MB_OK );
				g_maindlg->EndDialog( 0 );
				return;
			}
		}
		break;
	case MSG_S2C::MSG_ACTIVITY_LIST_ACK:
		{
			MSG_S2C::stActivityListAck ack;
			pakTool >> ack;
		}
		break;
	case MSG_S2C::MSG_UPDATE_ACTIVITY_ACK:
		{
			MSG_S2C::stUpdateActivityAck ack;
			pakTool >> ack;
		}
		break;
	case MSG_S2C::MSG_REMOVE_ACTIVITY_ACK:
		{
			MSG_S2C::stRemoveActivityAck ack;
			pakTool >> ack;
		}
		break;
	case MSG_S2C::MSG_DUTY_CHANGE_NOTIFY:
		{
			MSG_S2C::stDutyChangeNotify notify;
			pakTool >> notify;
		}
		break;
	default:
		break;
	}
}

void activity_client::net::send_packet( const PakHead& pak )
{
	CPacketSn cps;
	cps << pak;
	cps.SetProLen();
	send_message( cps.GetBuf(), cps.GetSize() );
}
