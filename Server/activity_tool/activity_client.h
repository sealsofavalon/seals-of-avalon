#pragma once

class activity_client
{
public:
	activity_client();
	~activity_client();

	class listener : public activity_mgr::activity_listener_base
	{
	public:
		virtual bool on_add_activity( activity_mgr::activity_base_t* p ,bool fromdb = false);
		virtual bool on_remove_activity( activity_mgr::activity_base_t* p );
		virtual void on_duty_change( activity_mgr::activity_base_t* p );
		virtual bool on_upate(activity_mgr::activity_base_t* p);
		virtual void on_upate();
		virtual void on_duty_update( activity_mgr::activity_base_t* p );
	};

	class net : public tcp_client
	{
	public:
		net();
		~net();

	public:
		virtual void on_close( const boost::system::error_code& error );
		virtual void on_connect();
		virtual void on_connect_failed( boost::system::error_code error );
		virtual void proc_message( const message_t& msg );
		void send_packet( const PakHead& pak );
	};

	listener* m_listener;
	net* m_network;
	uint32 m_lastpingtime;
	uint32 m_unixtime;
	uint32 m_maxid;
	uint32 m_servertime;
};

extern activity_client* g_activity_client;
