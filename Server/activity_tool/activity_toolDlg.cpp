// activity_toolDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "activity_tool.h"
#include "activity_toolDlg.h"
#include "activity_client.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

Cactivity_toolDlg* g_maindlg;

// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// Cactivity_toolDlg 对话框


std::string UTF8ToAnis(const std::string& str)
{
	char acUnicode[4096];
	char acAnsi[2048];

	unsigned short usLen;

	//UTF8 转换为 Unicode
	usLen = (unsigned short)MultiByteToWideChar(CP_UTF8, 0, str.c_str(), str.size(), (wchar_t*)acUnicode, sizeof(acUnicode)/2);

	//Unicode 转换为 Ansi
	usLen = (unsigned short)WideCharToMultiByte(936, 0, (wchar_t*)acUnicode, usLen, acAnsi, sizeof(acAnsi), NULL, false);

	return std::string(acAnsi, usLen);
}

std::string AnisToUTF8(const std::string& str)
{
	char acUnicode[4096];
	char acUTF8[4096];

	//Ansi 转换为 Unicode
	ui16 usLen = (unsigned short)MultiByteToWideChar(936, 0, str.c_str(), str.size(), (LPWSTR)acUnicode, sizeof(acUnicode)/2);

	//Unicode 转换为 Utf8
	usLen = (unsigned short)WideCharToMultiByte(CP_UTF8, 0, (LPCWSTR)acUnicode, usLen, acUTF8, sizeof(acUTF8), NULL, false);

	return std::string(acUTF8, usLen);
}



Cactivity_toolDlg::Cactivity_toolDlg(CWnd* pParent /*=NULL*/)
	: CDialog(Cactivity_toolDlg::IDD, pParent)
	, m_actid(0)
	, m_dtFrom(0)
	, m_dtTo(0)
	, m_checkSunday(FALSE)
	, m_checkMonday(FALSE)
	, m_checkTuesday(FALSE)
	, m_checkWednesday(FALSE)
	, m_checkThursday(FALSE)
	, m_checkFriday(FALSE)
	, m_checkSaturday(FALSE)
//	, m_strDescription(_T("input the system notify words of this activity on duty here..."))
	, m_on_duty(FALSE)
//	, m_strComment(_T("input the comment of this activity here..."))
//	, m_strDescription2(_T("input the system notify words of this activity off duty here..."))
{
	g_maindlg = this;
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void Cactivity_toolDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TREE1, m_treeAct);
	DDX_Control(pDX, IDC_EDIT1, m_edtID);
	DDX_Text(pDX, IDC_EDIT1, m_actid);
	DDX_Control(pDX, IDC_COMBO1, m_comboCategory);
	DDX_DateTimeCtrl(pDX, IDC_DATETIMEPICKER1, m_dtFrom);
	DDX_DateTimeCtrl(pDX, IDC_DATETIMEPICKER2, m_dtTo);
	DDX_Control(pDX, IDC_COMBO2, m_comboFrom);
	DDX_Control(pDX, IDC_COMBO3, m_comboTo);
	DDX_Control(pDX, IDC_COMBO4, m_BroadTime);
	DDX_Check(pDX, IDC_CHECK6, m_checkSunday);
	DDX_Check(pDX, IDC_CHECK1, m_checkMonday);
	DDX_Check(pDX, IDC_CHECK2, m_checkTuesday);
	DDX_Check(pDX, IDC_CHECK3, m_checkWednesday);
	DDX_Check(pDX, IDC_CHECK4, m_checkThursday);
	DDX_Check(pDX, IDC_CHECK7, m_checkFriday);
	DDX_Check(pDX, IDC_CHECK5, m_checkSaturday);
	DDX_Text(pDX, IDC_EDIT3, m_strDescription);
	DDX_Control(pDX, IDC_DATETIMEPICKER1, m_fromDateTimeCtrl);
	DDX_Control(pDX, IDC_DATETIMEPICKER2, m_toDateTimeCtrl);
	DDX_Check(pDX, IDC_CHECK9, m_on_duty);
	DDX_Text(pDX, IDC_EDIT4, m_strComment);
	DDX_Text(pDX, IDC_EDIT6, m_strDescription2);
}

BEGIN_MESSAGE_MAP(Cactivity_toolDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
//	ON_NOTIFY(TCN_SELCHANGE, IDC_TAB1, &Cactivity_toolDlg::OnTcnSelchangeTab1)
//ON_NOTIFY(TCN_SELCHANGE, IDC_TAB1, &Cactivity_toolDlg::OnTcnSelchangeTab1)
ON_CBN_SELCHANGE(IDC_COMBO1, &Cactivity_toolDlg::OnCbnSelchangeCombo1)
ON_BN_CLICKED(IDC_BUTTON1, &Cactivity_toolDlg::OnBnClickedButton1)
ON_BN_CLICKED(IDC_BUTTON2, &Cactivity_toolDlg::OnBnClickedButton2)
ON_NOTIFY(TVN_SELCHANGED, IDC_TREE1, &Cactivity_toolDlg::OnTvnSelchangedTree1)
ON_WM_TIMER()
ON_BN_CLICKED(IDC_BUTTON3, &Cactivity_toolDlg::OnBnClickedButton3)
END_MESSAGE_MAP()


// Cactivity_toolDlg 消息处理程序

BOOL Cactivity_toolDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	g_activity_client = new activity_client;
	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	uint32 test = make_unix_time( 2010, 8, 26, 15, 12, 50 );
	time_t test2 = 1282854181;//time(NULL);
	tm* pp = localtime( &test2 );
	uint32 test3 = (uint32)mktime( pp );

	m_dlgExp.Create(IDD_DIALOG_ACT_EXP, this); 
	m_aDlgs[0] = &m_dlgExp;
	m_dlgSpawn.Create( IDD_DIALOG_ACT_SPAWN, this );
	m_aDlgs[1] = &m_dlgSpawn;
	m_dlgBless.Create(IDD_DIALOG_ACT_BLESS, this);
	m_aDlgs[2] = &m_dlgBless;
	m_dlgSrvshutdown.Create( IDD_DIALOG_ACT_SRVSHUTDOWN, this );
	m_aDlgs[3] = &m_dlgSrvshutdown;

	m_dlgDonation.Create( IDD_DIALOG_ACT_DONATION, this );
	m_aDlgs[4] = &m_dlgDonation;

	m_dlgTreasure.Create( IDD_DIALOG_ACT_TREASURE, this );
	m_aDlgs[5]= &m_dlgTreasure;

	m_dlgShop.Create(IDD_DIALOG_SHOP, this);
	m_aDlgs[6] = &m_dlgShop;

// 	m_comboCategory.InsertString( 0, "" );
// 	m_comboCategory.InsertString( 1, "ACTIVITY_CATEGORY_SPAWN" );
// 	m_comboCategory.InsertString( 2, "ACTIVITY_CATEGORY_BLESS" );
// 	m_comboCategory.InsertString( 3, "ACTIVITY_CATEGORY_SRVSHUTDOWN" );

	m_comboCategory.SetCurSel( 0 );

	CRect rs; 
	GetClientRect(rs); 
	rs.top+=263; 
	rs.bottom-=15; 
	rs.left+=280; 
	rs.right-=17; 

	for( int i = 0; i < activity_mgr::ACTIVITY_CATEGORY_MAX; ++i )
	{
		m_aDlgs[i]->MoveWindow(rs); 
		const char* category_txt = g_activity_mgr->get_category_text( (activity_mgr::activity_category)i );
		m_comboCategory.InsertString( i, category_txt );
		m_hTreeRoot[i] = m_treeAct.InsertItem( category_txt, 0, 1, TVI_ROOT );
	}

	m_comboTo.InsertString( 0, "N/A" );
	m_comboFrom.InsertString( 0, "N/A" );
	for( int i = 1; i <= 24; ++i )
	{
		m_comboFrom.InsertString( i, (std::string( convert2string( i ) ) + " : 00").c_str() );
		m_comboTo.InsertString( i, (std::string( convert2string( i ) ) + " : 00").c_str() );
	}

	m_BroadTime.InsertString(0, "10m");
	m_BroadTime.InsertString(1, "30m");
	m_BroadTime.InsertString(2, "1h");
	m_BroadTime.InsertString(3, "3h");
	m_BroadTime.InsertString(4, "5h");
	m_BroadTime.InsertString(5, "7h");
	m_BroadTime.InsertString(6, "12h");

	m_comboFrom.SetCurSel( 0 );
	m_comboTo.SetCurSel( 0 );
	m_comboTo.SetCurSel( 0 );

	m_fromDateTimeCtrl.SetFormat("yyyy-MM-dd   HH:mm:ss");
	m_toDateTimeCtrl.SetFormat("yyyy-MM-dd   HH:mm:ss");
	m_dtFrom = CTime::GetCurrentTime();
	m_dtTo = CTime::GetCurrentTime();
	m_fromDateTimeCtrl.SetTime( &m_dtFrom );
	m_toDateTimeCtrl.SetTime( &m_dtTo );

	m_dlgExp.m_comboType.InsertString( 0, "GIVE_EXP_TYPE_KILL_MONSTER" );
	m_dlgExp.m_comboType.InsertString( 1, "GIVE_EXP_TYPE_QUEST" );
	m_dlgExp.m_comboType.InsertString( 2, "GIVE_EXP_TYPE_DYNAMIC_INSTANCE" );
	m_dlgExp.m_comboType.InsertString( 3, "GIVE_EXP_TYPE_GIFT" );
	m_dlgExp.m_comboType.InsertString( 4, "GIVE_EXP_TYPE_ACTIVITY" );
	m_dlgExp.m_comboType.SetCurSel( 0 );

	m_dlgShop.m_ShopItem_Category.InsertString(0, "类型");
	m_dlgShop.m_ShopItem_Category.InsertString(1, "宝石");
	m_dlgShop.m_ShopItem_Category.InsertString(2, "坐骑");
	m_dlgShop.m_ShopItem_Category.InsertString(3, "变身");
	m_dlgShop.m_ShopItem_Category.InsertString(4, "时装");
	m_dlgShop.m_ShopItem_Category.InsertString(5, "职业");
	m_dlgShop.m_ShopItem_Category.InsertString(6, "药剂");
	m_dlgShop.m_ShopItem_Category.InsertString(7, "宠物");
	m_dlgShop.m_ShopItem_Category.InsertString(8, "材料");
	m_dlgShop.m_ShopItem_Category.InsertString(9, "配方");
	m_dlgShop.m_ShopItem_Category.InsertString(10, "其他");
	m_dlgShop.m_ShopItem_Category.InsertString(11, "工会");
	m_dlgShop.m_ShopItem_Category.SetCurSel( 0 );

	m_dlgShop.m_DisCount.InsertString(0,"0.5折");
	m_dlgShop.m_DisCount.InsertString(1,"1折");
	m_dlgShop.m_DisCount.InsertString(2,"1.5折");
	m_dlgShop.m_DisCount.InsertString(3,"2折");
	m_dlgShop.m_DisCount.InsertString(4,"2.5折");
	m_dlgShop.m_DisCount.InsertString(5,"3折");
	m_dlgShop.m_DisCount.InsertString(6,"3.5折");
	m_dlgShop.m_DisCount.InsertString(7,"4折");
	m_dlgShop.m_DisCount.InsertString(8,"4.5折");
	m_dlgShop.m_DisCount.InsertString(9,"5折");
	m_dlgShop.m_DisCount.InsertString(10,"5.5折");
	m_dlgShop.m_DisCount.InsertString(11,"6折");
	m_dlgShop.m_DisCount.InsertString(12,"6.5折");
	m_dlgShop.m_DisCount.InsertString(13,"7折");
	m_dlgShop.m_DisCount.InsertString(14,"7.5折");
	m_dlgShop.m_DisCount.InsertString(15,"8折");
	m_dlgShop.m_DisCount.InsertString(16,"8.5折");
	m_dlgShop.m_DisCount.InsertString(17,"9折");
	m_dlgShop.m_DisCount.InsertString(18,"9.5折");
	
	m_dlgShop.m_DisCount.SetCurSel(18);
	New();
	g_activity_mgr->m_listener = g_activity_client->m_listener;

	FILE* fp = fopen( "activity_tool.conf", "r" );
	if( !fp )
	{
		AfxMessageBox( "cannot open activity_tool.conf!" );
		CDialog::OnOK();
		return TRUE;
	}
	char address[64];
	uint32 port;
	fscanf( fp, "IP: %s\n", address );
	fscanf( fp, "Port: %u\n", &port );
	fclose( fp );
	if( !g_activity_client->m_network->wait_connect( address, port ) )
	{
		CString message;
		message.Format( "cannot connect to address:%s port:%u", address, port );
		AfxMessageBox( message );
		CDialog::OnOK();
		return TRUE;
	}

	MSG_C2S::stActToolAccessReq req;
	req.user = "c++";
	req.password = "boost";
	g_activity_client->m_network->send_packet( req );

	this->SetTimer( 0, 20, NULL );
	this->SetTimer( 1, 15 * 1000, NULL );
	
	SetWindowText( "waiting for login..." );
	EnableWindow( false );
	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void Cactivity_toolDlg::OnRefreshActivity( activity_mgr::activity_base_t* act )
{
	m_actid = act->id;
	if( g_activity_client->m_maxid <= m_actid )
		g_activity_client->m_maxid = m_actid + 1;

	m_dtFrom = act->starttime;
	m_dtTo = act->expire;
//	displayfromunixtime( act->starttime, m_fromDateTimeCtrl );
//	displayfromunixtime( act->expire, m_toDateTimeCtrl );
	m_comboFrom.SetCurSel( act->starthour );
	m_comboTo.SetCurSel( act->expirehour );

	int index = 0;
	switch(act->next_broadcast_interval)
	{
	case BROADCAST_0:
		index = 0;
		break;
	case BROADCAST_1:
		index = 1;
		break;
	case BROADCAST_2:
		index = 2;
		break;
	case BROADCAST_3:
		index = 3;
		break;
	case BROADCAST_4:
		index = 4;
		break;
	case BROADCAST_5:
		index = 5;
		break;
	case BROADCAST_6:
		index = 6;
		break;
	default:
		index = 0;
	}
	m_BroadTime.SetCurSel(index);
	m_comboCategory.SetCurSel( act->category );
	m_comboCategory.EnableWindow(FALSE);
	m_checkSunday = act->repeat_dayofweek[0];
	m_checkMonday = act->repeat_dayofweek[1];
	m_checkTuesday = act->repeat_dayofweek[2];
	m_checkWednesday = act->repeat_dayofweek[3];
	m_checkThursday = act->repeat_dayofweek[4];
	m_checkFriday = act->repeat_dayofweek[5];
	m_checkSaturday = act->repeat_dayofweek[6];
	m_strDescription = UTF8ToAnis(act->description[1].c_str()).c_str();
	m_strDescription2 = UTF8ToAnis(act->description[0].c_str()).c_str();
	m_strComment = UTF8ToAnis(act->comment.c_str()).c_str();
	m_on_duty = act->on_duty;
	UpdateData( false );
	ShowDlg( (activity_mgr::activity_category)act->category );
	switch( act->category )
	{
	case activity_mgr::ACTIVITY_CATEGORY_EXP:
		{
			activity_mgr::exp_config_t* ep = static_cast<activity_mgr::exp_config_t*>( act );
			m_dlgExp.m_comboType.SetCurSel( ep->type );
			m_dlgExp.m_rate = ep->rate;
			m_dlgExp.UpdateData( false );
		}
		break;
	case activity_mgr::ACTIVITY_CATEGORY_SPAWN:
		{
			activity_mgr::spawn_config_t* sp = static_cast<activity_mgr::spawn_config_t*>( act );
			m_dlgSpawn.m_spawnid = sp->spawnid;
			m_dlgSpawn.m_checkRespawn = sp->respawn;
			m_dlgSpawn.m_mapid = sp->mapid;
			m_dlgSpawn.m_checkIsCreature = sp->is_creature;
			m_dlgSpawn.UpdateData( false );
		}
		break;
	case activity_mgr::ACTIVITY_CATEGORY_BLESS:
		{

		}
		break;
	case activity_mgr::ACTIVITY_CATEGORY_SRVSHUTDOWN:
		{
			activity_mgr::srvshutdown_config_t* sp = static_cast<activity_mgr::srvshutdown_config_t*>( act );
			m_dlgSrvshutdown.m_seconds = sp->seconds;
			m_dlgSrvshutdown.UpdateData( false );
		}
		break;
	case activity_mgr::ACTIVITY_CATEGORY_DONATION_EVENT:
		{
			activity_mgr::donation_event_config_t* sp = static_cast<activity_mgr::donation_event_config_t*>( act);
			m_dlgDonation.InitDonationData(sp);
		}
		break;
	case activity_mgr::ACTIVITY_CATEGORY_TREASURE:
		{
			activity_mgr::treasure_event_config_t*  sp = static_cast<activity_mgr::treasure_event_config_t*>( act);
			m_dlgTreasure.m_TreasureCount = sp->treasurecount;
			m_dlgTreasure.m_TreasureID = sp->treasureid;
			m_dlgTreasure.m_TreasurePosStart = sp->treasureposstart;
			m_dlgTreasure.m_TreasurePosEnd = sp->treasureposend;
			m_dlgTreasure.m_TreasureTime = sp->refurbishtime;
			m_dlgTreasure.m_TreasureItemCount = sp->treasureitemcount;
			m_dlgTreasure.m_Treasurescale = sp->treasurescale * 10;
			m_dlgTreasure.m_comboType.SetCurSel(sp->lockitemtype);

			m_dlgTreasure.OnCbnSelchangeComboType();
			m_dlgTreasure.UpdateData( false );
		}
		break;
	case activity_mgr::ACTIVITY_CATEGORY_SHOP:
		{
			activity_mgr::shop_event_config_t* sp = static_cast<activity_mgr::shop_event_config_t*>( act);
			if (sp->shop_item_category <= 10)
			{
				m_dlgShop.m_ShopItem_Category.SetCurSel(sp->shop_item_category);
			}
			int index = sp->discount / 5;
			if (index >= 1  && index <= 19)
			{
				m_dlgShop.m_DisCount.SetCurSel(index - 1);
			}
		}
		break ;
	default:
		assert( 0 );
		return;
	}
}

void Cactivity_toolDlg::ShowDlg( activity_mgr::activity_category category )
{
	for( int i = 0; i < activity_mgr::ACTIVITY_CATEGORY_MAX; ++i )
	{
		if( i != category )
		{
			m_aDlgs[i]->ShowWindow( SW_HIDE );
			m_aDlgs[i]->EnableWindow( 0 );
		}
	}
	m_aDlgs[category]->ShowWindow( SW_SHOW );
	m_aDlgs[category]->EnableWindow( 1 );
}

void Cactivity_toolDlg::New()
{
	m_actid = g_activity_client->m_maxid;
	m_dtFrom = time( NULL );
	m_dtTo = time( NULL );
	m_comboCategory.SetCurSel( 0 );
	m_comboCategory.EnableWindow(TRUE);
	m_comboFrom.SetCurSel( 0 );
	m_comboTo.SetCurSel( 0 );
	m_BroadTime.SetCurSel( 0 );
	m_checkSunday = false;
	m_checkMonday = false;
	m_checkTuesday = false;
	m_checkWednesday = false;
	m_checkThursday = false;
	m_checkFriday = false;
	m_checkSaturday = false;
	m_strDescription = "";//"input the description of this activity here...";
	m_strDescription2 = "";
	m_strComment = "";//"input the comment of this activity here...";
	ShowDlg( (activity_mgr::activity_category)0 );
	UpdateData( false );

	m_dlgDonation.InitDonationData(NULL);
}

void Cactivity_toolDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void Cactivity_toolDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR Cactivity_toolDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

// category
void Cactivity_toolDlg::OnCbnSelchangeCombo1()
{
	activity_mgr::activity_category category = (activity_mgr::activity_category)m_comboCategory.GetCurSel();
	ShowDlg( category );
}

// new
void Cactivity_toolDlg::OnBnClickedButton1()
{
	New();
}

// save
void Cactivity_toolDlg::OnBnClickedButton2()
{
	if( !UpdateData() )
		return;

	if( m_comboFrom.GetCurSel() > 0 && m_comboTo.GetCurSel() <= m_comboFrom.GetCurSel() )
	{
		AfxMessageBox( "Repeat day of week: time to must > from" );
		return;
	}
	
	activity_mgr::activity_base_t* p = g_activity_mgr->find_activity( m_actid );
	if( !p )
	{
		p = g_activity_mgr->create_act_from_category( (activity_mgr::activity_category)m_comboCategory.GetCurSel() );
		if( IDNO == ::MessageBox( GetSafeHwnd(), "do you want to create a new activity and save it?", "choose", MB_YESNO ) )
			return;
	}
	else
	{
		if( IDNO == ::MessageBox( GetSafeHwnd(), "do you want to overwrite this activity?", "choose", MB_YESNO ) )
			return;
	}
	if( !p )
		return;

	p->id = m_actid;
	p->category = (activity_mgr::activity_category)m_comboCategory.GetCurSel();
	p->starttime = m_dtFrom.GetTime();
	p->expire = m_dtTo.GetTime();
	p->starthour = m_comboFrom.GetCurSel();
	p->expirehour = m_comboTo.GetCurSel();
	p->repeat_dayofweek[0] = m_checkSunday;
	p->repeat_dayofweek[1] = m_checkMonday;
	p->repeat_dayofweek[2] = m_checkTuesday;
	p->repeat_dayofweek[3] = m_checkWednesday;
	p->repeat_dayofweek[4] = m_checkThursday;
	p->repeat_dayofweek[5] = m_checkFriday;
	p->repeat_dayofweek[6] = m_checkSaturday;
	p->description[1] = AnisToUTF8(m_strDescription.GetBuffer());
	p->description[0] = AnisToUTF8(m_strDescription2.GetBuffer());
	p->comment = AnisToUTF8(m_strComment.GetBuffer());

	int btime = m_BroadTime.GetCurSel() ;
	switch(btime)
	{
	case 0:
		p->next_broadcast_interval = BROADCAST_0;
		break;
	case 1:
		p->next_broadcast_interval = BROADCAST_1;
		break;
	case 2:
		p->next_broadcast_interval = BROADCAST_2 ;
		break;
	case 3:
		p->next_broadcast_interval = BROADCAST_3;
		break;
	case 4:
		p->next_broadcast_interval = BROADCAST_4;
		break;
	case 5:
		p->next_broadcast_interval = BROADCAST_5;
		break;
	case 6:
		p->next_broadcast_interval = BROADCAST_6;
		break;
	default:
		p->next_broadcast_interval = BROADCAST_0;
		break;
	}

	switch( p->category )
	{
	case activity_mgr::ACTIVITY_CATEGORY_EXP:
		{
			activity_mgr::exp_config_t* ep = static_cast<activity_mgr::exp_config_t*>( p );
			if( !m_dlgExp.UpdateData() )
				return;

			ep->rate = m_dlgExp.m_rate;
			ep->type = m_dlgExp.m_comboType.GetCurSel();
		}
		break;
	case activity_mgr::ACTIVITY_CATEGORY_SPAWN:
		{
			activity_mgr::spawn_config_t* sp = static_cast<activity_mgr::spawn_config_t*>( p );
			if( !m_dlgSpawn.UpdateData() )
				return;

			sp->spawnid = m_dlgSpawn.m_spawnid;
			sp->respawn = m_dlgSpawn.m_checkRespawn;
			sp->mapid = m_dlgSpawn.m_mapid;
			sp->is_creature = m_dlgSpawn.m_checkIsCreature;
		}
		break;
	case activity_mgr::ACTIVITY_CATEGORY_BLESS:
		break;
	case activity_mgr::ACTIVITY_CATEGORY_SRVSHUTDOWN:
		{
			activity_mgr::srvshutdown_config_t* sp = static_cast<activity_mgr::srvshutdown_config_t*>( p );
			if( !m_dlgSrvshutdown.UpdateData() )
				return;

			sp->seconds = m_dlgSrvshutdown.m_seconds;
		}
		break;
		//////////////////////////////////////////////////////////////////////////
	case activity_mgr::ACTIVITY_CATEGORY_DONATION_EVENT:
		{
			activity_mgr::donation_event_config_t* sp = static_cast<activity_mgr::donation_event_config_t*>( p );
			if (!m_dlgDonation.UpdateData())
			{
				return ;
			}
			
			sp->event_id = m_dlgDonation.m_EnventId;
			sp->event_title = m_dlgDonation.m_EventTitle.GetBuffer();
			sp->event_photo_count = m_dlgDonation.m_Event_photo.size();
			
			m_dlgDonation.SaveData();
			//return ;		
		}
		break;
	case activity_mgr::ACTIVITY_CATEGORY_TREASURE:
		{
			activity_mgr::treasure_event_config_t* sp = static_cast<activity_mgr::treasure_event_config_t*>( p );
			if( !m_dlgTreasure.UpdateData() )
				return;
			sp->treasurecount = m_dlgTreasure.m_TreasureCount;
			sp->treasureid = m_dlgTreasure.m_TreasureID;
			sp->lockitemtype = m_dlgTreasure.m_comboType.GetCurSel();

			if (sp->lockitemtype >= 1)
			{
				if (m_dlgTreasure.m_TreasureTime == 0)
				{
					AfxMessageBox( "随即刷新模式需要指定刷新时间!" );
					return ;
				}
			}
			sp->refurbishtime = m_dlgTreasure.m_TreasureTime;
			sp->treasureposstart = m_dlgTreasure.m_TreasurePosStart;
			sp->treasureposend = m_dlgTreasure.m_TreasurePosEnd;
			sp->treasureitemcount = m_dlgTreasure.m_TreasureItemCount;
			sp->treasurescale = m_dlgTreasure.m_Treasurescale / 10.0f ;
		}
		break;
	case activity_mgr::ACTIVITY_CATEGORY_SHOP:
		{
			activity_mgr::shop_event_config_t* sp = static_cast<activity_mgr::shop_event_config_t*>( p);
			if (!m_dlgShop.UpdateData())
			{
				return ;
			}
			int category = m_dlgShop.m_ShopItem_Category.GetCurSel();
			if (category == 0)
			{
				AfxMessageBox( " 请选择打折类型!" );
				return ;
			}
			sp->shop_item_category = category;
			sp->discount =  (m_dlgShop.m_DisCount.GetCurSel() + 1) * 5;

		}
		break;
	default:
		return;
	}
	MSG_C2S::stUpdateActivityReq req;
	req.SetPtr( p );
	g_activity_client->m_network->send_packet( req );
}

void Cactivity_toolDlg::OnTvnSelchangedTree1(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMTREEVIEW pNMTreeView = reinterpret_cast<LPNMTREEVIEW>(pNMHDR);
	
	activity_mgr::activity_base_t* p = reinterpret_cast<activity_mgr::activity_base_t*>( m_treeAct.GetItemData( pNMTreeView->itemNew.hItem ) );
	if( p )
		OnRefreshActivity( p );

	*pResult = 0;
}

void Cactivity_toolDlg::OnOK()
{
	//CDialog::OnOK();
}

void Cactivity_toolDlg::OnCancel()
{
	if( IDYES == ::MessageBox( GetSafeHwnd(), "do you want to quit this program?", "choose", MB_YESNO ) )
		CDialog::OnCancel();
}


void Cactivity_toolDlg::OnTimer(UINT_PTR nIDEvent)
{
	switch( nIDEvent )
	{
	case 0:
		g_activity_mgr->update();
		break;
	case 1:
		KillTimer( 1 );
		AfxMessageBox( "server response timed out! click OK to quit this program!" );
		EndDialog( 0 );
		break;
	case 2:
		{
			char titleinfo[128];
			int y, m, d, h, min, s;
			convert_unix_time( ++g_activity_client->m_servertime, &y, &m, &d, &h, &min, &s );
			sprintf( titleinfo, "%02d-%02d-%02d %02d:%02d:%02d", y, m, d, h, min, s );
			g_maindlg->SetWindowText( titleinfo );
		}
		break;
	}
	CDialog::OnTimer(nIDEvent);
}

void Cactivity_toolDlg::OnBnClickedButton3()
{
	if( g_activity_mgr->find_activity( m_actid ) )
	{
		if( IDNO == ::MessageBox( GetSafeHwnd(), "do you want to remove this activity?", "choose", MB_YESNO ) )
			return;
		MSG_C2S::stRemoveActivityReq req;
		req.actid = m_actid;
		g_activity_client->m_network->send_packet( req );
	}
}
