// activity_toolDlg.h : 头文件
//

#pragma once
#include "afxcmn.h"
#include "SpawnDlg.h"
#include "ExpDlg.h"
#include "BlessDlg.h"
#include "SrvshutdownDlg.h"
#include "DonationDlg.h"
#include "TreasureDlg.h"
#include "afxwin.h"
#include "afxdtctl.h"
#include "ShopDlg.h"

std::string UTF8ToAnis(const std::string& str);


std::string AnisToUTF8(const std::string& str);
// Cactivity_toolDlg 对话框
class Cactivity_toolDlg : public CDialog
{
// 构造
public:
	Cactivity_toolDlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
	enum { IDD = IDD_ACTIVITY_TOOL_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

public:
	void OnRefreshActivity( activity_mgr::activity_base_t* act );
	void ShowDlg( activity_mgr::activity_category category );
	void New();

public:
	CBlessDlg m_dlgBless;
	CExpDlg m_dlgExp;
	CSpawnDlg m_dlgSpawn;
	CSrvshutdownDlg m_dlgSrvshutdown;
	CDonationDlg m_dlgDonation;
	CTreasureDlg m_dlgTreasure;
	CShopdlg m_dlgShop;
	CTreeCtrl m_treeAct;
	CEdit m_edtID;
	int m_actid;
	CComboBox m_comboCategory;
	CTime m_dtFrom;
	CTime m_dtTo;
	CComboBox m_comboFrom;
	CComboBox m_comboTo;
	CComboBox m_BroadTime;
	BOOL m_checkSunday;
	BOOL m_checkMonday;
	BOOL m_checkTuesday;
	BOOL m_checkWednesday;
	BOOL m_checkThursday;
	BOOL m_checkFriday;
	BOOL m_checkSaturday;
	CString m_strDescription;

	CDialog* m_aDlgs[activity_mgr::ACTIVITY_CATEGORY_MAX];
	afx_msg void OnCbnSelchangeCombo1();
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton2();
	CDateTimeCtrl m_fromDateTimeCtrl;
	CDateTimeCtrl m_toDateTimeCtrl;
	BOOL m_on_duty;
	HTREEITEM m_hTreeRoot[activity_mgr::ACTIVITY_CATEGORY_MAX];
	afx_msg void OnTvnSelchangedTree1(NMHDR *pNMHDR, LRESULT *pResult);

	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnBnClickedButton3();
	CString m_strComment;
	CString m_strDescription2;
	afx_msg void OnCbnSelchangeCombo4();
};

extern Cactivity_toolDlg* g_maindlg;