//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by activity_tool.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_ACTIVITY_TOOL_DIALOG        102
#define IDR_MAINFRAME                   128
#define IDD_DIALOG_ACT_SPAWN            129
#define IDD_DIALOG_ACT_EXP              130
#define IDD_DIALOG_ACT_BLESS            131
#define IDD_DIALOG_ACT_SRVSHUTDOWN      132
#define IDD_DIALOG_ACT_DONATION         133
#define IDD_DIALOG_ACT_TREASURE         138
#define IDD_DIALOG_SHOP                 142
#define IDC_TREE1                       1000
#define IDC_EDIT1                       1001
#define IDC_COMBO1                      1002
#define IDC_EDIT2                       1002
#define IDC_CHECK1                      1003
#define IDC_EDIT7                       1003
#define IDC_CHECK2                      1004
#define IDC_EDIT5                       1004
#define IDC_CHECK3                      1005
#define IDC_CHECK4                      1008
#define IDC_COMBO2                      1009
#define IDC_CHECK5                      1010
#define IDC_CHECK6                      1011
#define IDC_COMBO3                      1012
#define IDC_DATETIMEPICKER1             1013
#define IDC_DATETIMEPICKER2             1014
#define IDC_EDIT3                       1017
#define IDC_CHECK7                      1018
#define IDC_BUTTON1                     1019
#define IDC_EDIT4                       1020
#define IDC_BUTTON4                     1020
#define IDC_EDIT6                       1021
#define IDC_BUTTON2                     1023
#define IDC_BUTTON3                     1024
#define IDC_CHECK9                      1026
#define IDC_CHECK_IS_CREATURE           1028
#define IDC_LIST1                       1029
#define IDC_LIST2                       1030
#define IDC_BUTTON_REMOVE               1030
#define IDC_MONTHCALENDAR1              1033
#define IDC_STATIC5                     1034
#define IDC_COMBO4                      1035
#define IDC_EDIT_COUNT                  1040
#define IDC_EDIT_POSSTART               1044
#define IDC_EDIT_ZUJIANID               1045
#define IDC_EDIT_TIME                   1046
#define IDC_EDIT_ITEMCOUNT              1047
#define IDC_EDIT_POSEND                 1048
#define IDC_EDIT_SCALE                  1049
#define IDC_COMBO_CATEGORY              1050
#define IDC_COMBO_DISCOUNT              1052
#define IDC_COMBO_TYPE                  1055
#define IDC_STATIC_1                    1056
#define IDC_STATIC_2                    1057
#define IDC_STATIC_3                    1058
#define IDC_STATIC_TIPS                 1059
#define IDC_STATIC_SHOW                 1060

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        143
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1061
#define _APS_NEXT_SYMED_VALUE           102
#endif
#endif
