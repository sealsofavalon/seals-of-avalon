#!/bin/sh

mkdir ac_build
cd ac_build
../AccountCenter/configure --prefix=/opt/jtx_server/AccountCenter

cd ..
mkdir cs_build
cd cs_build
../CenterServer/configure --prefix=/opt/jtx_server/CenterServer

cd ..
mkdir db_build
cd db_build
../DBServer/configure --prefix=/opt/jtx_server/DBServer

cd ..
mkdir login_build
cd login_build
../NewLoginServer/configure --prefix=/opt/jtx_server/NewLoginServer

cd ..
mkdir gate_build
cd gate_build
../GateServer/configure --prefix=/opt/jtx_server/GateServer

cd ..
mkdir gs_build
cd gs_build
../GameServer/configure --prefix=/opt/jtx_server/GameServer
cd ..

