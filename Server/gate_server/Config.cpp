#include "gate_server.h"

CGTConfig sGTConfig;

bool CGTConfig::Load()
{
	if( !Config.MainConfig.SetSource( "GT.conf" ) )
	{
		MyLog::log->error("Could not find configuration file [GT.conf].");
		return false;
	}
	m_nGateID = Config.MainConfig.GetIntDefault("ServerInfo", "GateID", 0);
	if( m_nGateID == 0 )
	{
		printf( "load GT.conf failed!!!\n" );
		exit( 0 );
		return false;
	}
	m_LoginIP		= Config.MainConfig.GetStringDefault("LoginServer", "IP", "127.0.0.1");
	m_nLoginPort	= Config.MainConfig.GetIntDefault("LoginServer", "Port", 7102);
	//m_DBIP		= Config.MainConfig.GetStringDefault("DBServer", "IP", "127.0.0.1");
	//m_nDBPort		= Config.MainConfig.GetIntDefault("DBServer", "Port", 7103);
	m_GateIP		= Config.MainConfig.GetStringDefault("GateServer", "IP", "127.0.0.1");
	m_nGatePort		= Config.MainConfig.GetIntDefault("GateServer", "Port", 7001);

	m_realm_data_ip = Config.MainConfig.GetStringDefault( "RealmDataDB", "IP", "192.168.1.5" );
	m_realm_data_dbname = Config.MainConfig.GetStringDefault( "RealmDataDB", "DBName", "sunyou_realm_data" );
	m_realm_data_port = Config.MainConfig.GetIntDefault( "RealmDataDB", "Port", 3306 );
	m_realm_data_user = Config.MainConfig.GetStringDefault( "RealmDataDB", "User", "root" );
	m_realm_data_password = Config.MainConfig.GetStringDefault( "RealmDataDB", "Password", "sunyou" );

	RealmDB = Database::CreateDatabaseInterface(1);

	// Initialize it
	if( !RealmDB->Initialize( m_realm_data_ip.c_str(), (unsigned int)m_realm_data_port, m_realm_data_user.c_str(),
		m_realm_data_password.c_str(), m_realm_data_dbname.c_str(), 1, 16384 ) )
	{
		MyLog::log->error("sql : sunyou_realm_data database initialization failed. Exiting.");
		return false;
	}

	for( int i = 0; i < 16; ++i )
	{
		m_groups[i].groupid = 0;
	}
	QueryResult* qr = RealmDB->Query( "select * from group_conf" );
	if( qr )
	{
		do
		{
			Field* f = qr->Fetch();
			uint32 groupid = f[0].GetUInt32();

			m_groups[groupid].groupid = groupid;
			m_groups[groupid].gsip = f[1].GetString();
			m_groups[groupid].gsport = f[2].GetUInt32();
			m_groups[groupid].dbip = f[3].GetString();
			m_groups[groupid].dbport = f[4].GetUInt32();
			m_groups[groupid].name = f[5].GetString();
			m_groups[groupid].maxplayer = f[6].GetUInt32();
		}
		while( qr->NextRow() );
		delete qr;
	}

	return true;
}
