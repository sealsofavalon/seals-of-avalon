#ifndef _CONFIG_HEAD
#define _CONFIG_HEAD

class CGTConfig
{
public:
	CGTConfig(void){}
	~CGTConfig(void){}

	bool Load();
	bool LoadGateServer();

	int			m_nGateID;
	string		m_LoginIP;
	//string		m_DBIP;
	string		m_GateIP;
	int			m_nLoginPort;
	//int			m_nDBPort;

	struct group_conf {
		uint32 groupid;
		std::string gsip;
		uint32 gsport;
		std::string dbip;
		uint32 dbport;
		std::string name;
		uint32 maxplayer;
	};

	group_conf		m_groups[16];

	int			m_nGatePort;

	string		m_realm_data_ip;
	string		m_realm_data_dbname;
	int			m_realm_data_port;
	string		m_realm_data_user;
	string		m_realm_data_password;
};

extern CGTConfig sGTConfig;

#endif // _CONFIG_HEAD
