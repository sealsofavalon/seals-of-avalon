#include "gate_server.h"

client_session::client_session( boost::asio::io_service& is ) : tcp_session( is )
{
	m_iscompress = false;
	m_uncommpress_type[0] = 10000;
	acct = 0;
	guid = 0;
	transid = 0;
	groupid = 0;
	ischild = 0;
	chenmitime = 0;
	lastlogout = 0;
	banned = 0;
	muted = 0;
	lastping = 0;
	accepttime = 0;
	isvalid = false;
	gs = NULL;
	gsold = NULL;
	db = NULL;
}

client_session::~client_session(void)
{
}

void client_session::reset()
{
	tcp_session::reset();
	m_iscompress = false;
	m_uncommpress_type[0] = 10000;
	acct = 0;
	guid = 0;
	transid = 0;
	groupid = 0;
	ischild = 0;
	gmflags.clear();
	chenmitime = 0;
	lastlogout = 0;
	banned = 0;
	muted = 0;
	lastping = 0;
	accepttime = 0;
	isvalid = 0;
	gs = NULL;
	gsold = NULL;
	db = NULL;
}

void client_session::init( prelogin_user* p )
{
	acct = p->acct;
	add_time = p->add_time;
	temp_password = p->temp_password;
	ischild = p->ischild;
	lastlogout = p->lastlogout;
	chenmitime = p->chenmitime;
	gmflags = p->gmflags;
	banned = p->banned;
	muted = p->muted;
}

void client_session::on_close( const boost::system::error_code& error )
{
	if( this == g_usermgr->get_user_by_account( acct ) )
	{
		MSG_C2S::stExitReq MsgExit;
		MsgExit.nTransID	= transid;//acct;
		MsgExit.accountid	= acct;
		g_loginclient->PostSend(MsgExit);
		// to be done : send to GS 如果此时有角色在线 通知GS
		//db->PostSend(MsgExit);
		//MsgExit.guid = guid;

		if( gs )
		{
			MSG_C2S::stLogout Msg;
			Msg.nTransID	= transid;//acct;
			gs->PostSend(Msg);
		}
		if( gsold )
		{
			MSG_C2S::stLogout Msg;
			Msg.nTransID	= transid;//acct;
			gsold->PostSend(Msg);
		}

		if( g_fpLogConnection )
		{
			int y, m, d, h, min, s;
			convert_unix_time( (uint32)time(NULL), &y, &m, &d, &h, &min, &s );
			fprintf( g_fpLogConnection, "-[%d-%d-%d %d:%d:%d] IP:[%s] Account:[%u] Logout\n", y, m, d, h, min, s, get_remote_address_string().c_str(), acct );
			fflush( g_fpLogConnection );
		}

		g_usermgr->on_user_disconnected( this );
	}
	// to be done : 如果在m_mapLoginClient中，此时并没有Account
	else if( acct == 0xFFFFFFFF && gs )
	{
		MSG_C2S::stLogout Msg;
		Msg.nTransID	= transid;//acct;
		gs->PostSend(Msg);
	}

	tcp_session::on_close( error );
}

void client_session::on_accept( tcp_server* p )
{
	MyLog::log->info( "client Connection[%s] Accepted", this->get_remote_address_string().c_str() );

	accepttime = (uint32)UNIXTIME;

	tcp_session::on_accept( p );
}

void client_session::proc_message( const message_t& msg )
{
	try
	{
		ParsePacket( msg.data, msg.len );
	}
	catch( const packet_exception& e )
	{
		this->close();
		m_father->add_ban_ip( this->get_remote_address_ui(), 120, net_global::BAN_REASON_HACK_PACKET );
		MyLog::log->error( "Recv hack packet from IP:[%s] account:[%d], kick out and ban this IP for 120 seconds! Exception:[%s]", this->get_remote_address_string().c_str(), this->acct, e.what() );
	}
	catch( ... )
	{
		this->close();
		m_father->add_ban_ip( this->get_remote_address_ui(), 120, net_global::BAN_REASON_HACK_PACKET );
		MyLog::log->error( "Recv hack packet from IP:[%s] account:[%d] and cause gate crash, kick out and ban this IP for 120 seconds!", this->get_remote_address_string().c_str() );
	}
}

void client_session::run()
{
	tcp_session::run();
	if( acct < 0xFFFFFFFF )
	{
		if( is_connected() && lastping > 0 && (int)((uint32)UNIXTIME - lastping) > 900 )
		{
			MyLog::log->info( "IP:[%s] ping time out. closing...", this->get_remote_address_string().c_str() );
			this->close();
			net_global::write_close_log( "IP:[%s] ping timed out", this->get_remote_address_string().c_str() );
			return;
		}

		if( is_connected() && !isvalid && (uint32)UNIXTIME - accepttime > 5 )
		{
			boost::mutex::scoped_lock lock( m_father->m_proc_mutex );
			std::map<unsigned int, unsigned int>::iterator it = m_father->m_idleip.find( this->get_remote_address_ui() );
			if( it != m_father->m_idleip.end() )
			{
				if( ++it->second > 50 )
				{
					m_father->m_idleip.erase( it );
					m_father->add_ban_ip( this->get_remote_address_ui(), 3600, net_global::BAN_REASON_TOO_MANY_IDLE_CONNECTION );
				}
			}
			else
				m_father->m_idleip[this->get_remote_address_ui()] = 1;

			this->close();
			net_global::write_close_log( "IP:[%s] no login req in 5 seconds, m_recv_size = %d", this->get_remote_address_string().c_str(), this->m_recv_size );
		}
	}
}

void client_session::PostSend( const void* data, uint16 len )
{
	send_message( data, len );
}

void client_session::PostSend( const PakHead& msg )
{
	CPacketSn cps;
	cps << msg;
	cps.SetProLen();
	send_message( cps.GetBuf(), cps.GetSize() );
}

bool client_session::ParsePacket(char* pData, ui32 nLen)
{
	CPacketUsn pakTool(pData, nLen);

	uint16 msgtype = pakTool.GetProNo();
	msgtype &= ~(1<<15);

	if( msgtype != MSG_C2S::LOGIN_REQ && msgtype != MSG_C2S::MSG_ACT_TOOL_ACCESS_REQ && msgtype != MSG_C2S::CMSG_PING  &&!isvalid )
	{
		assert( 0 );
		this->close();
		return false;
	}

	switch(msgtype)
	{
	case MSG_C2S::MSG_RETURN_2_LOGIN:
		{
		}
		break;
	case MSG_C2S::EXIT_REQ:
		{
			// remove the client from enter line
			this->close();
			net_global::write_close_log( "IP:[%s] case MSG_C2S::EXIT_REQ:", this->get_remote_address_string().c_str() );
			MyLog::log->debug("MSG_C2S::EXIT_REQ transid[%d]", pakTool.GetTransID());
		}
		break;
	case MSG_C2S::LOGIN_REQ:
		{
			MSG_C2S::stLoginReq MsgRecv;
			pakTool >> MsgRecv;
			if( ParseLoginReq(MsgRecv) )
				isvalid = true;
			else
			{
				this->close();
				net_global::write_close_log( "IP:[%s] acct:[%u] check temp password wrong!", this->get_remote_address_string().c_str(), MsgRecv.nAccountID );
			}
		}
		break;
	case MSG_C2S::MSG_SELECT_GROUP_REQ:
		{
			MSG_C2S::stSelectGroupReq MsgRecv;
			pakTool >> MsgRecv;
			ParseSelectGroupReq( MsgRecv );
		}
		break;
	case MSG_C2S::ENTERGAME_REQ:
		{
			MSG_C2S::stEnterGameReq MsgRecv;
			pakTool >> MsgRecv;

			guid = MsgRecv.guid;
			ParseEnterGameReq(MsgRecv);
		}
		break;
	case MSG_C2S::LOGOUT_REQ:
		{
			MSG_C2S::stLogoutReq MsgRecv;
			pakTool >> MsgRecv;
			ParseLogoutReq(MsgRecv);
		}
		break;
	case MSG_C2S::CREATEROLE_REQ:
	case MSG_C2S::DELROLE_REQ:
	case MSG_C2S::RESUMEROLE_REQ:
		{
			ParseRoleReq(pData, nLen);
		}
		break;
	case MSG_C2S::CMSG_PING:
		{			
			if (gs)
			{
				MSG_C2S::stPing MsgPing;
				pakTool >> MsgPing;

				pakTool.SetTransID( transid );
				gs->PostSend( pData, nLen );
				lastping = (uint32)UNIXTIME;
			}	
		}
		break;
	//////////////////////////////////////////////////////////////////////////
	case MSG_C2S::MSG_ACT_TOOL_ACCESS_REQ:
		{
			MSG_C2S::stActToolAccessReq req;
			pakTool >> req;
			if( req.user == "c++" && req.password == "boost" )
			{
				acct = 0xFFFFFFFF;
				transid = g_usermgr->dispatch_user( this );
				groupid = 0;
				for( int i = 0; i < 16; ++i )
				{
					if( g_groupmgr->m_groups[i]->id )
					{
						gs = g_groupmgr->m_groups[i]->gs;
						break;
					}
				}
				if( gs )
				{
					isvalid = true;
					pakTool.SetTransID( transid );
					gs->PostSend(pData, nLen);
					set_need_check_action_time( false );
				}
				else
					this->close();
			}
			else
			{
				m_father->add_ban_ip( get_remote_address_ui(), 600, net_global::BAN_REASON_HACK_PACKET );
				MyLog::log->warn( "Recv hack packet from IP:[%s], close and ban for 600 seconds!", get_remote_address_string().c_str() );
				this->close();
				return false;
			}
		}
		break;
	case MSG_C2S::MSG_QUERY_ACTIVITY_LIST:
	case MSG_C2S::MSG_UPDATE_ACTIVITY_REQ:
	case MSG_C2S::MSG_REMOVE_ACTIVITY_REQ:
		{
			if( gs )
			{
				pakTool.SetTransID( transid );
				gs->PostSend(pData, nLen);
			}
		}
		break;
	//////////////////////////////////////////////////////////////////////////
	default:
		{
			if( msgtype >= MSG_C2S::NUM_MSG_TYPES )
			{
				this->close();
				m_father->add_ban_ip( this->get_remote_address_ui(), 120, net_global::BAN_REASON_HACK_PACKET );
				MyLog::log->warn( "Recv hack packet from IP:[%s], close and ban for 120 seconds!", this->get_remote_address_string().c_str() );
				net_global::write_close_log( "IP:[%s] msgtype >= MSG_C2S::NUM_MSG_TYPES", this->get_remote_address_string().c_str() );
				return false;
			}
			if( gs )
			{
				pakTool.SetTransID( transid );
				gs->PostSend( pData, nLen );
			}
		}
	}
	return true;
}

bool client_session::ParseRoleReq(const char* pData, ui32 nLen)
{
	*(ui32*)(pData+2*sizeof(ui16)) = acct;
	db->PostSend(pData, nLen);
	return true;
}

bool client_session::ParseLoginReq(MSG_C2S::stLoginReq& Msg)
{
	if( !g_usermgr->apply_login_user( this, Msg.nAccountID, Msg.nTempPassword ) )
		return false;

	transid = g_usermgr->dispatch_user( this );
	
	if( g_fpLogConnection )
	{
		int y, m, d, h, min, s;
		convert_unix_time( (uint32)time(NULL), &y, &m, &d, &h, &min, &s );
		fprintf( g_fpLogConnection, "+[%d-%d-%d %d:%d:%d] IP:[%s] Account:[%u] Login\n", y, m, d, h, min, s, this->get_remote_address_string().c_str(), Msg.nAccountID );
		fflush( g_fpLogConnection );
	}

	MSG_S2C::stServerGroupListAck ack;
	MSG_S2C::stServerGroupListAck::_group g;

	for( int i = 1; i < 15; ++i )
	{
		if( g_groupmgr->m_groups[i]->id )
		{
			if( g_groupmgr->m_groups[i]->gs->is_connected() )
			{
				float size = (float)g_groupmgr->m_groups[i]->m_users.size();
				float rate = size / (float)sGTConfig.m_groups[i].maxplayer;
				if( rate >= 1.f ) {
					g.status = 4;
				}
				else if( rate >= 0.6f ) {
					g.status = 3;
				}
				else if( rate >= 0.3f ) {
					g.status = 2;
				}
				else
					g.status = 1;
			}
			else
				g.status = 0;

			g.id = i;
			g.name = sGTConfig.m_groups[i].name;

			ack.groups.push_back( g );
		}
	}
	PostSend( ack );

	MSG_GT2LS::stUserOnline olmsg;
	olmsg.acct = acct;
	g_loginclient->PostSend( olmsg );
	return true;
}

bool client_session::ParseSelectGroupReq(MSG_C2S::stSelectGroupReq& Msg)
{
	if( !sGTConfig.m_groups[Msg.groupid].groupid )
		return false;

	if( !g_groupmgr->m_groups[Msg.groupid]->gs->is_connected() )
		return false;

	if( !g_groupmgr->m_groups[Msg.groupid]->db->is_connected() )
		return false;

	groupid = Msg.groupid;

	gs = g_groupmgr->m_groups[groupid]->gs;
	db = g_groupmgr->m_groups[groupid]->db;
	db->RequestCharaInfo( transid, acct );
	return true;
}

/*
协议头：0x1001＋dwCreatureID(0x00000000) + 0X0000
协议体：len＋Account +len+角色名
*/
bool client_session::ParseEnterGameReq(MSG_C2S::stEnterGameReq& Msg)
{
	Msg.nTransID	= transid;

	Msg.AccountID = acct;
	Msg.guid = guid;
	Msg.strIP = get_remote_address_string();
	Msg.GMFlags = gmflags;
	Msg.Muted = muted;
	Msg.Baned = banned;
	Msg.bFangchenmiAccount = ischild;
	Msg.LastLogout = lastlogout;
	Msg.ChenmiTime = chenmitime;

	gs->PostSend(Msg);
	MyLog::log->notice( "send enter game req, acc:%d ip:%s name:%s", Msg.AccountID, get_remote_address_string().c_str(), Msg.AccountName.c_str() );

	g_usermgr->add_in_game_user( this );
	return true;
}

/*
协议头：0x1005＋dwCreatureID(0x00000000) + 0X0000
*/
bool client_session::ParseLogoutReq(MSG_C2S::stLogoutReq& Msg)
{
	Msg.nTransID = transid;
	gs->PostSend(Msg);
	return true;
}
