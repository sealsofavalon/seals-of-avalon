#ifndef _CLIENT_SESSION_HEAD
#define _CLIENT_SESSION_HEAD

class gs_client;
class db_client;

struct prelogin_user {
	uint32 acct;
	uint32 add_time;
	uint32 temp_password;
	uint8 ischild;
	uint32 lastlogout;
	uint32 chenmitime;
	std::string gmflags;
	uint32 banned;
	uint32 muted;
};

class client_session : public prelogin_user, public tcp_session
{
public:
	client_session( boost::asio::io_service& is );
	virtual ~client_session(void);

	virtual void reset();
	virtual void on_close( const boost::system::error_code& error );
	virtual void on_accept( tcp_server* p );
	virtual void proc_message( const message_t& msg );
	virtual void run();

	void init( prelogin_user* p );
	void PostSend( const void* data, uint16 len );
	void PostSend( const PakHead& msg );

	uint32 guid;
	uint32 transid;
	uint32 groupid;
	uint32 lastping;
	uint32 accepttime;
	bool isvalid;
	gs_client* gs;
	gs_client* gsold;
	db_client* db;

	bool ParsePacket(char* pData, ui32 nLen);
	bool ParseLoginReq(MSG_C2S::stLoginReq& Msg);
	bool ParseSelectGroupReq(MSG_C2S::stSelectGroupReq& Msg);
	bool ParseRoleReq(const char* pData, ui32 nLen);
	bool ParseEnterGameReq(MSG_C2S::stEnterGameReq& Msg);
	bool ParseLogoutReq(MSG_C2S::stLogoutReq& Msg);
};

#endif // _CLIENT_SESSION_HEAD
