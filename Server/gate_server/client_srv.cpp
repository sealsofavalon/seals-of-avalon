#include "gate_server.h"

client_srv::client_srv() : tcp_server( 1 )
{

}

client_srv::~client_srv()
{

}

tcp_session* client_srv::create_session()
{
	return new client_session( *net_global::get_io_service() );
}

