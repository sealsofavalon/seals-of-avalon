#ifndef _CLIENT_SRV_HEAD
#define _CLIENT_SRV_HEAD

class client_srv : public tcp_server
{
public:
	client_srv();
	~client_srv();

	virtual tcp_session* create_session();

};

#endif // _CLIENT_SRV_HEAD
