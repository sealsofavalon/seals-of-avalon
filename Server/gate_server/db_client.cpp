#include "gate_server.h"
#include "db_client.h"

db_client::db_client() : tcp_client( *net_global::get_io_service() )
{
	m_isreconnect = true;
}

db_client::~db_client(void)
{
}

void db_client::on_close( const boost::system::error_code& error )
{
	MyLog::log->error( "game server disconnected, error message:%s", error.message().c_str() );
	tcp_client::on_close( error );
}

void db_client::on_connect()
{
	tcp_client::on_connect();
}

void db_client::on_connect_failed( boost::system::error_code error )
{
	MyLog::log->error( "connect db server failed, error message:%s", error.message().c_str() );

	m_isreconnect = true;
}

void db_client::proc_message( const message_t& msg )
{
	CPacketUsn pakTool(msg.data, msg.len);

	switch(pakTool.GetProNo())
	{
	case MSG_S2C::ROLELIST_ACK:
		{
			MSG_S2C::stRoleListAck Msg;
			pakTool >> Msg;
			RoleList_Ack(Msg);
		}
		break;
	case MSG_S2C::CREATEROLE_ACK:
		{
			MSG_S2C::stCreateRoleAck Msg;
			pakTool >> Msg;
			CreateRole_Req(Msg);
		}
		break;
	case MSG_S2C::DELROLE_ACK:
		{
			MSG_S2C::stDelRoleAck Msg;
			pakTool >> Msg;
			DelRole_Ack(Msg);
		}
		break;
	case MSG_S2C::RESUMEROLE_ACK:
		{
			MSG_S2C::stResumeRoleAck Msg;
			pakTool >> Msg;
			ResumeRole_Ack(Msg);
		}
		break;
	default :
		{
			MyLog::log->error("db_client::ParsePacket Error porotcol:%u", pakTool.GetProNo());
		}
		break;
	}
}

void db_client::PostSend( const void* data, uint16 len )
{
	send_message( data, len );
}

void db_client::PostSend( const PakHead& msg )
{
	CPacketSn cps;
	cps << msg;
	cps.SetProLen();
	send_message( cps.GetBuf(), cps.GetSize() );
}


//创建角色回应
/*
协议头：CltSockID +0x0152 ＋0x00000000＋ErrCode（1字节） + 0x00
协议体：性别＋职业＋等级＋GS编号+len＋RoleName
注意：ErrCode＝0表示成功（此时才有协议体中的[性别＋职业＋等级]），＝1表示角色名重复，＝2表示系统错误,=3角色数达到上限,4=其它情况
*/
void db_client::CreateRole_Req(  MSG_S2C::stCreateRoleAck& Msg)
{
	client_session * pUser = g_usermgr->get_user_by_account(Msg.nTransID);
	if (!pUser)
	{
		MyLog::log->error("CreateRole_Req cannot found user, acct:[%d]", Msg.nTransID);
		return;
	}

	if (Msg.nError)
	{
		pUser->PostSend(Msg);
		return;
	}
	pUser->PostSend(Msg);
}
//删除角色回应
/*
协议头：CltSockID ＋0x0156 ＋ 0x00000000（4字节）  ＋ ErrCode（1字节） + 0x00
协议体：len＋RoleName
ErrCode:0=成功，1=无此角色，2＝数据库异常,3=其它情况
*/
void db_client::DelRole_Ack( MSG_S2C::stDelRoleAck& Msg)
{
	client_session * pUser = g_usermgr->get_user_by_account(Msg.nTransID);
	if (!pUser)
	{
		MyLog::log->error("DelRole_Ack cannot found user, acct:[%d]", Msg.nTransID);
		return;
	}

	if (Msg.nError)
	{
		pUser->PostSend(Msg);
		return;
	}
	pUser->PostSend(Msg);
}
//恢复角色回应
/*
协议：CltSockID ＋0x0158 ＋ 0x00000000（4字节）  ＋ ErrCode（1字节） + 0x00
协议体：len＋RoleName
ErrCode:0=成功，1=无此角色，2＝数据库异常,3=其它情况
*/
void db_client::ResumeRole_Ack( MSG_S2C::stResumeRoleAck& Msg)
{
	client_session * pUser = g_usermgr->get_user_by_account(Msg.nTransID);
	if (!pUser)
	{
		MyLog::log->error("ResumeRole_Ack cannot found user, acct:[%d]", Msg.nTransID);
		return;
	}

	if (Msg.nError)
	{
		pUser->PostSend(Msg);
		return;
	}
	pUser->PostSend(Msg);
}

//请求角色列表回应
/*
协议头：CltSockID +0x0154 ＋ 0x00000000（4字节） ＋ ErrCode（1字节） + 0x00
协议体：角色数(2字节)+角色数据1＋角色数据2＋。。。
角色数据：状态+性别＋职业＋等级＋GS编号+len＋RoleName
状态：0表示目前处于使用中，1表示目前处于已删除状态
注意：byErrCode=0表示找到账号角色，1表示数据库异常情况,2=其它异常,3=状态设置失败
*/
void db_client::RoleList_Ack( MSG_S2C::stRoleListAck& Msg)
{
	client_session * pUser = g_usermgr->get_user_by_transid(Msg.nTransID);
	if (!pUser)
	{
		MyLog::log->error("AckCharaInfo未找到pUser transid[%d] error[%d]", Msg.nTransID, Msg.nError);
		return;
	}

	pUser->PostSend(Msg);

	if( Msg.nError )
		MyLog::log->error( "db_client::RoleList_Ack  Msg.nError = %d", Msg.nError );
}

void db_client::RequestCharaInfo(DWORD dwCltID, ui32 nAccountID)
{
	MSG_C2S::stRoleListReq Msg;
	Msg.nTransID	= dwCltID;
	Msg.nAccountID	= nAccountID;
	PostSend(Msg);
}
