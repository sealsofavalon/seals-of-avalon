#ifndef _DB_CLIENT_HEAD
#define _DB_CLIENT_HEAD

class db_client : public tcp_client
{
public:
	db_client();
	~db_client(void);

	virtual void on_close( const boost::system::error_code& error );
	virtual void on_connect();
	virtual void on_connect_failed( boost::system::error_code error );
	virtual void proc_message( const message_t& msg );
	void PostSend( const void* data, uint16 len );
	void PostSend( const PakHead& msg );

	void RequestCharaInfo(DWORD dwCltID, ui32 nAccountID);

	gs_client* gs;

private:
	void RoleList_Ack( MSG_S2C::stRoleListAck& Msg );
	void CreateRole_Req( MSG_S2C::stCreateRoleAck& Msg );
	void DelRole_Ack( MSG_S2C::stDelRoleAck& Msg );
	void ResumeRole_Ack( MSG_S2C::stResumeRoleAck& Msg );
};

#endif // _DB_CLIENT_HEAD
