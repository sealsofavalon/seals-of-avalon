#include "gate_server.h"
#include "Config.h"
#include "../../new_common/Source/compression/zlib/ZlibCompressionStrategy.h"

Database* RealmDB = NULL;
login_client* g_loginclient = NULL;
client_srv* g_clientsrv = NULL;
user_mgr* g_usermgr = NULL;
group_mgr* g_groupmgr = NULL;
FILE* g_fpLogConnection = NULL;
zip_compress_strategy cs;

bool init()
{
	if( !MyLog::Init() )
	{
		printf( "cannot open file log.conf\n" );
		return false;
	}

	g_fpLogConnection = fopen( "user_connection.log", "a" );
	unsigned int mysqlthreadsafe = mysql_thread_safe();
	if( !mysqlthreadsafe )
	{
		MyLog::log->error( "mysql doesn't compiled on thread safe mode\n" );
		return false;
	}
	Database::StartThread();

	if( !sGTConfig.Load() )
		return false;

#ifdef _WIN32
	net_global::init_net_service( 1, 10, &cs, true, 100 );
#else
	net_global::init_net_service( 8, 10, &cs, true, 15000 );
#endif
	g_loginclient = new login_client;
	g_clientsrv = new client_srv;
	g_groupmgr = new group_mgr;
	g_usermgr = new user_mgr;

	g_groupmgr->init();
	g_loginclient->connect( sGTConfig.m_LoginIP.c_str(), sGTConfig.m_nLoginPort );

#ifdef _WIN32
	if( !g_clientsrv->create( sGTConfig.m_nGatePort, 50, 4 ) )
#else
	if( !g_clientsrv->create( sGTConfig.m_nGatePort, 2000, 10 ) )
#endif
	{
		MyLog::log->error( "listen port:%d failed!", sGTConfig.m_nGatePort );
		return false;
	}
	return true;
}

void run()
{
	uint32 now;
	while( true )
	{
		now = getMSTime();
		UNIXTIME = time( NULL );
		g_loginclient->run_no_wait();
		g_clientsrv->run_no_wait();
		g_groupmgr->run();
		g_usermgr->run();
		RealmDB->QueryTaskRun();

		if( getMSTime() - now < 2 )
		{
			Sleep( 1 );
		}
	}
}

void release()
{

}

int main()
{
	if( init() )
		run();
	else
		return -1;

	release();
	return 0;
}


