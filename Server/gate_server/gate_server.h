#ifndef _GATE_SERVER_HEAD
#define _GATE_SERVER_HEAD

#include <list>
#include <deque>
#include <vector>
#include <set>
#include <map>
#include <string>
using namespace std;

#define _ACTIVITY_PROTOCOL

#include "../../SDBase/Public/TypeDef.h"
#include "../../SDBase/Public/Timer.h"

#include "../Common/Platform/SystemConfig.h"
#include "../Common/Platform/Util.h"
#include "../Common/share/Database/Database.h"
#include "../Common/share/Database/MySQLDatabase.h"

#include "stdio.h"
#include "stdlib.h"

#include "../../new_common/Source/net/tcpclient.h"
#include "../../new_common/Source/net/tcpserver.h"
#include "../../new_common/Source/log4cpp-1.0/MyLog.h"
#include "../../SDBase/Protocol/C2S.h"
#include "../../SDBase/Protocol/S2C.h"
#include "../Common/Protocol/GT2DB.h"
#include "../Common/Protocol/DB2GT.h"
#include "../Common/Protocol/GT2CS.h"
#include "../Common/Protocol/CS2GT.h"
#include "../Common/Protocol/GS2GT.h"
#include "../Common/Protocol/GT2GS.h"
#include "../Common/Protocol/LS2GT.h"
#include "../Common/Protocol/GT2LS.h"
#include "../Common/share/Config/Config.h"

extern time_t UNIXTIME;

extern Database* RealmDB;
extern FILE* g_fpLogConnection;

#include "client_session.h"
#include "client_srv.h"
#include "Config.h"
#include "db_client.h"
#include "group_mgr.h"
#include "login_client.h"
#include "user_mgr.h"
#include "gs_client.h"

#endif // _GATE_SERVER_HEAD


