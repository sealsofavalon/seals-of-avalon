#include "gate_server.h"


void group_mgr::init()
{
	for( int i = 0; i < 16; ++i )
	{
		m_groups[i] = new group;
		if( sGTConfig.m_groups[i].groupid )
		{
			m_groups[i]->id = sGTConfig.m_groups[i].groupid;
			m_groups[i]->gs = new gs_client;
			m_groups[i]->gs->connect( sGTConfig.m_groups[i].gsip.c_str(), sGTConfig.m_groups[i].gsport );
			m_groups[i]->gs->set_reconnect( true );
			
			if( m_groups[i]->id < 15 )
			{
				m_groups[i]->db = new db_client;
				m_groups[i]->db->connect( sGTConfig.m_groups[i].dbip.c_str(), sGTConfig.m_groups[i].dbport );
				m_groups[i]->db->set_reconnect( true );

				m_groups[i]->gs->db = m_groups[i]->db;
				m_groups[i]->db->gs = m_groups[i]->gs;
			}
			else
			{
				m_groups[i]->gs->db = NULL;
				m_groups[i]->db = NULL;
			}

			m_groups[i]->gs->id = sGTConfig.m_groups[i].groupid;
		}
		else
		{
			m_groups[i]->id = 0;
		}
	}
}

void group_mgr::run()
{
	for( int i = 0; i < 16; ++i )
	{
		if( m_groups[i]->id )
		{
			if (m_groups[i]->gs)
				m_groups[i]->gs->run_no_wait();
			if (m_groups[i]->db)
				m_groups[i]->db->run_no_wait();
		}
	}
}
