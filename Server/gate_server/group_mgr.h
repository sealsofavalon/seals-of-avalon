#ifndef _GROUP_MGR_HEAD
#define _GROUP_MGR_HEAD

class client_session;
class gs_client;

struct group
{
	uint32 id;
	gs_client* gs;
	db_client* db;
	std::map<uint32, client_session*> m_users;
};

class group_mgr
{
public:
	group* m_groups[16];
	void init();
	void run();
};

extern group_mgr* g_groupmgr;

#endif // _GROUP_MGR_HEAD
