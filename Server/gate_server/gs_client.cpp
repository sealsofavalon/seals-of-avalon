#include "gate_server.h" 


gs_client::gs_client() : tcp_client( *net_global::get_io_service() )
{
	m_iscompress = false;
	m_isreconnect = true;
	m_uncommpress_type[0] = 10000;
	m_uncommpress_type[1] = 401;
}

gs_client::~gs_client(void)
{
}

void gs_client::on_close( const boost::system::error_code& error )
{
	MyLog::log->error( "game server disconnected, error message:%s", error.message().c_str() );
	g_usermgr->remove_gs( id );
	g_groupmgr->m_groups[id]->m_users.clear();
	tcp_client::on_close( error );
}

void gs_client::on_connect()
{
	tcp_client::on_connect();

	MSG_C2S::stGroupIDReq req;
	req.groupid = sGTConfig.m_nGateID;
	this->PostSend( req );
}

void gs_client::on_connect_failed( boost::system::error_code error )
{
	MyLog::log->error( "connect game server failed, error message:%s", error.message().c_str() );

	m_isreconnect = true;
}

void gs_client::proc_message( const message_t& msg )
{
	ParsePacket( (char*)msg.data, msg.len );
}

void gs_client::PostSend( const void* data, uint16 len )
{
	send_message( data, len );
}

void gs_client::PostSend( const PakHead& msg )
{
	CPacketSn cps;
	cps << msg;
	cps.SetProLen();
	send_message( cps.GetBuf(), cps.GetSize() );
}

bool gs_client::ParsePacket(char* pData, ui32 nLen)
{
	CPacketUsn pakTool(pData, nLen);
	uint16 msgtype = pakTool.GetProNo();
	msgtype &= ~(1<<15);
	switch(msgtype)
	{
	case MSG_S2C::ENTERGAME_ACK:
		{
			MSG_S2C::stEnterGameAck Msg;
			pakTool >> Msg;
			EnterGame_Ack(Msg);
		}
		break;
	case MSG_GS2GT::LOGOUT_ACK:
		{
			MSG_GS2GT::stLogoutAck Msg;
			pakTool >> Msg;
			Logout_Ack(Msg);
		}
		break;
	case MSG_GS2GT::EXIT_ACK:
		{
			MSG_GS2GT::stExitAck Msg;
			pakTool >> Msg;
			client_session* pUser = g_usermgr->get_user_by_transid( pakTool.GetTransID() );
			if (pUser)
			{
				MyLog::log->info( "guid:%lu was kicked from GS", Msg.guid );
				pUser->close();

				if( Msg.nError >= 100 )
				{
					pUser->get_father()->add_ban_ip( pUser->get_remote_address_ui(), 120, (net_global::ban_reason_t)( Msg.nError - 100 ) );
					MyLog::log->warn( "IP:[%s] cause GameServer crash, close and ban for 120 seconds!", pUser->get_remote_address_string().c_str() );
				}
			}
		}
		break;
	case MSG_GS2GT::SETMUTE_ACCOUNT:
		{
			MSG_GS2GT::stMute MsgRecv;
			pakTool >> MsgRecv;
			MSG_GT2LS::stMuteReq req;
			req.acct = MsgRecv.acct;
			req.muted = MsgRecv.mute;
			g_loginclient->PostSend( req );
		}
		break;
	case MSG_GS2GT::SETBAN_ACCOUNT:
		{
			MSG_GS2GT::stBan MsgRecv;
			pakTool >> MsgRecv;
			MSG_GT2LS::stBanReq req;
			req.acct = MsgRecv.acct;
			req.banned = MsgRecv.ban;
			g_loginclient->PostSend( req );
		}
		break;
	case MSG_GS2GT::MSG_JUMP_GS:
		{
			MSG_GS2GT::stJumpGS MsgRecv;
			pakTool >> MsgRecv;
			client_session* user = g_usermgr->get_user_by_transid( MsgRecv.transid );
			if( user && MsgRecv.groupid < 16 && MsgRecv.groupid > 0 )
			{
				user->gsold = user->gs;
				user->gs = g_groupmgr->m_groups[MsgRecv.groupid]->gs;
			}
		}
		break;
	default:
		{
			client_session* user = g_usermgr->get_user_by_transid( pakTool.GetTransID() );
			if( user )
				user->PostSend(pData, nLen);
		}
		break;
	}

	return true;
}

bool gs_client::EnterGame_Ack(MSG_S2C::stEnterGameAck& Msg)
{
	client_session* pUser = g_usermgr->get_user_by_transid(Msg.nTransID);
	if (!pUser)
		return false;
	// ��һλ��ErrorCode
	if (Msg.nError == 0)
	{
		pUser->guid = Msg.guid;
	}

	pUser->PostSend(Msg);
	return true;
}

bool gs_client::Logout_Ack(MSG_GS2GT::stLogoutAck& Msg)
{
	client_session* pUser = g_usermgr->get_user_by_transid(Msg.nTransID);
	if (!pUser)
		return false;

	if( pUser->gsold && pUser->gsold->id < 15 )
	{
		MSG_C2S::stLogout msg;
		msg.nTransID	= Msg.nTransID;//acct;
		pUser->gsold->PostSend(msg);

		pUser->gs = pUser->gsold;
		pUser->gsold = NULL;
	}

	pUser->guid = 0;
	if( pUser->db )
		pUser->db->RequestCharaInfo(Msg.nTransID, pUser->acct);

	g_usermgr->remove_in_game_user( pUser );
	return true;
}
