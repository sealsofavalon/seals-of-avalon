#ifndef _GS_CLIENT_HEAD
#define _GS_CLIENT_HEAD

class gs_client : public tcp_client
{
public:
	gs_client();
	~gs_client(void);

	virtual void on_close( const boost::system::error_code& error );
	virtual void on_connect();
	virtual void on_connect_failed( boost::system::error_code error );
	virtual void proc_message( const message_t& msg );
	void PostSend( const void* data, uint16 len );
	void PostSend( const PakHead& msg );

	bool ParsePacket(char* pData, ui32	nLen);
	bool EnterGame_Ack(MSG_S2C::stEnterGameAck& Msg);
	bool Logout_Ack(MSG_GS2GT::stLogoutAck& Msg);

	db_client* db;
	uint32 id;
};

#endif // _GS_CLIENT_HEAD
