#include "gate_server.h"
#include "login_client.h"



login_client::login_client() : tcp_client( *net_global::get_io_service() )
{
	m_isreconnect = true;
}

login_client::~login_client(void)
{
}

void login_client::on_close( const boost::system::error_code& error )
{
	tcp_client::on_close( error );
}

void login_client::on_connect()
{
	tcp_client::on_connect();
	MyLog::log->info( "connect LoginSever ip:%s success!", sGTConfig.m_LoginIP.c_str());

	MSG_GT2LS::stInitReq req;
	req.gateid = sGTConfig.m_nGateID;
	req.ip = sGTConfig.m_GateIP;
	req.port = sGTConfig.m_nGatePort;
	PostSend( req );

	MSG_GT2LS::stUpdateUserInfo msg;
	g_usermgr->build_update_list_message( msg );
	PostSend( msg );
}

void login_client::on_connect_failed( boost::system::error_code error )
{
}

void login_client::proc_message( const message_t& msg )
{
	CPacketUsn pakTool(msg.data, msg.len);

	switch(pakTool.GetProNo())
	{
	case MSG_LS2GT::MSG_KICKOUT_USER:
		{
			MSG_LS2GT::stKickoutUser recv; pakTool >> recv;
			client_session* user = g_usermgr->get_user_by_account( recv.acct );
			if( user )
				user->close();
		}
		break;
	case MSG_LS2GT::MSG_LOGIN_REQ:
		{
			MSG_LS2GT::stLoginReq recv; pakTool >> recv;
			g_usermgr->add_prelogin_user( recv.acct, recv.tmp_password, recv.ischild, recv.lastlogout, recv.chenmitime, recv.gmflags, recv.banned, recv.muted );
		}
		break;
	default:
		assert( 0 );
		return;
	}
}

void login_client::PostSend( const void* data, uint16 len )
{
	send_message( data, len );
}

void login_client::PostSend( const PakHead& msg )
{
	CPacketSn cps;
	cps << msg;
	cps.SetProLen();
	send_message( cps.GetBuf(), cps.GetSize() );
}

