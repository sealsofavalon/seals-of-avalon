#ifndef _LOGIN_CLIENT_HEAD
#define _LOGIN_CLIENT_HEAD

class login_client : public tcp_client
{
public:
	login_client();
	~login_client(void);

	virtual void on_close( const boost::system::error_code& error );
	virtual void on_connect();
	virtual void on_connect_failed( boost::system::error_code error );
	virtual void proc_message( const message_t& msg );
	void PostSend( const void* data, uint16 len );
	void PostSend( const PakHead& msg );

private:
};


extern login_client* g_loginclient;

#endif // _LOGIN_CLIENT_HEAD
