#include "gate_server.h"

user_mgr::user_mgr() : m_maxid( 0 )
{
	memset( m_storage, 0, sizeof( m_storage ) );
}

client_session* user_mgr::get_user_by_transid( uint32 transid )
{
	uint32 real_transid = transid & 0x00FFFFFF;
	if( real_transid < MAX_SESSION_STORAGE )
		return m_storage[real_transid];
	else
		return NULL;
}

client_session* user_mgr::get_user_by_account( uint32 acct )
{
	std::map<uint32, client_session*>::iterator it = m_users.find( acct );
	if( it != m_users.end() )
		return it->second;
	else
		return NULL;
}

uint32 user_mgr::dispatch_user( client_session* user )
{
	m_users[user->acct] = user;
	++m_maxid;
	m_storage[m_maxid] = user;
	return m_maxid | (uint32)sGTConfig.m_nGateID << 24;
}

void user_mgr::remove_gs( uint32 groupid )
{
	std::map<uint32, client_session*> tmp( g_groupmgr->m_groups[groupid]->m_users );
	for( std::map<uint32, client_session*>::iterator it = tmp.begin(); it != tmp.end(); ++it )
	{
		client_session* p = it->second;
		p->close();
	}
}

void user_mgr::add_prelogin_user( uint32 acct, uint32 temp_password, uint8 ischild, uint32 lastlogout, uint32 chenmitime, const std::string& gmflags, uint32 banned, uint32 muted )
{
	std::map<uint32, prelogin_user*>::iterator it = m_prelogin_users.find( acct );
	prelogin_user* p = NULL;
	if( it != m_prelogin_users.end() )
	{
		p = it->second;
	}
	else
	{
		p = new prelogin_user;
		m_prelogin_users[acct] = p;
	}

	p->acct = acct;
	p->add_time = (uint32)UNIXTIME;
	p->temp_password = temp_password;
	p->ischild = ischild;
	p->lastlogout = lastlogout;
	p->chenmitime = chenmitime;
	p->gmflags = gmflags;
	p->banned = banned;
	p->muted = muted;
}

bool user_mgr::apply_login_user( client_session* user, uint32 acct, uint32 temp_password )
{
	std::map<uint32, prelogin_user*>::iterator it = m_prelogin_users.find( acct );
	if( it != m_prelogin_users.end() )
	{
		prelogin_user* p = it->second;
		if( p->temp_password != temp_password )
			return false;

		user->init( p );
		delete p;
		m_prelogin_users.erase( it );

		m_users[acct] = user;
		return true;
	}
	else
		return false;
}

void user_mgr::run()
{
	for( std::map<uint32, prelogin_user*>::iterator it = m_prelogin_users.begin(); it != m_prelogin_users.end(); )
	{
		std::map<uint32, prelogin_user*>::iterator it2 = it++;
		prelogin_user* p = it2->second;
		if( (uint32)UNIXTIME - p->add_time > 10 )
		{
			delete p;
			m_prelogin_users.erase( it2 );
		}
	}
}

void user_mgr::add_in_game_user( client_session* user )
{
	g_groupmgr->m_groups[user->groupid]->m_users[user->acct] = user;
}

void user_mgr::remove_in_game_user( client_session* user )
{
	g_groupmgr->m_groups[user->groupid]->m_users.erase( user->acct );
}

void user_mgr::on_user_disconnected( client_session* user )
{
	m_users.erase( user->acct );
	if( user->transid < MAX_SESSION_STORAGE )
		m_storage[user->transid] = NULL;

	g_groupmgr->m_groups[user->groupid]->m_users.erase( user->acct );

	MSG_GT2LS::stUserOffline ofmsg;
	ofmsg.acct = user->acct;
	g_loginclient->PostSend( ofmsg );
}

void user_mgr::build_update_list_message( MSG_GT2LS::stUpdateUserInfo& msg )
{
	MSG_GT2LS::stUpdateUserInfo::user_info info;

	for( std::map<uint32, client_session*>::iterator it = m_users.begin(); it != m_users.end(); ++it )
	{
		client_session* user = it->second;
		info.acct = user->acct;
		info.banned = user->banned;
		info.muted = user->muted;
		info.gmflags = user->gmflags;
		info.ischild = user->ischild;

		msg.users.push_back( info );
	}
}

