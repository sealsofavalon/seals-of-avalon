#ifndef _USER_MGR_HEAD
#define _USER_MGR_HEAD

#include "client_session.h"

#ifdef _WIN32
static const uint32 MAX_SESSION_STORAGE = 10240;
#else
static const uint32 MAX_SESSION_STORAGE = 1024000;
#endif

class user_mgr
{
public:
	user_mgr();
	client_session* get_user_by_transid( uint32 transid );
	client_session* get_user_by_account( uint32 acct );
	uint32 dispatch_user( client_session* user );
	void remove_gs( uint32 groupid );
	void run();
	void add_prelogin_user( uint32 acct, uint32 temp_password, uint8 ischild, uint32 lastlogout, uint32 chenmitime, const std::string& gmflags, uint32 banned, uint32 muted );
	bool apply_login_user( client_session* user, uint32 acct, uint32 temp_password );
	void build_update_list_message( MSG_GT2LS::stUpdateUserInfo& msg );

	void add_in_game_user( client_session* user );
	void remove_in_game_user( client_session* user );
	void on_user_disconnected( client_session* user );

private:
	client_session* m_storage[MAX_SESSION_STORAGE];
	std::map<uint32, client_session*> m_users;
	uint32 m_maxid;
	std::map<uint32, prelogin_user*> m_prelogin_users;
};

extern user_mgr* g_usermgr;

#endif // _USER_MGR_HEAD

