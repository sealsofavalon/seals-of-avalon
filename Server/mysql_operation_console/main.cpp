#include <iostream>
#include <cstdlib>
#include <cstdio>
#include "Common.h"
#include "../Common/share/Database/Database.h"
#include "../Common/share/Database/MySQLDatabase.h"
#include "../../new_common/Source/log4cpp-1.0/MyLog.h"
#include "../../new_common/Source/ssl/MD5.h"

#include "../../new_common/Source/new_common.h"

/*
typedef mt_buffer_mgr<2, 100> my_mt_buffer_mgr;


class ct
{
public:
	ct()
	{
		pthread = new boost::thread( boost::bind( &ct::run, this ) );
	}
	~ct()
	{
		pthread->join();
		delete pthread;
	}
	void run()
	{
		while( true )
		{
			my_mt_buffer_mgr::buffer_type* p = NULL;
			{
				boost::mutex::scoped_lock lock( m );
				if( !q.empty() )
				{
					p = q.front();
					q.pop();
				}
				else
					cond.wait( m );
			}
			if( p )
			{
				strcpy( p->get_buff(), "sdfs dfsdf sdf sdf sdfsdf sdf " );
				p->release();
			}
			//Sleep( 1 );
		}
	}
	void push( my_mt_buffer_mgr::buffer_type* p )
	{
		boost::mutex::scoped_lock lock( m );
		q.push( p );
		cond.notify_one();
	}
	std::queue<my_mt_buffer_mgr::buffer_type*> q;
	boost::mutex m;
	boost::thread* pthread;
	boost::condition cond;
};

int main( void )
{
	my_mt_buffer_mgr mgr;

	ct t[10];
	int count = 0;
	while( true )
		for( int i = 0; i < 10; ++i )
		{
			int n = get_tick_count();
			my_mt_buffer_mgr::buffer_type* p = mgr.get_free_buffer();
			t[i].push( p );
			printf( "proc %d time:%d\n", ++count, get_tick_count() - n );
		}
	return 0;
}

void proc_before_sunyou_public_test()
{
	MySQLDatabase db_realmdata, db_character;
	int mysqlport;
	std::string mysqlip, mysqluser, mysqlpwd, mysqlname;

	std::cout << "welcome to use this tool, before sunyou j.tx public test. please do not execute twice, thank you." <<std::endl;
	std::cout << "first, connect realm data db" << std::endl;
	while( true )
	{
		std::cout << "<connect ip>";
		std::cin >> mysqlip;
		if( strnicmp( mysqlip.c_str(), "exit", 4 ) == 0 )
			return;
		std::cout << "<connect port>";
		std::cin >> mysqlport;
		std::cout << "<connect user>";
		std::cin >> mysqluser;
		std::cout << "<connect password>";
		std::cin >> mysqlpwd;
		std::cout << "<connect database name>";
		std::cin >> mysqlname;
		if( !db_realmdata.Initialize( mysqlip.c_str(), mysqlport, mysqluser.c_str(), mysqlpwd.c_str(), mysqlname.c_str(), 5, 16384 ) )
			std::cout << "connect mysql failed!!! please retry" << std::endl;
		else
		{
			break;
		}
	}
	std::cout << "second, connect character db" << std::endl;
	while( true )
	{
		std::cout << "<connect ip>";
		std::cin >> mysqlip;
		if( strnicmp( mysqlip.c_str(), "exit", 4 ) == 0 )
			return;
		std::cout << "<connect port>";
		std::cin >> mysqlport;
		std::cout << "<connect user>";
		std::cin >> mysqluser;
		std::cout << "<connect password>";
		std::cin >> mysqlpwd;
		std::cout << "<connect database name>";
		std::cin >> mysqlname;
		if( !db_character.Initialize( mysqlip.c_str(), mysqlport, mysqluser.c_str(), mysqlpwd.c_str(), mysqlname.c_str(), 5, 16384 ) )
			std::cout << "connect mysql failed!!! please retry" << std::endl;
		else
		{
			break;
		}
	}

	FILE* flog_level10 = fopen("log_level10.csv", "w+");
	FILE* flog_charge = fopen("log_charge.csv", "w+");
	QueryResult* qr;

	char szBuf[1024];
	sprintf(szBuf, "%10s,%20s,%20s,%20s,\n","�˺�id","��ֵ����","��������","������ʣ������");
	fwrite(szBuf, strlen(szBuf), 1, flog_charge);

	std::set<int> set_acct;
	qr = db_realmdata.Query( "select distinct acc_id from acc_point_card where type < 1001 " );
	if( qr )
	{
		do
		{
			Field * f = qr->Fetch();
			set_acct.insert( f[0].GetUInt32() );
//			printf("%d\n", f[0].GetUInt32());
		}
		while( qr->NextRow() );
		delete qr;
	}

	std::set<int>::iterator itr = set_acct.begin();
	for( ; itr != set_acct.end(); ++itr )
	{
		uint32 accid = *itr;
		qr = db_realmdata.Query( "select sum(init_points),sum(remain_points) from acc_point_card where acc_id = %u and type < 1001", accid);
		if(qr)
		{
			do
			{
				Field* f = qr->Fetch();
				sprintf(szBuf, "%10d,%20d,%20d,%20d,\n", accid, f[0].GetUInt32(), f[0].GetUInt32()*2/10, f[1].GetUInt32()+f[0].GetUInt32()*2/10);
				fwrite(szBuf, strlen(szBuf), 1, flog_charge);
			}
			while( qr->NextRow() );
			delete qr;
		}
	}


	if( !db_realmdata.WaitExecute( "update acc_point_card set remain_points = remain_points + init_points*2/10 where type < 1001" ) )
	{
		std::cout << "[update acc_point_card set remain_points = remain_points + init_points*2/10 where type < 1001] execute error!" << std::endl;
		return;
	}

	sprintf(szBuf, "%10s, %20s,\n", "�˺�id", "���͵���");
	fwrite(szBuf, strlen(szBuf), 1, flog_level10);
	qr = db_character.Query( "select distinct acct from characters where level > 9" );
	if( qr )
	{
		do
		{
			Field* f = qr->Fetch();
			uint32 accid = f[0].GetUInt32();
			bool b = db_realmdata.WaitExecute( "replace into acc_point_card values(%u, 'OFFLINE_%d', 1002, 500, 500, %u, 'bonus for internal test players' )", accid, accid, (uint32)time( NULL ) );
			if( b )
			{
				//std::cout << "proc account id:["  << accid << "] done!" << std::endl;
				sprintf(szBuf, "%10d, %20d,\n", accid, 500);
				fwrite(szBuf, strlen(szBuf), 1, flog_level10);
			}
			else
			{
				std::cout << "proc account id:["  << accid << "] failed!" << std::endl;
				sprintf(szBuf, "�˺�%d���ִ���!!!\n", accid);
				fwrite(szBuf, strlen(szBuf), 1, flog_level10);
			}
		}
		while( qr->NextRow() );
		delete qr;
	}
	fclose(flog_charge);
	fclose(flog_level10);
	std::cout << "done all!" << std::endl;
}
*/

void xj_bonus()
{
	MySQLDatabase ac_db, rd_db;
	std::string ip, user, pwd;

	while( true )
	{
		std::cout << "please input account center db ip:" << std::endl;
		std::cin >> ip;
		std::cout << "please input account center db user:" << std::endl;
		std::cin >> user;
		std::cout << "please input account center db password:" << std::endl;
		std::cin >> pwd;
		if( !ac_db.Initialize( ip.c_str(), 3306, user.c_str(), pwd.c_str(), "sunyou_account_center", 5, 16384 ) )
			std::cout << "connect account center db failed!!! please retry" << std::endl;
		else
			break;
	}

	while( true )
	{
		std::cout << "please input realm data db ip:" << std::endl;
		std::cin >> ip;
		std::cout << "please input realm data db user:" << std::endl;
		std::cin >> user;
		std::cout << "please input realm data db password:" << std::endl;
		std::cin >> pwd;
		if( !rd_db.Initialize( ip.c_str(), 3306, user.c_str(), pwd.c_str(), "sunyou_realm_data", 5, 16384 ) )
			std::cout << "connect realm data db failed!!! please retry" << std::endl;
		else
			break;
	}
	uint32 points;
	std::cout << "please input how much points to add to all users." << std::endl;
	std::cin >> points;

	QueryResult* qr = ac_db.Query( "select id from account" );
	if( qr )
	{
		do
		{
			uint32 id = qr->Fetch()[0].GetUInt32();
			std::cout << "proc id:" << id << std::endl;
			uint32 ipt = points;
			uint32 rpt = points;
			QueryResult* qr2 = rd_db.Query( "select init_points, remain_points from acc_point_card where card_serial = 'OFFLINE_%u'", id );
			if( qr2 )
			{
				ipt += qr2->Fetch()[0].GetUInt32();
				rpt += qr2->Fetch()[1].GetUInt32();
				delete qr2;
			}
			bool b = rd_db.WaitExecute( "replace into acc_point_card values(%u, 'OFFLINE_%u', 1002, %u, %u, %u, 'bonus for internal test players second step')",
							id, id, ipt, rpt, (uint32)time( NULL ) );
			if( !b )
				std::cout << "replace into error!" << std::endl;
		}
		while( qr->NextRow() );

		delete qr;

		std::cout << "done!" << std::endl;
	}
}

int main()
{
	/*
	if( !MyLog::Init() )
		return 0;

	xj_bonus();
	*/
	const char* p = setlocale( LC_ALL, "zh_CN.GBK" );

	if( p == NULL || p[0] == 0 )
	{
		printf( "set locale failed!\n" );
		return -1;
	}

	wchar_t dest[4096];
	size_t n = mbstowcs( dest, "good news everyone", 4096 );
	printf( "n == %d\n", n );
	wprintf( dest );

	return 0;

/*
	QueryResult* qr = db.Query( "select id, password from account" );
	if( qr )
	{
		do
		{
			int id = qr->Fetch()[0].GetInt32();
			std::string pwd = qr->Fetch()[1].GetString();
			std::string md5pwd = MD5Hash::_make_md5_pwd( pwd.c_str() );
			db.WaitExecute( "update account set password = '%s' where id = %d", md5pwd.c_str(), id );
			printf( "done!!! id:%d before :%s  md5: %s\n", id, pwd.c_str(), md5pwd.c_str() );
		}
		while( qr->NextRow() );
	}
	printf( "done all!!!" );
	return 0;


	std::string cmd;
	std::getline( std::cin, cmd );
	while( true )
	{
		std::cout << "<" << mysqlip << "|" << mysqlname << ">";
		std::getline( std::cin, cmd );
		if( cmd.length() == 0 )
			continue;
		if( strnicmp( cmd.c_str(), "select", 6 ) == 0 )
		{
			QueryResult* qr = db.Query( cmd.c_str() );
			if( qr )
			{
				do
				{
					Field* f = qr->Fetch();
					int count = qr->GetFieldCount();
					for( int i = 0; i < count; ++i )
						std::cout << "[" << f[i].GetString() << "]";

					std::cout << std::endl;
				}
				while( qr->NextRow() );
				delete qr;
			}
			else
				std::cout << "no record matched!" << std::endl;
		}
		else if( strnicmp( cmd.c_str(), "exit", 4 ) == 0 )
			break;
		else if( db.WaitExecute( cmd.c_str() ) )
			std::cout << "execute succeed!" << std::endl;
	}
    return 0;
*/
}
//#endif
