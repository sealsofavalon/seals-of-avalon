#!/bin/sh

cd ac_build
make clean
make install
cd ../cs_build
make clean
make install
cd ../db_build
make clean
make install
cd ../login_build
make clean
make install
cd ../gate_build
make clean
make install
cd ../gs_build
make clean
make install
cd /opt/jtx_server

