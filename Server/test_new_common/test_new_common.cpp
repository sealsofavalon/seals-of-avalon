// test_new_common.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include <algorithm>
#include <functional>
#include "../../new_common/Source/new_common.h"

#include "../Common/share/Common.h"
#include "../Common/share/Singleton.h"
#include "../../../SDBase/Public/TypeDef.h"
#include "../Common/share/Database/Database.h"
#include "../Common/share/Database/MySQLDatabase.h"

#include <conio.h>

int main( void )
{
	MyLog::Init();
	MySQLDatabase* g_db = new MySQLDatabase();
	if( !g_db->Initialize( "192.168.70.6", 3306, "root", "sunyou", "sunyou_world_xinjiang_ver", 5, 16384 ) )
	{
		MyLog::log->error( "connect db failed! press any key to exit." );
		getch();
		return false;
	}
	
	char cmd[512];
	printf( "welcome to use this tool for export tables. please input db name.\n" );
	while( true )
	{
		scanf( "%s", cmd );
		if( 0 == stricmp( cmd, "exit" ) )
			break;
		QueryResult* qr = NULL;
		
		if( 0 == stricmp( cmd, "creature_proto_copy" ) )
			qr = g_db->Query( "select entry, npcflags from %s", cmd );
		else if( 0 == stricmp( cmd, "creatureloot" ) )
			qr = g_db->Query( "select entry, entryid, itemid from %s", cmd );
		else if( 0 == stricmp( cmd, "objectloot" ) )
			qr = g_db->Query( "select `index`, entryid, itemid from %s", cmd );
		else
			qr = g_db->Query( "select * from %s", cmd );
		if( qr )
		{
			printf( "exporting ...\n" );
			std::string file_name( "exports\\" );
			file_name += cmd;
			file_name += ".txt";
			FILE* fp = fopen( file_name.c_str(), "w" );
			if( !fp )
			{
				delete qr;
				printf( "cannot open file:%s", file_name );
				continue;
			}
			uint32 fc = qr->GetFieldCount();
			uint32 rc = qr->GetRowCount();
			
			do
			{
				Field* f = qr->Fetch();
				for( uint32 i = 0; i < fc; ++i )
				{
					const char* data = f[i].GetString();
					if( i == fc - 1 )
						fprintf( fp, "%s", data );
					else
						fprintf( fp, "%s\t", data );
				}
				//if( --rc > 0 )
				fprintf( fp, "\n" );
			}
			while( qr->NextRow() );
			
			fclose( fp );
			printf( "%s done! %d row exported.\n", file_name.c_str(), qr->GetRowCount() );
			delete qr;
		}
		else
			printf( "cannot found this db\n" );
	}
	return 0;
}
