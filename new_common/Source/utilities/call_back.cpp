#include "call_back.h"


void call_back_mgr::poll()
{
	fast_queue<i_call> temp;
	{
		boost::mutex::scoped_lock lock( m_mutex );
		if( !m_listcb.empty() )
			temp.swap( m_listcb );
		else
			return;
	}
	while( !temp.empty() )
	{
		i_call* p = temp.pop();
		p->call();
		delete p;
	}
}

void call_back_mgr::clear()
{
	boost::mutex::scoped_lock lock( m_mutex );
	while( m_listcb.empty() )
		delete m_listcb.pop();
}
